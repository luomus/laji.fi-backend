package fi.luomus.lajitietokeskus.taxonomy.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.io.LineIterator;
import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.BoldRecords;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonomyDAO_Imple.TaxonLoadingException;

public class BoldService {

	// Example feed response
	//
	//<?xml version="1.0" encoding="UTF-8"?>
	//<campaign xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.boldsystems.org/schemas/campaign_progress_metadata3.2.xsd">
	//
	// <species>
	//		..
	// </species>
	//
	// <species>
	//	<taxon>
	//		<taxon_id>124463</taxon_id>
	//		<name>Abies balsamea</name>
	//	</taxon>
	//	<heirarchy>
	//		<phylum>
	//			<taxon>
	//				<taxon_id>251587</taxon_id>
	//				<name>Pinophyta</name>
	//			</taxon>
	//		</phylum>
	//		<class>
	//			<taxon>
	//				<taxon_id>48341</taxon_id>
	//				<name>Pinopsida</name>
	//			</taxon>
	//		</class>
	//		<order>
	//			<taxon>
	//				<taxon_id>48346</taxon_id>
	//				<name>Pinales</name>
	//			</taxon>
	//		</order>
	//		<family>
	//			<taxon>
	//				<taxon_id>48450</taxon_id>
	//				<name>Pinaceae</name>
	//			</taxon>
	//		</family>
	//		<genus>
	//			<taxon>
	//				<taxon_id>124462</taxon_id>
	//				<name>Abies</name>
	//			</taxon>
	//		</genus>
	//	</heirarchy>
	//	<stats>
	//		<barcodes>17</barcodes>
	//		<specimens>25</specimens>
	//		<publicrecords>14</publicrecords>
	//		<associatedbins>BOLD:AAO9081,BOLD:AAO9082</associatedbins>
	//	</stats>
	//	<collection_sites>
	//		...
	//	</collection_sites>
	// </species>
	//</campaign>

	public static class BoldRecordTaxon {
		private final BoldRecords records;
		private String phylumName;
		private String className;
		private String familyName;
		private BoldRecordTaxon(BoldRecords records) {
			this.records = records;
		}
		public BoldRecords getRecords() {
			return records;
		}
		public String getPhylumName() {
			return phylumName;
		}
		public void setPhylumName(String phylumName) {
			this.phylumName = phylumName;
		}
		public String getClassName() {
			return className;
		}
		public void setClassName(String className) {
			this.className = className;
		}
		public String getFamilyName() {
			return familyName;
		}
		public void setFamilyName(String familyName) {
			this.familyName = familyName;
		}
		@Override
		public String toString() {
			return "BoldRecordTaxon [records=" + records + ", phylumName=" + phylumName + ", className=" + className + ", familyName=" + familyName + "]";
		}
	}

	private final XMLReader reader = new XMLReader();
	private Config config;
	private ErrorReporter errorReporter;
	private final File folder;

	public BoldService(Config config, ErrorReporter errorReporter) {
		this.config = config;
		this.errorReporter = errorReporter;
		this.folder = new File(config.baseFolder() + config.get("StorageFolder"));
	}

	public BoldService(Config config, ErrorReporter errorReporter, File folder) {
		this.config = config;
		this.errorReporter = errorReporter;
		this.folder = folder;
	}

	public void reloadFeed() {
		try {
			System.out.println("Laji-backend: Reloading BOLD feed...");
			tryToReloadFeed();
			System.out.println("Laji-backend: Reloading BOLD feed: SUCCESS");
		} catch (Exception e) {
			errorReporter.report("Reloading BOLD feed", e);
		}
	}

	private void tryToReloadFeed() throws Exception {
		File finalFile = dataFile();
		File tempFile = new File(folder, "bold-data-"+DateUtils.getFilenameDatetime()+".xml");
		File backupFile = new File(folder, "bold-data-backup.xml");
		String url = config.get("BOLD_feed_URL");
		System.out.println("Laji-backend: Reading bold data from " + url);
		try (HttpClientService client = new HttpClientService(); FileOutputStream out = new FileOutputStream(tempFile)) {
			client.contentToStream(new HttpGet(url), out);
		}
		if (finalFile.exists()) {
			if (backupFile.exists()) {
				backupFile.delete();
			}
			finalFile.renameTo(backupFile);
		}
		tempFile.renameTo(finalFile);
	}

	private File dataFile() {
		return new File(folder, "bold-data.xml");
	}

	public Map<String, List<BoldRecordTaxon>> load() throws TaxonLoadingException {
		try {
			return tryToLoad();
		} catch (Exception e) {
			throw new TaxonLoadingException("Loading bold data", e);
		}
	}

	private Map<String, List<BoldRecordTaxon>> tryToLoad() throws IOException, FileNotFoundException {
		File file =  dataFile();
		Map<String, List<BoldRecordTaxon>> records = new HashMap<>();
		System.out.println("Laji-backend: Reading bold data from " + file.getAbsolutePath());
		LineIterator i = org.apache.commons.io.FileUtils.lineIterator(file, "UTF-8");
		StringBuilder b = new StringBuilder();
		try {
			while (i.hasNext()) {
				String line = i.next().trim();
				if (line.equals("<species>")) {
					b.setLength(0);
				}
				b.append(line);
				if (line.equals("</species>")) {
					addRecord(records, b.toString());
				}
			}
		} finally {
			LineIterator.closeQuietly(i);
		}
		return records;
	}

	private void addRecord(Map<String, List<BoldRecordTaxon>> records, String xml) {
		Node node = reader.parse(xml).getRootNode();
		addRecord(records, node);
	}

	private void addRecord(Map<String, List<BoldRecordTaxon>> records, Node node) {
		String scientificName = parseScientificName(node);
		if (!given(scientificName)) return;
		BoldRecordTaxon recordTaxon = parseRecordTaxon(node);
		if (recordTaxon == null) return;
		if (!records.containsKey(scientificName)) {
			records.put(scientificName, new ArrayList<>());
		}
		records.get(scientificName).add(recordTaxon);
	}

	private BoldRecordTaxon parseRecordTaxon(Node node) {
		BoldRecords records = parseRecords(node);
		if (!records.hasRecords()) return null;
		BoldRecordTaxon recordTaxon = new BoldRecordTaxon(records);

		if (!node.hasChildNodes("heirarchy")) return recordTaxon;
		Node heirarchy = node.getNode("heirarchy");
		recordTaxon.setPhylumName(parseHeirarchy("phylum", heirarchy));
		recordTaxon.setClassName(parseHeirarchy("class", heirarchy));
		recordTaxon.setFamilyName(parseHeirarchy("family", heirarchy));
		return recordTaxon;
	}

	private String parseHeirarchy(String nodeName, Node heirarchy) {
		//		<phylum>
		//			<taxon>
		//				<taxon_id>251587</taxon_id>
		//				<name>Pinophyta</name>
		//			</taxon>
		//		</phylum>
		if (!heirarchy.hasChildNodes(nodeName)) return null;
		Node rankNode = heirarchy.getNode(nodeName);
		return parseScientificName(rankNode);
	}

	private BoldRecords parseRecords(Node node) {
		BoldRecords records = new BoldRecords();
		//	<stats>
		//		<barcodes>17</barcodes>
		//		<specimens>25</specimens>
		//		<publicrecords>14</publicrecords>
		//		<associatedbins>BOLD:AAO9081,BOLD:AAO9082</associatedbins>
		//	</stats>
		if (!node.hasChildNodes("stats")) return records;
		Node stats = node.getNode("stats");
		records.setBarcodes(parseInteger("barcodes", stats));
		records.setSpecimens(parseInteger("specimens", stats));
		records.setPublicRecords(parseInteger("publicrecords", stats));
		if (stats.hasChildNodes("associatedbins")) {
			String bins = stats.getNode("associatedbins").getContents();
			Pattern.compile(",").splitAsStream(bins).forEach(b->records.addToBins(b));
		}
		return records;
	}

	private Integer parseInteger(String nodeName, Node stats) {
		if (!stats.hasChildNodes(nodeName)) return null;
		try {
			return Integer.valueOf(stats.getNode(nodeName).getContents());
		} catch (Exception e) {
			return null;
		}
	}

	private String parseScientificName(Node node) {
		//	<taxon>
		//		<taxon_id>124463</taxon_id>
		//		<name>Abies balsamea</name>
		//	</taxon>
		if (!node.hasChildNodes("taxon")) return null;
		Node taxon = node.getNode("taxon");
		if (!taxon.hasChildNodes("name")) return null;
		return taxon.getNode("name").getContents();
	}

	private boolean given(String s) {
		return s != null && s.trim().length() > 0;
	}

}
