package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.commons.utils.Utils;
import fi.luomus.java.tests.commons.taxonomy.TaxonContainerStub;
import fi.luomus.lajitietokeskus.TaxonToMapTests.FakeTaxonDao;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAOImple;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAOImple.IndexNames;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonToMap;

public class ElasticSearchDAOImpleTests {

	private static final TaxonContainer CONTAINER = new TaxonContainerStub();

	private static final IndexNames INDEX_NAMES = new IndexNames(IndexNames.INDEX_TYPE_TAXA, "taxon_test_1", "taxon_test_2", "taxon_test");

	private ElasticSearchDAOImple dao;

	@Before
	public void before() throws Exception {
		System.out.println(" --- before test --- start");
		Config config = TestConfig.getConfig();
		dao = new ElasticSearchDAOImple(
				config.get("ES_address"),
				config.get("ES_cluster"),
				INDEX_NAMES,
				new ErrorReportingToSystemErr(),
				new TaxonToMap(new FakeTaxonDao(), TaxonToMapTests.getHardCodedEvaluationProperties(), TaxonToMapTests.getHardCodedAreas()),
				false);
		dao.deleteAll();
		System.out.println(" --- ------ ---- --- end");
	}

	@After
	public void after() {
		if (dao != null) dao.close();
	}

	@Test
	public void test_first_load_set_alias_and_swap_alias() throws Exception {
		assertFalse(dao.aliasExists());
		assertFalse(dao.indexExists(INDEX_NAMES.index1));
		assertFalse(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index2, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index1, dao.getActiveIndex());
		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));

		dao.emptyTemp();

		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index2, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index1, dao.getActiveIndex());

		dao.switchTempToActiveIndex();

		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index1, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index2, dao.getActiveIndex());

		dao.emptyTemp();

		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index1, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index2, dao.getActiveIndex());

		dao.switchTempToActiveIndex();

		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index2, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index1, dao.getActiveIndex());
	}

	@Test
	public void test_simple_taxon() throws Exception {
		Qname id = new Qname("MX.2");
		String somethingThatAlwaysChanges = Utils.generateGUID();

		Taxon taxon = new Taxon(id, CONTAINER);
		taxon.setScientificName("Parus major");
		taxon.addAdditionalId(somethingThatAlwaysChanges);
		taxon.setTaxonomicOrder(2L);
		taxon.setTaxonRank(new Qname("MX.species"));

		dao.push(taxon);

		assertEquals("[]", dao.getPushErrors().toString());

		JSONObject actual = dao.getFromTempIndex(taxon.getId());
		JSONObject expected = new JSONObject(getContents("mx2_expected.json").replace("GUID", somethingThatAlwaysChanges));
		assertEquals(expected.beautify(), actual.beautify());
	}

	@Test
	public void test_complex_taxon() throws Exception {
		Taxon taxon = TaxonToMapTests.createTestData();
		taxon.setTaxonomicOrder(1L);

		dao.push(taxon);

		assertEquals("[]", dao.getPushErrors().toString());

		JSONObject actual = dao.getFromTempIndex(taxon.getId());
		JSONObject expected = new JSONObject(getContents("mx1_expected.json"));
		assertEquals(expected.beautify(), actual.beautify());
	}

	private String getContents(String filename) throws Exception {
		return TestUtil.getContents(ElasticSearchDAOImpleTests.class, filename);
	}

}
