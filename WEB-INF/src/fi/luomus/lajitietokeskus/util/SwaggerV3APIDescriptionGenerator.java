package fi.luomus.lajitietokeskus.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonSearch.MatchType;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.taxonomy.services.TaxonomySearchApiServlet;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.AbstractBaseEntity;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.models.UnitOfMeasurement;
import fi.luomus.traitdb.models.search.TraitSearchRow;
import fi.luomus.traitdb.services.TraitAPI;
import fi.luomus.traitdb.services.TraitAPIRows;
import fi.luomus.traitdb.services.TraitAPISearch;

public class SwaggerV3APIDescriptionGenerator {

	private static final String TRAITAPI_TSV_VALIDATION_MODEL = "TraitTSVValidationResponse";
	private static final String TRAITAPI_MULTI_VALIDATION_MODEL = "TraitMultiValidationResponse";
	private static final String TRAITAPI_OK_MODEL = "TraitAPIOKResponse";
	private static final String TRAITAPI_ERROR_MODEL = "TraitAPIError";
	private static final String TRAITAPI_SEACRH_RESPONSE_MODEL = "TraitAPISearchResponse";
	private static final String TAXON_SEACRH_ERROR_MODEL = "TaxonSearchError";
	private static final String TAXON_SEACRH_RESPONSE_MODEL = "TaxonSearchResponse";

	private static final String APPLICATION_JSON = "application/json";
	private static final String APPLICATION_ZIP = "application/zip";
	private static final String TEXT_TSV = "text/tab-separated-values";

	private static final String PUT = "put";
	private static final String POST = "post";
	private static final String DELETE = "delete";
	private static final String GET = "get";
	private static final String REF = "$ref";
	private static final String RESPONSES = "responses";
	private static final String CONTENT = "content";
	private static final String FORMAT = "format";
	private static final String OBJECT = "object";
	private static final String PROPERTIES = "properties";
	private static final String PATH = "path";
	private static final String TAGS = "tags";
	private static final String TAXON_TAG = "Taxon";
	private static final String TRAIT_TAG = "Trait";
	private static final String STRING = "string";
	private static final String ITEMS = "items";
	private static final String MAXIMUM = "maximum";
	private static final String MINIMUM = "minimum";
	private static final String INTEGER = "integer";
	private static final String BOOLEAN = "boolean";
	private static final String DEFAULT = "default";
	private static final String ARRAY = "array";
	private static final String ENUM = "enum";
	private static final String X_ENUM_ORIGIN = "x-enum-origin";
	private static final String ADDITIONAL_PROPERTIES = "additionalProperties";
	private static final String QUERY = "query";
	private static final String TYPE = "type";
	private static final String SCHEMA = "schema";
	private static final String REQUIRED = "required";
	private static final String IN = "in";
	private static final String NAME = "name";
	private static final String PARAMETERS = "parameters";
	private static final String EXAMPLES = "examples";
	private static final String DESCRIPTION = "description";
	private static final String SUMMARY = "summary";
	private static final String REQUEST_BODY = "requestBody";

	public static JSONObject generateSwaggerDescription(Map<Class<?>, Map<String, Object>> taxaClassProperties, Map<Class<?>, Map<String, Object>> traitClassProperties) {
		JSONObject response = new JSONObject();
		response.setString("openapi", "3.1.0");
		response.setObject("info", info());
		response.getArray("servers").appendObject(new JSONObject().setString("url", "https://localhost:8081/lajitietokeskus/api"));
		response.setObject("paths", paths());
		response.getObject("components").setObject("schemas", componentSchemas(taxaClassProperties, traitClassProperties));
		response.setArray(TAGS, new JSONArray().appendObject(new JSONObject().setString(NAME, TAXON_TAG)).appendObject(new JSONObject().setString(NAME, TRAIT_TAG)));
		return response;
	}

	private static JSONObject info() {
		JSONObject info = new JSONObject();
		info.setString("title", "Laji backend");
		info.getObject("contact").setString("email", "helpdesk@laji.fi");
		info.getObject("contact").setString("url", "https://bitbucket.org/luomus/laji.fi-backend/issues");
		info.getObject("license").setString(NAME, "The MIT License (MIT)");
		info.getObject("license").setString("url", "https://opensource.org/licenses/MIT");
		info.setString("version", "beta");
		return info;
	}

	private static final boolean ALLOW_PERSON_AUTH = true;
	private static final boolean NO_PERSON_AUTH = false;

	private static JSONObject paths() {

		JSONObject paths = new JSONObject();
		paths.getObject("/taxa/search").setObject(GET, taxonSearchApi());
		paths.getObject("/taxa/{id}").setObject(GET, taxonApi());

		paths.getObject("/trait/search").setObject(GET, traitSearch());
		paths.getObject("/trait/search/download").setObject(GET, traitSearchDownload());

		paths.getObject("/trait/rows/{id}").setObject(GET, getSingle(InputRow.class, "Get trait input row by id", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/rows/validate").setObject(POST, validateInsert(InputRow.class, "Validate adding a new input row", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/rows/validate-update/{id}").setObject(POST, validateUpdate(InputRow.class, "Validate update of an input row", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/rows/validate-delete/{id}").setObject(POST, validateDelete("Validate deleting input row", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/rows").setObject(POST, insert(InputRow.class, "Insert new input row", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/rows/{id}").setObject(PUT, update(InputRow.class, "Update an input row", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/rows/{id}").setObject(DELETE, delete("Delete an input row", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/rows/tsv2rows/validate").setObject(POST, validateTSV2Rows());
		paths.getObject("/trait/rows/tsv2rows").setObject(POST, executeTSV2Rows());
		paths.getObject("/trait/rows/multi/validate").setObject(POST, validateMulti(InputRow.class, "Validate adding a batch of input rows"));
		paths.getObject("/trait/rows/multi").setObject(POST, insertMulti(InputRow.class, "Store a batch of input rows"));
		paths.getObject("/trait/rows/search").setObject(GET, traitInputSearch());

		paths.getObject("/trait/datasets").setObject(GET, getAll(Dataset.class, "Get all datasets", NO_PERSON_AUTH));
		paths.getObject("/trait/datasets/{id}").setObject(GET, getSingle(Dataset.class, "Get dataset by id", NO_PERSON_AUTH));
		paths.getObject("/trait/datasets/validate").setObject(POST, validateInsert(Dataset.class, "Validate adding a new dataset", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/datasets/validate-update/{id}").setObject(POST, validateUpdate(Dataset.class, "Validate dataset update", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/datasets/validate-delete/{id}").setObject(POST, validateDelete("Validate deleting a dataset", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/datasets").setObject(POST, insert(Dataset.class, "Insert new dataset", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/datasets/{id}").setObject(PUT, update(Dataset.class, "Update a dataset", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/datasets/{id}").setObject(DELETE, delete("Delete a dataset", ALLOW_PERSON_AUTH));

		paths.getObject("/trait/dataset-permissions").setObject(GET, getAll(DatasetPermissions.class, "Get users all permissions (defined by personToken or - in absence - of the access_token)", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/dataset-permissions/{datasetId}").setObject(GET, getSingle(DatasetPermissions.class, "Get permissions of a dataset", NO_PERSON_AUTH));
		paths.getObject("/trait/dataset-permissions/validate-update/{datasetId}").setObject(POST, validateUpdate(DatasetPermissions.class, "Validate permission submission", ALLOW_PERSON_AUTH));
		paths.getObject("/trait/dataset-permissions/{datasetId}").setObject(PUT, update(DatasetPermissions.class, "Update permissions of a dataset", ALLOW_PERSON_AUTH));

		paths.getObject("/trait/trait-groups").setObject(GET, getAll(TraitGroup.class, "Get all trait groups", NO_PERSON_AUTH));
		paths.getObject("/trait/trait-groups/{id}").setObject(GET, getSingle(TraitGroup.class, "Get trait group by id", NO_PERSON_AUTH));
		paths.getObject("/trait/trait-groups/validate").setObject(POST, validateInsert(TraitGroup.class, "Validate adding a new trait group", NO_PERSON_AUTH));
		paths.getObject("/trait/trait-groups/validate-update/{id}").setObject(POST, validateUpdate(TraitGroup.class, "Validate trait group update", NO_PERSON_AUTH));
		paths.getObject("/trait/trait-groups/validate-delete/{id}").setObject(POST, validateDelete("Validate deleting a trait group", NO_PERSON_AUTH));
		paths.getObject("/trait/trait-groups").setObject(POST, insert(TraitGroup.class, "Insert new trait group", NO_PERSON_AUTH));
		paths.getObject("/trait/trait-groups/{id}").setObject(PUT, update(TraitGroup.class, "Update a trait group", NO_PERSON_AUTH));
		paths.getObject("/trait/trait-groups/{id}").setObject(DELETE, delete("Delete a trait group", NO_PERSON_AUTH));

		paths.getObject("/trait/traits").setObject(GET, getAll(Trait.class, "Get all traits", NO_PERSON_AUTH));
		paths.getObject("/trait/traits/{id}").setObject(GET, getSingle(Trait.class, "Get trait by id", NO_PERSON_AUTH));
		paths.getObject("/trait/traits/validate").setObject(POST, validateInsert(Trait.class, "Validate adding a new trait", NO_PERSON_AUTH));
		paths.getObject("/trait/traits/validate-update/{id}").setObject(POST, validateUpdate(Trait.class, "Validate trait update", NO_PERSON_AUTH));
		paths.getObject("/trait/traits/validate-delete/{id}").setObject(POST, validateDelete("Validate deleting a trait", NO_PERSON_AUTH));
		paths.getObject("/trait/traits").setObject(POST, insert(Trait.class, "Insert new trait", NO_PERSON_AUTH));
		paths.getObject("/trait/traits/{id}").setObject(PUT, update(Trait.class, "Update a trait", NO_PERSON_AUTH));
		paths.getObject("/trait/traits/{id}").setObject(DELETE, delete("Delete a trait", NO_PERSON_AUTH));

		paths.getObject("/trait/units").setObject(GET, getAll(UnitOfMeasurement.class, "Get all units of measurement", NO_PERSON_AUTH));
		paths.getObject("/trait/units/{id}").setObject(GET, getSingle(UnitOfMeasurement.class, "Get unit of measurement by id", NO_PERSON_AUTH));
		return paths;
	}

	private static JSONObject componentSchemas(Map<Class<?>, Map<String, Object>> taxaClassProperties, Map<Class<?>, Map<String, Object>> traitClassProperties) {
		JSONObject schemas = new JSONObject();
		schemas.setObject(TAXON_SEACRH_RESPONSE_MODEL, taxonSearchResponseSchema());
		schemas.setObject(TAXON_SEACRH_ERROR_MODEL, taxonSearchErrorSchema());
		for (Map.Entry<Class<?>, Map<String, Object>> classes : taxaClassProperties.entrySet()) {
			schemas.setObject(classes.getKey().getSimpleName(), schema(classes.getValue()));
		}

		schemas.setObject(TRAITAPI_ERROR_MODEL, traitErrorSchema());
		schemas.setObject(TRAITAPI_OK_MODEL, traitOkResponseSchema());
		schemas.setObject(TRAITAPI_SEACRH_RESPONSE_MODEL, traitSearchResponseSchema());
		schemas.setObject(ValidationResponse.class.getSimpleName(), traitValidationResponseSchema());
		schemas.setObject(TRAITAPI_TSV_VALIDATION_MODEL, traitTsvValidationResponseSchema());
		schemas.setObject(TRAITAPI_MULTI_VALIDATION_MODEL, traitMultiValidationResponseSchema());
		for (Map.Entry<Class<?>, Map<String, Object>> classes : traitClassProperties.entrySet()) {
			schemas.setObject(classes.getKey().getSimpleName(), schema(classes.getValue()));
		}
		return schemas;
	}

	@SuppressWarnings("unchecked")
	private static JSONObject schema(Map<String, Object> properties) {
		JSONObject schema = new JSONObject();

		try {
			if (properties.containsKey(TYPE) && properties.get(TYPE) instanceof String) {
				schema.setString(TYPE, (String)properties.get(TYPE));
				if (properties.containsKey(ENUM)) {
					for (String e : (List<String>)properties.get(ENUM)) {
						schema.getArray(ENUM).appendString(e);
					}
				}
				if (properties.containsKey(X_ENUM_ORIGIN)) {
					schema.setString(X_ENUM_ORIGIN, (String)properties.get(X_ENUM_ORIGIN));
				}
				if (properties.containsKey("ref")) {
					schema.getObject(ITEMS).setString(REF, ref((String)properties.get("ref")));
				}
				return schema;
			}
		} catch (Exception e) {
			throw new IllegalStateException("Primitive value type mapping failed for " + properties, e);
		}

		schema.setString(TYPE, OBJECT);
		for (Map.Entry<String, Object> e : properties.entrySet()) {
			String field = e.getKey();
			Object propertyDef = e.getValue();
			try {
				if (propertyDef instanceof Map) {
					Map<String, Object> map = (Map<String, Object>) propertyDef;
					String type = (String)map.get(TYPE);
					String ref = (String)map.get("ref");
					if (ARRAY.equals(type)) {
						schema.getObject(PROPERTIES).getObject(field).setString(TYPE, ARRAY);
						if (ref != null) {
							schema.getObject(PROPERTIES).getObject(field).getObject(ITEMS).setString(REF, ref(ref));
						} else {
							schema.getObject(PROPERTIES).getObject(field).setObject(ITEMS, schema((Map<String, Object>) map.get("item-schema")));
						}
					} else if (ref != null) {
						schema.getObject(PROPERTIES).getObject(field).setString(REF, ref(ref));
					} else if (type != null) {
						List<String> enums = map.containsKey(ENUM) ? (List<String>)map.get(ENUM) : null;
						String enumId = map.containsKey(X_ENUM_ORIGIN) ? (String)map.get(X_ENUM_ORIGIN) : null;
						String format = map.containsKey(FORMAT) ? (String)map.get(FORMAT) : null;
						String example = map.containsKey("example") ? (String)map.get("example") : null;
						property(schema, field, type, map.containsKey("desc") ? (String)map.get("desc") : null, enums, enumId, format, example);
					} else {
						schema.getObject(PROPERTIES).setObject(field, schema(map));
					}
				} else if (field.equals("required-fields")) {
					((List<String>)properties.get("required-fields")).forEach(f->schema.getArray("required").appendString(f));
				} else {
					throw new IllegalStateException("Unknown");
				}
			} catch (Exception ex) {
				throw new IllegalStateException("Invalid definition for " + e.getKey() +": " + propertyDef, ex);
			}
		}
		return schema;
	}

	private static JSONObject traitOkResponseSchema() {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, OBJECT);
		property(schema, TraitAPI.OK, STRING, "ok");
		return schema;
	}

	private static JSONObject traitSearchResponseSchema() {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, OBJECT);
		schema.getObject(PROPERTIES).getObject(TraitAPISearch.CURRENT_PAGE).setString(TYPE, INTEGER);
		schema.getObject(PROPERTIES).getObject(TraitAPISearch.NEXT_PAGE).setString(TYPE, INTEGER);
		schema.getObject(PROPERTIES).getObject(TraitAPISearch.LAST_PAGE).setString(TYPE, INTEGER);
		schema.getObject(PROPERTIES).getObject(TraitAPISearch.PAGE_SIZE).setString(TYPE, INTEGER);
		schema.getObject(PROPERTIES).getObject(TraitAPISearch.TOTAL).setString(TYPE, INTEGER);
		schema.getObject(PROPERTIES).setObject("results", refArray(TraitSearchRow.class));
		schema.getObject(PROPERTIES).getObject(ValidationResponse.ERRORS).getObject(ADDITIONAL_PROPERTIES).setString(TYPE, STRING);
		return schema;
	}

	private static JSONObject traitValidationResponseSchema() {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, OBJECT);
		schema.getObject(PROPERTIES).getObject(TraitAPIRows.PASS).setString(TYPE, BOOLEAN);
		schema.getObject(PROPERTIES).getObject(ValidationResponse.ERRORS).setString(TYPE, OBJECT);
		schema.getObject(PROPERTIES).getObject(ValidationResponse.ERRORS).getObject(ADDITIONAL_PROPERTIES).setString(TYPE, STRING);
		return schema;
	}

	private static JSONObject traitMultiValidationResponseSchema() {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, OBJECT);
		schema.getObject(PROPERTIES).getObject(TraitAPIRows.PASS).setString(TYPE, BOOLEAN);
		schema.getObject(PROPERTIES).setObject(TraitAPIRows.ROWS_VALIDATION, refArray(ValidationResponse.class));
		return schema;
	}

	private static JSONObject traitTsvValidationResponseSchema() {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, OBJECT);
		schema.getObject(PROPERTIES).getObject(TraitAPIRows.PASS).setString(TYPE, BOOLEAN);
		schema.getObject(PROPERTIES).getObject(TraitAPIRows.HEADER_VALIDATION).setString(REF, ref(ValidationResponse.class));
		schema.getObject(PROPERTIES).setObject(TraitAPIRows.ROWS_VALIDATION, refArray(ValidationResponse.class));
		return schema;
	}

	private static JSONObject traitErrorSchema() {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, OBJECT);
		property(schema, TraitAPI.STATUS, INTEGER, "HTTP Status Code");
		property(schema, TraitAPI.MESSAGE, STRING, "Error message");
		property(schema, TraitAPI.STACKTRACE, STRING, "Stacktrace");
		return schema;
	}

	private static JSONObject taxonSearchErrorSchema() {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, OBJECT);
		JSONObject errorSchema = schema.getObject(PROPERTIES).getObject(TaxonomySearchApiServlet.ERROR);
		errorSchema.setString(TYPE, OBJECT);
		property(errorSchema, TaxonomySearchApiServlet.STATUS_CODE, INTEGER, "HTTP Status Code");
		property(errorSchema, TaxonomySearchApiServlet.ERROR_NAME, STRING, null);
		property(errorSchema, TaxonomySearchApiServlet.ERROR_MESSAGE, STRING, null);
		return schema;
	}

	private static JSONObject taxonSearchResponseSchema() {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, ARRAY);
		JSONObject matchSchema = schema.getObject(ITEMS);
		matchSchema.setString(TYPE, OBJECT);
		property(matchSchema, TaxonomySearchApiServlet.MATCHING_NAME, STRING, "Name that matched the search word");
		property(matchSchema, TaxonomySearchApiServlet.NAME_TYPE, STRING, "Type of the name.", Utils.list(
				"MX.scientificName",
				"MX.vernacularName",
				"MX.hasSynonym",
				"MX.hasBasionym",
				"MX.alternativeVernacularName",
				"MX.hasMisappliedName",
				"MX.birdlifeCode",
				"MX.obsoleteVernacularName",
				"MX.euringCode",
				"MX.hasSubjectiveSynonym",
				"MX.hasAlternativeName",
				"MX.hasOrthographicVariant",
				"MX.hasObjectiveSynonym",
				"MX.hasMisspelledName",
				"MX.colloquialVernacularName",
				"MX.hasUncertainSynonym",
				"MX.hasHomotypicSynonym",
				"MX.tradeName",
				"MX.hasHeterotypicSynonym"),
				null, null, null);
		property(matchSchema, TaxonomySearchApiServlet.ID, STRING, "Taxon identifier of the taxon that has the matching name; in the short Qname format, for example 'MX.123'");
		property(matchSchema, TaxonomySearchApiServlet.SCIENTIFIC_NAME, STRING, "Accepted scientific name of the taxon that has the matching name");
		property(matchSchema, TaxonomySearchApiServlet.SCIENTIFIC_NAME_AUTHORSHIP, STRING, "Author of the above mentioned scientific name");
		property(matchSchema, TaxonomySearchApiServlet.TAXON_RANK, STRING, "Taxonomic rank of the taxon that has the matching name; in the short Qname format, for example 'MX.genus'");
		property(matchSchema, TaxonomySearchApiServlet.CURSIVE_NAME, BOOLEAN, "Should the matching name be cursived");
		property(matchSchema, TaxonomySearchApiServlet.FINNISH, BOOLEAN, "Is the taxon that has the mathing name marked as a Finnish taxon");
		property(matchSchema, TaxonomySearchApiServlet.SPECIES, BOOLEAN, "Is the taxon that has the mathing name species level or lower, or a higher taxon");

		matchSchema.getObject(PROPERTIES).getObject(TaxonomySearchApiServlet.VERNACULAR_NAME).setString(REF, ref(LocalizedText.class));

		JSONObject informalGroups = matchSchema.getObject(PROPERTIES).getObject(TaxonomySearchApiServlet.INFORMAL_GROUPS);
		informalGroups.setString(TYPE, ARRAY);
		JSONObject informalGroupItem = informalGroups.getObject(ITEMS);
		informalGroupItem.setString(TYPE, OBJECT);
		property(informalGroupItem, TaxonomySearchApiServlet.ID, STRING, "Identifier of the informal taxon group that the matching taxon belongs to); in the short Qname format, for example 'MVL.1'");
		informalGroupItem.getObject(PROPERTIES).getObject(TaxonomySearchApiServlet.INFORMAL_GROUP_NAME).setString(REF, ref(LocalizedText.class));

		property(matchSchema, TaxonomySearchApiServlet.KINGDOM_SCIENTIFIC_NAME, STRING, "Scientific name of the kingdom that the matching taxon belongs to");
		property(matchSchema, TaxonomySearchApiServlet.TYPE, STRING, "Type of the matching name", Utils.list(
				TaxonomySearchApiServlet.EXACT_MATCHES,
				TaxonomySearchApiServlet.PARTIAL_MATCHES,
				TaxonomySearchApiServlet.LIKELY_MATCHES),
				null, null, null);
		return schema;
	}

	private static void property(JSONObject schema, String field, String type, String description) {
		property(schema, field, type, description, null, null, null, null);
	}

	private static void property(JSONObject schema, String field, String type, String description, List<String> enums, String enumId, String format, String example) {
		schema.getObject(PROPERTIES).getObject(field).setString(TYPE, type.toLowerCase());
		if (description != null) {
			schema.getObject(PROPERTIES).getObject(field).setString(DESCRIPTION, description);
		}
		if (enums != null) {
			for (String s : enums) {
				schema.getObject(PROPERTIES).getObject(field).getArray(ENUM).appendString(s);
			}
		}
		if (enumId != null) {
			schema.getObject(PROPERTIES).getObject(field).setString(X_ENUM_ORIGIN, enumId);
		}
		if (format != null) {
			schema.getObject(PROPERTIES).getObject(field).setString(FORMAT, format);
		}
		if (example != null) {
			schema.getObject(PROPERTIES).getObject(field).getArray(EXAMPLES).appendString(example);
		}
	}

	private static JSONObject taxonApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Get taxon by id");
		desc.setString(DESCRIPTION, "Get taxon by id");
		pathParameter(desc,"id", "Id of the taxon", true, STRING);
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(Taxon.class)));
		desc.setArray(TAGS, TAXON_TAG_ARRAY);
		return desc;
	}

	private static JSONObject taxonSearchApi() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Taxon name search");
		desc.setString(DESCRIPTION, "Search taxa by scientific, vernacular and other types of names. Search word must be at least 3 characters for any matches to be returned.");
		queryParameter(desc, TaxonomySearchApiServlet.QUERY, "Search term", true, STRING);
		queryParameter(desc, TaxonomySearchApiServlet.LIMIT, "How many matches to return", false, TaxonomySearchApiServlet.DEFAULT_LIMIT, 1, 1000);
		queryParameter(desc, TaxonomySearchApiServlet.MATCH_TYPE, "Default: All match types. Default: All match types; exact = exact matches, partial = partially matching, likely = fuzzy matching. Multiple values are separated by a comma (,).", false, ARRAY, null, Utils.list(MatchType.EXACT, MatchType.PARTIAL, MatchType.LIKELY));

		queryParameter(desc, TaxonomySearchApiServlet.ONLY_SPECIES, "Match only species (and lower taxon ranks)", false, BOOLEAN);
		queryParameter(desc, TaxonomySearchApiServlet.ONLY_FINNISH, "Match only Finnish taxa", false, BOOLEAN);
		queryParameter(desc, TaxonomySearchApiServlet.ONLY_INVASIVE, "Match only Finnish invasive aliean species", false, BOOLEAN);

		queryParameter(desc, TaxonomySearchApiServlet.INCLUDE_HIDDEN, "Include hidden taxa", false, BOOLEAN);

		queryParameter(desc, TaxonomySearchApiServlet.REQUIRED_INFORMAL_TAXON_GROUP, "Limit matches to certain informal taxon groups. Separate multiple values by a comma (,).", false, STRING);
		queryParameter(desc, TaxonomySearchApiServlet.EXCLUDED_INFORMAL_TAXON_GROUP, "Exclode matches from certain informal taxon groups. Separate multiple values by a comma (,).", false, STRING);
		queryParameter(desc, TaxonomySearchApiServlet.REQUIRED_TAXON_SET, "Limit matches to certain taxon sets. Separate multiple values by a comma (,).", false, STRING);

		queryParameter(desc, TaxonomySearchApiServlet.EXCLUDED_NAME_TYPES, "Matching names have a type (for example MX.vernacularName, MX.hasMisappliedName); List name types you do not want included in the matches. Multiple values are separated by a comma (,).", false, STRING);
		queryParameter(desc, TaxonomySearchApiServlet.INCLUDED_NAME_TYPES, "Matching names have a type (for example MX.vernacularName, MX.hasMisappliedName); List name types you want included in the natches. Multiple values are separated by a comma (,).", false, STRING);
		queryParameter(desc, TaxonomySearchApiServlet.INCLUDED_LANGUAGES, "Include matches only from one or more languages (fi, sv, en). Scientific names do not have any language and are unaffected by this parameter. Multiple values are separated by a comma (,).", false, STRING);

		queryParameter(desc, TaxonomySearchApiServlet.CHECKLIST, "Search taxa from specified checklist. Defaults to FinBIF master checklist.", false, STRING);
		queryParameter(desc, TaxonomySearchApiServlet.OBSERVATION_MODE, "If observationMode is set, \" sp.\" is catenated to higher taxa scientific name matches.", false, BOOLEAN);

		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(TAXON_SEACRH_RESPONSE_MODEL)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TAXON_SEACRH_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TAXON_SEACRH_ERROR_MODEL)));
		desc.setArray(TAGS, TAXON_TAG_ARRAY);
		return desc;
	}

	private static JSONObject traitSearch() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Search traits");
		desc.setString(DESCRIPTION, "Search traits from all published datasets.");

		queryParameter(desc, TraitAPI.PAGE_SIZE,"Page size", false, TraitAPI.DEFAULT_PAGE_SIZE, 1, TraitAPISearch.MAX_PAGE_SIZE);
		queryParameter(desc, TraitAPI.PAGE, "Page number", false, 1, 1, null);
		searchAnyFieldParam(desc, TraitSearchRow.class);

		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(TRAITAPI_SEACRH_RESPONSE_MODEL)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject traitSearchDownload() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Download traits");
		desc.setString(DESCRIPTION, "Search traits from all published datasets and download results as Zipped TSV file. Returns maximum of " + TraitAPISearch.DOWNLOAD_MAX_ROWS + " rows.");

		searchAnyFieldParam(desc, TraitSearchRow.class);

		response(desc, 200, "Succesful response", new Content(APPLICATION_ZIP));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject traitInputSearch() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Search input rows");
		desc.setString(DESCRIPTION, "Search stored traits of a single dataset. Requires permissions to the dataset.");
		personTokenParameter(desc);
		datasetParameter(desc);
		queryParameter(desc, TraitAPI.PAGE_SIZE,"Page size", false, 10, 1, TraitAPIRows.MAX_PAGE_SIZE);
		searchAnyFieldParam(desc, InputRow.class);

		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, refArray(InputRow.class)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject getAll(Class<? extends AbstractBaseEntity> entityClass, String methodDescription, boolean allowAuth) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		if (allowAuth) {
			personTokenParameter(desc);
		}
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, refArray(entityClass)));
		if (allowAuth) {
			response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		}
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject getSingle(Class<? extends AbstractBaseEntity> entityClass, String methodDescription, boolean allowAuth) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		idPathParam(desc, entityClass);
		if (allowAuth) {
			personTokenParameter(desc);
		}
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(entityClass)));
		if (allowAuth) {
			response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		}
		response(desc, 404, "No entity with the given id", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject validateInsert(Class<? extends AbstractBaseEntity> entityClass, String methodDescription, boolean allowAuth) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		requestBody(desc, entityClass);
		if (allowAuth) {
			personTokenParameter(desc);
		}
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(ValidationResponse.class)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject validateUpdate(Class<? extends AbstractBaseEntity> entityClass, String methodDescription, boolean allowAuth) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		idPathParam(desc, entityClass);
		requestBody(desc, entityClass);
		if (allowAuth) {
			personTokenParameter(desc);
		}
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(ValidationResponse.class)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 404, "No entity with the given id", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject insert(Class<? extends AbstractBaseEntity> entityClass, String methodDescription, boolean allowAuth) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		requestBody(desc, entityClass);
		if (allowAuth) {
			personTokenParameter(desc);
		}
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(entityClass)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 422, "Validation failure - should call validation endpoints before calling upsert operations.", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject update(Class<? extends AbstractBaseEntity> entityClass, String methodDescription, boolean allowAuth) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		idPathParam(desc, entityClass);
		requestBody(desc, entityClass);
		if (allowAuth) {
			personTokenParameter(desc);
		}
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(entityClass)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 404, "No entity with the given id", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 422, "Validation failure - should call validation endpoints before calling upsert operations.", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject validateDelete(String methodDescription, boolean allowAuth) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		idPathParam(desc, null);
		if (allowAuth) {
			personTokenParameter(desc);
		}
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(ValidationResponse.class)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 404, "No entity with the given id", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject delete(String methodDescription, boolean allowAuth) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		idPathParam(desc, null);
		if (allowAuth) {
			personTokenParameter(desc);
		}
		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(TRAITAPI_OK_MODEL)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 404, "No entity with the given id", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 422, "Validation failure - should call validation endpoints before calling upsert operations.", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject validateTSV2Rows() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Validate TSV to InputRows conversion");
		JSONObject body = desc.getObject(REQUEST_BODY);
		body.setBoolean(REQUIRED, true);
		body.getObject(CONTENT).getObject(TEXT_TSV).getObject(SCHEMA).setString(TYPE, STRING);
		personTokenParameter(desc);
		datasetParameter(desc);

		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(TRAITAPI_TSV_VALIDATION_MODEL)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject executeTSV2Rows() {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, "Convert TSV lines to InputRows");
		JSONObject body = desc.getObject(REQUEST_BODY);
		body.setBoolean(REQUIRED, true);
		body.getObject(CONTENT).getObject(TEXT_TSV).getObject(SCHEMA).setString(TYPE, STRING);
		personTokenParameter(desc);
		datasetParameter(desc);

		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, refArray(InputRow.class)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 422, "Validation failure - should call validation endpoint before doing conversion.", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject validateMulti(Class<? extends AbstractBaseEntity> entityClass, String methodDescription) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		multiBody(entityClass, desc);
		personTokenParameter(desc);

		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(TRAITAPI_MULTI_VALIDATION_MODEL)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static JSONObject insertMulti(Class<? extends AbstractBaseEntity> entityClass, String methodDescription) {
		JSONObject desc = new JSONObject();
		desc.setString(SUMMARY, methodDescription);
		multiBody(entityClass, desc);
		personTokenParameter(desc);

		response(desc, 200, "Succesful response", new Content(APPLICATION_JSON, ref(TRAITAPI_OK_MODEL)));
		response(desc, 400, "Illegal arguments", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 403, "No access / Invalid authorization", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 422, "Validation failure - should call validation endpoint before doing insert.", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		response(desc, 500, "Unknown failure", new Content(APPLICATION_JSON, ref(TRAITAPI_ERROR_MODEL)));
		desc.setArray(TAGS, TRAIT_TAG_ARRAY);
		return desc;
	}

	private static void searchAnyFieldParam(JSONObject desc, Class<?> searchClass) {
		String example = searchClass == TraitSearchRow.class ?
				"Example: subject.type=TDF.typeTaxon&trait.id=TDF.1&trait.id=TDF.2&subjectFinBIFTaxon.higherTaxa.kingdom=Animalia" :
					"Example: subject.type=TDF.typeTaxon&traits.statisticalMethod=TDF.statisticalMethodSD&traits.statisticalMethod=TDF.statisticalMethodAvg";
		JSONObject anyFieldParam = new JSONObject()
				.setString(NAME, "searchParams")
				.setString(IN, QUERY)
				.setBoolean(REQUIRED, false)
				.setString(DESCRIPTION, "" +
						"A set of key-value pairs used to filter search results.\n" +
						"Each key represents a field name in the result model, and its corresponding value specifies the criteria that must be met for the records to match.\n" +
						"If field is given multiple time as parameter, at least one of the given values must match.\n" +
						example);
		anyFieldParam.getObject(SCHEMA).setString(TYPE, OBJECT);
		anyFieldParam.getObject(SCHEMA).getObject(ADDITIONAL_PROPERTIES).setString(TYPE, STRING);
		desc.getArray(PARAMETERS).appendObject(anyFieldParam);
	}

	private static void parameter(JSONObject desc, String in, String name, String description, boolean required, String type, Object defaultValue, Collection<?> enumValues, Integer min, Integer max, String format) {
		JSONObject param = new JSONObject()
				.setString(NAME, name)
				.setString(IN, in)
				.setBoolean(REQUIRED, required)
				.setString(DESCRIPTION, description);

		param.getObject(SCHEMA).setString(TYPE, type);
		if (format != null) param.getObject(SCHEMA).setString(FORMAT, format);

		if (ARRAY.equals(type)) {
			if (enumValues == null) throw new IllegalStateException("No enum given for array: " + name);
		}
		if (enumValues != null) {
			for (Object o : enumValues) {
				if (Collection.class.isAssignableFrom(o.getClass())) throw new IllegalStateException("Enum value must not be a collection: " + name);
				param.getObject(SCHEMA).getArray(ENUM).appendString(o.toString());
			}
		}

		if (defaultValue != null) {
			if (defaultValue instanceof Boolean) {
				param.getObject(SCHEMA).setBoolean(DEFAULT, (boolean)defaultValue);
			} else if (defaultValue.getClass() == int.class || defaultValue instanceof Integer) {
				param.getObject(SCHEMA).setInteger(DEFAULT, (int)defaultValue);
			} else {
				param.getObject(SCHEMA).setString(DEFAULT, defaultValue.toString());
			}
		}
		if (min != null) param.getObject(SCHEMA).setInteger(MINIMUM, min);
		if (max !=  null) param.getObject(SCHEMA).setInteger(MAXIMUM, max);

		desc.getArray(PARAMETERS).appendObject(param);
	}

	private static void queryParameter(JSONObject desc, String name, String description, boolean required, String type) {
		parameter(desc, QUERY, name, description, required, type, null, null, null, null, null);
	}

	private static void queryParameter(JSONObject desc, String name, String description, boolean required, int defaultValue, int min, Integer max) {
		parameter(desc, QUERY, name, description, required, INTEGER, defaultValue, null, min, max, null);
	}

	private static void queryParameter(JSONObject desc, String name, String description, boolean required, String type, Object defaultValue, Collection<?> enumValues) {
		parameter(desc, QUERY, name, description, required, type, defaultValue, enumValues, null, null, null);
	}

	private static void pathParameter(JSONObject desc, String name, String description, boolean required, String type) {
		parameter(desc, PATH, name, description, required, type, null, null, null, null, null);
	}

	private static void idPathParam(JSONObject desc, Class<? extends AbstractBaseEntity> entityClass) {
		String name = "id";
		String description = entityClass != null ? "Id of the " + entityClass.getSimpleName() : "Id";
		if (entityClass == DatasetPermissions.class) {
			name = "datasetId";
			description = "Id of the dataset";
		}
		parameter(desc, PATH, name, description, true, STRING, null, null, null, null, null);
	}

	private static void personTokenParameter(JSONObject desc) {
		queryParameter(desc, TraitAPI.PERSON_TOKEN, "Identity of a logged in user", false, STRING);
	}

	private static void datasetParameter(JSONObject desc) {
		queryParameter(desc, TraitAPIRows.DATASET_ID, "Id if the dataset", true, STRING);
	}

	private static void requestBody(JSONObject desc, Class<? extends AbstractBaseEntity> entityClass) {
		JSONObject body = desc.getObject(REQUEST_BODY);
		body.setBoolean(REQUIRED, true);
		body.getObject(CONTENT).getObject(APPLICATION_JSON).getObject(SCHEMA).setString(REF, ref(entityClass));
	}

	private static void multiBody(Class<? extends AbstractBaseEntity> entityClass, JSONObject desc) {
		JSONObject body = desc.getObject(REQUEST_BODY);
		body.setBoolean(REQUIRED, true);
		body.getObject(CONTENT).getObject(APPLICATION_JSON).setObject(SCHEMA, refArray(entityClass));
	}

	private static void response(JSONObject desc, int status, String description, Content content) {
		response(desc, status, description, Utils.list(content));
	}

	private static void response(JSONObject desc, int status, String description, List<Content> contents) {
		if (status != 200 && !desc.getObject(RESPONSES).hasKey(""+200)) throw new IllegalStateException("Should always define 200 first, errors after");

		JSONObject response = new JSONObject();
		response.setString(DESCRIPTION, description);

		for (Content content : contents) {
			if (content.ref != null) {
				response.getObject(CONTENT).getObject(content.contentType).getObject(SCHEMA).setString(REF, content.ref);
			} else if (content.schema != null) {
				response.getObject(CONTENT).getObject(content.contentType).setObject(SCHEMA, content.schema);
			} else if (content.contentType.equals(APPLICATION_ZIP)) {
				JSONObject schema = response.getObject(CONTENT).getObject(content.contentType).getObject(SCHEMA);
				schema.setString(TYPE, STRING);
				schema.setString(FORMAT, "binary");
				schema.getArray(EXAMPLES).appendString("Single TSV file ZIP compressed");
			} else {
				response.getObject(CONTENT).getObject(content.contentType).getObject(SCHEMA).setString(TYPE, STRING);
			}
		}
		desc.getObject(RESPONSES).setObject(""+status, response);
	}

	private static class Content {
		private final String contentType;
		private final String ref;
		private final JSONObject schema;
		public Content(String contentType) {
			this.contentType = contentType;
			this.ref = null;
			this.schema = null;
		}
		public Content(String contentType, String ref) {
			this.contentType = contentType;
			this.ref = ref;
			this.schema = null;
		}
		public Content(String contentType, JSONObject schema) {
			this.contentType = contentType;
			this.ref = null;
			this.schema = schema;
		}
	}

	private static String ref(String ref) {
		return "#/components/schemas/" + ref;
	}

	private static String ref(Class<?> refClass) {
		return ref(refClass.getSimpleName());
	}

	private static JSONObject refArray(Class<?> refClass) {
		JSONObject schema = new JSONObject();
		schema.setString(TYPE, ARRAY);
		schema.getObject(ITEMS).setString(REF, ref(refClass));
		return schema;
	}

	private static final JSONArray TAXON_TAG_ARRAY = new JSONArray().appendString(TAXON_TAG);
	private static final JSONArray TRAIT_TAG_ARRAY = new JSONArray().appendString(TRAIT_TAG);

}