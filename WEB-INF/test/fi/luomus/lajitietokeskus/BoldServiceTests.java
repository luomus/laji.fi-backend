package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.lajitietokeskus.taxonomy.dao.BoldService;
import fi.luomus.lajitietokeskus.taxonomy.dao.BoldService.BoldRecordTaxon;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonomyDAO_Imple.TaxonLoadingException;

public class BoldServiceTests {

	@Test
	public void loading() throws TaxonLoadingException {
		BoldService service = new BoldService(TestConfig.getConfig(), new ErrorReportingToSystemErr(), EvaluationReaderTests.getTestDataFolder());
		Map<String, List<BoldRecordTaxon>> recordTaxa = service.load();

		assertEquals(3, recordTaxa.keySet().size());
		assertEquals(3, recordTaxa.values().stream().mapToInt(l->l.size()).sum());

		assertEquals("" +
				"[BoldRecordTaxon [records=BoldRecords [publicRecords=4, specimens=4, barcodes=2, bins=[BOLD:AAO9081, BOLD:AAO9082]], phylumName=Heterokontophyta, className=Oomycota, familyName=Saprolegniaceae]]",
				recordTaxa.get("Achlya dubia").toString());
	}

}
