package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.dao.FinBIFBibService;
import fi.luomus.lajitietokeskus.dao.FinBIFBibService.FinBIFBibServiceDataProvider;
import fi.luomus.lajitietokeskus.models.CMSContent;

public class FinBIFBibServiceTests {

	@Test
	public void test() throws Exception {
		FinBIFBibService service = new FinBIFBibService(new FinBIFBibServiceDataProvider() {
			@Override
			public JSONArray getData() {
				try {
					return new JSONArray(getContents("bib-1.json"));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});

		CMSContent content = new CMSContent();
		service.generateContent(content);
		assertEquals(getContents("bib-1-expected.html"), content.getById("finbif-bib-all").getContent());
	}

	@Test
	public void test_actual() {
		FinBIFBibService service = new FinBIFBibService(TestConfig.getConfig());
		CMSContent content = new CMSContent();
		service.generateContent(content);
		int allCount = Utils.countNumberOf("<article ", content.getById("finbif-bib-all").getContent());
		int topCount = Utils.countNumberOf("<article ", content.getById("finbif-bib-top").getContent());
		assertTrue(allCount > 5);
		assertEquals(3, topCount);
		System.out.println(content.getById("finbif-bib-all").getContent());
	}

	private String getContents(String filename) throws Exception {
		return TestUtil.getContents(FinBIFBibServiceTests.class, filename);
	}

}
