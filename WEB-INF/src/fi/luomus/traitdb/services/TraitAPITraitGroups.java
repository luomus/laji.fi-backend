package fi.luomus.traitdb.services;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOEntity;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.services.TraitAPI.Method;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIAuthorizedRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIEndpoint;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIResource;

public class TraitAPITraitGroups implements TraitAPIResource {

	private final TraitAPIRequest req;
	private final TraitDAOEntity<TraitGroup> dao;

	public TraitAPITraitGroups(TraitAPIRequest req) {
		this.req = req;
		this.dao = req.getDAO().groups();
	}

	@Override
	public TraitAPIEndpoint endpoint() {
		String endpoint = req.getEndpoint();
		if (Method.GET == req.getMethod()) {
			if ("".equals(endpoint)) return getAll();
			if ("{id}".equals(endpoint)) return getSingle(req.getId());
		}
		if (Method.POST == req.getMethod()) {
			if ("validate".equals(endpoint)) return validate(req.getJson());
			if ("validate-update/{id}".equals(endpoint)) return validateUpdate(req.getId(), req.getJson());
			if ("validate-delete/{id}".equals(endpoint)) return validateDelete(req.getId());
			if ("".equals(endpoint)) return insert(req.getJson());
		}
		if (Method.PUT == req.getMethod()) {
			if ("{id}".equals(endpoint)) return update(req.getId(), req.getJson());
		}
		if (Method.DELETE == req.getMethod()) {
			if ("{id}".equals(endpoint)) return delete(req.getId());
		}
		throw req.noEndpointException();
	}

	private void validateIdMatches(Qname id, TraitGroup group) {
		if (group.getId() == null || !group.getId().equals(id)) throw new IllegalArgumentException("Id in path and model do not match");
	}

	private TraitAPIAuthorizedRequest allowAll(TraitAPIEndpoint endpoint) {
		return new TraitAPIAuthorizedRequest(endpoint);
	}

	private TraitAPIEndpoint delete(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				dao.validateDelete(id).requirePass();
				dao.delete(id);
				return "ok";
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

	private TraitAPIEndpoint validateDelete(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.validateDelete(id);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

	private TraitAPIEndpoint update(Qname id, JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				TraitGroup group = new TraitGroup(json);
				validateIdMatches(id, group);
				dao.validate(group, false).requirePass();
				return dao.update(group);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

	private TraitAPIEndpoint validateUpdate(Qname id, JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				TraitGroup group = new TraitGroup(json);
				validateIdMatches(id, group);
				return dao.validate(group, false);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

	private TraitAPIEndpoint insert(JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				TraitGroup group = new TraitGroup(json);
				dao.validate(group, true).requirePass();
				dao.insert(group);
				return group;
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

	private TraitAPIEndpoint validate(JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				TraitGroup group = new TraitGroup(json);
				return dao.validate(group, true);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

	private TraitAPIEndpoint getSingle(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.get(id);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

	private TraitAPIEndpoint getAll() {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.getAll();
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

}
