package fi.luomus.lajitietokeskus.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;

public class HomePageServlet extends BaseServlet {

	private static final long serialVersionUID = 6003033342219545040L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String locale = getLocale(req);
		String location = "https://laji.fi";
		if ("en".equals(locale)) location = "https://laji.fi/en";
		if ("sv".equals(locale)) location = "https://laji.fi/sv";
		return redirectTo(location);
	}

}
