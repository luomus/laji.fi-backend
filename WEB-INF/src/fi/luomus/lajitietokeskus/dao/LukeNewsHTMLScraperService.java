package fi.luomus.lajitietokeskus.dao;

import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import fi.luomus.commons.utils.DateUtils;
import fi.luomus.lajitietokeskus.models.ExternalNewsItem;
import fi.luomus.lajitietokeskus.models.NewsItem;

public class LukeNewsHTMLScraperService {

	public static List<NewsItem> scrape(String html) throws Exception {
		List<NewsItem> news = new ArrayList<>();
		Document doc = Jsoup.parse(html);
		Element newsDiv = doc.select("div .section-cards").first();
		for (Element e : newsDiv.select("div .card__content")) {
			String id = e.parent().attr("data-history-node-id");
			String link = e.select("a[href]").attr("href");
			String title = e.select("a[href]").text().trim();
			String date = e.select("div .card__content--top").select("div").last().text().trim();
			news.add(newsItem(id, link, title, date));
		}
		return news;
	}

	private static NewsItem newsItem(String id, String link, String title, String date) throws Exception {
		Timestamp posted = toTimestamp(date);
		URL url = new URL("https://www.luke.fi" + link);
		return new ExternalNewsItem(id, posted, title, "luke.fi", url);
	}

	private static Timestamp toTimestamp(String s) throws ParseException {
		return new Timestamp(DateUtils.convertToDate(s).getTime());
	}

}
