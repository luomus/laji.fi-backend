package fi.luomus.traitdb.services;

import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.traitdb.dao.TraitDAO.SearchDAO;
import fi.luomus.traitdb.services.TraitAPI.Method;
import fi.luomus.traitdb.services.TraitAPI.PagingParameters;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIAuthorizedRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIEndpoint;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIResource;

public class TraitAPISearch implements TraitAPIResource {

	public static final int DOWNLOAD_MAX_ROWS = 1000000;
	public static final int MAX_PAGE_SIZE = 10000;
	public static final String RESULTS = "results";
	public static final String TOTAL = "total";
	public static final String PAGE_SIZE = "pageSize";
	public static final String LAST_PAGE = "lastPage";
	public static final String NEXT_PAGE = "nextPage";
	public static final String PREV_PAGE = "prevPage";
	public static final String CURRENT_PAGE = "currentPage";

	private final TraitAPIRequest req;

	public TraitAPISearch(TraitAPIRequest req) {
		this.req = req;
	}

	@Override
	public TraitAPIEndpoint endpoint() {
		String endpoint = req.getEndpoint();
		if (req.getMethod() == Method.GET) {
			if ("".equals(endpoint)) return new TraitAPISearchEndpoint();
			if ("download".equals(endpoint)) return new TraitApiDownloadEndpoint();
		}
		throw req.noEndpointException();
	}

	private class TraitAPISearchEndpoint implements TraitAPIEndpoint {
		@Override
		public TraitAPIAuthorizedRequest authorize() {
			return new TraitAPIAuthorizedRequest(this);
		}

		@Override
		public Object execute() {
			if (req.getPagingParameters().pageSize > MAX_PAGE_SIZE) throw new IllegalArgumentException("Max page size is " + MAX_PAGE_SIZE);
			SearchDAO dao = req.getDAO().search();
			long total = dao.searchTotal(req.getParams());
			JSONArray results = dao.search(req.getParams(), req.getPagingParameters().pageSize, req.getPagingParameters().page);
			Pages pages = new Pages(total, req.getPagingParameters());
			JSONObject response = new JSONObject();
			response.setInteger(CURRENT_PAGE, pages.getCurrentPage());
			if (pages.getPrevPage() != null) {
				response.setInteger(PREV_PAGE, pages.getPrevPage());
			}
			if (pages.getNextPage() !=  null) {
				response.setInteger(NEXT_PAGE, pages.getNextPage());
			}
			response.setInteger(LAST_PAGE, pages.getLastPage());
			response.setInteger(PAGE_SIZE, pages.getPageSize());
			response.setInteger(TOTAL, Math.toIntExact(total));
			response.setArray(RESULTS, results);
			return response;
		}
	}

	private class TraitApiDownloadEndpoint implements TraitAPIEndpoint {

		@Override
		public TraitAPIAuthorizedRequest authorize() {
			return new TraitAPIAuthorizedRequest(this);
		}

		@Override
		public Object execute() throws Exception {
			OutputStream out = null;
			try {
				String filename = "trait_download_" + DateUtils.getFilenameDatetime();
				HttpServletResponse res = req.getResponse();
				res.setContentType("application/zip");
				res.setHeader("Content-disposition","attachment; filename="+filename+".zip");
				out = res.getOutputStream();
				req.getDAO().search().download(req.getParams(), DOWNLOAD_MAX_ROWS, out, filename);
				return new ResponseData().setOutputAlreadyPrinted();
			} catch (Exception e) {
				if (out == null) {
					throw e;
				}
				// res.getOutputStream has already been called so returning a different response is not longer possible
				if (userAborted(e)) {
					// user aborted download: there is no-one to receive response, just don't return anything; also don't want to throw the Exception because don't want to receive error report about user aborted download
					return new ResponseData().setOutputAlreadyPrinted();
				}
				throw new ServletException(e);
			} finally {
				if (out != null) {
					try { out.flush(); } catch (Exception e) {}
				}
			}
		}

		private boolean userAborted(Exception e) {
			String stack = LogUtils.buildStackTrace(e, 5);
			return stack.contains("ClientAbortException");
		}
	}

	private class Pages {
		private final int currentPage;
		private final int pageSize;
		private final int lastPage;

		private Pages(long totalResults, PagingParameters pagingParameters) {
			this.currentPage = pagingParameters.page;
			this.pageSize = pagingParameters.pageSize;
			this.lastPage = lastPage(totalResults, pageSize);
		}

		private int lastPage(long totalResults, int pageSize) {
			int lastPage = (int) Math.ceil( (double) totalResults / pageSize);
			if (lastPage == 0) lastPage = 1;
			return lastPage;
		}

		public int getCurrentPage() {
			return currentPage;
		}


		public Integer getPrevPage() {
			if (currentPage > 1) {
				return currentPage - 1;
			}
			return null;
		}

		public Integer getNextPage() {
			if (currentPage != lastPage) {
				return currentPage + 1;
			}
			return null;
		}

		public int getLastPage() {
			return lastPage;
		}

		public int getPageSize() {
			return pageSize;
		}
	}
}