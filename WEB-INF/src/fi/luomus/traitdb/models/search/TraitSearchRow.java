package fi.luomus.traitdb.models.search;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.models.AbstractBaseEntity;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.FieldDef;
import fi.luomus.traitdb.models.Subject;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitGroup;

public class TraitSearchRow extends AbstractBaseEntity {

	@Override
	protected boolean onlySearchFields() { return true; }

	@FieldDef(order=0, etsOverride="measurementID")
	private Qname id;

	@FieldDef(order=1.1)
	private Subject subject;

	@FieldDef(order=1.2)
	private Integer year;

	@FieldDef(order=1.3)
	private Integer month;

	@FieldDef(order=1.4)
	private Integer day;

	@FieldDef(order=1.5)
	private String eventDate;

	@FieldDef(order=1.6)
	private String geodeticDatum;

	@FieldDef(order=2.1)
	private Trait trait;

	@FieldDef(order=2.1)
	private TraitGroup traitGroup;

	@FieldDef(order=3, enumeration="TDF.statisticalMethodEnum")
	private Qname statisticalMethod;

	@FieldDef(order=4.1, etsOverride="traitValue")
	private String value;
	@FieldDef(order=4.2)
	private Double valueNumeric;
	@FieldDef(order=4.3, enumeration="TDF.unitOfMeasurementEnum", etsOverride="traitUnit")
	private Qname unit;
	@FieldDef(order=4.4)
	private Double measurementAccuracy;

	@FieldDef(order=5.1, etsOverride="verbatimTraitValue")
	private String originalValue;
	@FieldDef(order=5.2)
	private Double originalValueNumeric;
	@FieldDef(order=5.3, enumeration="TDF.unitOfMeasurementEnum", etsOverride="verbatimTraitUnit")
	private Qname originalUnit;
	@FieldDef(order=5.4)
	private Double originalMeasurementAccuracy;

	@FieldDef(order=6.1)
	private TraitTaxon subjectFinBIFTaxon;
	@FieldDef(order=6.2)
	private TraitTaxon subjectGBIFTaxon;

	@FieldDef(order=7.1, enumeration="MY.lifeStages")
	private Qname objectTaxonLifeStage;
	@FieldDef(order=7.2)
	private String objectTaxonVerbatim;
	@FieldDef(order=7.3)
	private TraitTaxon objectFinBIFTaxon;
	@FieldDef(order=7.4)
	private TraitTaxon objectGBIFTaxon;

	@FieldDef(order=8.1)
	private boolean warnings;
	@FieldDef(order=8.2)
	private String measurementRemarks;
	@FieldDef(order=8.3, etsOverride="references")
	private String reference;

	@FieldDef(order=10.0)
	private Dataset dataset;

	@FieldDef(order=10.1, enumeration="MZ.intellectualRightsEnum")
	private Qname license;

	public TraitSearchRow() {
		super();
	}

	public TraitSearchRow(JSONObject json) {
		super(json);
	}

	public Qname getId() {
		return id;
	}

	public Subject getSubject() {
		return subject;
	}

	public Integer getYear() {
		return year;
	}

	public Integer getMonth() {
		return month;
	}

	public Integer getDay() {
		return day;
	}

	public String getEventDate() {
		return eventDate;
	}

	public String getGeodeticDatum() {
		return geodeticDatum;
	}

	public Trait getTrait() {
		return trait;
	}

	public TraitGroup getTraitGroup() {
		return traitGroup;
	}

	public Qname getStatisticalMethod() {
		return statisticalMethod;
	}

	public String getValue() {
		return value;
	}

	public Double getValueNumeric() {
		return valueNumeric;
	}

	public Qname getUnit() {
		return unit;
	}

	public Double getMeasurementAccuracy() {
		return measurementAccuracy;
	}

	public String getOriginalValue() {
		return originalValue;
	}

	public Double getOriginalValueNumeric() {
		return originalValueNumeric;
	}

	public Qname getOriginalUnit() {
		return originalUnit;
	}

	public Double getOriginalMeasurementAccuracy() {
		return originalMeasurementAccuracy;
	}

	public TraitTaxon getSubjectFinBIFTaxon() {
		return subjectFinBIFTaxon;
	}

	public TraitTaxon getSubjectGBIFTaxon() {
		return subjectGBIFTaxon;
	}

	public Qname getObjectTaxonLifeStage() {
		return objectTaxonLifeStage;
	}

	public String getObjectTaxonVerbatim() {
		return objectTaxonVerbatim;
	}

	public TraitTaxon getObjectFinBIFTaxon() {
		return objectFinBIFTaxon;
	}

	public TraitTaxon getObjectGBIFTaxon() {
		return objectGBIFTaxon;
	}

	public boolean isWarnings() {
		return warnings;
	}

	public String getMeasurementRemarks() {
		return measurementRemarks;
	}

	public String getReference() {
		return reference;
	}

	public Dataset getDataset() {
		return dataset;
	}

	public Qname getLicense() {
		return license;
	}

	public void setId(Qname id) {
		this.id = id;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public void setGeodeticDatum(String geodeticDatum) {
		this.geodeticDatum = geodeticDatum;
	}

	public void setTrait(Trait trait) {
		this.trait = trait;
	}

	public void setTraitGroup(TraitGroup traitGroup) {
		this.traitGroup = traitGroup;
	}

	public void setStatisticalMethod(Qname statisticalMethod) {
		this.statisticalMethod = statisticalMethod;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setValueNumeric(Double valueNumeric) {
		this.valueNumeric = valueNumeric;
	}

	public void setUnit(Qname unit) {
		this.unit = unit;
	}

	public void setMeasurementAccuracy(Double measurementAccuracy) {
		this.measurementAccuracy = measurementAccuracy;
	}

	public void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}

	public void setOriginalValueNumeric(Double originalValueNumeric) {
		this.originalValueNumeric = originalValueNumeric;
	}

	public void setOriginalUnit(Qname originalUnit) {
		this.originalUnit = originalUnit;
	}

	public void setOriginalMeasurementAccuracy(Double originalMeasurementAccuracy) {
		this.originalMeasurementAccuracy = originalMeasurementAccuracy;
	}

	public void setSubjectFinBIFTaxon(TraitTaxon subjectFinBIFTaxon) {
		this.subjectFinBIFTaxon = subjectFinBIFTaxon;
	}

	public void setSubjectGBIFTaxon(TraitTaxon subjectGBIFTaxon) {
		this.subjectGBIFTaxon = subjectGBIFTaxon;
	}

	public void setObjectTaxonLifeStage(Qname objectTaxonLifeStage) {
		this.objectTaxonLifeStage = objectTaxonLifeStage;
	}

	public void setObjectTaxonVerbatim(String objectTaxonVerbatim) {
		this.objectTaxonVerbatim = objectTaxonVerbatim;
	}

	public void setObjectFinBIFTaxon(TraitTaxon objectFinBIFTaxon) {
		this.objectFinBIFTaxon = objectFinBIFTaxon;
	}

	public void setObjectGBIFTaxon(TraitTaxon objectGBIFTaxon) {
		this.objectGBIFTaxon = objectGBIFTaxon;
	}

	public void setWarnings(boolean warnings) {
		this.warnings = warnings;
	}

	public void setMeasurementRemarks(String measurementRemarks) {
		this.measurementRemarks = measurementRemarks;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	public void setLicense(Qname license) {
		this.license = license;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataset == null) ? 0 : dataset.hashCode());
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((eventDate == null) ? 0 : eventDate.hashCode());
		result = prime * result + ((geodeticDatum == null) ? 0 : geodeticDatum.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((license == null) ? 0 : license.hashCode());
		result = prime * result + ((measurementAccuracy == null) ? 0 : measurementAccuracy.hashCode());
		result = prime * result + ((measurementRemarks == null) ? 0 : measurementRemarks.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((objectFinBIFTaxon == null) ? 0 : objectFinBIFTaxon.hashCode());
		result = prime * result + ((objectGBIFTaxon == null) ? 0 : objectGBIFTaxon.hashCode());
		result = prime * result + ((objectTaxonLifeStage == null) ? 0 : objectTaxonLifeStage.hashCode());
		result = prime * result + ((objectTaxonVerbatim == null) ? 0 : objectTaxonVerbatim.hashCode());
		result = prime * result + ((originalMeasurementAccuracy == null) ? 0 : originalMeasurementAccuracy.hashCode());
		result = prime * result + ((originalUnit == null) ? 0 : originalUnit.hashCode());
		result = prime * result + ((originalValue == null) ? 0 : originalValue.hashCode());
		result = prime * result + ((originalValueNumeric == null) ? 0 : originalValueNumeric.hashCode());
		result = prime * result + ((reference == null) ? 0 : reference.hashCode());
		result = prime * result + ((statisticalMethod == null) ? 0 : statisticalMethod.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((subjectFinBIFTaxon == null) ? 0 : subjectFinBIFTaxon.hashCode());
		result = prime * result + ((subjectGBIFTaxon == null) ? 0 : subjectGBIFTaxon.hashCode());
		result = prime * result + ((trait == null) ? 0 : trait.hashCode());
		result = prime * result + ((traitGroup == null) ? 0 : traitGroup.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((valueNumeric == null) ? 0 : valueNumeric.hashCode());
		result = prime * result + (warnings ? 1231 : 1237);
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TraitSearchRow other = (TraitSearchRow) obj;
		if (dataset == null) {
			if (other.dataset != null)
				return false;
		} else if (!dataset.equals(other.dataset))
			return false;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (eventDate == null) {
			if (other.eventDate != null)
				return false;
		} else if (!eventDate.equals(other.eventDate))
			return false;
		if (geodeticDatum == null) {
			if (other.geodeticDatum != null)
				return false;
		} else if (!geodeticDatum.equals(other.geodeticDatum))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (license == null) {
			if (other.license != null)
				return false;
		} else if (!license.equals(other.license))
			return false;
		if (measurementAccuracy == null) {
			if (other.measurementAccuracy != null)
				return false;
		} else if (!measurementAccuracy.equals(other.measurementAccuracy))
			return false;
		if (measurementRemarks == null) {
			if (other.measurementRemarks != null)
				return false;
		} else if (!measurementRemarks.equals(other.measurementRemarks))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (objectFinBIFTaxon == null) {
			if (other.objectFinBIFTaxon != null)
				return false;
		} else if (!objectFinBIFTaxon.equals(other.objectFinBIFTaxon))
			return false;
		if (objectGBIFTaxon == null) {
			if (other.objectGBIFTaxon != null)
				return false;
		} else if (!objectGBIFTaxon.equals(other.objectGBIFTaxon))
			return false;
		if (objectTaxonLifeStage == null) {
			if (other.objectTaxonLifeStage != null)
				return false;
		} else if (!objectTaxonLifeStage.equals(other.objectTaxonLifeStage))
			return false;
		if (objectTaxonVerbatim == null) {
			if (other.objectTaxonVerbatim != null)
				return false;
		} else if (!objectTaxonVerbatim.equals(other.objectTaxonVerbatim))
			return false;
		if (originalMeasurementAccuracy == null) {
			if (other.originalMeasurementAccuracy != null)
				return false;
		} else if (!originalMeasurementAccuracy.equals(other.originalMeasurementAccuracy))
			return false;
		if (originalUnit == null) {
			if (other.originalUnit != null)
				return false;
		} else if (!originalUnit.equals(other.originalUnit))
			return false;
		if (originalValue == null) {
			if (other.originalValue != null)
				return false;
		} else if (!originalValue.equals(other.originalValue))
			return false;
		if (originalValueNumeric == null) {
			if (other.originalValueNumeric != null)
				return false;
		} else if (!originalValueNumeric.equals(other.originalValueNumeric))
			return false;
		if (reference == null) {
			if (other.reference != null)
				return false;
		} else if (!reference.equals(other.reference))
			return false;
		if (statisticalMethod == null) {
			if (other.statisticalMethod != null)
				return false;
		} else if (!statisticalMethod.equals(other.statisticalMethod))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (subjectFinBIFTaxon == null) {
			if (other.subjectFinBIFTaxon != null)
				return false;
		} else if (!subjectFinBIFTaxon.equals(other.subjectFinBIFTaxon))
			return false;
		if (subjectGBIFTaxon == null) {
			if (other.subjectGBIFTaxon != null)
				return false;
		} else if (!subjectGBIFTaxon.equals(other.subjectGBIFTaxon))
			return false;
		if (trait == null) {
			if (other.trait != null)
				return false;
		} else if (!trait.equals(other.trait))
			return false;
		if (traitGroup == null) {
			if (other.traitGroup != null)
				return false;
		} else if (!traitGroup.equals(other.traitGroup))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (valueNumeric == null) {
			if (other.valueNumeric != null)
				return false;
		} else if (!valueNumeric.equals(other.valueNumeric))
			return false;
		if (warnings != other.warnings)
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TraitSearchRow [id=" + id + ", subject=" + subject + ", year=" + year + ", month=" + month + ", day=" + day + ", eventDate=" + eventDate + ", geodeticDatum="
				+ geodeticDatum + ", trait=" + trait + ", traitGroup=" + traitGroup + ", statisticalMethod=" + statisticalMethod + ", value=" + value + ", valueNumeric="
				+ valueNumeric + ", unit=" + unit + ", measurementAccuracy=" + measurementAccuracy + ", originalValue=" + originalValue + ", originalValueNumeric="
				+ originalValueNumeric + ", originalUnit=" + originalUnit + ", originalMeasurementAccuracy=" + originalMeasurementAccuracy + ", subjectFinBIFTaxon="
				+ subjectFinBIFTaxon + ", subjectGBIFTaxon=" + subjectGBIFTaxon + ", objectTaxonLifeStage=" + objectTaxonLifeStage + ", objectTaxonVerbatim=" + objectTaxonVerbatim
				+ ", objectFinBIFTaxon=" + objectFinBIFTaxon + ", objectGBIFTaxon=" + objectGBIFTaxon + ", warnings=" + warnings + ", measurementRemarks=" + measurementRemarks
				+ ", reference=" + reference + ", dataset=" + dataset + ", license=" + license + "]";
	}

	public static String getTSVHeader() {
		try {
			StringBuilder line = new StringBuilder();
			headers(line, TraitSearchRow.class, null);
			line.append("\n");
			return line.toString();
		} catch (Exception e) {
			throw new IllegalStateException("TSV header generation failed", e);
		}
	}

	@SuppressWarnings("unchecked")
	private static void headers(StringBuilder line, Class<? extends AbstractBaseEntity> entityClass, String prefix) {
		for (Field f : getSortedFields(entityClass, true)) {
			if (AbstractBaseEntity.class.isAssignableFrom(f.getType())) {
				String newPrefix = null;
				if (f.getName().equals("subjectFinBIFTaxon")) newPrefix = "subject_finbif_";
				if (f.getName().equals("subjectGBIFTaxon")) newPrefix = "subject_gbif_";
				if (f.getName().equals("objectFinBIFTaxon")) newPrefix = "object_finbif_";
				if (f.getName().equals("objectGBIFTaxon")) newPrefix = "object_gbif_";
				headers(line, (Class<? extends AbstractBaseEntity>) f.getType(), newPrefix);
			} else {
				String ets = f.getAnnotation(FieldDef.class).etsOverride();
				if (ets.equals("skip")) continue;
				String fieldName = ets.isEmpty() ? f.getName() : ets;
				if (prefix != null) fieldName = prefix + fieldName;
				toTSV(line, fieldName);
			}
		}
	}

	public String toTSV(TraitDAO dao) {
		try {
			StringBuilder line = new StringBuilder();
			data(line, this, dao);
			line.append("\n");
			return line.toString();
		} catch (Exception e) {
			throw new IllegalStateException("TSV conversion failed", e);
		}
	}

	@SuppressWarnings("unchecked")
	private void data(StringBuilder line, AbstractBaseEntity entity, TraitDAO dao) throws Exception {
		for (Field f : getSortedFields(entity.getClass(), true)) {
			Method getter = ReflectionUtil.getGetter(f.getName(), entity.getClass());
			Object v = getter.invoke(entity);
			if (AbstractBaseEntity.class.isAssignableFrom(f.getType())) {
				if (v == null) {
					emptyValues(line, (Class<? extends AbstractBaseEntity>) f.getType());
				} else {
					data(line, (AbstractBaseEntity) v, dao);
				}
			} else {
				FieldDef def = f.getAnnotation(FieldDef.class);
				if (def.etsOverride().equals("skip")) continue;
				toTSV(line, toValue(v, def, dao));
			}
		}
	}

	private void emptyValues(StringBuilder line, Class<? extends AbstractBaseEntity> entityClass) {
		for (Field f : getSortedFields(entityClass, true)) {
			if (f.getAnnotation(FieldDef.class).etsOverride().equals("skip")) continue;
			toTSV(line, "");
		}
	}

	private String toValue(Object o, FieldDef def, TraitDAO dao) {
		if (o == null) return "";
		if (!def.enumeration().isEmpty()) {
			if (Collection.class.isAssignableFrom(o.getClass())) {
				List<String> values = new ArrayList<>();
				for (Object lv : (Collection<?>)o) {
					if (lv != null) values.add(enumValue(lv, def, dao));
				}
				return values.toString();
			}
			return enumValue(o, def, dao);
		}

		if (o instanceof DateValue) {
			return ((DateValue)o).toIsoString();
		}
		if (o instanceof Date) {
			return DateUtils.format((Date)o, YYYY_MM_DD);
		}
		return o.toString();
	}

	private String enumValue(Object o, FieldDef def, TraitDAO dao) {
		return dao.getEnumeration(new Qname(def.enumeration())).get(new Qname(o.toString())).getName();
	}

	private static void toTSV(StringBuilder line, String s) {
		s = s.replace("\t", " ").replace("\r", " ").replace("\n", " ");
		while (s.contains("  ")) {
			s = s.replace("  ", " ");
		}
		s = s.trim();
		line.append(s).append("\t");
	}

}

