package fi.luomus.lajitietokeskus.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.lajitietokeskus.dao.DAOImple;
import fi.luomus.lajitietokeskus.models.LajitietokeskusPageResponseData;
import fi.luomus.lajitietokeskus.taxonomy.dao.GBIFTaxonDAO;
import fi.luomus.lajitietokeskus.taxonomy.dao.GBIFTaxonDAOImple;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonomyDAO_Imple;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAOImple;

public abstract class BaseServlet extends fi.luomus.commons.services.BaseServlet {

	private static final long serialVersionUID = -304879218962642512L;

	private static DAOImple dao;
	private static TaxonomyDAO_Imple taxonomyDao;
	private static GBIFTaxonDAOImple gbifTaxonDao;
	private static TraitDAO traitDao;

	private static final Object LOCK = new Object();

	protected GBIFTaxonDAOImple getGBIFTaxonDAO() {
		if (gbifTaxonDao == null) {
			gbifTaxonDao = initGbifTaxonDAO();
		}
		return gbifTaxonDao;
	}

	private static GBIFTaxonDAOImple initGbifTaxonDAO() {
		synchronized (LOCK) {
			if (gbifTaxonDao != null) return gbifTaxonDao;
			try {
				return new GBIFTaxonDAOImple();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	protected TraitDAO getTraitDAO() {
		if (traitDao == null) {
			traitDao = initTraitDAO(getConfig(), getTaxonomyDAO(), getGBIFTaxonDAO(), getErrorReporter());
		}
		return traitDao;
	}

	private static TraitDAO initTraitDAO(Config config, TaxonomyDAO taxonomyDAO, GBIFTaxonDAO gbifTaxonDAO, ErrorReporter errorReporter) {
		synchronized (LOCK) {
			if (traitDao != null) return traitDao;
			try {
				return new TraitDAOImple(config, taxonomyDAO, gbifTaxonDAO, errorReporter);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	protected TaxonomyDAO_Imple getTaxonomyDAO() {
		if (taxonomyDao == null) {
			taxonomyDao = initTaxonomyDAO(getConfig(), getErrorReporter());
		}
		return taxonomyDao;
	}

	private static TaxonomyDAO_Imple initTaxonomyDAO(Config config, ErrorReporter errorReporter) {
		synchronized (LOCK) {
			if (taxonomyDao != null) return taxonomyDao;
			try {
				return new TaxonomyDAO_Imple(config, errorReporter);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	protected DAOImple getDAO() {
		if (dao == null) {
			dao = initDAO(getConfig(), getErrorReporter());
		}
		return dao;
	}

	private static DAOImple initDAO(Config config, ErrorReporter errorReporter) {
		synchronized (LOCK) {
			if (dao != null) return dao;
			return new DAOImple(config, errorReporter);
		}
	}

	@Override
	protected String configFileName() {
		return "lajitietokeskus.properties";
	}

	@Override
	protected void applicationInitOnlyOnce() {
	}

	@Override
	protected void applicationInit() {}

	@Override
	protected void applicationDestroy() {
		try { if (taxonomyDao != null) taxonomyDao.close(); } catch (Exception e) { e.printStackTrace(); }
		try { if (traitDao != null) traitDao.close(); } catch (Exception e) { e.printStackTrace(); }
		try { if (gbifTaxonDao != null) gbifTaxonDao.close(); } catch (Exception e) { e.printStackTrace(); }
		try { if (dao != null) dao.close(); } catch (Exception e) { e.printStackTrace(); }
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

	protected LajitietokeskusPageResponseData initData() throws Exception {
		return new LajitietokeskusPageResponseData(getTaxonomyDAO());
	}

	protected static boolean given(String value) {
		return value != null && value.length() > 0;
	}

	protected String getLastPart(HttpServletRequest req) {
		String path = req.getPathInfo();
		if (path == null || path.equals("/")) {
			return "";
		}
		path = path.substring(path.lastIndexOf("/")+1);
		return path;
	}

}
