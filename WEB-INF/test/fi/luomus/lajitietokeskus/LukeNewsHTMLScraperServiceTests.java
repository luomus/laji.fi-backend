package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fi.luomus.lajitietokeskus.dao.LukeNewsHTMLScraperService;
import fi.luomus.lajitietokeskus.models.NewsItem;

public class LukeNewsHTMLScraperServiceTests {

	@Test
	public void parsingNews() throws Exception {
		String html = getContents("luke_news.htm");
		List<NewsItem> news = LukeNewsHTMLScraperService.scrape(html);
		assertEquals(18, news.size());

		NewsItem n1 = news.get(0);
		assertEquals("Kotimaisen taigametsähanhen sulkiva kanta vakaa jo kolmatta vuotta", n1.getTitle());
		assertEquals("luke.fi", n1.getTag());
		assertEquals("luke.fi-23684", n1.getId());
		assertEquals("2023-08-30 00:00:00.0", n1.getPosted().toString());
		assertEquals("https://www.luke.fi/fi/seurannat/metsanhanhen-pesimaaikainen-kannan-seuranta-ja-pesimaalueiden-kartoitus/kotimaisen-taigametsahanhen-sulkiva-kanta-vakaa-jo-kolmatta-vuotta", n1.getExternalUrl().toString());
	}

	private String getContents(String filename) throws Exception {
		return TestUtil.getContents(LukeNewsHTMLScraperServiceTests.class, filename);
	}

}
