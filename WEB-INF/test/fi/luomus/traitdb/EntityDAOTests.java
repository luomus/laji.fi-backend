package fi.luomus.traitdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.TestConfig;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOEntity;
import fi.luomus.traitdb.dao.TraitDAOImple;
import fi.luomus.traitdb.dao.db.OracleDAOImple;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.models.Enumeration;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Subject;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitEnumerationValue;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.models.TraitValue;
import fi.luomus.traitdb.models.UnitOfMeasurement;

public class EntityDAOTests {

	private TraitDAOImple dao;
	private List<Qname> cleanUpGroupIds = new ArrayList<>();
	private List<Qname> cleanUpTraitIds = new ArrayList<>();
	private List<Qname> cleanUpTraitEnumIds = new ArrayList<>();
	private List<Qname> cleanUpDatasetIds = new ArrayList<>();
	private List<Qname> cleanUpRowIds = new ArrayList<>();

	@Before
	public void init() {
		this.dao = new TraitDAOImple(TestConfig.getConfig(), null, null, new ErrorReportingToSystemErr());
	}

	@After
	public void cleanup() {
		if (!cleanUpRowIds.isEmpty()) {
			System.out.println("CLEANUP ROWS " + cleanUpRowIds);
			for (Qname id : cleanUpRowIds) {
				try {
					dao.rows().delete(id);
				} catch (EntityNotFoundException e) {}
				catch (Exception e) { e.printStackTrace(); }
			}
		}
		if (!cleanUpGroupIds.isEmpty()) {
			System.out.println("CLEANUP TRAIT GROUPS " + cleanUpGroupIds);
			for (Qname id : cleanUpGroupIds) {
				try {
					dao.groups().delete(id);
				} catch (EntityNotFoundException e) {}
				catch (Exception e) { e.printStackTrace(); }
			}
		}

		if (!cleanUpTraitIds.isEmpty()) {
			System.out.println("CLEANUP TRAITS " + cleanUpTraitIds);
			for (Qname id : cleanUpTraitIds) {
				try {
					dao.traits().delete(id);
				} catch (EntityNotFoundException e) {}
				catch (Exception e) { e.printStackTrace(); }
			}
		}

		if (!cleanUpTraitEnumIds.isEmpty()) {
			System.out.println("CLEANUP TRAIT ENUMS " + cleanUpTraitEnumIds);
			for (Qname id : cleanUpTraitEnumIds) {
				try {
					dao.traitEnumerations().delete(id);
				} catch (EntityNotFoundException e) {}
				catch (Exception e) { e.printStackTrace(); }
			}
		}

		if (!cleanUpDatasetIds.isEmpty()) {
			System.out.println("CLEANUP DATASETS " + cleanUpDatasetIds);
			for (Qname id : cleanUpDatasetIds) {
				try {
					dao.datasets().delete(id);
				} catch (EntityNotFoundException e) {}
				catch (Exception e) { e.printStackTrace(); }
			}
		}
		dao.close();
	}

	@Test
	public void traitGroup() {
		TraitDAOEntity<TraitGroup> dao = this.dao.groups();

		int countBefore = dao.getAll().size();

		TraitGroup g1 = new TraitGroup();
		g1.setName("groupname");

		TraitGroup g2 = new TraitGroup();
		g2.setName("groupname");
		g2.setDescription("desc2");

		assertEquals("{description=Required field}", dao.validate(g1, true).getErrors().toString());
		assertEquals("{}", dao.validate(g2, true).getErrors().toString());

		assertEquals("{id=Required field, description=Required field}", dao.validate(g1, false).getErrors().toString());
		assertEquals("{id=Required field}", dao.validate(g2, false).getErrors().toString());

		g1.setDescription("desc1");

		g1 = dao.insert(g1);
		assertTrue(g1.getId().toString().startsWith("TDF."));
		cleanUpGroupIds.add(g1.getId());

		assertEquals(1, dao.getAll().size()-countBefore);

		assertEquals("{name=Group by this name already exists}", dao.validate(g2, true).getErrors().toString());

		g1.setName("group1");
		g1 = dao.update(g1);
		assertEquals("group1", g1.getName());

		assertEquals("{}", dao.validate(g2, true).getErrors().toString());
		g2 = dao.insert(g2);
		cleanUpGroupIds.add(g2.getId());

		assertEquals(2, dao.getAll().size()-countBefore);

		Map<String, List<String>> searchParams = new HashMap<>();

		searchParams.put("name", Utils.singleEntryList(Utils.generateGUID()));
		Collection<TraitGroup> res = dao.search(searchParams, Integer.MAX_VALUE);
		assertEquals(0, res.size());

		searchParams.put("name", Utils.singleEntryList("group1"));
		res = dao.search(searchParams, Integer.MAX_VALUE);
		assertEquals(1, res.size());
		assertEquals(g1.getId(), res.iterator().next().getId());

		searchParams.put("description", Utils.list("desc1", "foobar"));
		res = dao.search(searchParams, Integer.MAX_VALUE);
		assertEquals(1, res.size());
		assertEquals(g1.getId(), res.iterator().next().getId());

		searchParams.put("description", Utils.singleEntryList(Utils.generateGUID()));
		assertEquals(0, dao.search(searchParams, Integer.MAX_VALUE).size());

		dao.delete(g1.getId());
		try {
			dao.get(g1.getId());
			fail("should have been deleted");
		} catch (EntityNotFoundException e) {
			assertEquals("Not found: " + g1.getId().toString(), e.getMessage());
		}

		searchParams.put("name", Utils.singleEntryList("group1"));
		assertEquals(0, dao.search(searchParams, Integer.MAX_VALUE).size());

		assertEquals(g2, dao.get(g2.getId()));

		assertEquals("{id=Not found: FOOBAR}", dao.validateDelete(new Qname("FOOBAR")).getErrors().toString());
		assertEquals("{}", dao.validateDelete(g2.getId()).getErrors().toString());
	}

	@Test
	public void unitOfMeasurement() {
		TraitDAOEntity<UnitOfMeasurement> dao = this.dao.units();
		String actual = dao.getAll().stream().map(u->u.toString()).collect(Collectors.joining("\n"));
		String expected = "" +
				"UnitOfMeasurement [id=TDF.umNM, unit=nm, base=TDF.umM, conversionFactor=1.0E-9, isBaseUnit=false, order=1]\n" +
				"UnitOfMeasurement [id=TDF.umUM, unit=μm, base=TDF.umM, conversionFactor=1.0E-6, isBaseUnit=false, order=2]\n" +
				"UnitOfMeasurement [id=TDF.umMM, unit=mm, base=TDF.umM, conversionFactor=0.001, isBaseUnit=false, order=3]\n" +
				"UnitOfMeasurement [id=TDF.umCM, unit=cm, base=TDF.umM, conversionFactor=0.01, isBaseUnit=false, order=4]\n" +
				"UnitOfMeasurement [id=TDF.umM, unit=m, base=TDF.umM, conversionFactor=1.0, isBaseUnit=true, order=5]\n" +
				"UnitOfMeasurement [id=TDF.umKM, unit=km, base=TDF.umM, conversionFactor=1000.0, isBaseUnit=false, order=6]\n" +
				"UnitOfMeasurement [id=TDF.umG, unit=g, base=TDF.umG, conversionFactor=1.0, isBaseUnit=true, order=10]\n" +
				"UnitOfMeasurement [id=TDF.umKG, unit=kg, base=TDF.umG, conversionFactor=1000.0, isBaseUnit=false, order=11]\n" +
				"UnitOfMeasurement [id=TDF.umA, unit=years, base=TDF.umA, conversionFactor=1.0, isBaseUnit=true, order=20]\n" +
				"UnitOfMeasurement [id=TDF.umS, unit=s, base=TDF.umS, conversionFactor=1.0, isBaseUnit=true, order=21]\n" +
				"UnitOfMeasurement [id=TDF.umML, unit=mL, base=TDF.umL, conversionFactor=0.001, isBaseUnit=false, order=30]\n" +
				"UnitOfMeasurement [id=TDF.umL, unit=L, base=TDF.umL, conversionFactor=1.0, isBaseUnit=true, order=31]\n" +
				"UnitOfMeasurement [id=TDF.umMOL, unit=mol, base=TDF.umMOL, conversionFactor=1.0, isBaseUnit=true, order=40]\n" +
				"UnitOfMeasurement [id=TDF.umMMOL, unit=mmol, base=TDF.umMOL, conversionFactor=0.001, isBaseUnit=false, order=41]\n" +
				"UnitOfMeasurement [id=TDF.umUMOL, unit=μmol, base=TDF.umMOL, conversionFactor=1.0E-6, isBaseUnit=false, order=42]\n" +
				"UnitOfMeasurement [id=TDF.umHZ, unit=Hz, base=TDF.umHZ, conversionFactor=1.0, isBaseUnit=true, order=50]\n" +
				"UnitOfMeasurement [id=TDF.umP, unit=%, base=TDF.umP, conversionFactor=1.0, isBaseUnit=true, order=60]\n" +
				"UnitOfMeasurement [id=TDF.umPPT, unit=‰, base=TDF.umP, conversionFactor=0.1, isBaseUnit=false, order=61]\n" +
				"UnitOfMeasurement [id=TDF.umPPM, unit=ppm, base=TDF.umPPM, conversionFactor=1.0, isBaseUnit=true, order=62]\n" +
				"UnitOfMeasurement [id=TDF.umC, unit=°C, base=TDF.umC, conversionFactor=1.0, isBaseUnit=true, order=70]\n" +
				"UnitOfMeasurement [id=TDF.umMM2, unit=mm², base=TDF.umM2, conversionFactor=1.0E-6, isBaseUnit=false, order=80]\n" +
				"UnitOfMeasurement [id=TDF.umCM2, unit=cm², base=TDF.umM2, conversionFactor=0.001, isBaseUnit=false, order=81]\n" +
				"UnitOfMeasurement [id=TDF.umM2, unit=m², base=TDF.umM2, conversionFactor=1.0, isBaseUnit=true, order=82]\n" +
				"UnitOfMeasurement [id=TDF.umARE, unit=are, base=TDF.umM2, conversionFactor=100.0, isBaseUnit=false, order=83]\n" +
				"UnitOfMeasurement [id=TDF.umHA, unit=ha, base=TDF.umM2, conversionFactor=10000.0, isBaseUnit=false, order=84]\n" +
				"UnitOfMeasurement [id=TDF.umKM2, unit=km², base=TDF.umM2, conversionFactor=1000000.0, isBaseUnit=false, order=85]\n" +
				"UnitOfMeasurement [id=TDF.umMM3, unit=mm³, base=TDF.umM3, conversionFactor=1.0E-9, isBaseUnit=false, order=90]\n" +
				"UnitOfMeasurement [id=TDF.umCM3, unit=cm³, base=TDF.umM3, conversionFactor=1.0E-6, isBaseUnit=false, order=91]\n" +
				"UnitOfMeasurement [id=TDF.umM3, unit=m³, base=TDF.umM3, conversionFactor=1.0, isBaseUnit=true, order=93]\n" +
				"UnitOfMeasurement [id=TDF.umGMOL, unit=g/mol, base=TDF.umGMOL, conversionFactor=1.0, isBaseUnit=true, order=100]\n" +
				"UnitOfMeasurement [id=TDF.umMOLL, unit=mol/L, base=TDF.umMOLL, conversionFactor=1.0, isBaseUnit=true, order=101]\n" +
				"UnitOfMeasurement [id=TDF.umCM2H, unit=cm²/h, base=TDF.umCM2H, conversionFactor=1.0, isBaseUnit=true, order=102]";
		assertEquals(expected, actual);
	}

	@Test
	public void enums() {
		Enumeration statMethods = dao.getEnumeration(new Qname("TDF.statisticalMethodEnum"));

		String actual = statMethods.getAll().stream().map(e->e.toString()).collect(Collectors.joining("\n"));
		String expected = "" +
				"EnumerationValue [id=TDF.statisticalMethodMin, name=Minimum]\n" +
				"EnumerationValue [id=TDF.statisticalMethodMax, name=Maximum]\n" +
				"EnumerationValue [id=TDF.statisticalMethodAvg, name=Average]\n" +
				"EnumerationValue [id=TDF.statisticalMethodMedian, name=Median]\n" +
				"EnumerationValue [id=TDF.statisticalMethodSD, name=Standard Deviation]\n" +
				"EnumerationValue [id=TDF.statisticalMethodMode, name=Mode]";
		assertEquals(expected, actual);
	}

	@Test
	public void unit_of_measurement_enum() {
		Enumeration unitsOfM = dao.getEnumeration(new Qname("TDF.unitOfMeasurementEnum"));
		String actual = unitsOfM.get(new Qname("TDF.umL")).toString();
		assertEquals("EnumerationValue [id=TDF.umL, name=L]", actual);
	}

	@Test
	public void traitEnums() {
		TraitDAOEntity<TraitEnumerationValue> dao = this.dao.traitEnumerations();

		TraitEnumerationValue ev1 = new TraitEnumerationValue();
		ev1.setDataEntryName("A");
		ev1.setName("Animal Eater");
		ev1.setDescription("desc");
		ev1.setOrder(1);

		TraitEnumerationValue ev2 = new TraitEnumerationValue();
		ev2.setDataEntryName("P");
		ev2.setName("Plant Eater");
		ev2.setDescription("desc");
		ev2.setOrder(2);

		dao.insert(Utils.list(ev1, ev2));
		cleanUpTraitEnumIds.add(ev1.getId());
		cleanUpTraitEnumIds.add(ev2.getId());
		assertNotNull(ev1.getId());
		assertNotNull(ev2.getId());

		assertEquals(ev1, dao.get(ev1.getId()));
		assertEquals(ev2, dao.get(ev2.getId()));
	}

	@Test
	public void traits() throws Exception {
		TraitDAOEntity<Trait> dao = this.dao.traits();

		String random = Utils.generateGUID();
		int countBefore = dao.getAll().size();

		boolean insert = true;
		boolean update = false;

		Trait t1 = new Trait();
		assertEquals("{group=Required field, dataEntryName=Required field, name=Required field, description=Required field, range=Required field}", dao.validate(t1, insert).getErrors().toString());

		t1.setDataEntryName("scientific_name");
		t1.setName("scientificName");
		t1.setGroup(new Qname("nonexistinggroup"+random));
		t1.setDescription("desc");
		t1.setRange(new Qname("nonexistingrange"+random));
		t1.setReference("ref");
		t1.getIdentifiers().add("id1");
		t1.getIdentifiers().add("id2");

		assertEquals("{range=Unknown value nonexistingrange"+random+"}", dao.validate(t1, insert).getErrors().toString());

		t1.setRange(new Qname("xsd:string"));
		assertEquals("{group=Unknown group nonexistinggroup"+random+", dataEntryName=This is a reserved term and can not be used as a trait name, name=This is a reserved term and can not be used as a trait name}", dao.validate(t1, insert).getErrors().toString());

		TraitGroup group = new TraitGroup();
		group.setName(random);
		group.setDescription("desc");
		Qname groupId = this.dao.groups().insert(group).getId();
		cleanUpGroupIds.add(groupId);

		t1.setGroup(groupId);

		t1.setDataEntryName("wing_length" + random);
		t1.setName("Wing length" + random);

		assertEquals("{}", dao.validate(t1, insert).getErrors().toString());

		dao.insert(t1);
		cleanUpTraitIds.add(t1.getId());
		assertNotNull(t1.getId());
		assertEquals(countBefore+1, dao.getAll().size());
		assertEquals(t1.getName(), dao.get(t1.getId()).getName());

		t1.setDescription("updated desc");
		assertEquals("{}", dao.validate(t1, update).getErrors().toString());
		dao.update(t1);

		assertEquals(countBefore+1, dao.getAll().size());
		assertEquals("updated desc", dao.get(t1.getId()).getDescription());

		Trait t2 = new Trait(t1.toJSON());
		t2.setId(null);
		t2.setDataEntryName(t1.getDataEntryName().toUpperCase());

		assertEquals("{name=Trait by this name already exists, dataEntryName=Trait by this short name already exists}", dao.validate(t2, insert).getErrors().toString());

		t2.setDataEntryName("what_eater"+random);
		t2.setName("What does this eat"+random);
		t2.setExampleValues("Pizza, Banana");

		assertEquals("{}", dao.validate(t2, insert).getErrors().toString());

		TraitEnumerationValue ev1 = new TraitEnumerationValue();
		ev1.setDataEntryName("A");
		ev1.setName("Animal Eater");
		ev1.setDescription("desc");

		TraitEnumerationValue ev2 = new TraitEnumerationValue();
		ev2.setDataEntryName("P");
		ev2.setName("Plant Eater");
		ev2.setDescription("desc");

		t2.getEnumerations().add(ev2);
		t2.getEnumerations().add(ev1);

		assertEquals("{}", dao.validate(t2, insert).getErrors().toString());

		TraitEnumerationValue ev3 = new TraitEnumerationValue();
		ev3.setDataEntryName("P");
		ev3.setName("Pasta Eater");
		ev3.setDescription("desc");

		t2.getEnumerations().add(ev3);
		assertEquals("{enumerations [3] dataEntryName=Enumeration by this short name already exists}", dao.validate(t2, insert).getErrors().toString());

		t2.getEnumerations().remove(2);

		dao.insert(t2);
		cleanUpTraitIds.add(t2.getId());
		t2.getEnumerations().stream().map(e->e.getId()).forEach(cleanUpTraitEnumIds::add);

		assertNotNull(t2.getId());
		assertEquals("P,A", t2.getEnumerations().stream().map(e->e.getDataEntryName()).collect(Collectors.joining(",")));

		ev3.setDataEntryName("PASTA");
		t2.getEnumerations().add(ev3);

		assertEquals("{}", dao.validate(t2, update).getErrors().toString());

		dao.update(t2);
		assertEquals("P,A,PASTA", dao.get(t2.getId()).getEnumerations().stream().map(e->e.getDataEntryName()).collect(Collectors.joining(",")));

		t2.getEnumerations().clear();
		t2.getEnumerations().add(ev3);
		t2.getEnumerations().add(ev1);

		dao.update(t2);
		assertEquals("PASTA,A", dao.get(t2.getId()).getEnumerations().stream().map(e->e.getDataEntryName()).collect(Collectors.joining(",")));

		assertEquals("{id=Not found: FOOBAR}", dao.validateDelete(new Qname("FOOBAR")).getErrors().toString());
		assertEquals("{}", dao.validateDelete(t2.getId()).getErrors().toString());
	}

	@Test
	public void datasets() {
		TraitDAOEntity<Dataset> dao = this.dao.datasets();
		String random = Utils.generateGUID();
		boolean insert = true;
		boolean update = false;

		Dataset d1 = createDataset(random);

		assertEquals("{}", dao.validate(d1, insert).getErrors().toString());

		Dataset expected1 = new Dataset(d1.toJSON());
		assertEquals(""+
				"Dataset [id=null, name=test"+random+", description=desc, citation=cite me, intellectualOwner=myorg, personResponsible=me, contactEmail=me@me.me, institutionCode=null, methods=methd, taxonomicCoverage=taxcov, temporalCoverage=tempcov, geographicCoverage=geocov, coverageBasis=basis, finbifDOI=null, gbifDOI=null, additionalIdentifiers=null, published=false, shareToFinBIF=false, shareToGBIF=false]",
				expected1.toString());

		dao.insert(d1);
		cleanUpDatasetIds.add(d1.getId());

		// make original match what we get from db
		expected1.setId(d1.getId());
		expected1.getAdditionalIdentifiers();

		assertEquals(expected1.toString(), d1.toString());

		d1 = dao.get(d1.getId());
		assertEquals(expected1.toString(), d1.toString());

		d1.setPublished(true);
		d1.setShareToFinBIF(true);
		d1.setShareToGBIF(true);

		assertEquals("{}", dao.validate(d1, update).getErrors().toString());
		dao.update(d1);
		d1 = dao.get(d1.getId());

		assertEquals(true, d1.isPublished());
		assertEquals(true, d1.isShareToFinBIF());
		assertEquals(true, d1.isShareToGBIF());

		Dataset d2 = new Dataset(d1.toJSON());
		d2.setId(null);
		d2.setDescription("desc for 2");

		assertEquals("{name=Dataset by this name already exists}", dao.validate(d2, insert).getErrors().toString());
		d2.setName("test2"+random);
		assertEquals("{}", dao.validate(d2, insert).getErrors().toString());

		dao.insert(d2);
		cleanUpDatasetIds.add(d2.getId());

		TraitDAOEntity<DatasetPermissions> permissionDao = this.dao.permissions();

		DatasetPermissions p1 = new DatasetPermissions();
		p1.setDatasetId(new Qname("FOOBAR"));
		p1.getUserIds().add("u1"+random);
		p1.getUserIds().add("u1"+random);
		p1.getUserIds().add("u2"+random);

		assertEquals("{datasetId=Unknown dataset FOOBAR}", permissionDao.validate(p1, insert).getErrors().toString());
		p1.setDatasetId(d1.getId());
		assertEquals("{}", permissionDao.validate(p1, insert).getErrors().toString());

		permissionDao.update(p1);

		p1 = permissionDao.get(d1.getId());
		assertEquals("u1"+random+",u2"+random, p1.getUserIds().stream().collect(Collectors.joining(",")));

		p1.getUserIds().removeIf(id->id.equals("u2"+random));
		p1.getUserIds().add("u3"+random);

		permissionDao.update(p1);

		p1 = permissionDao.get(d1.getId());
		assertEquals("u1"+random+",u3"+random, p1.getUserIds().stream().collect(Collectors.joining(",")));

		DatasetPermissions p2 = new DatasetPermissions();
		p2.setDatasetId(d2.getId());
		p2.getUserIds().add("u1"+random);
		p2.getUserIds().add("u2"+random);
		p2.getUserIds().add("u3"+random);

		permissionDao.update(p2);

		Collection<DatasetPermissions> all = permissionDao.getAll();

		Set<Qname> expectedUser1 = new TreeSet<>(Utils.set(d1.getId(), d2.getId()));
		Set<Qname> expectedUser2 = new TreeSet<>(Utils.set(d2.getId()));
		Set<Qname> expectedUser3 = new TreeSet<>(Utils.set(d1.getId(), d2.getId()));

		Set<Qname> actualUser1 = new TreeSet<>();
		Set<Qname> actualUser2 = new TreeSet<>();
		Set<Qname> actualUser3 = new TreeSet<>();
		for (DatasetPermissions p : all) {
			if (p.getUserIds().contains("u1"+random)) actualUser1.add(p.getDatasetId());
			if (p.getUserIds().contains("u2"+random)) actualUser2.add(p.getDatasetId());
			if (p.getUserIds().contains("u3"+random)) actualUser3.add(p.getDatasetId());
		}
		assertEquals(expectedUser1, actualUser1);
		assertEquals(expectedUser2, actualUser2);
		assertEquals(expectedUser3, actualUser3);

		assertEquals("{id=Not found: FOOBAR}", dao.validateDelete(new Qname("FOOBAR")).getErrors().toString());
		assertEquals("{}", dao.validateDelete(d1.getId()).getErrors().toString());
	}

	private Dataset createDataset(String random) {
		Dataset d1 = new Dataset();
		d1.setName("test"+random);
		d1.setDescription("desc");
		d1.setCitation("cite me");
		d1.setIntellectualOwner("myorg");
		d1.setPersonResponsible("me");
		d1.setContactEmail("me@me.me");
		d1.setMethods("methd");
		d1.setTaxonomicCoverage("taxcov");
		d1.setTemporalCoverage("tempcov");
		d1.setGeographicCoverage("geocov");
		d1.setCoverageBasis("basis");
		return d1;
	}

	@Test
	public void traitData() {
		String random = Utils.generateGUID();
		boolean insert = true;
		boolean update = false;

		Dataset dataset = createDataset(random);
		dao.datasets().insert(dataset);
		cleanUpDatasetIds.add(dataset.getId());

		TraitGroup group = new TraitGroup();
		group.setName("testgroup"+random);
		group.setDescription("desc");
		dao.groups().insert(group);
		cleanUpGroupIds.add(group.getId());

		Trait numberTrait = createNumberTrait(random, group);
		Trait taxonTrait = createTaxonTrait(random, group);
		Trait enumTrait = createEnumTrait(random, group);

		dao.traits().insert(numberTrait);
		dao.traits().insert(taxonTrait);
		dao.traits().insert(enumTrait);
		cleanUpTraitIds.add(numberTrait.getId());
		cleanUpTraitIds.add(taxonTrait.getId());
		cleanUpTraitIds.add(enumTrait.getId());
		enumTrait.getEnumerations().stream().map(e->e.getId()).forEach(cleanUpTraitEnumIds::add);

		Subject subject = new Subject();
		subject.setDatasetId(new Qname("FOOBAR"));

		InputRow row = new InputRow(subject);

		assertEquals(""+
				"{type=Required field, createdBy=Required field, datasetId=Unknown dataset FOOBAR, traits=Must have at least one trait}",
				dao.rows().validate(row, insert).getErrors().toString());

		subject.setDatasetId(dataset.getId());
		subject.setType(new Qname("FOOBAR"));
		subject.setCreatedBy(EntityDAOTests.class.getName());

		subject.setLifeStage(new Qname("FOOBAR"));
		assertEquals(""+
				"{type=Unknown value FOOBAR, lifeStage=Unknown value FOOBAR, traits=Must have at least one trait}",
				dao.rows().validate(row, insert).getErrors().toString());

		subject.setType(new Qname("TDF.typeIndividual"));
		subject.setLifeStage(new Qname("MY.lifeStageAdult"));

		assertEquals("{traits=Must have at least one trait}",
				dao.rows().validate(row, insert).getErrors().toString());

		assertEquals("{id=Required field, traits=Must have at least one trait. If you want to delete all traits, delete the entire row.}",
				dao.rows().validate(row, update).getErrors().toString());

		TraitValue numberValue = new TraitValue();
		TraitValue taxonValue = new TraitValue();
		TraitValue enumValue = new TraitValue();

		row.addTrait(numberValue);
		assertEquals("{traits[0]traitId=Required field, traits[0]value=Required field}", dao.rows().validate(row, insert).getErrors().toString());

		numberValue.setTraitId(new Qname("FOOBAR"));
		numberValue.setValue("a");
		assertEquals("{traits[0].traitId=Unknown trait: FOOBAR}", dao.rows().validate(row, insert).getErrors().toString());

		numberValue.setTraitId(numberTrait.getId());
		assertEquals("{traits[0]unit=Unit of measurement is required for the selected trait, traits[0]value=Value for this trait should be a decimal number}", dao.rows().validate(row, insert).getErrors().toString());

		numberValue.setValue("-1,3");
		numberValue.setUnit(new Qname("TDF.umCM"));
		assertEquals("{traits[0]unit=The unit of measurement you entered is not compatible with the specified trait}", dao.rows().validate(row, insert).getErrors().toString());

		numberValue.setUnit(new Qname("TDF.umKG"));
		assertEquals("{}", dao.rows().validate(row, insert).getErrors().toString());

		numberTrait.setRange(new Qname("xsd:positiveInteger"));
		dao.traits().update(numberTrait);
		assertEquals("{traits[0]value=Value for this trait should be an integer}", dao.rows().validate(row, insert).getErrors().toString());

		numberValue.setValue("0");
		assertEquals("{traits[0]value=Value for this trait should be a positive integer}", dao.rows().validate(row, insert).getErrors().toString());

		numberValue.setValue("5225");
		numberValue.setMeasurementAccuracy(-5.0);
		numberValue.setStatisticalMethod(new Qname("TDF.statisticalMethodAvg"));
		numberValue.setObjectTaxonLifeStage(new Qname("MY.lifeStageAdult"));
		assertEquals("" +
				"{traits[0]measurementAccuracy=Must be creater than zero, traits[0]objectTaxonLifeStage=Only available for taxon traits}",
				dao.rows().validate(row, insert).getErrors().toString());

		numberValue.setMeasurementAccuracy(5.0);
		numberValue.setObjectTaxonLifeStage(null);
		assertEquals("{}", dao.rows().validate(row, insert).getErrors().toString());

		row.addTrait(taxonValue);
		taxonValue.setTraitId(taxonTrait.getId());
		taxonValue.setValue("5");
		taxonValue.setUnit(new Qname("TDF.umKG"));
		taxonValue.setMeasurementAccuracy(1.0);
		taxonValue.setStatisticalMethod(new Qname("TDF.statisticalMethodAvg"));
		assertEquals(""+
				"{traits[1]measurementAccuracy=Only available for numeric traits, traits[1]statisticalMethod=Only available for numeric traits, traits[1]unit=Only available for numeric traits}",
				dao.rows().validate(row, insert).getErrors().toString());

		taxonValue.setUnit(null);
		taxonValue.setMeasurementAccuracy(null);
		taxonValue.setStatisticalMethod(null);

		taxonValue.setObjectTaxonLifeStage(new Qname("foobar"));
		assertEquals("{traits[1]objectTaxonLifeStage=Unknown value foobar}", dao.rows().validate(row, insert).getErrors().toString());

		taxonValue.setObjectTaxonLifeStage(new Qname("MY.lifeStageAdult"));
		assertEquals("{}", dao.rows().validate(row, insert).getErrors().toString());

		row.addTrait(enumValue);
		enumValue.setTraitId(enumTrait.getId());
		enumValue.setValue("foobar");
		enumValue.setWarnings(true);
		assertEquals("{traits[2]value=Must be one of A, B}", dao.rows().validate(row, insert).getErrors().toString());
		assertEquals("[{traits[2]value=Must be one of A, B}]", dao.rows().validate(Utils.list(row)).toString());

		enumValue.setValue("a");
		assertEquals("{}", dao.rows().validate(row, insert).getErrors().toString());

		assertEquals("{id=Required field}", dao.rows().validate(row, update).getErrors().toString());

		try {
			dao.rows().insert(row);
		} finally {
			cleanUpRowIds.add(row.getSubject().getId());
		}

		InputRow stored = dao.rows().get(row.getSubject().getId());
		assertEquals(row.toString(), stored.toString());

		assertNotNull(row.getSubject().getId());
		for (TraitValue v : row.getTraits()) {
			assertNotNull(v.getId());
		}

		assertEquals("{}", dao.rows().validate(row, update).getErrors().toString());
		dao.rows().update(row);

		stored = dao.rows().get(row.getSubject().getId());
		assertEquals(row.toString(), stored.toString());

		TraitValue newValue = new TraitValue();
		newValue.setTraitId(numberTrait.getId());
		newValue.setValue("1");
		newValue.setStatisticalMethod(new Qname("TDF.statisticalMethodSD"));
		newValue.setUnit(new Qname("TDF.umKG"));
		row.addTrait(newValue);

		assertEquals("{}", dao.rows().validate(row, update).getErrors().toString());

		dao.rows().update(row);
		for (TraitValue v : row.getTraits()) {
			assertNotNull(v.getId());
		}

		stored = dao.rows().get(row.getSubject().getId());
		assertEquals(row.toString(), stored.toString());

		assertEquals("{id=Dataset is used. Can not delete before all data has been removed.}", dao.datasets().validateDelete(dataset.getId()).getErrors().toString());
		assertEquals("{id=Trait is used. Can not delete.}", dao.traits().validateDelete(numberTrait.getId()).getErrors().toString());
		assertEquals("{id=Trait group has traits. Can not delete before all traits have been removed.}", dao.groups().validateDelete(group.getId()).getErrors().toString());


		InputRow multiRow1 = new InputRow(row.toJSON());
		InputRow multiRow2 = new InputRow(row.toJSON());
		clearIds(multiRow1);
		clearIds(multiRow2);

		List<InputRow> multi = Utils.list(multiRow1, multiRow2);

		multiRow2.getSubject().setLat(60000.0);
		assertEquals("[{}, {lat=Latitude must be between -90 and 90 degrees}]", dao.rows().validate(multi).toString());

		multiRow2.getSubject().setLat(60.0);
		multiRow2.getSubject().setLon(30.0);
		multiRow2.getSubject().setElevation(100);
		multiRow2.getSubject().setDateBegin(new DateValue(13, 6, 2000));

		assertEquals("[{}, {}]", dao.rows().validate(multi).toString());

		dao.rows().insert(multi);
		cleanUpRowIds.add(multiRow1.getSubject().getId());
		cleanUpRowIds.add(multiRow2.getSubject().getId());

		Map<String, List<String>> searchParams = new HashMap<>();
		searchParams.put("subject.datasetId", Utils.singleEntryList(dataset.getId().toString()));
		Collection<InputRow> allStored = dao.rows().search(searchParams, Integer.MAX_VALUE);
		assertEquals(3, allStored.size());
		for (InputRow r : allStored) {
			assertEquals(4, r.getTraits().size());
		}

		searchParams.put("traits.statisticalMethod", Utils.list("TDF.statisticalMethodSD", "foobar"));
		Collection<InputRow> filtered = dao.rows().search(searchParams, Integer.MAX_VALUE);
		assertEquals(3, filtered.size());
		for (InputRow r : filtered) {
			assertEquals(1, r.getTraits().size());
		}

		searchParams.put("subject.lat", Utils.singleEntryList("60,0"));
		searchParams.put("subject.lon", Utils.singleEntryList("30"));
		searchParams.put("subject.created", Utils.singleEntryList(DateUtils.format(row.getSubject().getCreated(), "yyyy-MM-dd")));
		searchParams.put("subject.dateBegin", Utils.singleEntryList("2000-06-13"));
		searchParams.put("subject.elevation", Utils.singleEntryList("100"));
		filtered = dao.rows().search(searchParams, Integer.MAX_VALUE);
		assertEquals(1, filtered.size());
		for (InputRow r : filtered) {
			assertEquals(1, r.getTraits().size());
		}
	}

	private void clearIds(InputRow row) {
		row.getSubject().setId(null);
		row.getSubject().setCreated(null);
		for (TraitValue v : row.getTraits()) {
			v.setId(null);
		}
	}

	private Trait createNumberTrait(String random, TraitGroup group) {
		Trait numberTrait = new Trait();
		numberTrait.setGroup(group.getId());
		numberTrait.setDataEntryName("ntrait"+random);
		numberTrait.setName("ntrait"+random);
		numberTrait.setDescription("desc");
		numberTrait.setBaseUnit(new Qname("TDF.umG"));
		numberTrait.setRange(new Qname("xsd:decimal"));
		return numberTrait;
	}

	private Trait createTaxonTrait(String random, TraitGroup group) {
		Trait taxonTrait = new Trait();
		taxonTrait.setGroup(group.getId());
		taxonTrait.setDataEntryName("txtrait"+random);
		taxonTrait.setName("txtrait"+random);
		taxonTrait.setDescription("desc");
		taxonTrait.setRange(new Qname("MX.taxon"));
		return taxonTrait;
	}

	private Trait createEnumTrait(String random, TraitGroup group) {
		Trait enumTrait = new Trait();
		enumTrait.setGroup(group.getId());
		enumTrait.setDataEntryName("etrait"+random);
		enumTrait.setName("etrait"+random);
		enumTrait.setDescription("desc");
		enumTrait.setRange(new Qname("xsd:string"));
		TraitEnumerationValue e1 = new TraitEnumerationValue();
		e1.setDataEntryName("A");
		e1.setName("A");
		e1.setDescription("desc");
		TraitEnumerationValue e2 = new TraitEnumerationValue();
		e2.setDataEntryName("B");
		e2.setName("B");
		e2.setDescription("desc");
		enumTrait.getEnumerations().add(e1);
		enumTrait.getEnumerations().add(e2);
		return enumTrait;
	}

	@Test
	public void sqlGeneration() {
		assertEquals("" +
				"INSERT INTO subject ( age_years,author,basis_of_record,coordinate_accuracy,country,created_by,dataset_id,date_begin,date_end,elevation,finbif_occurrence_id,functional_group_name,gbif_occurrence_id,gbif_taxon_id,habitat,higher_geography,individual_count,kingdom,lat,lat_max,lat_min,life_stage,locality,location_identifiers,lon,lon_max,lon_min,measurement_determined_by,measurement_determined_date,modified,modified_by,municipality,occurrence_remarks,other_occurrence_id,other_taxon_id,scientific_name,season_begin,season_end,sex,taxon_id,type,year_begin,year_end ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ) ",
				OracleDAOImple.SUBJECT_INSERT_SQL);

		assertEquals("" +
				"INSERT INTO traitvalue ( measurement_accuracy,measurement_remarks,object_taxon_life_stage,reference,statistical_method,trait_id,unit,value,warnings,subject_id ) VALUES ( ?,?,?,?,?,?,?,?,?,? ) ",
				OracleDAOImple.TRAIT_VALUE_INSERT_SQL);

		assertEquals("" +
				"UPDATE subject SET age_years=?, author=?, basis_of_record=?, coordinate_accuracy=?, country=?, date_begin=?, date_end=?, elevation=?, finbif_occurrence_id=?, functional_group_name=?, gbif_occurrence_id=?, gbif_taxon_id=?, habitat=?, higher_geography=?, individual_count=?, kingdom=?, lat=?, lat_max=?, lat_min=?, life_stage=?, locality=?, location_identifiers=?, lon=?, lon_max=?, lon_min=?, measurement_determined_by=?, measurement_determined_date=?, modified=?, modified_by=?, municipality=?, occurrence_remarks=?, other_occurrence_id=?, other_taxon_id=?, scientific_name=?, season_begin=?, season_end=?, sex=?, taxon_id=?, type=?, year_begin=?, year_end=? WHERE id = ?",
				OracleDAOImple.SUBJECT_UPDATE_SQL);

		assertEquals("" +
				"UPDATE traitvalue SET measurement_accuracy=?, measurement_remarks=?, object_taxon_life_stage=?, reference=?, statistical_method=?, trait_id=?, unit=?, value=?, warnings=? WHERE id = ?",
				OracleDAOImple.TRAIT_VALUE_UPDATE_SQL);

		Map<String, List<String>> params = new LinkedHashMap<>();
		params.put("foo", Utils.singleEntryList("bar"));
		params.put("subject.datasetId", Utils.singleEntryList("HR.123"));
		params.put("traits.foo", Utils.singleEntryList("bar"));
		params.put("traits.value", Utils.list("a", "b"));
		try {
			OracleDAOImple.searchSQL(params, 1);
			fail();
		} catch (IllegalArgumentException ex) {
			assertEquals("Invalid search parameter foo", ex.getMessage());
		}
		params.remove("foo");
		try {
			OracleDAOImple.searchSQL(params, 1);
			fail();
		} catch (IllegalArgumentException ex) {
			assertEquals("Invalid search parameter traits.foo", ex.getMessage());
		}
		params.remove("traits.foo");
		params.put("subject.created", Utils.singleEntryList("asdas"));
		assertEquals("" +
				"SELECT subject.age_years,subject.author,subject.basis_of_record,subject.coordinate_accuracy,subject.country,subject.created,subject.created_by,subject.dataset_id,subject.date_begin,subject.date_end,subject.elevation,subject.finbif_occurrence_id,subject.functional_group_name,subject.gbif_occurrence_id,subject.gbif_taxon_id,subject.habitat,subject.higher_geography,subject.id,subject.individual_count,subject.kingdom,subject.lat,subject.lat_max,subject.lat_min,subject.life_stage,subject.locality,subject.location_identifiers,subject.lon,subject.lon_max,subject.lon_min,subject.measurement_determined_by,subject.measurement_determined_date,subject.modified,subject.modified_by,subject.municipality,subject.occurrence_remarks,subject.other_occurrence_id,subject.other_taxon_id,subject.scientific_name,subject.season_begin,subject.season_end,subject.sex,subject.taxon_id,subject.type,subject.year_begin,subject.year_end,traits.id,traits.measurement_accuracy,traits.measurement_remarks,traits.object_taxon_life_stage,traits.reference,traits.statistical_method,traits.trait_id,traits.unit,traits.value,traits.warnings FROM subject JOIN traitvalue traits ON subject.id = traits.subject_id WHERE 1=1 AND subject.dataset_id IN(?) AND traits.value IN(?,?) AND TRUNC(subject.created) IN(?) ORDER BY subject.id, traits.id FETCH FIRST 1 ROWS ONLY",
				OracleDAOImple.searchSQL(params, 1));
	}

}
