package fi.luomus.traitdb.models;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

@ClassDef(triplestorePrefix="TDF")
public class Trait extends AbstractBaseEntity implements Comparable<Trait> {

	@FieldDef(order=1, etsOverride="traitID", required=true)
	private Qname id;

	@FieldDef(order=2, ignoreInSearch=true, required=true)
	private Qname group;

	@FieldDef(order=3, length=50, etsOverride="verbatimTraitName", required=true)
	private String dataEntryName;

	@FieldDef(order=4, length=200, etsOverride="traitName", required=true)
	private String name;

	@FieldDef(order=5, length=2000, etsOverride="traitDescription", required=true)
	private String description;

	@FieldDef(order=6, ignoreInSearch=true)
	private String exampleValues;

	@FieldDef(order=7, enumeration="TDF.unitOfMeasurementEnum", etsOverride="expectedUnit")
	private Qname baseUnit;

	@FieldDef(order=8, enumeration="TDF.rangeEnum", etsOverride="valueType", required=true)
	private Qname range;

	@FieldDef(order=9, ignoreInSearch=true)
	private List<TraitEnumerationValue> enumerations;

	@FieldDef(order=10, length=2000, etsOverride="source")
	private String reference;

	@FieldDef(order=11, length=200, etsOverride="identifier")
	private List<String> identifiers;

	public Trait() {
		super();
	}

	public Trait(JSONObject json) throws ParseException {
		super(json);
	}

	public Qname getId() {
		return id;
	}

	public Qname getGroup() {
		return group;
	}

	public String getDataEntryName() {
		return dataEntryName;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getExampleValues() {
		return exampleValues;
	}

	public Qname getBaseUnit() {
		return baseUnit;
	}

	public Qname getRange() {
		return range;
	}

	public List<TraitEnumerationValue> getEnumerations() {
		if (enumerations == null) enumerations = new ArrayList<>();
		return enumerations;
	}

	public String getReference() {
		return reference;
	}

	public List<String> getIdentifiers() {
		if (identifiers == null) identifiers = new ArrayList<>();
		return identifiers;
	}

	public void setId(Qname id) {
		this.id = id;
	}

	public void setGroup(Qname group) {
		this.group = group;
	}

	public void setDataEntryName(String dataEntryName) {
		this.dataEntryName = dataEntryName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setExampleValues(String exampleValues) {
		this.exampleValues = exampleValues;
	}

	public void setBaseUnit(Qname baseUnit) {
		this.baseUnit = baseUnit;
	}

	public void setRange(Qname range) {
		this.range = range;
	}

	public void setEnumerations(List<TraitEnumerationValue> enumerations) {
		this.enumerations = enumerations;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setIdentifiers(List<String> identifiers) {
		this.identifiers = identifiers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((baseUnit == null) ? 0 : baseUnit.hashCode());
		result = prime * result + ((dataEntryName == null) ? 0 : dataEntryName.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((enumerations == null) ? 0 : enumerations.hashCode());
		result = prime * result + ((exampleValues == null) ? 0 : exampleValues.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((identifiers == null) ? 0 : identifiers.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((range == null) ? 0 : range.hashCode());
		result = prime * result + ((reference == null) ? 0 : reference.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trait other = (Trait) obj;
		if (baseUnit == null) {
			if (other.baseUnit != null)
				return false;
		} else if (!baseUnit.equals(other.baseUnit))
			return false;
		if (dataEntryName == null) {
			if (other.dataEntryName != null)
				return false;
		} else if (!dataEntryName.equals(other.dataEntryName))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (enumerations == null) {
			if (other.enumerations != null)
				return false;
		} else if (!enumerations.equals(other.enumerations))
			return false;
		if (exampleValues == null) {
			if (other.exampleValues != null)
				return false;
		} else if (!exampleValues.equals(other.exampleValues))
			return false;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (identifiers == null) {
			if (other.identifiers != null)
				return false;
		} else if (!identifiers.equals(other.identifiers))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (range == null) {
			if (other.range != null)
				return false;
		} else if (!range.equals(other.range))
			return false;
		if (reference == null) {
			if (other.reference != null)
				return false;
		} else if (!reference.equals(other.reference))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Trait [id=" + id + ", group=" + group + ", dataEntryName=" + dataEntryName + ", name=" + name + ", description=" + description + ", exampleValues=" + exampleValues
				+ ", baseUnit=" + baseUnit + ", range=" + range + ", enumerations=" + enumerations + ", reference=" + reference + ", identifiers=" + identifiers + "]";
	}

	@Override
	public int compareTo(Trait other) {
		int c = this.getGroup().compareTo(other.getGroup());
		if (c != 0) return c;
		return this.getName().compareTo(other.getName());
	}

}
