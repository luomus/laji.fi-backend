package fi.luomus.lajitietokeskus.taxonomy.dao;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.Content;
import fi.luomus.commons.containers.Content.Context;
import fi.luomus.commons.containers.ContentContextDescription;
import fi.luomus.commons.containers.ContentGroups;
import fi.luomus.commons.containers.ContentGroups.ContentGroup;
import fi.luomus.commons.containers.ContentGroups.ContentVariable;
import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.LocalizedTexts;
import fi.luomus.commons.containers.LocalizedURL;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfProperties;
import fi.luomus.commons.containers.rdf.RdfProperty;
import fi.luomus.commons.containers.rdf.RdfPropertyRange;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.taxonomy.BoldRecords;
import fi.luomus.commons.taxonomy.HabitatOccurrenceCounts;
import fi.luomus.commons.taxonomy.HabitatOccurrenceCounts.HabitatOccurrenceCount;
import fi.luomus.commons.taxonomy.Occurrences;
import fi.luomus.commons.taxonomy.Occurrences.Occurrence;
import fi.luomus.commons.taxonomy.PublicInformation;
import fi.luomus.commons.taxonomy.RedListStatus;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.commons.taxonomy.iucn.EndangermentObject;
import fi.luomus.commons.taxonomy.iucn.Evaluation;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;

public class TaxonToMap {

	private static final Qname SPECIES = new Qname("MX.species");
	private static final Qname SUBGENUS = new Qname("MX.subgenus");
	private static final Qname AGGREGATE = new Qname("MX.aggregate");
	private static final Qname SUBTRIBE = new Qname("MX.subtribe");
	private static final Qname SUBFAMILY = new Qname("MX.subfamily");
	private static final Qname SUPERFAMILY = new Qname("MX.superfamily");
	private static final Qname SUBORDER = new Qname("MX.suborder");
	private static final Qname SUBCLASS = new Qname("MX.subclass");
	private static final Qname GENUS = new Qname("MX.genus");
	private static final Qname TRIBE = new Qname("MX.tribe");
	private static final Qname FAMILY = new Qname("MX.family");
	private static final Qname ORDER = new Qname("MX.order");
	private static final Qname CLASS = new Qname("MX.class");
	private static final Qname DIVISION = new Qname("MX.division");
	private static final Qname SUBPHYLUM = new Qname("MX.subphylum");
	private static final Qname PHYLUM = new Qname("MX.phylum");
	private static final Qname KINGDOM = new Qname("MX.kingdom");
	private static final Qname DOMAIN = new Qname("MX.domain");

	public static final List<Qname> HIGHER_RANKS = Utils.list(
			DOMAIN,
			KINGDOM,
			PHYLUM,
			SUBPHYLUM,
			DIVISION,
			CLASS,
			SUBCLASS,
			ORDER,
			SUBORDER,
			SUPERFAMILY,
			FAMILY,
			SUBFAMILY,
			TRIBE,
			SUBTRIBE,
			GENUS);

	public static final String TAXON_RANK_PREFIX = "MX.";

	private static final List<Method> TAXON_GETTERS = new ArrayList<>();
	static {
		for (Method method : Taxon.class.getMethods()) {
			PublicInformation annotation = method.getAnnotation(PublicInformation.class);
			if (annotation == null) continue;
			TAXON_GETTERS.add(method);
		}
		Collections.sort(TAXON_GETTERS, new Comparator<Method>() {
			@Override
			public int compare(Method m1, Method m2) {
				PublicInformation a1 = m1.getAnnotation(PublicInformation.class);
				PublicInformation a2 = m2.getAnnotation(PublicInformation.class);
				return Double.valueOf(a1.order()).compareTo(a2.order());
			}
		});
	}

	private static final String SPECIES_CARD_AUTHORS = "speciesCardAuthors";

	private static final Map<String, String> TAXON_FIELD_NAME_OVERRIDES;
	static {
		TAXON_FIELD_NAME_OVERRIDES = new HashMap<>();
		TAXON_FIELD_NAME_OVERRIDES.put("parentQname", "isPartOf");
		TAXON_FIELD_NAME_OVERRIDES.put("nonHiddenParentQname", "isPartOfNonHidden");
		TAXON_FIELD_NAME_OVERRIDES.put("checklist", "nameAccordingTo");
		TAXON_FIELD_NAME_OVERRIDES.put("alternativeVernacularNames", "alternativeVernacularName");
		TAXON_FIELD_NAME_OVERRIDES.put("obsoleteVernacularNames", "obsoleteVernacularName");
		TAXON_FIELD_NAME_OVERRIDES.put("colloquialVernacularNames", "colloquialVernacularName");
		TAXON_FIELD_NAME_OVERRIDES.put("tradeNames", "tradeName");
		TAXON_FIELD_NAME_OVERRIDES.put("hidden", "hiddenTaxon");
		TAXON_FIELD_NAME_OVERRIDES.put("experts", "taxonExpert");
		TAXON_FIELD_NAME_OVERRIDES.put("editors", "taxonEditor");
		TAXON_FIELD_NAME_OVERRIDES.put("primary", null);
		TAXON_FIELD_NAME_OVERRIDES.put("parentChain", "parents");
		TAXON_FIELD_NAME_OVERRIDES.put("nonHiddenParentChain", "nonHiddenParents");
		TAXON_FIELD_NAME_OVERRIDES.put("parentChainIncludeSelf", "parentsIncludeSelf");
		TAXON_FIELD_NAME_OVERRIDES.put("nonHiddenParentChainIncludeSelf", "nonHiddenParentsIncludeSelf");
		TAXON_FIELD_NAME_OVERRIDES.put("typesOfOccurrenceInFinland", "typeOfOccurrenceInFinland");
	}

	private interface Mapper {
		Object map(Object value, TaxonToMap taxonMapper) throws Exception;
		Map<String, Object> getMapping(TaxonToMap taxonMapper);
	}

	private final TaxonomyDAO dao;
	private final RdfProperties evaluationProperties;
	private final Map<String, Area> areas;

	public TaxonToMap(TaxonomyDAO dao, RdfProperties evaluationProperties, Map<String, Area> areas) {
		this.dao = dao;
		this.evaluationProperties = evaluationProperties;
		this.areas = areas;
	}

	public Map<String, Object> map(Taxon taxon) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		for (Method getter : TAXON_GETTERS) {
			addToMap(taxon, map, getter, this);
		}
		map.put("taxonomicOrder", taxon.getTaxonomicOrder());
		Map<String, Object> higherTaxa =  mapHigherTaxa(taxon);
		if (!higherTaxa.isEmpty()) {
			map.put("parent", higherTaxa);
		}

		Taxon synonymParent = taxon.getSynonymParent();
		if (synonymParent !=  null) {
			map.put("synonymOf", SIMPLE_TAXON_MAPPER.map(synonymParent, this));
		}

		addEvaluations(map, taxon);
		addHabitatSearchStrings(map, taxon.getPrimaryHabitat(), taxon.getSecondaryHabitats(), this.evaluationProperties);

		if (!map.containsKey("scientificName")) {
			map.put("scientificName", "");
		}

		return map;
	}

	private void addEvaluations(Map<String, Object> map, Taxon taxon) throws Exception {
		Evaluation latest = taxon.getLatestRedListEvaluation();
		if (latest != null) {
			map.put("latestRedListEvaluation", EVALUATION_MAPPER.map(latest, this));
		}
		map.put("hasLatestRedListEvaluation", latest != null);
	}

	private static void addHabitatSearchStrings(Map<String, Object> map, HabitatObject primaryHabitat, List<HabitatObject> secondaryHabitats, RdfProperties evaluationProperties) {
		List<HabitatObject> habitats = Utils.list(primaryHabitat);
		Set<String> primaryHabitatSearchStrings = generateHabitatSearchStrings(habitats, evaluationProperties);
		if (!primaryHabitatSearchStrings.isEmpty()) {
			map.put("primaryHabitatSearchStrings", primaryHabitatSearchStrings);
		}
		habitats.addAll(secondaryHabitats);
		Set<String> anyHabitatSearchStrings = generateHabitatSearchStrings(habitats, evaluationProperties);
		if (!anyHabitatSearchStrings.isEmpty()) {
			map.put("anyHabitatSearchStrings", anyHabitatSearchStrings);
		}
	}

	private static Set<String> generateHabitatSearchStrings(List<HabitatObject> habitats, RdfProperties evaluationProperties) {
		Set<String> combinations = new TreeSet<>(habitatComparator());
		for (HabitatObject h : habitats) {
			addHabitatSearchStrings(combinations, h, evaluationProperties);
		}
		return combinations;
	}

	private static Comparator<String> habitatComparator() {
		return new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				boolean c1 = o1.contains("[");
				boolean c2 = o2.contains("[");
				if (!c1 && !c2) return o1.compareTo(o2);
				if (c1 && !c2) return 1;
				if (!c1 && c2) return -1;

				String h1 = o1.substring(0, o1.indexOf("["));
				String h2 = o2.substring(0, o2.indexOf("["));
				int c = h1.compareTo(h2);
				if (c != 0) return c;

				int s1 = Utils.countNumberOf(",", o1);
				int s2 = Utils.countNumberOf(",", o2);
				c = s1-s2;
				if (c != 0) return c;

				return o1.compareTo(o2);
			}
		};
	}

	private static void addHabitatSearchStrings(Set<String> combinations, HabitatObject h, RdfProperties evaluationProperties) {
		List<String> habitatAndParentHabitats = getHabitatAndParentHabitats(h, evaluationProperties);
		if (habitatAndParentHabitats.isEmpty()) return;

		combinations.addAll(habitatAndParentHabitats);

		if (h.getHabitatSpecificTypes().isEmpty()) return;

		List<String> specifierCombinations = getSpecifierCombinations(h);
		for (String habitat : habitatAndParentHabitats) {
			for (String specifierCombination : specifierCombinations) {
				combinations.add(habitat+specifierCombination);
			}
		}
	}

	private static List<String> getSpecifierCombinations(HabitatObject h) {
		// https://theproductiveprogrammer.blog/GeneratingCombinations.java.php
		List<String> combinations = new ArrayList<>();
		int n = h.getHabitatSpecificTypes().size();
		for (int i = 0; i < (1 << n); i++) {
			List<String> combination = new ArrayList<>();
			for (int k = 0; k < n; k++) {
				// (is the bit "on" in this number?)
				if ((i & (1 << k)) != 0) {
					// then it's included in the list
					combination.add(h.getHabitatSpecificTypes().get(k).toString());
				}
			}
			if (combination.size() > 1) {
				for (List<String> permutation : permute(combination)) {
					combinations.add(permutation.toString().replace(" ", ""));
				}
			} else {
				combinations.add(combination.toString().replace(" ", ""));
			}
		}
		return combinations;
	}

	private static List<List<String>> permute(List<String> combination) {
		// https://theproductiveprogrammer.blog/GeneratingPermutations.java.php
		List<List<String>> res = new ArrayList<>();
		heaps_algorithm(combination.toArray(new String[combination.size()]), combination.size(), res);
		return res;
	}

	private static void heaps_algorithm(String[] a, int n, List<List<String>> res) {
		if (n == 1) {
			res.add(Lists.newArrayList(a));
			return;
		}
		for (int i = 0; i < (n - 1); i++) {
			heaps_algorithm(a, n-1, res);
			if (n % 2 == 0) swap(a, n-1, i);
			else swap(a, n-1, 0);
		}
		heaps_algorithm(a, n-1, res);
	}

	private static void swap(String[] a, int i1, int i2) {
		String tmp = a[i1];
		a[i1] = a[i2];
		a[i2] = tmp;
	}

	private static List<String> getHabitatAndParentHabitats(HabitatObject h, RdfProperties evaluationProperties) {
		if (h == null) return Collections.emptyList();
		List<String> habitats = new ArrayList<>();
		String habitat = h.getHabitat().toString();
		habitats.add(habitat);
		Collection<RdfProperty> range = evaluationProperties.getProperty(Evaluation.HABITAT).getRange().getValues();
		RdfProperty p = getRangeValue(habitat, range);
		if (p == null) return habitats;
		while (p.getAltParent() != null) {
			habitat = p.getAltParent().toString();
			p = getRangeValue(habitat, range);
			if (p == null) return habitats;
			habitats.add(habitat);
		}
		return habitats;
	}

	private static RdfProperty getRangeValue(String value, Collection<RdfProperty> range) {
		for (RdfProperty p : range) {
			if (p.getQname().toString().equals(value)) return p;
		}
		return null;
	}

	private List<RdfProperty> includedEvaluationProperties = null;

	private List<RdfProperty> getIncludedEvaluationProperties() {
		if (includedEvaluationProperties == null) {
			includedEvaluationProperties = initIncludedEvaluationProperties();
		}
		return includedEvaluationProperties;
	}

	private List<RdfProperty> initIncludedEvaluationProperties() {
		List<RdfProperty> props = new ArrayList<>();
		props.add(evaluationProperties.getProperty(Evaluation.EVALUATION_YEAR));
		props.add(evaluationProperties.getProperty(Evaluation.RED_LIST_STATUS));
		props.add(evaluationProperties.getProperty(Evaluation.EXTERNAL_IMPACT));
		props.add(evaluationProperties.getProperty(Evaluation.CRITERIA_FOR_STATUS));
		props.add(evaluationProperties.getProperty(Evaluation.POSSIBLY_RE));
		props.add(evaluationProperties.getProperty(Evaluation.REASON_FOR_STATUS_CHANGE));
		props.add(evaluationProperties.getProperty(Evaluation.LAST_SIGHTING_NOTES));
		return props;
	}

	private Map<String, Object> mapHigherTaxa(Taxon taxon) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		for (Qname rank : HIGHER_RANKS) {
			addHigherTaxa(rank, taxon, map);
		}
		addHigherTaxa(SUBGENUS, taxon, map);
		addHigherTaxa(AGGREGATE, taxon, map);
		addHigherTaxa(SPECIES, taxon, map);
		return map;
	}

	private void addHigherTaxa(Qname rank, Taxon taxon, Map<String, Object> map) throws Exception {
		Taxon higher = taxon.getParentOfRank(rank);
		if (higher == null) return;
		map.put(rank.toString().replace(TAXON_RANK_PREFIX, ""), SIMPLE_TAXON_MAPPER.map(higher, this));
	}

	private static void addToMap(Object object, Map<String, Object> map, Method getter, TaxonToMap taxonMapper) throws Exception {
		Object value = getter.invoke(object);
		if (value == null) return;
		Object mappedValue = getMapper(value).map(value, taxonMapper);
		if (mappedValue != null) {
			String fieldName = ReflectionUtil.cleanGetterFieldName(getter);
			if (TAXON_FIELD_NAME_OVERRIDES.containsKey(fieldName)) {
				fieldName = TAXON_FIELD_NAME_OVERRIDES.get(fieldName);
				if (fieldName == null) return;
			}
			map.put(fieldName, mappedValue);
		}
	}

	private static Mapper getMapper(Class<?> type) {
		Mapper mapper = MAPPERS.get(type);
		if (mapper != null) return mapper;
		if (Collection.class.isAssignableFrom(type)) return COLLECTION_MAPPER;
		throw new UnsupportedOperationException("No mapper for " + type);
	}

	private static Mapper getMapper(Object value) {
		return getMapper(value.getClass());
	}

	private static final Map<Class<?>, Mapper> MAPPERS = new HashMap<>();

	private static final Mapper VALUE_AS_IS_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) {
			return value;
		}

		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			throw new UnsupportedOperationException("Not implemented for value as is mapper");
		}
	};

	private static final Mapper TO_STRING_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) {
			return value.toString();
		}

		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			return mapping(String.class);
		}
	};

	private static final Mapper TO_MAP_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) throws Exception {
			Map<String, Object> map = new LinkedHashMap<>();
			List<Method> getters = new ArrayList<>();
			for (Method getter : ReflectionUtil.getGetters(value.getClass())) {
				if (getter.getParameterCount() == 0) {
					getters.add(getter);
				}
			}
			Collections.sort(getters, new Comparator<Method>() {
				@Override
				public int compare(Method o1, Method o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			for (Method getter : getters) {
				TaxonToMap.addToMap(value, map, getter, taxonMapper);
			}
			return map;
		}

		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			throw new UnsupportedOperationException("Not implemented for to map mapper");
		}
	};

	private static final Mapper SIMPLE_TAXON_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) throws Exception {
			Map<String, Object> map = new LinkedHashMap<>();
			Taxon taxon = (Taxon) value;
			map.put("id", taxon.getId().toString());
			if (taxon.getScientificName() != null) map.put("scientificName", taxon.getScientificName());
			if (taxon.getScientificNameAuthorship() != null) map.put("scientificNameAuthorship", taxon.getScientificNameAuthorship());
			if (!taxon.getVernacularName().isEmpty()) map.put("vernacularName", TaxonToMap.map(taxon.getVernacularName()));
			if (taxon.getTaxonRank() != null) map.put("taxonRank", taxon.getTaxonRank().toString());
			map.put("cursiveName", taxon.isCursiveName());
			if (taxon.getNotes() != null) map.put("notes", taxon.getNotes());
			if (taxon.getBold() != null) {
				map.put("bold", TO_MAP_MAPPER.map(taxon.getBold(), taxonMapper));

			}
			map.put("hasBold", taxon.hasBold());
			return map;
		}

		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			Map<String, Object> mapping = new LinkedHashMap<>();
			mapping.put("id", mapping(String.class));
			mapping.put("scientificName", mapping(String.class));
			mapping.put("scientificNameAuthorship", mapping(String.class));
			mapping.put("vernacularName", ref(LocalizedText.class));
			mapping.put("taxonRank", mapping(String.class));
			mapping.put("cursiveName", mapping(boolean.class));
			mapping.put("notes", mapping(String.class));
			mapping.put("bold", ref(BoldRecords.class));
			mapping.put("hasBold", mapping(boolean.class));
			return mapping;
		}
	};

	private static final Mapper COLLECTION_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) throws Exception {
			Collection<?> collection = (Collection<?>) value;
			if (collection.isEmpty()) return null;
			List<Object> mappedValues = new ArrayList<>();
			for (Object collectionValue : collection) {
				if (collectionValue == null) continue;
				Object mappedCollectionValue = getMapper(collectionValue).map(collectionValue, taxonMapper);
				if (mappedCollectionValue != null) {
					mappedValues.add(mappedCollectionValue);
				}
			}
			return mappedValues;
		}

		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			throw new UnsupportedOperationException("Not implemented for collection mapper");
		}
	};

	private static final Mapper CONTENT_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) throws Exception {
			Content content = (Content) value;
			if (content.isEmpty()) return null;
			List<Object> mappedContent = new ArrayList<>();
			for (Context context : content.getContexts()) {
				Object mapped = CONTEXT_MAPPER.map(context, taxonMapper);
				if (mapped != null) {
					mappedContent.add(mapped);
				}
			}
			return mappedContent;
		}

		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			return arrayRef(Context.class);
		}
	};

	private static final Mapper CONTEXT_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) throws Exception {
			Context context = (Context) value;
			ContentGroups contentGroups = taxonMapper.dao.getContentGroups();
			Map<String, ContentContextDescription> contextDescriptions = taxonMapper.dao.getContentContextDescriptions();
			Map<String, Object> mappedContext = new LinkedHashMap<>();
			mappedContext.put("id", context.getId().toString());
			ContentContextDescription contextDescription = contextDescriptions.get(context.getId().toString());
			if (contextDescription != null) {
				mappedContext.put("title", TaxonToMap.map(contextDescription.getName()));
			}
			List<Object> groupsOfContext = new ArrayList<>();
			mappedContext.put("groups", groupsOfContext);
			Map<String, Object> speciesCardAuthors = new LinkedHashMap<>();
			for (ContentGroup group : contentGroups) {
				List<Object> mappedVariables = new ArrayList<>();
				for (ContentVariable variable : group) {
					Qname variableId = variable.getField();
					Map<String, Object> mappedVariable = new LinkedHashMap<>();
					for (String locale : context.getLocalesWithContent()) {
						if (!given(locale)) continue;
						String text = context.getText(variableId, locale);
						if (given(text)) {
							mappedVariable.put(locale, taxonMapper.replaceTaxonLinks(text));
						}
					}
					if (!mappedVariable.isEmpty()) {
						String cleanedVariableName = variableId.toString().replace(TAXON_RANK_PREFIX, "");
						Map<String, Object> variableOrAuthors = null;
						if (cleanedVariableName.equals(SPECIES_CARD_AUTHORS)) {
							variableOrAuthors = speciesCardAuthors;
						} else {
							variableOrAuthors = new LinkedHashMap<>();
							mappedVariables.add(variableOrAuthors);
						}
						variableOrAuthors.put("variable", variableId.toString());
						variableOrAuthors.put("title", TaxonToMap.map(variable.getTitle()));
						variableOrAuthors.put("content", mappedVariable);
					}
				}
				if (!mappedVariables.isEmpty()) {
					Map<String, Object> mappedGroup = new LinkedHashMap<>();
					mappedGroup.put("group", group.getId().toString());
					mappedGroup.put("title", TaxonToMap.map(group.getTitle()));
					mappedGroup.put("variables", mappedVariables);
					groupsOfContext.add(mappedGroup);
				}
			}
			if (!speciesCardAuthors.isEmpty()) {
				mappedContext.put(SPECIES_CARD_AUTHORS, speciesCardAuthors);
			}
			if (mappedContext.isEmpty()) {
				return null;
			}
			return mappedContext;
		}
		private boolean given(String text) {
			return text != null && text.length() > 0;
		}
		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			Map<String, Object> mapping = new LinkedHashMap<>();
			mapping.put("id", mapping(String.class));
			mapping.put("title", ref(LocalizedText.class));
			mapping.put("groups", groupsMapping());
			mapping.put(SPECIES_CARD_AUTHORS, variableContent());
			return mapping;
		}
		private Map<String, Object> variableContent() {
			Map<String, Object> mapping = new LinkedHashMap<>();
			mapping.put("variable", mapping(String.class));
			mapping.put("title", ref(LocalizedText.class));
			mapping.put("content", ref(LocalizedText.class));
			return mapping;
		}
		private Map<String, Object> groupsMapping() {
			Map<String, Object> mapping = new LinkedHashMap<>();
			mapping.put("type", "array");
			mapping.put("item-schema", groupMapping());
			return mapping;
		}
		private Map<String, Object> groupMapping() {
			Map<String, Object> mapping = new LinkedHashMap<>();
			mapping.put("group", mapping(String.class));
			mapping.put("title", ref(LocalizedText.class));
			mapping.put("variables", variablesMapping());
			return mapping;
		}
		private Map<String, Object> variablesMapping() {
			Map<String, Object> mapping = new LinkedHashMap<>();
			mapping.put("type", "array");
			mapping.put("item-schema", variableMapping());
			return mapping;
		}
		private Map<String, Object> variableMapping() {
			Map<String, Object> mapping = new LinkedHashMap<>();
			mapping.put("variable", mapping(String.class));
			mapping.put("title", ref(LocalizedText.class));
			mapping.put("content", ref(LocalizedText.class));
			return mapping;
		}
	};

	public String replaceTaxonLinks(String text) throws Exception {
		if (text == null) return null;
		if (!text.contains("[MX.")) return text;
		StringBuilder b = new StringBuilder();
		String currentTagStart = "";
		boolean inTag = false;
		String taxonId = "MX.";
		for (char c : text.toCharArray()) {
			if (inTag) {
				if (c == ']') {
					addTaxonLink(b, taxonId);
					taxonId = "MX.";
					inTag = false;
				} else {
					taxonId += c;
				}
				continue;
			}
			if (c == '[' || c == 'M' || c == 'X' || c == '.') {
				currentTagStart += c;
				if (currentTagStart.equals("[MX.")) {
					inTag = true;
					currentTagStart = "";
					continue;
				}
			} else {
				b.append(currentTagStart);
				currentTagStart = "";
				b.append(c);
			}
		}
		return b.toString().trim();
	}

	private void addTaxonLink(StringBuilder b, String taxonId) throws Exception {
		if (validTaxonId(taxonId)) {
			String taxonInfo = taxonInfo(taxonId);
			b.append("<a href=\"").append(new Qname(taxonId).toURI()).append("\" target=\"_blank\">").append(taxonInfo).append("</a>");
		} else {
			b.append("[").append(taxonId).append("]");
		}
	}

	private String taxonInfo(String taxonId) throws Exception {
		Taxon taxon = getTaxon(new Qname(taxonId));
		if (taxon == null) return taxonId;
		if (given(taxon.getScientificName())) {
			if (taxon.isCursiveName()) {
				return "<em>" + taxon.getScientificName() + "</em>";
			}
			return taxon.getScientificName();
		}
		if (taxon.getVernacularName().hasTextForLocale("en")) {
			// virus
			return taxon.getVernacularName().forLocale("en");
		}
		if (taxon.getVernacularName().hasTextForLocale("fi")) {
			// some strange population group etc
			return taxon.getVernacularName().forLocale("fi");
		}
		return taxon.getId().toURI();
	}

	private Taxon getTaxon(Qname taxonId) throws Exception {
		if (!dao.getTaxonContainer().hasTaxon(taxonId)) return null;
		return dao.getTaxon(taxonId);
	}

	private static final Set<Character> NUMBERS = Utils.set('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

	private boolean validTaxonId(String taxonId) {
		if (taxonId == null) return false;
		if (!taxonId.trim().toUpperCase().equals(taxonId)) return false;
		if (!taxonId.startsWith("MX.")) return false;
		if (taxonId.equals("MX.")) return false;
		for (char c : taxonId.substring("MX.".length()).toCharArray()) {
			if (!NUMBERS.contains(c)) return false;
		}
		return true;
	}

	private static final Object map(LocalizedText localizedText) throws Exception {
		return LOCALIZED_TEXT_MAPPER.map(localizedText, null);
	}

	private static final Mapper LOCALIZED_TEXT_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) {
			LocalizedText localizedText = (LocalizedText) value;
			if (localizedText.isEmpty()) return null;
			Map<String, Object> texts = new LinkedHashMap<>();
			for (Map.Entry<String, String> e : localizedText.getAllTexts().entrySet()) {
				if (!given(e.getKey())) continue;
				texts.put(e.getKey(), e.getValue());
			}
			return texts;
		}

		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			Map<String, Object> mapping = new LinkedHashMap<>();
			mapping.put("fi", mapping(String.class));
			mapping.put("sv", mapping(String.class));
			mapping.put("en", mapping(String.class));
			return mapping;
		}
	};

	private static final Mapper EVALUATION_MAPPER = new Mapper() {
		@Override
		public Object map(Object value, TaxonToMap taxonMapper) throws Exception {
			List<RdfProperty> includedEvaluationProperties = taxonMapper.getIncludedEvaluationProperties();
			Evaluation evaluation = (Evaluation) value;
			Map<String, Object> map = new LinkedHashMap<>();

			properties(includedEvaluationProperties, evaluation, map);
			primaryHabitat(taxonMapper, evaluation, map);
			secondaryHabitats(taxonMapper, evaluation, map);
			addHabitatSearchStrings(map, evaluation.getPrimaryHabitat(), evaluation.getSecondaryHabitats(), taxonMapper.evaluationProperties);
			endangermentReasons(evaluation, map);
			threaths(evaluation, map);
			occurrences(taxonMapper, evaluation, map);
			redListIndex(evaluation, map);

			return map;
		}

		@Override
		public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
			Map<String, Object> mapping = new LinkedHashMap<>();
			for (RdfProperty property : taxonMapper.getIncludedEvaluationProperties()) {
				String propertyName = property.getQname().toString().replace("MKV.", "");
				if (property.isRepeated()) {
					mapping.put(propertyName, arrayMapping(property. getRange()));
				} else {
					mapping.put(propertyName, mapping(property.getRange()));
				}
			}
			mapping.put("primaryHabitat", ref(HabitatObject.class));
			mapping.put("secondaryHabitats", arrayRef(HabitatObject.class));
			mapping.put("primaryHabitatSearchStrings", arrayMapping(String.class));
			mapping.put("anyHabitatSearchStrings", arrayMapping(String.class));
			mapping.put("endangermentReasons", arrayMapping(String.class));
			mapping.put("primaryEndangermentReason", mapping(String.class));
			mapping.put("threats", arrayMapping(String.class));
			mapping.put("primaryThreat", mapping(String.class));
			mapping.put("occurrences", arrayRef(Occurrence.class));
			mapping.put("threatenedAtArea", arrayMapping(String.class));
			mapping.put("calculatedRedListIndex", mapping(Integer.class));
			mapping.put("calculatedCorrectedRedListIndex", mapping(Integer.class));
			mapping.put("correctedStatusForRedListIndex", mapping(String.class));
			return mapping;
		}

		private void redListIndex(Evaluation evaluation, Map<String, Object> map) {
			Integer calculatedRedListIndex = evaluation.getCalculatedRedListIndex();
			Integer calculatedCorrectedRedListIndex = evaluation.getCalculatedCorrectedRedListIndex();
			String correctedStatusForRedListIndex = evaluation.getCorrectedStatusForRedListIndex();
			if (calculatedRedListIndex != null) map.put("calculatedRedListIndex", calculatedRedListIndex);
			if (calculatedCorrectedRedListIndex != null) map.put("calculatedCorrectedRedListIndex", calculatedCorrectedRedListIndex);
			if (correctedStatusForRedListIndex != null) map.put("correctedStatusForRedListIndex", correctedStatusForRedListIndex);
		}

		private void occurrences(TaxonToMap taxonMapper, Evaluation evaluation, Map<String, Object> map) throws Exception {
			List<Qname> threatenedAtArea = new ArrayList<>();
			if (!evaluation.getOccurrences().isEmpty()) {
				List<Object> list = new ArrayList<>();
				List<Occurrence> occurrences = new ArrayList<>(evaluation.getOccurrences());
				taxonMapper.sort(occurrences);
				for (Occurrence o : occurrences) {
					list.add(TO_MAP_MAPPER.map(o, taxonMapper));
					if (Boolean.TRUE.equals(o.getThreatened())) {
						threatenedAtArea.add(o.getArea());
					}
				}
				map.put("occurrences", list);
				map.put("threatenedAtArea", threatenedAtArea);
			}
		}

		private void threaths(Evaluation evaluation, Map<String, Object> map) {
			if (!evaluation.getThreats().isEmpty()) {
				List<Object> list = new ArrayList<>();
				for (EndangermentObject e : evaluation.getThreats()) {
					list.add(e.getEndangerment().toString());
				}
				map.put("threats", list);
				map.put("primaryThreat", evaluation.getThreats().get(0).toString());
			}
		}

		private void endangermentReasons(Evaluation evaluation, Map<String, Object> map) {
			if (!evaluation.getEndangermentReasons().isEmpty()) {
				List<Object> list = new ArrayList<>();
				for (EndangermentObject e : evaluation.getEndangermentReasons()) {
					list.add(e.getEndangerment().toString());
				}
				map.put("endangermentReasons", list);
				map.put("primaryEndangermentReason", evaluation.getEndangermentReasons().get(0).toString());
			}
		}

		private void secondaryHabitats(TaxonToMap taxonMapper, Evaluation evaluation, Map<String, Object> map) throws Exception {
			if (!evaluation.getSecondaryHabitats().isEmpty()) {
				List<Object> list = new ArrayList<>();
				for (HabitatObject h : evaluation.getSecondaryHabitats()) {
					list.add(TO_MAP_MAPPER.map(h, taxonMapper));
				}
				map.put("secondaryHabitats", list);
			}
		}

		private void primaryHabitat(TaxonToMap taxonMapper, Evaluation evaluation, Map<String, Object> map) throws Exception {
			if (evaluation.getPrimaryHabitat() != null) {
				map.put("primaryHabitat", TO_MAP_MAPPER.map(evaluation.getPrimaryHabitat(), taxonMapper));
			}
		}

		private void properties(List<RdfProperty> includedEvaluationProperties, Evaluation evaluation, Map<String, Object> map) {
			for (RdfProperty property : includedEvaluationProperties) {
				String propertyName = property.getQname().toString().replace("MKV.", "");
				if (property.isRepeated()) {
					List<Object> list = new ArrayList<>();
					for (Statement s : evaluation.getModel().getStatements(property.getQname())) {
						Object statementValue = getValue(s, property);
						list.add(statementValue);
					}
					map.put(propertyName, list);
				} else {
					if (evaluation.getModel().hasStatements(property.getQname())) {
						Statement s = evaluation.getModel().getStatements(property.getQname()).get(0);
						Object statementValue = getValue(s, property);
						map.put(propertyName, statementValue);
					}
				}
			}
		}

		private Object getValue(Statement s, RdfProperty p) {
			if (s.isResourceStatement()) return s.getObjectResource().getQname().toString();
			String value = s.getObjectLiteral().getContent();
			if (p.isBooleanProperty()) return Boolean.valueOf(value);
			if (p.isDecimalProperty()) return Double.valueOf(value);
			if (p.isIntegerProperty()) return Integer.valueOf(value);
			return value;
		}
	};

	static {
		MAPPERS.put(String.class, VALUE_AS_IS_MAPPER);
		MAPPERS.put(Boolean.class, VALUE_AS_IS_MAPPER);
		MAPPERS.put(Integer.class, VALUE_AS_IS_MAPPER);
		MAPPERS.put(Qname.class, TO_STRING_MAPPER);
		MAPPERS.put(URL.class, TO_STRING_MAPPER);
		MAPPERS.put(URI.class, TO_STRING_MAPPER);
		MAPPERS.put(Content.class, CONTENT_MAPPER);
		MAPPERS.put(Context.class, CONTEXT_MAPPER);
		MAPPERS.put(LocalizedText.class, LOCALIZED_TEXT_MAPPER);
		MAPPERS.put(RedListStatus.class, TO_MAP_MAPPER);
		MAPPERS.put(LocalizedURL.class, TO_MAP_MAPPER);
		MAPPERS.put(Image.class, TO_MAP_MAPPER);
		MAPPERS.put(HabitatObject.class, TO_MAP_MAPPER);
		MAPPERS.put(BoldRecords.class, TO_MAP_MAPPER);
		MAPPERS.put(Taxon.class, SIMPLE_TAXON_MAPPER);
		MAPPERS.put(Evaluation.class, EVALUATION_MAPPER);

		MAPPERS.put(LocalizedTexts.class, new Mapper() {
			@Override
			public Object map(Object value, TaxonToMap taxonMapper) {
				LocalizedTexts localizedTexts = (LocalizedTexts) value;
				if (localizedTexts.isEmpty()) return null;
				Map<String, Object> texts = new LinkedHashMap<>();
				for (Map.Entry<String, List<String>> e : localizedTexts.getAllTexts().entrySet()) {
					if (!given(e.getKey())) continue;
					texts.put(e.getKey(), e.getValue());
				}
				return texts;
			}

			@Override
			public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
				return arrayRef(LocalizedText.class);
			}
		});

		MAPPERS.put(Occurrences.class, new Mapper() {
			@Override
			public Object map(Object value, TaxonToMap taxonMapper) throws Exception {
				Occurrences occurrences = (Occurrences) value;
				if (!occurrences.hasOccurrences()) return null;
				List<Object> list = new ArrayList<>();
				for (Occurrence occurrence : occurrences) {
					Object mappedValue = TO_MAP_MAPPER.map(occurrence, taxonMapper);
					list.add(mappedValue);
				}
				return list;
			}

			@Override
			public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
				return arrayRef(Occurrence.class);
			}
		});

		MAPPERS.put(HabitatOccurrenceCounts.class, new Mapper() {
			@Override
			public Object map(Object value, TaxonToMap taxonMapper) throws Exception {
				HabitatOccurrenceCounts counts = (HabitatOccurrenceCounts) value;
				if (!counts.hasCounts()) return null;
				List<Object> list = new ArrayList<>();
				for (HabitatOccurrenceCount count : counts) {
					Object mappedValue = TO_MAP_MAPPER.map(count, taxonMapper);
					list.add(mappedValue);
				}
				return list;
			}

			@Override
			public Map<String, Object> getMapping(TaxonToMap taxonMapper) {
				return arrayRef(HabitatOccurrenceCount.class);
			}
		});
	}

	private static boolean given(String s) {
		return s != null && !s.trim().isEmpty();
	}

	private void sort(List<Occurrence> occurrences) {
		Collections.sort(occurrences, new Comparator<Occurrence>() {
			@Override
			public int compare(Occurrence o1, Occurrence o2) {
				Area a1 = o1.getArea() == null ? null : areas.get(o1.getArea().toString());
				Area a2 = o2.getArea() == null ? null : areas.get(o2.getArea().toString());
				if (a1 == null || a2 == null) return 0;
				if (a1.getName() == null || a2.getName() == null) return 0;
				String name1 = a1.getName().forLocale("fi");
				String name2 = a2.getName().forLocale("fi");
				if (name1 == null || name2 == null) return 0;
				return name1.compareTo(name2);
			}
		});

	}

	public static class SimpleTaxon {}

	private static Set<Class<?>> REF_CLASSES = Utils.set(
			SimpleTaxon.class,
			RedListStatus.class,
			LocalizedURL.class,
			Image.class,
			HabitatObject.class,
			BoldRecords.class,
			Occurrence.class,
			HabitatOccurrenceCount.class,
			LocalizedText.class,
			Evaluation.class,
			Content.class,
			Context.class,
			Taxon.class);

	public Map<Class<?>, Map<String, Object>> getMappings() {
		Map<Class<?>, Map<String, Object>> mappings = new LinkedHashMap<>();
		mappings.put(Taxon.class, getTaxonMappings());
		mappings.put(SimpleTaxon.class, SIMPLE_TAXON_MAPPER.getMapping(this));
		mappings.put(LocalizedText.class, LOCALIZED_TEXT_MAPPER.getMapping(this));
		mappings.put(LocalizedURL.class, genericMapping(LocalizedURL.class));
		mappings.put(RedListStatus.class, genericMapping(RedListStatus.class));
		mappings.put(HabitatObject.class, genericMapping(HabitatObject.class));
		mappings.put(Occurrence.class, genericMapping(Occurrence.class));
		mappings.put(HabitatOccurrenceCount.class, genericMapping(HabitatOccurrenceCount.class));
		mappings.put(Image.class, genericMapping(Image.class));
		mappings.put(BoldRecords.class, genericMapping(BoldRecords.class));
		mappings.put(Evaluation.class, EVALUATION_MAPPER.getMapping(this));
		mappings.put(Content.class, CONTENT_MAPPER.getMapping(this));
		mappings.put(Context.class, CONTEXT_MAPPER.getMapping(this));
		return mappings;
	}

	private Map<String, Object> genericMapping(Class<?> type) {
		Map<String, Object> mapping = new LinkedHashMap<>();
		List<Method> getters = new ArrayList<>();
		for (Method getter : ReflectionUtil.getGetters(type)) {
			if (getter.getParameterCount() == 0) {
				getters.add(getter);
			}
		}
		Collections.sort(getters, new Comparator<Method>() {
			@Override
			public int compare(Method o1, Method o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		for (Method getter : getters) {
			addMapping(mapping, getter, false);
		}
		return mapping;
	}

	private Map<String, Object> getTaxonMappings() {
		Map<String, Object> mapping = new LinkedHashMap<>();
		for (Method getter : TAXON_GETTERS) {
			addMapping(mapping, getter, true);
		}
		mapping.put("taxonomicOrder", mapping(long.class));

		Map<String, Object> parents = new LinkedHashMap<>();
		for (Qname rank : HIGHER_RANKS) {
			parents.put(rank.toString().replace(TAXON_RANK_PREFIX, ""), ref(SimpleTaxon.class));
		}
		parents.put(SUBGENUS.toString().replace(TAXON_RANK_PREFIX, ""), ref(SimpleTaxon.class));
		parents.put(AGGREGATE.toString().replace(TAXON_RANK_PREFIX, ""), ref(SimpleTaxon.class));
		parents.put(SPECIES.toString().replace(TAXON_RANK_PREFIX, ""), ref(SimpleTaxon.class));
		mapping.put("parent", parents);

		mapping.put("synonymOf", ref(SimpleTaxon.class));

		mapping.put("latestRedListEvaluation", ref(Evaluation.class));
		mapping.put("hasLatestRedListEvaluation", mapping(boolean.class));

		mapping.put("primaryHabitatSearchStrings", arrayMapping(String.class));
		mapping.put("anyHabitatSearchStrings", arrayMapping(String.class));
		return mapping;
	}

	private void addMapping(Map<String, Object> mapping, Method getter, boolean taxon) {
		String fieldName = ReflectionUtil.cleanGetterFieldName(getter);
		if (taxon) {
			if (TAXON_FIELD_NAME_OVERRIDES.containsKey(fieldName)) {
				fieldName = TAXON_FIELD_NAME_OVERRIDES.get(fieldName);
				if (fieldName == null) return;
			}
		}
		try {
			Class<?> type = getter.getReturnType();
			if (primitive(type)) {
				mapping.put(fieldName, mapping(type));
				return;
			}

			if (REF_CLASSES.contains(type)) {
				mapping.put(fieldName, ref(type));
				return;
			}

			Mapper mapper = MAPPERS.get(type);
			if (mapper != null) {
				mapping.put(fieldName, mapper.getMapping(this));
				return;
			}

			if (Collection.class.isAssignableFrom(type)) {
				Type returnType = getter.getGenericReturnType();
				if (returnType instanceof ParameterizedType) {
					ParameterizedType parameterizedType = (ParameterizedType) returnType;
					Type[] typeArguments = parameterizedType.getActualTypeArguments();
					if (typeArguments.length == 1 && typeArguments[0] instanceof Class) {
						Class<?> collectionParamType = (Class<?>) typeArguments[0];

						if (primitive(collectionParamType)) {
							mapping.put(fieldName, arrayMapping(collectionParamType));
							return;
						}

						if (REF_CLASSES.contains(collectionParamType)) {
							mapping.put(fieldName, arrayRef(collectionParamType));
							return;
						}

						mapper = MAPPERS.get(collectionParamType);
						if (mapper != null) {
							mapping.put(fieldName, mapper.getMapping(this));
							return;
						}
					}
				}
				throw new IllegalStateException("No mapping for collection?");
			}

			mapping.put(fieldName, mapping(type));

		} catch (Exception e) {
			throw new IllegalStateException("Not implemented for " + fieldName, e);
		}
	}

	private boolean primitive(Class<?> type) {
		return type.isPrimitive() || type == Boolean.class || type == Integer.class || type == String.class || type == Qname.class;
	}

	private static Map<String, Object> mapping(Class<?> type) {
		String typeName = type.getSimpleName().toLowerCase();
		if (type == int.class || type == long.class) typeName = "integer";
		if (type == Qname.class) typeName = "string";

		Map<String, Object> mapping = new LinkedHashMap<>();
		mapping.put("type", typeName);
		if (type == Qname.class) {
			mapping.put("desc", "Qname identifier");
		}
		return mapping;
	}

	private static Map<String, Object> arrayMapping(Class<?> arrayType) {
		Map<String, Object> mapping = new LinkedHashMap<>();
		mapping.put("type", "array");
		mapping.put("item-schema", mapping(arrayType));
		return mapping;
	}

	private static Map<String, Object> mapping(RdfPropertyRange range) {
		Map<String, Object> mapping = new LinkedHashMap<>();
		mapping.put("type", range.getQname().toString().toLowerCase().replace("xsd:", "").replace("gyear", "integer"));
		if (range.hasRangeValues()) {
			mapping.put("type", "string");
			mapping.put("enum", range.getValues().stream().map(p->p.getQname().toString()).collect(Collectors.toList()));
			mapping.put("x-enum-origin", range.getQname().toString());
		}
		return mapping;
	}

	private static Map<String, Object> arrayMapping(RdfPropertyRange range) {
		Map<String, Object> mapping = new LinkedHashMap<>();
		mapping.put("type", "array");
		mapping.put("item-schema", mapping(range));
		return mapping;
	}

	private static Map<String, Object> ref(Class<?> referencedClass) {
		Map<String, Object> mapping = new LinkedHashMap<>();
		if (referencedClass == Taxon.class) {
			mapping.put("ref", SimpleTaxon.class.getSimpleName());
		} else {
			mapping.put("ref", referencedClass.getSimpleName());
		}
		return mapping;
	}

	private static Map<String, Object> arrayRef(Class<?> referencedClass) {
		Map<String, Object> mapping = new LinkedHashMap<>();
		mapping.put("type", "array");
		if (referencedClass == Taxon.class) {
			mapping.put("ref", SimpleTaxon.class.getSimpleName());
		} else {
			mapping.put("ref", referencedClass.getSimpleName());
		}
		return mapping;
	}

}
