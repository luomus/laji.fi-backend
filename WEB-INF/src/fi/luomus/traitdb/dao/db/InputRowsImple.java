package fi.luomus.traitdb.dao.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOEntity;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Subject;
import fi.luomus.traitdb.models.TraitValue;

public class InputRowsImple implements TraitDAOEntity<InputRow> {

	private static final int MAX_TRAIT_COUNT = 10000;
	private final TraitDAO dao;
	private final DbDAO db;

	public InputRowsImple(TraitDAO dao, DbDAO db) {
		this.dao = dao;
		this.db = db;
	}

	@Override
	public ValidationResponse validate(InputRow entity, boolean insert) throws UnsupportedOperationException {
		Map<String, String> errors = entity.validate(insert, dao);
		if (entity.getSubject() != null) {
			Qname datasetId = entity.getSubject().getDatasetId();
			if (datasetId != null) {
				try {
					dao.datasets().get(datasetId);
				} catch (EntityNotFoundException e) {
					errors.put("datasetId", "Unknown dataset " + datasetId);
				}
				if (!insert && entity.getSubject().getId() != null) {
					try {
						InputRow existing = dao.rows().get(entity.getSubject().getId());
						if (!existing.getSubject().getDatasetId().equals(entity.getSubject().getDatasetId())) {
							errors.put("datasetId", "Moving data to other dataset is not allowd");
						}
					} catch (EntityNotFoundException e) {
						errors.put("id", "Unknown row " + entity.getSubject().getId());
					}
				}
			}
		}
		if (entity.getTraits() != null) {
			int i = -1;
			for (TraitValue tv : entity.getTraits()) {
				i++;
				Qname traitId = tv.getTraitId();
				if (traitId != null) {
					try {
						dao.traits().get(traitId);
					} catch (EntityNotFoundException e) {
						errors.put("traits["+i+"].traitId", "Unknown trait: " + traitId);
					}
				}
			}
		}
		if (entity.getTraits().isEmpty()) {
			if (insert) {
				errors.put("traits", "Must have at least one trait");
			} else {
				errors.put("traits", "Must have at least one trait. If you want to delete all traits, delete the entire row.");
			}
		}
		if (entity.getTraits().size() > MAX_TRAIT_COUNT) {
			errors.put("traits", "Subject can have maximum of " + MAX_TRAIT_COUNT + " traits");
		}
		return new ValidationResponse(errors);
	}

	@Override
	public List<ValidationResponse> validate(Collection<InputRow> entities) throws UnsupportedOperationException {
		List<ValidationResponse> res = new ArrayList<>(entities.size());
		Set<Qname> datasetIds = new HashSet<>();
		for (InputRow row : entities) {
			res.add(validate(row, true));
			datasetIds.add(row.getSubject().getDatasetId());
		}
		if (datasetIds.size() > 1) {
			res.forEach(vr->vr.getErrors().put("datasetId", "Must only submit data of one dataset at a time"));
		}
		return res;
	}

	@Override
	public InputRow insert(InputRow entity) throws UnsupportedOperationException {
		insert(Collections.singleton(entity));
		return entity;
	}

	@Override
	public void insert(Collection<InputRow> entities) throws UnsupportedOperationException {
		db.insert(entities);
	}

	@Override
	public InputRow update(InputRow entity) throws EntityNotFoundException, UnsupportedOperationException {
		db.update(entity);
		return entity;
	}

	@Override
	public void delete(Qname id) throws EntityNotFoundException, UnsupportedOperationException {
		Subject subject = new Subject();
		subject.setId(id);
		InputRow row = new InputRow(subject);
		db.delete(row);
	}

	@Override
	public Collection<InputRow> getAll() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public InputRow get(Qname id) throws EntityNotFoundException {
		Map<String, List<String>> params = new HashMap<>();
		params.put("subject.id", Utils.singleEntryList(id.toString()));
		List<InputRow> res = db.search(params, MAX_TRAIT_COUNT);
		if (res.isEmpty()) throw new EntityNotFoundException(id);
		return res.get(0);
	}

	@Override
	public Collection<InputRow> search(Map<String, List<String>> searchParams, int limit) throws UnsupportedOperationException {
		return db.search(searchParams, limit);
	}

	@Override
	public ValidationResponse validateDelete(Qname id) throws UnsupportedOperationException {
		Map<String, String> errors = new HashMap<>();
		try {
			get(id);
		} catch (EntityNotFoundException e) {
			errors.put("id", "Not found: " + id);
		}
		return new ValidationResponse(errors);
	}

}
