package fi.luomus.traitdb.models;

import java.util.Date;
import java.util.Map;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.traitdb.dao.TraitDAO;

public class Subject extends AbstractBaseEntity {

	@FieldDef(order=1, required=true, etsOverride="subjectID")
	private Qname id;

	@FieldDef(order=2, ignoreInSearch=true, required=true)
	private Qname datasetId;

	@FieldDef(order=3, enumeration="TDF.typeEnum", required=true)
	private Qname type;

	@FieldDef(order=4.0, enumeration="MY.recordBases")
	private Qname basisOfRecord;

	@FieldDef(order=4.1, length=200)
	private Qname finbifOccurrenceId;

	@FieldDef(order=4.2, length=200)
	private String gbifOccurrenceId;

	@FieldDef(order=4.3, length=200)
	private String otherOccurrenceId;

	@FieldDef(order=5.1, length=200)
	private String kingdom;

	@FieldDef(order=5.2, length=200, etsOverride="verbatimScientificName")
	private String scientificName;

	@FieldDef(order=5.3, length=200)
	private String author;

	@FieldDef(order=5.4, length=200)
	private String functionalGroupName;

	@FieldDef(order=6.1, ignoreInSearch=true)
	private Qname taxonId;

	@FieldDef(order=6.2, length=12, ignoreInSearch=true)
	private Integer gbifTaxonId;

	@FieldDef(order=6.3, length=200)
	private String otherTaxonId;

	@FieldDef(order=7.1, enumeration="MY.sexes")
	private Qname sex;

	@FieldDef(order=7.2, enumeration="MY.lifeStages")
	private Qname lifeStage;

	@FieldDef(order=7.3, length=10, etsOverride="age")
	private Double ageYears;

	@FieldDef(order=7.3, length=4)
	private Integer individualCount;

	@FieldDef(order=8.1)
	private DateValue dateBegin;

	@FieldDef(order=8.2)
	private DateValue dateEnd;

	@FieldDef(order=8.3)
	private Integer yearBegin;

	@FieldDef(order=8.4)
	private Integer yearEnd;

	@FieldDef(order=8.5)
	private Integer seasonBegin;

	@FieldDef(order=8.6)
	private Integer seasonEnd;

	@FieldDef(order=9.11, etsOverride="decimalLatitude")
	private Double lat;

	@FieldDef(order=9.12, etsOverride="decimalLongitude")
	private Double lon;

	@FieldDef(order=9.21)
	private Double latMin;

	@FieldDef(order=9.22)
	private Double latMax;

	@FieldDef(order=9.23)
	private Double lonMin;

	@FieldDef(order=9.24)
	private Double lonMax;

	@FieldDef(order=9.3, length=12)
	private Integer coordinateAccuracy;

	@FieldDef(order=10.1, length=12)
	private Integer elevation;

	@FieldDef(order=11.1, length=200)
	private String higherGeography;

	@FieldDef(order=11.2, length=200)
	private String country;

	@FieldDef(order=11.3, length=200)
	private String municipality;

	@FieldDef(order=11.4, length=200, etsOverride="verbatimLocality")
	private String locality;

	@FieldDef(order=11.5, length=200)
	private String locationIdentifiers;

	@FieldDef(order=11.6, length=2000)
	private String habitat;

	@FieldDef(order=12.1, length=2000)
	private String occurrenceRemarks;

	@FieldDef(order=13.1, length=200)
	private String measurementDeterminedBy;

	@FieldDef(order=13.2)
	private Date measurementDeterminedDate;

	@FieldDef(order=100.1)
	private Date created;

	@FieldDef(order=100.2)
	private Date modified;

	@FieldDef(order=100.3, length=200, required=true, ignoreInSearch=true)
	private String createdBy;

	@FieldDef(order=100.4, length=200, ignoreInSearch=true)
	private String modifiedBy;

	public Subject() {
		super();
	}

	public Subject(JSONObject json) throws ParseException {
		super(json);
	}

	public Qname getId() {
		return id;
	}

	public Qname getDatasetId() {
		return datasetId;
	}

	public Qname getType() {
		return type;
	}

	public Qname getBasisOfRecord() {
		return basisOfRecord;
	}

	public Qname getFinbifOccurrenceId() {
		return finbifOccurrenceId;
	}

	public String getGbifOccurrenceId() {
		return gbifOccurrenceId;
	}

	public String getOtherOccurrenceId() {
		return otherOccurrenceId;
	}

	public String getKingdom() {
		return kingdom;
	}

	public String getScientificName() {
		return scientificName;
	}

	public String getAuthor() {
		return author;
	}

	public String getFunctionalGroupName() {
		return functionalGroupName;
	}

	public Qname getTaxonId() {
		return taxonId;
	}

	public Integer getGbifTaxonId() {
		return gbifTaxonId;
	}

	public String getOtherTaxonId() {
		return otherTaxonId;
	}

	public Qname getSex() {
		return sex;
	}

	public Qname getLifeStage() {
		return lifeStage;
	}

	public Double getAgeYears() {
		return ageYears;
	}

	public Integer getIndividualCount() {
		return individualCount;
	}

	public DateValue getDateBegin() {
		return dateBegin;
	}

	public DateValue getDateEnd() {
		return dateEnd;
	}

	public Integer getYearBegin() {
		return yearBegin;
	}

	public Integer getYearEnd() {
		return yearEnd;
	}

	public Integer getSeasonBegin() {
		return seasonBegin;
	}

	public Integer getSeasonEnd() {
		return seasonEnd;
	}

	public Double getLat() {
		return lat;
	}

	public Double getLon() {
		return lon;
	}

	public Double getLatMin() {
		return latMin;
	}

	public Double getLatMax() {
		return latMax;
	}

	public Double getLonMin() {
		return lonMin;
	}

	public Double getLonMax() {
		return lonMax;
	}

	public Integer getCoordinateAccuracy() {
		return coordinateAccuracy;
	}

	public Integer getElevation() {
		return elevation;
	}

	public String getHigherGeography() {
		return higherGeography;
	}

	public String getCountry() {
		return country;
	}

	public String getMunicipality() {
		return municipality;
	}

	public String getLocality() {
		return locality;
	}

	public String getLocationIdentifiers() {
		return locationIdentifiers;
	}

	public String getHabitat() {
		return habitat;
	}

	public String getOccurrenceRemarks() {
		return occurrenceRemarks;
	}

	public String getMeasurementDeterminedBy() {
		return measurementDeterminedBy;
	}

	public Date getMeasurementDeterminedDate() {
		return measurementDeterminedDate;
	}

	public Date getCreated() {
		return created;
	}

	public Date getModified() {
		return modified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setId(Qname id) {
		this.id = id;
	}

	public void setDatasetId(Qname datasetId) {
		this.datasetId = datasetId;
	}

	public void setType(Qname type) {
		this.type = type;
	}

	public void setBasisOfRecord(Qname basisOfRecord) {
		this.basisOfRecord = basisOfRecord;
	}

	public void setFinbifOccurrenceId(Qname finbifOccurrenceId) {
		this.finbifOccurrenceId = finbifOccurrenceId;
	}

	public void setGbifOccurrenceId(String gbifOccurrenceId) {
		this.gbifOccurrenceId = gbifOccurrenceId;
	}

	public void setOtherOccurrenceId(String otherOccurrenceId) {
		this.otherOccurrenceId = otherOccurrenceId;
	}

	public void setKingdom(String kingdom) {
		this.kingdom = kingdom;
	}

	public void setScientificName(String scientificName) {
		this.scientificName = scientificName;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setFunctionalGroupName(String functionalGroupName) {
		this.functionalGroupName = functionalGroupName;
	}

	public void setTaxonId(Qname taxonId) {
		this.taxonId = taxonId;
	}

	public void setGbifTaxonId(Integer gbifTaxonId) {
		this.gbifTaxonId = gbifTaxonId;
	}

	public void setOtherTaxonId(String otherTaxonId) {
		this.otherTaxonId = otherTaxonId;
	}

	public void setSex(Qname sex) {
		this.sex = sex;
	}

	public void setLifeStage(Qname lifeStage) {
		this.lifeStage = lifeStage;
	}

	public void setAgeYears(Double ageYears) {
		this.ageYears = ageYears;
	}

	public void setIndividualCount(Integer individualCount) {
		this.individualCount = individualCount;
	}

	public void setDateBegin(DateValue dateBegin) {
		this.dateBegin = dateBegin;
	}

	public void setDateEnd(DateValue dateEnd) {
		this.dateEnd = dateEnd;
	}

	public void setYearBegin(Integer yearBegin) {
		this.yearBegin = yearBegin;
	}

	public void setYearEnd(Integer yearEnd) {
		this.yearEnd = yearEnd;
	}

	public void setSeasonBegin(Integer seasonBegin) {
		this.seasonBegin = seasonBegin;
	}

	public void setSeasonEnd(Integer seasonEnd) {
		this.seasonEnd = seasonEnd;
	}

	public void setLat(Double lat) {
		this.lat = round(lat);
	}

	public void setLon(Double lon) {
		this.lon = round(lon);
	}

	public void setLatMin(Double latMin) {
		this.latMin = round(latMin);
	}

	public void setLatMax(Double latMax) {
		this.latMax = round(latMax);
	}

	public void setLonMin(Double lonMin) {
		this.lonMin = round(lonMin);
	}

	public void setLonMax(Double lonMax) {
		this.lonMax = round(lonMax);
	}

	public void setCoordinateAccuracy(Integer coordinateAccuracy) {
		this.coordinateAccuracy = coordinateAccuracy;
	}

	public void setElevation(Integer elevation) {
		this.elevation = elevation;
	}

	public void setHigherGeography(String higherGeography) {
		this.higherGeography = higherGeography;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public void setLocationIdentifiers(String locationIdentifiers) {
		this.locationIdentifiers = locationIdentifiers;
	}

	public void setHabitat(String habitat) {
		this.habitat = habitat;
	}

	public void setOccurrenceRemarks(String occurrenceRemarks) {
		this.occurrenceRemarks = occurrenceRemarks;
	}

	public void setMeasurementDeterminedBy(String measurementDeterminedBy) {
		this.measurementDeterminedBy = measurementDeterminedBy;
	}

	public void setMeasurementDeterminedDate(Date measurementDeterminedDate) {
		this.measurementDeterminedDate = measurementDeterminedDate;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ageYears == null) ? 0 : ageYears.hashCode());
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((basisOfRecord == null) ? 0 : basisOfRecord.hashCode());
		result = prime * result + ((coordinateAccuracy == null) ? 0 : coordinateAccuracy.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((created == null) ? 0 : created.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((datasetId == null) ? 0 : datasetId.hashCode());
		result = prime * result + ((dateBegin == null) ? 0 : dateBegin.hashCode());
		result = prime * result + ((dateEnd == null) ? 0 : dateEnd.hashCode());
		result = prime * result + ((elevation == null) ? 0 : elevation.hashCode());
		result = prime * result + ((finbifOccurrenceId == null) ? 0 : finbifOccurrenceId.hashCode());
		result = prime * result + ((functionalGroupName == null) ? 0 : functionalGroupName.hashCode());
		result = prime * result + ((gbifOccurrenceId == null) ? 0 : gbifOccurrenceId.hashCode());
		result = prime * result + ((gbifTaxonId == null) ? 0 : gbifTaxonId.hashCode());
		result = prime * result + ((habitat == null) ? 0 : habitat.hashCode());
		result = prime * result + ((higherGeography == null) ? 0 : higherGeography.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((individualCount == null) ? 0 : individualCount.hashCode());
		result = prime * result + ((kingdom == null) ? 0 : kingdom.hashCode());
		result = prime * result + ((lat == null) ? 0 : lat.hashCode());
		result = prime * result + ((latMax == null) ? 0 : latMax.hashCode());
		result = prime * result + ((latMin == null) ? 0 : latMin.hashCode());
		result = prime * result + ((lifeStage == null) ? 0 : lifeStage.hashCode());
		result = prime * result + ((locality == null) ? 0 : locality.hashCode());
		result = prime * result + ((locationIdentifiers == null) ? 0 : locationIdentifiers.hashCode());
		result = prime * result + ((lon == null) ? 0 : lon.hashCode());
		result = prime * result + ((lonMax == null) ? 0 : lonMax.hashCode());
		result = prime * result + ((lonMin == null) ? 0 : lonMin.hashCode());
		result = prime * result + ((measurementDeterminedBy == null) ? 0 : measurementDeterminedBy.hashCode());
		result = prime * result + ((measurementDeterminedDate == null) ? 0 : measurementDeterminedDate.hashCode());
		result = prime * result + ((modified == null) ? 0 : modified.hashCode());
		result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
		result = prime * result + ((municipality == null) ? 0 : municipality.hashCode());
		result = prime * result + ((occurrenceRemarks == null) ? 0 : occurrenceRemarks.hashCode());
		result = prime * result + ((otherOccurrenceId == null) ? 0 : otherOccurrenceId.hashCode());
		result = prime * result + ((otherTaxonId == null) ? 0 : otherTaxonId.hashCode());
		result = prime * result + ((scientificName == null) ? 0 : scientificName.hashCode());
		result = prime * result + ((seasonBegin == null) ? 0 : seasonBegin.hashCode());
		result = prime * result + ((seasonEnd == null) ? 0 : seasonEnd.hashCode());
		result = prime * result + ((sex == null) ? 0 : sex.hashCode());
		result = prime * result + ((taxonId == null) ? 0 : taxonId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((yearBegin == null) ? 0 : yearBegin.hashCode());
		result = prime * result + ((yearEnd == null) ? 0 : yearEnd.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subject other = (Subject) obj;
		if (ageYears == null) {
			if (other.ageYears != null)
				return false;
		} else if (!ageYears.equals(other.ageYears))
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (basisOfRecord == null) {
			if (other.basisOfRecord != null)
				return false;
		} else if (!basisOfRecord.equals(other.basisOfRecord))
			return false;
		if (coordinateAccuracy == null) {
			if (other.coordinateAccuracy != null)
				return false;
		} else if (!coordinateAccuracy.equals(other.coordinateAccuracy))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (created == null) {
			if (other.created != null)
				return false;
		} else if (!created.equals(other.created))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (datasetId == null) {
			if (other.datasetId != null)
				return false;
		} else if (!datasetId.equals(other.datasetId))
			return false;
		if (dateBegin == null) {
			if (other.dateBegin != null)
				return false;
		} else if (!dateBegin.equals(other.dateBegin))
			return false;
		if (dateEnd == null) {
			if (other.dateEnd != null)
				return false;
		} else if (!dateEnd.equals(other.dateEnd))
			return false;
		if (elevation == null) {
			if (other.elevation != null)
				return false;
		} else if (!elevation.equals(other.elevation))
			return false;
		if (finbifOccurrenceId == null) {
			if (other.finbifOccurrenceId != null)
				return false;
		} else if (!finbifOccurrenceId.equals(other.finbifOccurrenceId))
			return false;
		if (functionalGroupName == null) {
			if (other.functionalGroupName != null)
				return false;
		} else if (!functionalGroupName.equals(other.functionalGroupName))
			return false;
		if (gbifOccurrenceId == null) {
			if (other.gbifOccurrenceId != null)
				return false;
		} else if (!gbifOccurrenceId.equals(other.gbifOccurrenceId))
			return false;
		if (gbifTaxonId == null) {
			if (other.gbifTaxonId != null)
				return false;
		} else if (!gbifTaxonId.equals(other.gbifTaxonId))
			return false;
		if (habitat == null) {
			if (other.habitat != null)
				return false;
		} else if (!habitat.equals(other.habitat))
			return false;
		if (higherGeography == null) {
			if (other.higherGeography != null)
				return false;
		} else if (!higherGeography.equals(other.higherGeography))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (individualCount == null) {
			if (other.individualCount != null)
				return false;
		} else if (!individualCount.equals(other.individualCount))
			return false;
		if (kingdom == null) {
			if (other.kingdom != null)
				return false;
		} else if (!kingdom.equals(other.kingdom))
			return false;
		if (lat == null) {
			if (other.lat != null)
				return false;
		} else if (!lat.equals(other.lat))
			return false;
		if (latMax == null) {
			if (other.latMax != null)
				return false;
		} else if (!latMax.equals(other.latMax))
			return false;
		if (latMin == null) {
			if (other.latMin != null)
				return false;
		} else if (!latMin.equals(other.latMin))
			return false;
		if (lifeStage == null) {
			if (other.lifeStage != null)
				return false;
		} else if (!lifeStage.equals(other.lifeStage))
			return false;
		if (locality == null) {
			if (other.locality != null)
				return false;
		} else if (!locality.equals(other.locality))
			return false;
		if (locationIdentifiers == null) {
			if (other.locationIdentifiers != null)
				return false;
		} else if (!locationIdentifiers.equals(other.locationIdentifiers))
			return false;
		if (lon == null) {
			if (other.lon != null)
				return false;
		} else if (!lon.equals(other.lon))
			return false;
		if (lonMax == null) {
			if (other.lonMax != null)
				return false;
		} else if (!lonMax.equals(other.lonMax))
			return false;
		if (lonMin == null) {
			if (other.lonMin != null)
				return false;
		} else if (!lonMin.equals(other.lonMin))
			return false;
		if (measurementDeterminedBy == null) {
			if (other.measurementDeterminedBy != null)
				return false;
		} else if (!measurementDeterminedBy.equals(other.measurementDeterminedBy))
			return false;
		if (measurementDeterminedDate == null) {
			if (other.measurementDeterminedDate != null)
				return false;
		} else if (!measurementDeterminedDate.equals(other.measurementDeterminedDate))
			return false;
		if (modified == null) {
			if (other.modified != null)
				return false;
		} else if (!modified.equals(other.modified))
			return false;
		if (modifiedBy == null) {
			if (other.modifiedBy != null)
				return false;
		} else if (!modifiedBy.equals(other.modifiedBy))
			return false;
		if (municipality == null) {
			if (other.municipality != null)
				return false;
		} else if (!municipality.equals(other.municipality))
			return false;
		if (occurrenceRemarks == null) {
			if (other.occurrenceRemarks != null)
				return false;
		} else if (!occurrenceRemarks.equals(other.occurrenceRemarks))
			return false;
		if (otherOccurrenceId == null) {
			if (other.otherOccurrenceId != null)
				return false;
		} else if (!otherOccurrenceId.equals(other.otherOccurrenceId))
			return false;
		if (otherTaxonId == null) {
			if (other.otherTaxonId != null)
				return false;
		} else if (!otherTaxonId.equals(other.otherTaxonId))
			return false;
		if (scientificName == null) {
			if (other.scientificName != null)
				return false;
		} else if (!scientificName.equals(other.scientificName))
			return false;
		if (seasonBegin == null) {
			if (other.seasonBegin != null)
				return false;
		} else if (!seasonBegin.equals(other.seasonBegin))
			return false;
		if (seasonEnd == null) {
			if (other.seasonEnd != null)
				return false;
		} else if (!seasonEnd.equals(other.seasonEnd))
			return false;
		if (sex == null) {
			if (other.sex != null)
				return false;
		} else if (!sex.equals(other.sex))
			return false;
		if (taxonId == null) {
			if (other.taxonId != null)
				return false;
		} else if (!taxonId.equals(other.taxonId))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (yearBegin == null) {
			if (other.yearBegin != null)
				return false;
		} else if (!yearBegin.equals(other.yearBegin))
			return false;
		if (yearEnd == null) {
			if (other.yearEnd != null)
				return false;
		} else if (!yearEnd.equals(other.yearEnd))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Subject [id=" + id + ", datasetId=" + datasetId + ", type=" + type + ", basisOfRecord=" + basisOfRecord + ", finbifOccurrenceId=" + finbifOccurrenceId
				+ ", gbifOccurrenceId=" + gbifOccurrenceId + ", otherOccurrenceId=" + otherOccurrenceId + ", kingdom=" + kingdom + ", scientificName=" + scientificName
				+ ", author=" + author + ", functionalGroupName=" + functionalGroupName + ", taxonId=" + taxonId + ", gbifTaxonId=" + gbifTaxonId + ", otherTaxonId=" + otherTaxonId
				+ ", sex=" + sex + ", lifeStage=" + lifeStage + ", ageYears=" + ageYears + ", individualCount=" + individualCount + ", dateBegin=" + dateBegin + ", dateEnd="
				+ dateEnd + ", yearBegin=" + yearBegin + ", yearEnd=" + yearEnd + ", seasonBegin=" + seasonBegin + ", seasonEnd=" + seasonEnd + ", lat=" + lat + ", lon=" + lon
				+ ", latMin=" + latMin + ", latMax=" + latMax + ", lonMin=" + lonMin + ", lonMax=" + lonMax + ", coordinateAccuracy=" + coordinateAccuracy + ", elevation="
				+ elevation + ", higherGeography=" + higherGeography + ", country=" + country + ", municipality=" + municipality + ", locality=" + locality
				+ ", locationIdentifiers=" + locationIdentifiers + ", habitat=" + habitat + ", occurrenceRemarks=" + occurrenceRemarks + ", measurementDeterminedBy="
				+ measurementDeterminedBy + ", measurementDeterminedDate=" + measurementDeterminedDate + ", created=" + created + ", modified=" + modified + ", createdBy="
				+ createdBy + ", modifiedBy=" + modifiedBy + "]";
	}

	@Override
	public Map<String, String> validate(boolean insert, TraitDAO dao) {
		Map<String, String> errors = super.validate(insert, dao);
		positive("ageYears", getAgeYears(), errors);
		positive("individualCount", getIndividualCount(), errors);
		dateRange("dateBegin", "dateEnd", getDateBegin(), getDateEnd(), errors);
		yearRange("yearBegin", "yearEnd", getYearBegin(), getYearEnd(), errors);
		season("seasonBegin", getSeasonBegin(), errors);
		season("seasonEnd", getSeasonEnd(), errors);
		lat("lat", getLat(), errors);
		lat("latMin", getLatMin(), errors);
		lat("latMax", getLatMax(), errors);
		lon("lon", getLon(), errors);
		lon("lonMin", getLonMin(), errors);
		lon("lonMax", getLonMax(), errors);
		minMax("latMin", "latMax", getLatMin(), getLatMax(), errors);
		minMax("lonMin", "lonMax", getLonMin(), getLonMax(), errors);
		positive("coordinateAccuracy", getCoordinateAccuracy(), errors);
		elevation(errors);
		date("measurementDeterminedDate", getMeasurementDeterminedDate(), errors);
		return errors;
	}

	private void elevation(Map<String, String> errors) {
		if (getElevation() == null) return;
		if (getElevation() < -11000) errors.put("elevation", "Too small value");
		if (getElevation() > 10000) errors.put("elevation", "Too large value");
	}

	private void positive(String field, Object number, Map<String, String> errors) {
		if (number == null) return;
		double v = Double.valueOf(number.toString());
		if (v <= 0) errors.put(field, "Must be positive number");
	}

	private void date(String field, DateValue date, Map<String, String> errors) {
		if (date == null) return;
		if (date.hasEmptyFields()) {
			errors.put(field, "Provide full date in format yyyy-mm-dd");
			return;
		}
		try {
			DateUtils.convertToDate(date);
		} catch (Exception e) {
			errors.put(field, "Invalid date");
		}
		year(field, date.getYearAsInt(), errors);
	}

	private void year(String field, Integer year, Map<String, String> errors) {
		if (year == null) return;
		year(field, year.intValue(), errors);
	}

	private void year(String field, int year, Map<String, String> errors) {
		if (year < 1500) errors.put(field, "Too far in the past");
		if (year > DateUtils.getCurrentYear()) errors.put(field, "In the future");
	}

	private void date(String field, Date date, Map<String, String> errors) {
		if (date == null) return;
		date(field, DateUtils.convertToDateValue(date), errors);
	}

	private void dateRange(String begin, String end, DateValue dateBegin, DateValue dateEnd, Map<String, String> errors) {
		date(begin, dateBegin, errors);
		date(end, dateEnd, errors);
		if (dateBegin != null && dateEnd != null) {
			if (DateUtils.convertToDate(dateBegin).after(DateUtils.convertToDate(dateEnd))) {
				errors.put(begin, "Begin should be before end date");
				errors.put(end, "Begin should be before end date");
			}
		}
	}

	private void yearRange(String begin, String end, Integer yearBegin, Integer yearEnd, Map<String, String> errors) {
		year(begin, yearBegin, errors);
		year(end, yearEnd, errors);
		if (yearBegin != null && yearEnd != null) {
			if (yearBegin > yearEnd) {
				errors.put(begin, "Begin should be before end year");
				errors.put(end, "Begin should be before end year");
			}
		}
	}

	private void season(String field, Integer season, Map<String, String> errors) {
		if (season == null) return;
		if (season < 101) errors.put(field, "Invalid season; first day of the year is 101");
		if (season > 3112) errors.put(field, "Invalid season; last day of the year is 3112");
	}

	private void minMax(String minField, String maxField, Double min, Double max, Map<String, String> errors) {
		if (min == null && max == null) return;
		if (min == null && max != null) errors.put(maxField, "Min and max must both be given");
		if (min != null && max == null) errors.put(maxField, "Min and max must both be given");
		if (min != null && max != null && min > max) {
			errors.put(minField, "Min should be smaller than max");
			errors.put(maxField, "Min should be smaller than max");
		}
	}

	private void lat(String field, Double lat, Map<String, String> errors) {
		if (lat == null) return;
		if (lat < -90 || lat > 90) errors.put(field, "Latitude must be between -90 and 90 degrees");
	}

	private void lon(String field, Double lon, Map<String, String> errors) {
		if (lon == null) return;
		if (lon < -180 || lon > 180) errors.put(field, "Longitude must be between -180 and 180 degrees");
	}

	private Double round(Double coord) {
		if (coord == null) return null;
		return Utils.round(coord, 6);
	}

}
