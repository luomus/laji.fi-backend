package fi.luomus.traitdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.TestConfig;
import fi.luomus.lajitietokeskus.TestUtil;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAOImple;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAOImple.IndexNames;
import fi.luomus.traitdb.models.search.TraitSearchRow;

public class TraitElasticSearchDAOImpleTests {

	private static final IndexNames INDEX_NAMES = new IndexNames(IndexNames.INDEX_TYPE_TRAITS, "trait_test_1", "trait_test_2", "trait_test");

	private ElasticSearchDAOImple dao;

	@Before
	public void before() throws Exception {
		System.out.println(" --- before test --- start");
		Config config = TestConfig.getConfig();
		dao = new ElasticSearchDAOImple(
				config.get("ES_address"),
				config.get("ES_cluster"),
				INDEX_NAMES,
				new ErrorReportingToSystemErr(),
				null,
				false);
		dao.deleteAll();
		System.out.println(" --- ------ ---- --- end");
	}

	@After
	public void after() {
		if (dao != null) dao.close();
	}

	@Test
	public void test_first_load_set_alias_and_swap_alias() throws Exception {
		assertFalse(dao.aliasExists());
		assertFalse(dao.indexExists(INDEX_NAMES.index1));
		assertFalse(dao.indexExists(INDEX_NAMES.index2));

		dao.emptyTemp();

		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index2, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index1, dao.getActiveIndex());

		dao.switchTempToActiveIndex();

		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index1, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index2, dao.getActiveIndex());

		dao.emptyTemp();

		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index1, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index2, dao.getActiveIndex());

		dao.switchTempToActiveIndex();

		assertTrue(dao.aliasExists());
		assertTrue(dao.indexExists(INDEX_NAMES.index1));
		assertTrue(dao.indexExists(INDEX_NAMES.index2));
		assertEquals(INDEX_NAMES.index2, dao.getTempIndex());
		assertEquals(INDEX_NAMES.index1, dao.getActiveIndex());
	}

	@Test
	public void test_pushing_search_row() throws Exception {
		TraitSearchRow row = EntityTests.createTestRow();

		assertEquals("trait_test_1", dao.getActiveIndex());
		assertTrue(dao.aliasExists());
		dao.pushActive(row);

		assertEquals("[]", dao.getPushErrors().toString());

		dao.refreshSearchIndex();

		JSONObject actual = dao.getFromActiveIndex(row.getId());
		JSONObject expected = new JSONObject(getContents("search_row_expected.json"));
		assertEquals(expected.beautify(), actual.beautify());

		Map<String, List<String>> searchParams = new HashMap<>();
		searchParams.put("subject.yearBegin", Utils.list("2000", "9999"));

		List<String> searchIndicies = Utils.singleEntryList(INDEX_NAMES.alias);

		List<JSONObject> searchResults = dao.search(searchIndicies, searchParams, 10, 1);
		assertEquals(1, searchResults.size());
		assertEquals(expected.beautify(), searchResults.get(0).beautify());

		searchParams.put("objectFinBIFTaxon.higherTaxa.kingdom", Utils.list("Animalia"));
		searchResults = dao.search(searchIndicies, searchParams, 10, 1);
		assertEquals(1, searchResults.size());
		assertEquals(expected.beautify(), searchResults.get(0).beautify());

		searchParams.put("objectFinBIFTaxon.higherTaxa.kingdom", Utils.list("animaliâ")); // case-insensitive with non-ascii
		searchResults = dao.search(searchIndicies, searchParams, 10, 1);
		assertEquals(1, searchResults.size());
		assertEquals(expected.beautify(), searchResults.get(0).beautify());

		searchParams.put("subject.yearBegin", Utils.singleEntryList("2001"));
		searchResults = dao.search(searchIndicies, searchParams, 10, 1);
		assertEquals(0, searchResults.size());

		row.getSubject().setYearBegin(2001);
		dao.pushActive(row);
		assertEquals("[]", dao.getPushErrors().toString());
		dao.refreshSearchIndex();

		searchResults = dao.search(searchIndicies, searchParams, 10, 1);
		assertEquals(1, searchResults.size());

		assertEquals(null, dao.getFromTempIndex(row.getId()));

		dao.delete(row.getId());
		assertEquals("[]", dao.getPushErrors().toString());
		assertEquals(null, dao.getFromActiveIndex(row.getId()));
		assertEquals(null, dao.getFromTempIndex(row.getId()));
	}

	private String getContents(String filename) throws Exception {
		return TestUtil.getContents(TraitElasticSearchDAOImpleTests.class, filename);
	}

}
