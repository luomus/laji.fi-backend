package fi.luomus.traitdb.dao.triplestore;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.Param;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.TraitEnumerationValue;

public class TraitEnumerationValuesImple extends TriplestoreBaseDAO<TraitEnumerationValue> {

	public TraitEnumerationValuesImple(Config config) {
		super(config);
	}

	@Override
	public ValidationResponse validate(TraitEnumerationValue entity, boolean insert) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	protected Class<TraitEnumerationValue> concreteClass() {
		return TraitEnumerationValue.class;
	}

	@Override
	protected Model createModel(Qname id, TraitEnumerationValue entity) {
		Model model = new Model(id);
		model.setType("TDF.traitEnumValue");
		model.addStatementIfObjectGiven(new Predicate("TDF.dataEntryName"), entity.getDataEntryName());
		model.addStatementIfObjectGiven(new Predicate("TDF.name"), entity.getName());
		model.addStatementIfObjectGiven(new Predicate("TDF.description"), entity.getDescription());
		model.addStatementIfObjectGiven(new Predicate("sortOrder"), Integer.toString(entity.getOrder()));
		return model;
	}

	@Override
	protected List<TraitEnumerationValue> getAllActual() {
		Document doc = query("search", new Param("type", "TDF.traitEnumValue"));
		List<TraitEnumerationValue> values = new ArrayList<>();
		for (Node n : doc.getRootNode().getChildNodes()) {
			TraitEnumerationValue ev = new TraitEnumerationValue();
			ev.setId(getId(n));
			ev.setName(getLiteral("TDF.name", n));
			ev.setDataEntryName(getLiteral("TDF.dataEntryName", n));
			ev.setDescription(getLiteral("TDF.description", n));
			ev.setOrder(Integer.valueOf(getLiteral("sortOrder", n)));
			values.add(ev);
		}
		return values;
	}

	@Override
	public ValidationResponse validateDelete(Qname id) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

}
