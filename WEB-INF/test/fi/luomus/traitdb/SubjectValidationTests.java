package fi.luomus.traitdb;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.traitdb.models.Enumeration;
import fi.luomus.traitdb.models.Subject;

public class SubjectValidationTests {

	private static class FakeDAO extends TraitDAOStub {
		@Override
		public Enumeration getEnumeration(Qname id) {
			Enumeration e = new Enumeration(id);
			e.add(new Qname("foobar"), "foobar");
			return e;
		}
	}

	private final FakeDAO dao = new FakeDAO();

	private Subject createSubject() {
		Subject subject = new Subject();
		subject.setDatasetId(new Qname("foobar"));
		subject.setType(new Qname("foobar"));
		subject.setCreatedBy("foobar");
		assertEquals("{}", subject.validate(true, dao).toString());
		return subject;
	}

	@Test
	public void age() {
		Subject subject = createSubject();
		subject.setAgeYears(-0.000001);
		assertEquals("{ageYears=Must be positive number}", subject.validate(true, dao).toString());
		subject.setAgeYears(0.0);
		assertEquals("{ageYears=Must be positive number}", subject.validate(true, dao).toString());
		subject.setAgeYears(0.00011408);
		assertEquals("{}", subject.validate(true, dao).toString());
	}

	@Test
	public void individualCount() {
		Subject subject = createSubject();
		subject.setIndividualCount(0);
		assertEquals("{individualCount=Must be positive number}", subject.validate(true, dao).toString());
		subject.setIndividualCount(-1);
		assertEquals("{individualCount=Must be positive number}", subject.validate(true, dao).toString());
		subject.setIndividualCount(1);
		assertEquals("{}", subject.validate(true, dao).toString());
	}

	@Test
	public void coordinateAccuracy() {
		Subject subject = createSubject();
		subject.setCoordinateAccuracy(0);
		assertEquals("{coordinateAccuracy=Must be positive number}", subject.validate(true, dao).toString());
		subject.setCoordinateAccuracy(-1);
		assertEquals("{coordinateAccuracy=Must be positive number}", subject.validate(true, dao).toString());
		subject.setCoordinateAccuracy(1);
		assertEquals("{}", subject.validate(true, dao).toString());
	}

	@Test
	public void dates() {
		Subject subject = createSubject();
		subject.setDateBegin(new DateValue(null, null, 1950));
		assertEquals("{dateBegin=Provide full date in format yyyy-mm-dd}", subject.validate(true, dao).toString());

		subject.setDateBegin(new DateValue(1, 1, 1450));
		assertEquals("{dateBegin=Too far in the past}", subject.validate(true, dao).toString());

		subject.setDateBegin(new DateValue(1, 1, DateUtils.getCurrentYear()+1));
		assertEquals("{dateBegin=In the future}", subject.validate(true, dao).toString());

		subject.setDateBegin(null);
		subject.setDateEnd(new DateValue(1, 1, 2000));
		assertEquals("{}", subject.validate(true, dao).toString());

		subject.setDateBegin(new DateValue(1, 1, 2005));
		assertEquals("{dateBegin=Begin should be before end date, dateEnd=Begin should be before end date}", subject.validate(true, dao).toString());

		subject.setDateBegin(new DateValue(29, 2, 2024));
		subject.setDateEnd(null);
		assertEquals("{}", subject.validate(true, dao).toString());

		subject.setDateBegin(new DateValue(30, 2, 2024));
		assertEquals("{dateBegin=Invalid date}", subject.validate(true, dao).toString());
	}

	@Test
	public void mesDate() {
		Subject subject = createSubject();
		subject.setMeasurementDeterminedDate(new Date((DateUtils.getCurrentEpoch()*1000)+(1000L*60*60*24*400)));
		assertEquals("{measurementDeterminedDate=In the future}", subject.validate(true, dao).toString());
		subject.setMeasurementDeterminedDate(new Date((DateUtils.getCurrentEpoch()*1000)-(1000L*60*60*24*356*3000)));
		assertEquals("{measurementDeterminedDate=Too far in the past}", subject.validate(true, dao).toString());
	}

	@Test
	public void year() {
		Subject subject = createSubject();
		subject.setYearBegin(1450);
		assertEquals("{yearBegin=Too far in the past}", subject.validate(true, dao).toString());
		subject.setYearBegin(null);
		subject.setYearEnd(2000);
		assertEquals("{}", subject.validate(true, dao).toString());
		subject.setYearBegin(2000);
		assertEquals("{}", subject.validate(true, dao).toString());
		subject.setYearBegin(2005);
		assertEquals("{yearBegin=Begin should be before end year, yearEnd=Begin should be before end year}", subject.validate(true, dao).toString());
	}

	@Test
	public void season() {
		Subject subject = createSubject();
		subject.setSeasonBegin(1);
		assertEquals("{seasonBegin=Invalid season; first day of the year is 101}", subject.validate(true, dao).toString());
		subject.setSeasonBegin(5000);
		assertEquals("{seasonBegin=Invalid season; last day of the year is 3112}", subject.validate(true, dao).toString());
		subject.setSeasonBegin(null);
		subject.setYearEnd(2000);
		assertEquals("{}", subject.validate(true, dao).toString());
		subject.setYearBegin(2000);
		assertEquals("{}", subject.validate(true, dao).toString());
		subject.setYearBegin(2005);
		assertEquals("{yearBegin=Begin should be before end year, yearEnd=Begin should be before end year}", subject.validate(true, dao).toString());
	}

	@Test
	public void coordinates() {
		Subject subject = createSubject();
		subject.setLat(-250.0);
		subject.setLon(-250.0);
		subject.setLatMin(-250.0);
		subject.setLatMax(-250.0);
		subject.setLonMin(-250.0);
		subject.setLonMax(-250.0);
		assertEquals("" +
				"{lat=Latitude must be between -90 and 90 degrees, latMin=Latitude must be between -90 and 90 degrees, latMax=Latitude must be between -90 and 90 degrees, lon=Longitude must be between -180 and 180 degrees, lonMin=Longitude must be between -180 and 180 degrees, lonMax=Longitude must be between -180 and 180 degrees}",
				subject.validate(true, dao).toString());
		subject.setLat(250.0);
		subject.setLon(250.0);
		subject.setLatMin(250.0);
		subject.setLatMax(250.0);
		subject.setLonMin(250.0);
		subject.setLonMax(250.0);
		assertEquals("" +
				"{lat=Latitude must be between -90 and 90 degrees, latMin=Latitude must be between -90 and 90 degrees, latMax=Latitude must be between -90 and 90 degrees, lon=Longitude must be between -180 and 180 degrees, lonMin=Longitude must be between -180 and 180 degrees, lonMax=Longitude must be between -180 and 180 degrees}",
				subject.validate(true, dao).toString());

		subject.setLat(60.0);
		subject.setLon(20.0);
		subject.setLatMin(60.0);
		subject.setLatMax(60.0);
		subject.setLonMin(20.0);
		subject.setLonMax(20.0);
		assertEquals("" +
				"{}",
				subject.validate(true, dao).toString());
		subject.setLonMin(null);
		subject.setLatMax(null);
		assertEquals("" +
				"{latMax=Min and max must both be given, lonMax=Min and max must both be given}",
				subject.validate(true, dao).toString());

		subject.setLatMin(60.0);
		subject.setLatMax(50.0);
		subject.setLonMin(20.0);
		subject.setLonMax(10.0);
		assertEquals("" +
				"{latMin=Min should be smaller than max, latMax=Min should be smaller than max, lonMin=Min should be smaller than max, lonMax=Min should be smaller than max}",
				subject.validate(true, dao).toString());
	}

	@Test
	public void elevation() {
		Subject subject = createSubject();
		subject.setElevation(-20000);
		assertEquals("{elevation=Too small value}", subject.validate(true, dao).toString());
		subject.setElevation(-1);
		assertEquals("{}", subject.validate(true, dao).toString());
		subject.setElevation(0);
		assertEquals("{}", subject.validate(true, dao).toString());
		subject.setElevation(7000);
		assertEquals("{}", subject.validate(true, dao).toString());
		subject.setElevation(12000);
		assertEquals("{elevation=Too large value}", subject.validate(true, dao).toString());
	}
}
