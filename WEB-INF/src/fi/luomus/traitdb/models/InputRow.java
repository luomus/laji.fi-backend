package fi.luomus.traitdb.models;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.json.JSONObject;

public class InputRow extends AbstractBaseEntity {

	@FieldDef(order=1, required=true)
	private Subject subject;
	@FieldDef(order=2)
	private List<TraitValue> traits;

	public InputRow() {
		super();
	}

	public InputRow(Subject subject) {
		super();
		this.subject = subject;
	}

	public InputRow(JSONObject json) throws ParseException {
		super(json);
	}

	public Subject getSubject() {
		return subject;
	}

	public List<TraitValue> getTraits() {
		if (traits == null) {
			traits = new ArrayList<>();
		}
		return traits;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setTraits(List<TraitValue> traits) {
		this.traits = traits;
	}

	public void addTrait(TraitValue t) {
		getTraits().add(t);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((traits == null) ? 0 : traits.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InputRow other = (InputRow) obj;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (traits == null) {
			if (other.traits != null)
				return false;
		} else if (!traits.equals(other.traits))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InputRow [subject=" + subject + ", traits=" + traits + "]";
	}

}
