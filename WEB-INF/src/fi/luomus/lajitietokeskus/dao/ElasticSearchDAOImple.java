package fi.luomus.lajitietokeskus.dao;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest.AliasActions;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonToMap;
import fi.luomus.traitdb.models.search.TraitSearchRow;

public class ElasticSearchDAOImple implements ElasticSearchDAO {

	public static class IndexNames {

		public static String INDEX_TYPE_TAXA = "taxa";
		public static String INDEX_TYPE_TRAITS = "traits";

		public final String indexType;
		public final String index1;
		public final String index2;
		public final String alias;
		public IndexNames(String indexType, String index1, String index2, String alias) {
			this.indexType = indexType.toLowerCase().replace(".", "_");
			this.index1 = index1.toLowerCase().replace(".", "_");
			this.index2 = index2.toLowerCase().replace(".", "_");
			this.alias = alias.toLowerCase().replace(".", "_");
		}
		@Override
		public String toString() {
			return "IndexNames [indexType=" + indexType + ", index1=" + index1 + ", index2=" + index2 + ", alias=" + alias + "]";
		}
	}

	private static final Collection<Class<? extends Plugin>> NO_PLUGINS = Collections.emptyList();

	private final IndexNames indexNames;
	private final BulkProcessor bulkProcessor;
	private final List<PushError> errors = new ArrayList<>();
	private final ErrorReporter errorReporter;
	private final TransportClient transportClient;
	private final Client client;
	private final TaxonToMap taxonMapper;
	private final Map<Qname, Object> pushedData = Collections.synchronizedMap(new LinkedHashMap<>());
	private final Object LOCK = new Object();
	private final boolean log;

	public ElasticSearchDAOImple(String cluserAddress, String clusterName, IndexNames indexNames, ErrorReporter errorReporter, TaxonToMap taxonMapper, boolean prodMode) {
		this.indexNames = indexNames;
		this.errorReporter = errorReporter;
		this.taxonMapper = taxonMapper;
		InetAddress inetAddress = toInetAddress(cluserAddress);
		Settings settings = Settings.builder().put("cluster.name", clusterName).build();

		transportClient = new PreBuiltTransportClient(settings, NO_PLUGINS);
		client =  transportClient.addTransportAddress(new InetSocketTransportAddress(inetAddress, 9300));
		this.bulkProcessor = BulkProcessor.builder(client, new MyBulkProcessorLister())
				.setBulkActions(1000)
				.setBulkSize(new ByteSizeValue(20, ByteSizeUnit.MB))
				.setConcurrentRequests(prodMode ? 5 : 0)
				.build();
		this.log = indexType().equals(IndexNames.INDEX_TYPE_TAXA) || !prodMode;
		System.out.println("ES DAO initialized for " + indexNames);
	}

	private InetAddress toInetAddress(String cluserAddress) {
		try {
			return InetAddress.getByName(cluserAddress);
		} catch (UnknownHostException e) {
			throw new IllegalArgumentException("Invalid cluster address", e);
		}
	}

	private class MyBulkProcessorLister implements BulkProcessor.Listener {

		@Override
		public void afterBulk(long executionId, BulkRequest req, BulkResponse res) {
			synchronized (LOCK) {
				for (BulkItemResponse resItem : res.getItems()) {
					Qname id = new Qname(resItem.getId());
					if (resItem.isFailed())  {
						String error = resItem.getFailure().toString();
						errors.add(new PushError(id, error, pushedData.get(id), new Exception("item failed")));
						errorReporter.report("ES PUSH: Item failure for id " + id + " : " + error);
					}
					pushedData.remove(id);
				}
			}
		}

		@Override
		public void afterBulk(long executionId, BulkRequest req, Throwable e) {
			errorReporter.report("ES PUSH: Unknown failure", e);
		}

		@Override
		public void beforeBulk(long executionId, BulkRequest req) {}
	}

	@Override
	public void push(Taxon taxon) {
		try {
			if (taxon == null) throw new IllegalArgumentException("Null taxon given");
			String tempIndex = getTempIndex();
			Map<String, Object> data = getData(taxon);
			pushedData.put(taxon.getId(), data);
			bulkProcessor.add(client.prepareIndex(tempIndex, indexType()).setSource(data).setId(taxon.getId().toString()).request());
		} catch (Exception e) {
			errorReporter.report("Pushing taxon " + taxon.getId(), e);
			errors.add(new PushError(taxon.getId(), LogUtils.buildStackTrace(e), pushedData.get(taxon.getId()), e));
		}
	}

	private Map<String, Object> getData(Taxon taxon) throws Exception {
		Map<String, Object> data = taxonMapper.map(taxon);
		return data;
	}

	@Override
	public void pushActive(TraitSearchRow row) {
		push(true, row);
	}

	@Override
	public void pushTemp(TraitSearchRow row) {
		push(false, row);
	}

	private void push(boolean active, TraitSearchRow row) {
		try {
			if (row == null) throw new IllegalArgumentException("Null trait data given");
			String index = active ? getActiveIndex() : getTempIndex();
			String json = row.toJSON().toString();
			pushedData.put(row.getId(), json);
			bulkProcessor.add(client.prepareIndex(index, indexType()).setSource(json, XContentType.JSON).setId(row.getId().toString()).request());
		} catch (Exception e) {
			errorReporter.report("Pushing trait row " + row.getId(), e);
			errors.add(new PushError(row.getId(), LogUtils.buildStackTrace(e), pushedData.get(row.getId()), e));
		}
	}

	@Override
	public void delete(Qname id) {
		try {
			if (id == null) throw new IllegalArgumentException("Null id given for deletion");
			DeleteRequest deleteRequest = new DeleteRequest(getActiveIndex(), indexType(), id.toString());
			pushedData.put(id, "DELETE");
			bulkProcessor.add(deleteRequest);
		} catch (Exception e) {
			errorReporter.report("Deleting trait row " + id, e);
			errors.add(new PushError(id, LogUtils.buildStackTrace(e), id, e));
		}
	}

	@Override
	public List<PushError> getPushErrors() throws WaitTimeoutException {
		waitForPushCompletion();
		return errors;
	}

	@Override
	public void close() {
		if (bulkProcessor != null) {
			try {bulkProcessor.flush(); } catch (Exception e) {}
			try { bulkProcessor.awaitClose(3, TimeUnit.SECONDS); } catch (Exception e) {}
		}
		if (transportClient != null) {
			try { transportClient.close(); } catch (Exception e) { }
		}
		if (client != null) {
			try { client.close(); } catch (Exception e) { }
		}
		System.out.println("ES DAO ("+indexNames+") closed down");
	}

	@Override
	public JSONObject get(Qname id) throws Exception {
		return getFromActiveIndex(id);
	}

	public JSONObject getFromTempIndex(Qname id) throws Exception {
		return get(id, getTempIndex());
	}

	public JSONObject getFromActiveIndex(Qname id) throws Exception {
		return get(id, getActiveIndex());
	}

	private JSONObject get(Qname id, String index) {
		String data = client.prepareGet(index, indexType(), id.toString())
				.setOperationThreaded(false)
				.get().getSourceAsString();
		if (data == null) return null;
		return new JSONObject(data);
	}

	@Override
	public void switchTempToActiveIndex() throws Exception {
		if (!indexExists(indexNames.index1)) createIndex(indexNames.index1);
		if (!indexExists(indexNames.index2)) createIndex(indexNames.index2);
		String aliasTarget = null;
		IndicesAliasesRequest req = null;
		if (aliasExists()) {
			aliasTarget = getTempIndex();
			req = new IndicesAliasesRequest()
					.addAliasAction(AliasActions.remove()
							.alias(indexNames.alias)
							.indices(indexNames.index1, indexNames.index2))
					.addAliasAction(AliasActions.add().alias(indexNames.alias).index(aliasTarget));
		} else {
			aliasTarget = indexNames.index1;
			req = new IndicesAliasesRequest().addAliasAction(AliasActions.add().alias(indexNames.alias).index(aliasTarget));
		}

		boolean ok = client.admin().indices().aliases(req).actionGet().isAcknowledged();

		if (!ok) throw new Exception("Failed to set " + indexNames.alias + " -> " + aliasTarget);
		System.out.println("Changed alias " + indexNames.alias + " -> " + aliasTarget);
		activeIndex = null; // will be resolved
	}

	@Override
	public void emptyTemp() throws Exception {
		String temp = getTempIndex();
		if (indexExists(temp)) {
			deleteIndex(temp);
		}
		createIndex(temp);
	}

	private void createIndex(String indexName) throws Exception {
		CreateIndexRequest req = new CreateIndexRequest(indexName)
				.settings(Settings.builder().put("mapping.total_fields.limit", 5000).put("max_result_window", 2000000));
		if (IndexNames.INDEX_TYPE_TRAITS.equals(indexType())) {
			req.settings(TRAIT_SETTINGS, XContentType.JSON);
			req.mapping(indexType(), TRAIT_MAPPING, XContentType.JSON);
		} else {
			req.mapping(indexType(), TAXA_MAPPING, XContentType.JSON);
		}
		boolean ok = client.admin().indices().create(req).actionGet().isAcknowledged();
		if (!ok) throw new Exception("Failed to create index " + indexName);
		System.out.println("Created index " + indexName);
	}

	private void deleteIndex(String indexName) throws Exception {
		boolean ok = client.admin().indices().delete(new DeleteIndexRequest(indexName)).actionGet().isAcknowledged();
		if (!ok) throw new Exception("Failed to delete index " + indexName);
		System.out.println("Deleted index " + indexName);
	}

	private void deleteAlias() throws Exception {
		boolean ok = client.admin().indices().aliases(new IndicesAliasesRequest().addAliasAction(AliasActions.remove().alias(indexNames.alias))).actionGet().isAcknowledged();
		if (!ok) throw new Exception("Failed to delete alias " + indexNames.alias);
		System.out.println("Deleted alias" + indexNames.alias);
	}

	public boolean indexExists(String indexName) {
		boolean exists= client.admin().indices().exists(new IndicesExistsRequest(indexName)).actionGet().isExists();
		return exists;
	}

	public boolean aliasExists() {
		return aliasExists(indexNames.alias);
	}

	private boolean aliasExists(String alias) {
		return client.admin().indices().aliasesExist(new GetAliasesRequest(alias)).actionGet().isExists();
	}

	private String activeIndex = null;

	public String getActiveIndex() throws Exception {
		if (activeIndex == null) {
			activeIndex = resolveActiveIndex();
		}
		return activeIndex;
	}

	private String resolveActiveIndex() throws Exception {
		ImmutableOpenMap<String, List<AliasMetaData>> aliases = client.admin().indices().getAliases(new GetAliasesRequest(indexNames.alias)).get().getAliases();
		if (aliases.isEmpty()) {
			switchTempToActiveIndex();
			return resolveActiveIndex();
		}
		if (aliases.keys().size() != 1) {
			throw new Exception("More than 1 index has alias " + indexNames.alias + ": " + aliases.keys());
		}
		String active = aliases.keys().iterator().next().value;
		System.out.println("Active index resolved to " + active);
		return active;
	}

	public String getTempIndex() throws Exception {
		if (indexNames.index1.equals(getActiveIndex())) {
			return indexNames.index2;
		}
		return indexNames.index1;
	}

	@Override
	public void deleteAll() throws Exception {
		if (indexExists(indexNames.index1)) deleteIndex(indexNames.index1);
		if (indexExists(indexNames.index2)) deleteIndex(indexNames.index2);
		if (aliasExists()) deleteAlias();
	}

	private String indexType() {
		return indexNames.indexType;
	}

	@Override
	public void waitForPushCompletion() throws WaitTimeoutException {
		if (log) System.out.println("Waiting for push completion");
		bulkProcessor.flush();
		if (log) System.out.println("Flushed");
		int maxWait = 60*3;
		while (true) {
			synchronized (LOCK) {
				if (pushedData.isEmpty()) {
					if (log) System.out.println("Push completed");
					return;
				}
			}
			if (log) System.out.println("Waiting... (" + maxWait + "s) " + pushedData.size() + " remaining");
			if (maxWait-- < 0) throw new WaitTimeoutException(pushedData);
			try { Thread.sleep(1000); } catch (InterruptedException e) {}
		}
	}

	@Override
	public List<JSONObject> search(Collection<String> aliases, Map<String, List<String>> searchParams, int pageSize, int page) {
		if (pageSize < 1) throw new IllegalArgumentException("Page size must be 1 or greater");
		if (page < 1) throw new IllegalArgumentException("Page must be 1 or greater");
		if (aliases == null || aliases.isEmpty()) return Collections.emptyList();

		List<JSONObject> result = new ArrayList<>();
		SearchSourceBuilder sourceBuilder = params(searchParams);
		sourceBuilder.size(pageSize);
		if (page != 1) {
			sourceBuilder.from((page -1)*pageSize);
		}
		sourceBuilder.sort(SortBuilders.fieldSort("id").order(SortOrder.ASC));

		SearchRequest request = new SearchRequest(aliases(aliases), sourceBuilder);
		request.indicesOptions().ignoreUnavailable();

		SearchResponse searchResponse = null;
		try {
			searchResponse = client.search(request).actionGet();
		} catch (IndexNotFoundException infe) {
			errorReporter.report("Some index was not found - falling back to resolving existing aliases", infe);
			List<String> existingAliases = aliases.stream().filter(a->aliasExists(a)).collect(Collectors.toList());
			if (existingAliases.isEmpty()) return Collections.emptyList();
			request = new SearchRequest(aliases(existingAliases), sourceBuilder);
			request.indicesOptions().ignoreUnavailable();
			searchResponse = client.search(request).actionGet();
		}
		for (SearchHit hit : searchResponse.getHits().getHits()) {
			result.add(new JSONObject(hit.getSourceAsString()));
		}
		return result;
	}

	private SearchSourceBuilder params(Map<String, List<String>> searchParams) {
		SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

		for (Map.Entry<String, List<String>> entry : searchParams.entrySet()) {
			String key = entry.getKey();
			List<String> values = entry.getValue();

			BoolQueryBuilder keyBoolQueryBuilder = QueryBuilders.boolQuery();

			for (String value : values) {
				keyBoolQueryBuilder.should(QueryBuilders.termQuery(key, value));
			}

			boolQueryBuilder.must(keyBoolQueryBuilder);
		}

		sourceBuilder.query(boolQueryBuilder);
		return sourceBuilder;
	}

	@Override
	public long searchTotal(List<String> aliases, Map<String, List<String>> searchParams) {
		if (aliases == null || aliases.isEmpty()) return 0;

		SearchSourceBuilder sourceBuilder = params(searchParams);
		sourceBuilder.size(0);

		SearchRequest request = new SearchRequest(aliases(aliases), sourceBuilder);
		request.indicesOptions().ignoreUnavailable();

		SearchResponse searchResponse = null;
		try {
			searchResponse = client.search(request).actionGet();
		} catch (IndexNotFoundException infe) {
			errorReporter.report("Some index was not found - falling back to resolving existing aliases", infe);
			List<String> existingAliases = aliases.stream().filter(a->aliasExists(a)).collect(Collectors.toList());
			if (existingAliases.isEmpty()) return 0;
			request = new SearchRequest(aliases(existingAliases), sourceBuilder);
			request.indicesOptions().ignoreUnavailable();
			searchResponse = client.search(request).actionGet();
		}
		return searchResponse.getHits().getTotalHits();
	}

	private String[] aliases(Collection<String> aliases) {
		return aliases.toArray(new String[0]);
	}

	@Override
	public void refreshSearchIndex() {
		client.admin().indices().refresh(new RefreshRequest(indexNames.alias)).actionGet();
	}

	private static final String TAXA_MAPPING = "" +
			"{" +
			"  \"" + IndexNames.INDEX_TYPE_TAXA + "\": { "+
			"    \"dynamic_templates\": [ "+
			"      { "+
			"        \"strings\": { "+
			"          \"match_mapping_type\": \"string\", "+
			"          \"mapping\": { "+
			"            \"type\": \"keyword\", "+
			"            \"ignore_above\": 512 "+
			"          } "+
			"        } "+
			"      } "+
			"    ] "+
			"  } "+
			"}";

	private static final String TRAIT_MAPPING = "" +
			"{" +
			"  \"" + IndexNames.INDEX_TYPE_TRAITS + "\": { " +
			"    \"dynamic_templates\": [ " +
			"      { " +
			"        \"strings\": { " +
			"          \"match_mapping_type\": \"string\", " +
			"          \"mapping\": { " +
			"            \"type\": \"keyword\", " +
			"            \"normalizer\": \"lowerascii\", " +
			"            \"ignore_above\": 512 " +
			"          } " +
			"        } " +
			"      } " +
			"    ], " +
			"    \"properties\": { " +
			"      \"id\": { " +
			"        \"type\": \"keyword\" " +
			"      }, " +
			"      \"subject\": { " +
			"        \"properties\": { " +
			"          \"ageYears\": { " +
			"            \"type\": \"float\" " +
			"          }, " +
			"          \"lat\": { " +
			"            \"type\": \"float\" " +
			"          }, " +
			"          \"lon\": { " +
			"            \"type\": \"float\" " +
			"          }, " +
			"          \"latMin\": { " +
			"            \"type\": \"float\" " +
			"          }, " +
			"          \"latMax\": { " +
			"            \"type\": \"float\" " +
			"          }, " +
			"          \"lonMin\": { " +
			"            \"type\": \"float\" " +
			"          }, " +
			"          \"lonMax\": { " +
			"            \"type\": \"float\" " +
			"          } " +
			"        } " +
			"      }, " +
			"      \"valueNumeric\": { " +
			"        \"type\": \"float\" " +
			"      }, " +
			"      \"measurementAccuracy\": { " +
			"        \"type\": \"float\" " +
			"      }, " +
			"      \"originalValueNumeric\": { " +
			"        \"type\": \"float\" " +
			"      }, " +
			"      \"originalMeasurementAccuracy\": { " +
			"        \"type\": \"float\" " +
			"      } " +
			"    } " +
			"  } " +
			"}";

	private static final String TRAIT_SETTINGS = "" +
			"{" +
			"    \"index\": {" +
			"      \"analysis\": {" +
			"        \"normalizer\": {" +
			"          \"lowerascii\": {" +
			"            \"type\": \"custom\"," +
			"            \"filter\": [" +
			"              \"lowercase\"," +
			"              \"asciifolding\"" +
			"            ]" +
			"          }" +
			"        }" +
			"      }" +
			"    }" +
			"}";

}
