package fi.luomus.lajitietokeskus.dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.http.HttpClientService.NotFoundException;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.models.CMSContent;
import fi.luomus.lajitietokeskus.models.CMSContent.CMSContentNode;
import fi.luomus.lajitietokeskus.models.CMSPage;
import fi.luomus.lajitietokeskus.models.News;
import fi.luomus.lajitietokeskus.models.NewsItem;
import fi.luomus.lajitietokeskus.models.PersonToken;

public class DAOImple implements DAO, AutoCloseable {

	private final Config config;
	private final ErrorReporter errorReporter;
	private final SingleObjectCache<News> cachedNews;
	private final SingleObjectCache<CMSContent> cachedCMSContent;
	private HttpClientService apiClient;

	public DAOImple(Config config, ErrorReporter errorReporter) {
		System.out.println("Lajitietokeskus DAO created");
		this.config = config;
		this.errorReporter = errorReporter;
		cachedNews = new SingleObjectCache<>(new NewsLoader(), 10, TimeUnit.MINUTES);
		cachedCMSContent = new SingleObjectCache<>(new CMSLoader(), config.stagingMode() ? 1 : 30, TimeUnit.MINUTES);
	}

	private class NewsLoader implements SingleObjectCache.CacheLoader<News> {

		private static final String LAJI_FI_WORDPRESS_SQL = "" +
				" SELECT wp_posts.id,  " +
				"       locales.name AS locale,   " +
				"       wp_posts.post_date,   " +
				"	    wp_posts.post_modified,   " +
				"       wp_users.display_name,   " +
				"	    wp_posts.post_title,   " +
				"       wp_posts.post_content,  " +
				"       other.name AS tag,  " +
				"       image.guid as featureImageUrl,   " +
				"       image.post_excerpt as featureImageCaption,   " +
				"       imagemeta.meta_value as featureImageAlt   " +
				" FROM lajitietokeskus_wp.wp_posts  " +
				" JOIN lajitietokeskus_wp.wp_users ON (wp_posts.post_author = wp_users.ID)  " +
				" JOIN lajitietokeskus_wp.wp_term_relationships localeRelationships ON  " +
				"	(localeRelationships.object_id = wp_posts.id AND localeRelationships.term_taxonomy_id IN (6,7,8))  " +
				" JOIN lajitietokeskus_wp.wp_terms locales ON   " +
				"	(locales.term_id = localeRelationships.term_taxonomy_id)  " +
				" LEFT JOIN lajitietokeskus_wp.wp_term_relationships otherRelationships ON  " +
				"	(otherRelationships.object_id = wp_posts.id AND otherRelationships.term_taxonomy_id NOT IN (6,7,8))  " +
				" LEFT JOIN lajitietokeskus_wp.wp_terms other ON   " +
				"	(other.term_id = otherRelationships.term_taxonomy_id)  " +
				" LEFT JOIN lajitietokeskus_wp.wp_postmeta meta ON (wp_posts.id = meta.post_id and meta.meta_key = '_thumbnail_id') " +
				" LEFT JOIN lajitietokeskus_wp.wp_posts image on (meta.meta_value = image.id and image.post_type = 'attachment') " +
				" LEFT JOIN lajitietokeskus_wp.wp_postmeta imagemeta ON (image.id = imagemeta.post_id and imagemeta.meta_key = '_wp_attachment_image_alt') " +
				" WHERE wp_posts.post_type = 'post'  " +
				" AND wp_posts.post_status = 'publish' " +
				" ORDER BY locale, wp_posts.post_date desc ";
		private static final String INVASIVE_WORDPRESS_SQL = "" +
				" SELECT wp_posts.id,  " +
				"       locales.name AS locale,   " +
				"       wp_posts.post_date,   " +
				"	    wp_posts.post_modified,   " +
				"       wp_users.display_name,   " +
				"	    wp_posts.post_title,   " +
				"       wp_posts.post_content,  " +
				"       image.guid as featureImageUrl,   " +
				"       image.post_excerpt as featureImageCaption,   " +
				"       imagemeta.meta_value as featureImageAlt   " +
				" FROM `vieras-cms`.wp_posts  " +
				" JOIN `vieras-cms`.wp_users ON (wp_posts.post_author = wp_users.ID)  " +
				" JOIN `vieras-cms`.wp_term_relationships localeRelationships ON  " +
				"	(localeRelationships.object_id = wp_posts.id AND localeRelationships.term_taxonomy_id IN (4,5,6))  " +
				" JOIN `vieras-cms`.wp_terms locales ON   " +
				"	(locales.term_id = localeRelationships.term_taxonomy_id)  " +
				" LEFT JOIN `vieras-cms`.wp_postmeta meta ON (wp_posts.id = meta.post_id and meta.meta_key = '_thumbnail_id') " +
				" LEFT JOIN `vieras-cms`.wp_posts image on (meta.meta_value = image.id and image.post_type = 'attachment') " +
				" LEFT JOIN `vieras-cms`.wp_postmeta imagemeta ON (image.id = imagemeta.post_id and imagemeta.meta_key = '_wp_attachment_image_alt') " +
				" WHERE wp_posts.post_type = 'post'  " +
				" AND wp_posts.post_status = 'publish' " +
				" ORDER BY locale, wp_posts.post_date desc ";

		private News previous = null;

		@Override
		public News load() {
			News loaded = loadWithTimeOut();
			if (loaded.successfullyLoaded()) {
				previous = loaded;
				return loaded;
			}
			if (previous != null && previous.successfullyLoaded()) {
				return previous;
			}
			return loaded;
		}

		private News loadWithTimeOut() {
			final News news = new News();
			ExecutorService executor = Executors.newCachedThreadPool();
			List<Callable<Void>> loaders = Arrays.asList(
					() -> { lajifi_wordpress(news); return null; },
					() -> { invasive_wordpress(news); return null; },
					() -> { luomus(news); return null; },
					//() -> { syke(news); return null; },
					() -> { luke(news); return null; }
					);
			try {
				List<Future<Void>> result = executor.invokeAll(loaders, 1, TimeUnit.MINUTES);
				if (result.stream().anyMatch(f->f.isCancelled())) {
					errorReporter.report("Loading news timed out");
					return news.loadingFailed();
				}
				return news;
			} catch (Exception e) {
				throw exceptionAndReport("Loading news with timeout", e);
			} finally {
				executor.shutdownNow();
			}
		}

		private void lajifi_wordpress(News news) {
			ConnectionDescription desc = config.connectionDescription("MySQLRead");
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = new SimpleTransactionConnection(desc);
				p = con.prepareStatement(LAJI_FI_WORDPRESS_SQL);
				rs = p.executeQuery();
				while (rs.next()) {
					String id = rs.getString(1);
					String locale = rs.getString(2);
					Timestamp posted = rs.getTimestamp(3);
					Timestamp modified = rs.getTimestamp(4);
					String author = rs.getString(5);
					String title = rs.getString(6);
					String content = rs.getString(7);
					String tag = rs.getString(8);
					if (tag == null) tag = "release";
					String featuredImage = rs.getString(9);
					String featureImageCaption = rs.getString(10);
					String featureImageAtl = rs.getString(11);
					news.addForLocale(locale,
							new NewsItem(id, posted, modified, author, title, content, tag)
							.setFeaturedImage(featuredImage, featureImageCaption, featureImageAtl));
				}
			} catch (Exception e) {
				errorReporter.report("Loading news from cms.laji.fi", e);
				news.loadingFailed();
			} finally {
				Utils.close(p, rs, con);
			}
		}

		private void invasive_wordpress(News news) {
			ConnectionDescription desc = config.connectionDescription("InvasiveMySQLRead");
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				con = new SimpleTransactionConnection(desc);
				p = con.prepareStatement(INVASIVE_WORDPRESS_SQL);
				rs = p.executeQuery();
				while (rs.next()) {
					String id = rs.getString(1);
					String locale = rs.getString(2);
					Timestamp posted = rs.getTimestamp(3);
					Timestamp modified = rs.getTimestamp(4);
					String author = rs.getString(5);
					if ("pomo".equals(author)) author = "";
					String title = rs.getString(6);
					String content = rs.getString(7);
					String featuredImage = rs.getString(8);
					String featureImageCaption = rs.getString(9);
					String featureImageAtl = rs.getString(10);
					news.addForLocale(locale,
							new NewsItem("i-"+id, posted, modified, author, title, content, "vieraslajit.fi")
							.setFeaturedImage(featuredImage, featureImageCaption, featureImageAtl));
				}
			} catch (Exception e) {
				news.loadingFailed();
				errorReporter.report("Loading news from vieras-cms.laji.fi", e);
			} finally {
				Utils.close(p, rs, con);
			}
		}

		private void luomus(News news) {
			try (HttpClientService client = new HttpClientService()) {
				JSONObject json = client.contentAsJson(new HttpGet("https://www.helsinki.fi/fi/feeds/news/json_export?page=0&topics_and_themes%5B%5D=864"));
				for (NewsItem n : LuomusNewsService.parse(json)) {
					news.addForLocale("fi", n);
				}
				json = client.contentAsJson(new HttpGet("https://www.helsinki.fi/en/feeds/news/json_export?page=0&topics_and_themes%5B%5D=864"));
				for (NewsItem n : LuomusNewsService.parse(json)) {
					news.addForLocale("en", n);
				}
				json = client.contentAsJson(new HttpGet("https://www.helsinki.fi/sv/feeds/news/json_export?page=0&topics_and_themes%5B%5D=864"));
				for (NewsItem n : LuomusNewsService.parse(json)) {
					news.addForLocale("sv", n);
				}
			} catch (Exception e) {
				news.loadingFailed();
				errorReporter.report("Loading Luomus news from Helsinki.fi", e);
			}
		}

		// Syke website has changed and currently doesn't have any fresh news about topics we'd be interested
		//		private void syke(News news) {
		//			try (HttpClientService client = new HttpClientService()) {
		//				String html = client.contentAsString(new HttpGet("https://www.syke.fi/fi-FI/Ajankohtaista/Uutiset/uutislista?n=28814&d=0&n2=vieraslajit;vedenalainen_luonto;uhanalaiset_lajit;tulokaslajit;rauhoitetut_lajit;nisakkaat;linnut;levinneisyys;lajit;lajien_suojelu;kasvit;kalat;elaimet&page=1"));
		//				for (NewsItem n : SykeNewsHTMLScraperService.scrape(html)) {
		//					news.addForLocale("fi", n);
		//				}
		//			} catch (Exception e) {
		//				news.loadingFailed();
		//				errorReporter.report("Loading news from SYKE", e);
		//			}
		//		}

		private void luke(News news) {
			try (HttpClientService client = new HttpClientService()) {
				lukeForLocale("fi", "https://www.luke.fi/fi/ajankohtaista?topics=Seuranta", news, client);
				lukeForLocale("sv", "https://www.luke.fi/sv/aktuellt?topics=Uppf%C3%B6ljning", news, client);
			} catch (Exception e) {
				news.loadingFailed();
				errorReporter.report("Loading news from LUKE", e);
			}
		}

		private void lukeForLocale(String locale, String url, News news, HttpClientService client) throws IOException, ClientProtocolException, Exception {
			String html = client.contentAsString(new HttpGet(url));
			for (NewsItem n : LukeNewsHTMLScraperService.scrape(html)) {
				news.addForLocale(locale, n);
			}
		}
	}

	@Override
	public News getNews() throws Exception {
		return cachedNews.get();
	}

	public HttpClientService getTriplestoreClient() throws Exception {
		return new HttpClientService(config.get("TriplestoreURL"), config.get("TriplestoreUsername"), config.get("TriplestorePassword"));
	}

	private class CMSLoader implements SingleObjectCache.CacheLoader<CMSContent> {

		private static final String CMS_CONTENT_TEMPLATE_SQL = "" +
				" SELECT  p.id, menu.parent_id, menu.menuorder, p.post_date, p.post_modified, u.display_name, p.post_title, p.post_content,		  " +
				"         image.guid as featureImageUrl, image.post_excerpt as featureImageCaption, imagemeta.meta_value as featureImageAlt       " +
				" FROM    [SCHEMA].wp_posts p																									  " +
				" JOIN    [SCHEMA].wp_users u ON (p.post_author = u.ID)																			  " +
				" LEFT JOIN (																													  " +
				"         SELECT parentid.meta_value AS parent_id, childid.meta_value AS child_id, childorders.menu_order AS menuorder			  " +
				"         FROM 		[SCHEMA].wp_postmeta menuparents																			  " +
				"         JOIN      [SCHEMA].wp_postmeta parentid ON (parentid.meta_key = '_menu_item_object_id' AND parentid.post_id = menuparents.meta_value)	" +
				"         JOIN      [SCHEMA].wp_postmeta childid ON (childid.meta_key = '_menu_item_object_id' AND childid.post_id = menuparents.post_id)		" +
				"         JOIN 		[SCHEMA].wp_posts childorders ON (childorders.id = childid.post_id)											  " +
				"         WHERE		menuparents.meta_key = '_menu_item_menu_item_parent'														  " +
				" ) menu ON (p.id = menu.child_id)																								  " +
				" LEFT JOIN [SCHEMA].wp_postmeta meta ON (p.id = meta.post_id and meta.meta_key = '_thumbnail_id')        						  " +
				" LEFT JOIN [SCHEMA].wp_posts image on (meta.meta_value = image.id and image.post_type = 'attachment')     						  " +
				" LEFT JOIN [SCHEMA].wp_postmeta imagemeta ON (image.id = imagemeta.post_id and imagemeta.meta_key = '_wp_attachment_image_alt')  " +
				" WHERE p.post_status = 'publish'																								  " +
				" AND p.post_type = 'page'																									      ";

		private static final String CMS_CONTENT_TAG_TEMPLATE_SQL = "" +
				" SELECT	tr.object_id as postId, t.name as tag                                                                   " +
				" FROM		[SCHEMA].wp_term_relationships tr                                                                       " +
				" JOIN		[SCHEMA].wp_term_taxonomy tt on tr.term_taxonomy_id = tt.term_taxonomy_id and tt.taxonomy = 'post_tag'  " +
				" JOIN		[SCHEMA].wp_terms t on t.term_id = tt.term_id                                                           ";

		private final String LAJI_CMS_CONTENT_SQL = initSQL("lajitietokeskus_wp");
		private final String INVASIVE_CMS_CONTENT_SQL = initSQL("`vieras-cms`");
		private final String IUCN_CMS_CONTENT_SQL = initSQL("`iucn-cms`");
		private final String EXHIB_CMS_CONTENT_SQL = initSQL("`exhib-cms`");
		private final String EXHIB_CMS_TAG_SQL = initSQL("`exhib-cms`", CMS_CONTENT_TAG_TEMPLATE_SQL);

		private String initSQL(String schema) {
			return initSQL(schema, CMS_CONTENT_TEMPLATE_SQL);
		}

		private String initSQL(String schema, String sql) {
			return sql.replace("[SCHEMA]", schema);
		}

		private CMSContent previous = null;

		@Override
		public CMSContent load() {
			CMSContent loaded = loadWithTimeOut();
			if (loaded.successfullyLoaded()) {
				previous = loaded;
				return loaded;
			}
			if (previous != null && previous.successfullyLoaded()) {
				return previous;
			}
			return loaded;
		}

		private CMSContent loadWithTimeOut() {
			final CMSContent content = new CMSContent();
			ExecutorService executor = Executors.newCachedThreadPool();
			List<Callable<Void>> loaders = Arrays.asList(
					() -> { lajiFiCMS(content); return null; },
					() -> { invasiveCMS(content); return null; },
					() -> { iucnCMS(content); return null; },
					() -> { finbifBib(content); return null; },
					() -> { exhibCMS(content); return null; }
					);
			try {
				List<Future<Void>> result = executor.invokeAll(loaders, 1, TimeUnit.MINUTES);
				if (result.stream().anyMatch(f->f.isCancelled())) {
					errorReporter.report("Loading CMS content timed out");
					return content.loadingFailed();
				}
				return content;
			} catch (Exception e) {
				throw exceptionAndReport("Loading CMS content with timeout", e);
			} finally {
				executor.shutdownNow();
			}
		}

		private void finbifBib(CMSContent cmsContent) {
			try {
				new FinBIFBibService(config).generateContent(cmsContent);
			} catch (Exception e) {
				cmsContent.loadingFailed();
				errorReporter.report("Loading finbib-bib content", e);
			}
		}

		private void iucnCMS(CMSContent cmsContent) {
			try {
				loadContent(cmsContent, IUCN_CMS_CONTENT_SQL, "IucnMySQLRead", "r-");
			} catch (Exception e) {
				cmsContent.loadingFailed();
				errorReporter.report("Loading iucn-cms.laji.fi content", e);
			}
		}

		private void invasiveCMS(CMSContent cmsContent) {
			try {
				loadContent(cmsContent, INVASIVE_CMS_CONTENT_SQL, "InvasiveMySQLRead", "i-");
			} catch (Exception e) {
				cmsContent.loadingFailed();
				errorReporter.report("Loading vieras-cms.laji.fi content", e);
			}
		}

		private void lajiFiCMS(CMSContent cmsContent) {
			try {
				loadContent(cmsContent, LAJI_CMS_CONTENT_SQL, "MySQLRead", "");
			} catch (Exception e) {
				cmsContent.loadingFailed();
				errorReporter.report("Loading cms.laji.fi content", e);
			}
		}

		private void exhibCMS(CMSContent cmsContent) {
			try {
				loadContent(cmsContent, EXHIB_CMS_CONTENT_SQL, "MySQLRead", "ex-");
				loadTags(cmsContent, EXHIB_CMS_TAG_SQL, "MySQLRead", "ex-");
			} catch (Exception e) {
				cmsContent.loadingFailed();
				errorReporter.report("Loading cms.laji.fi content", e);
			}
		}

		private void loadTags(CMSContent cmsContent, String sql, String connectionDescName, String idPrefix) throws SQLException {
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				ConnectionDescription desc = config.connectionDescription(connectionDescName);
				con = new SimpleTransactionConnection(desc);
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				while (rs.next()) {
					String postId = postId(idPrefix, rs.getString(1));
					String tag = rs.getString(2);
					if (cmsContent.contains(postId)) {
						cmsContent.getById(postId).addTag(tag);
					}
				}
			} finally {
				Utils.close(p, rs, con);
			}
		}

		private void loadContent(CMSContent cmsContent, String sql, String connectionDescName, String idPrefix) throws Exception {
			Map<String, String> parents = loadContentAndParents(cmsContent, sql, connectionDescName, idPrefix);
			setParents(cmsContent, parents);
		}

		private Map<String, String> loadContentAndParents(CMSContent cmsContent, String sql, String connectionDescName, String idPrefix) throws SQLException {
			Map<String, String> parents = new HashMap<>();
			TransactionConnection con = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			try {
				ConnectionDescription desc = config.connectionDescription(connectionDescName);
				con = new SimpleTransactionConnection(desc);
				p = con.prepareStatement(sql);
				rs = p.executeQuery();
				while (rs.next()) {
					add(cmsContent, rs, idPrefix, parents);
				}
			} finally {
				Utils.close(p, rs, con);
			}
			return parents;
		}

		private void setParents(CMSContent cmsContent, Map<String, String> parents) {
			List<String> errors = new ArrayList<>();
			for (Map.Entry<String, String> e : parents.entrySet()) {
				String childId = e.getKey();
				String parentId = e.getValue();
				CMSPage parent = cmsContent.getById(parentId);
				CMSPage child = cmsContent.getById(childId);
				if (child == null || parent == null) {
					errors.add("Should be child-parent relationship for child: " + childId + ", parent: " + parentId + " but page missing: child: " + debug(child) + ", parent: " + debug(parent));
					continue;
				}
				if (child.getCopyNumber() != null) {
					String parentCopyId = parentId+"c"+child.getCopyNumber();
					if (cmsContent.contains(parentCopyId)) {
						parentId = parentCopyId;
					}
				}
				CMSContentNode parentNode = cmsContent.getTreeNode(parentId);
				CMSContentNode childNode = cmsContent.getTreeNode(childId);
				if (parentNode == null || childNode == null) {
					errors.add("Should be child-parent relationship for child: " + childId + ", parent: " + parentId + " but tree node missing: child: " + debug(childNode, child) + ", parent: " + debug(parentNode, parent));
					continue;
				}
				parentNode.addChild(childId);
				childNode.setParentId(parentId);
			}
			if (!errors.isEmpty()) {
				throw new IllegalStateException(errors.stream().collect(Collectors.joining("\n")));
			}
		}

		private String debug(CMSContentNode node, CMSPage page) {
			return "[node:" + (node == null ? "null" : "exists") + ", page:" + debug(page) + "]";
		}

		private String debug(CMSPage page) {
			if (page == null) return "null";
			return page.getTitle();
		}

		private void add(CMSContent cmsContent, ResultSet rs, String idPrefix, Map<String, String> parents) throws SQLException {
			//p.id, menu.parent_id, menu.menuorder, p.post_date, p.post_modified, u.display_name, p.post_title, p.post_content
			String postId = rs.getString(1);
			String parentId = rs.getString(2);
			int menuOrder = rs.getString(3) == null ? Integer.MAX_VALUE : rs.getInt(3);
			Timestamp posted = rs.getTimestamp(4);
			Timestamp modified = rs.getTimestamp(5);
			String author = rs.getString(6);
			String title = rs.getString(7);
			String content = rs.getString(8);
			String featuredImage = rs.getString(9);
			String featureImageCaption = rs.getString(10);
			String featureImageAtl = rs.getString(11);

			if (postId != null) postId = postId(idPrefix, postId);
			if (parentId != null) parentId = postId(idPrefix, parentId);
			Integer copyN = null;
			if (cmsContent.contains(postId)) {
				copyN = getPostIdCopy(postId, cmsContent);
				postId += "c"+copyN;
			}
			cmsContent.add((CMSPage)new CMSPage(postId, posted, modified, author, title, content, menuOrder, copyN).setFeaturedImage(featuredImage, featureImageCaption, featureImageAtl));
			if (parentId != null) {
				parents.put(postId, parentId);
			}
		}

		private String postId(String idPrefix, String postId) {
			return idPrefix + postId;
		}

		private Integer getPostIdCopy(String postId, CMSContent cmsContent) {
			int i = 1;
			while (cmsContent.contains(postId+"c"+i)) {
				i++;
			}
			return i;
		}
	}

	@Override
	public CMSContent getCMSContent() throws Exception {
		return cachedCMSContent.get();
	}

	private HttpClientService getApiClient() {
		if (apiClient == null) {
			synchronized (this) {
				if (apiClient == null) {
					apiClient = new HttpClientService();
				}
			}
		}
		return apiClient;
	}

	private final Cached<String, String> cachedApiUsers = new Cached<>(new CacheLoader<String, String>() {

		@Override
		public String load(String accessToken) {
			try {
				return tryToLoad(accessToken);
			} catch (Exception e) {
				throw exceptionAndReport("Access tokens", e);
			}
		}

		private String tryToLoad(String accessToken) throws Exception {
			HttpClientService client = getApiClient();
			URIBuilder uri = new URIBuilder(getApiUrl() + "/api-users")
					.addParameter("access_token", getApiKey()) // our api access token
					.addParameter("accessToken", accessToken); // token to query
			JSONObject response = null;
			try {
				response = client.contentAsJson(new HttpGet(uri.getURI()));
			} catch (NotFoundException nfe) {
				return null;
			}
			String email = response.getString("email");
			if (!given(email)) return null;
			String systemId = response.getString("systemID");
			if (given(systemId)) {
				return systemId;
			}
			return email;
		}

	}, 1, TimeUnit.HOURS, 5000);

	private final Cached<String, PersonToken> cachedPersonTokens = new Cached<>(new CacheLoader<String, PersonToken>() {

		@Override
		public PersonToken load(String personToken) {
			try {
				return tryToLoad(personToken);
			} catch (Exception e) {
				throw exceptionAndReport("Person tokens", e);
			}
		}

		private PersonToken tryToLoad(String personToken) throws Exception {
			HttpClientService client = getApiClient();
			URIBuilder uri = new URIBuilder(getApiUrl() + "/person-token/"+personToken)
					.addParameter("access_token", getApiKey()); // our api access token
			JSONObject response;
			try {
				response = client.contentAsJson(new HttpGet(uri.getURI()));
			} catch (Exception e) {
				try (CloseableHttpResponse res = client.execute(new HttpGet(uri.getURI()))) {
					if (res.getStatusLine().getStatusCode() == 400) {
						return null;
					}
				}
				throw e;
			}
			String personId = response.getString("personId");
			String target = response.getString("target");
			if (!given(personId) || !given(target)) return null;
			return new PersonToken(target, personId);
		}

	}, 1, TimeUnit.HOURS, 5000);

	@Override
	public String getApiUser(String accessToken) throws IllegalArgumentException {
		if (!given(accessToken)) throw new IllegalArgumentException("Must give accessToken");
		String user = cachedApiUsers.get(accessToken);
		if (user == null) throw new IllegalArgumentException("Invalid access token");
		return user;
	}

	@Override
	public PersonToken getPersonToken(String personToken) throws IllegalArgumentException {
		if (!given(personToken)) throw new IllegalArgumentException("Must give personToken");
		PersonToken info = cachedPersonTokens.get(personToken);
		if (info == null) throw new IllegalArgumentException("Invalid person token");
		return info;
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private String getApiUrl() {
		return config.get("ApiBaseURL");
	}

	private String getApiKey() {
		return config.get("ApiKey");
	}

	private RuntimeException exceptionAndReport(String message, Exception e) {
		RuntimeException generated = new RuntimeException(message, e);
		errorReporter.report(generated);
		return generated;
	}

	@Override
	public void close() throws Exception {
		HttpClientService apiClient = getApiClient();
		if (apiClient != null) apiClient.close();
	}

}
