package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple;
import fi.luomus.commons.taxonomy.InformalTaxonGroupContainer;
import fi.luomus.commons.taxonomy.Occurrences.Occurrence;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.lajitietokeskus.taxonomy.dao.EvaluationReader;

public class EvaluationReaderTests {

	private InformalTaxonGroupContainer groupContainer;
	private InMemoryTaxonContainerImple container;
	private EvaluationReader reader;

	@Before
	public void init() {
		groupContainer = new InformalTaxonGroupContainer(groups());
		container = new InMemoryTaxonContainerImple(null, groupContainer, null);
		container.addRedListEvaluationGroupOfTaxon(new Qname("MVL.1"), EvaluationReader.BIOTA_QNAME);
		reader = new EvaluationReader(container, getTestDataFolder());
	}

	private Map<String, InformalTaxonGroup> groups() {
		Map<String, InformalTaxonGroup> map = new HashMap<>();
		map.put("MVL.1", new InformalTaxonGroup());
		return map;
	}

	@Test
	public void reader() throws Exception {
		Collection<Taxon> taxaAsCol = reader.readEvaluationData(2020, new Qname("MR.1"));

		assertEquals(4, taxaAsCol.size());
		Map<Qname, Taxon> taxa = taxaAsCol.stream().collect(Collectors.toMap(Taxon::getId, t->t));

		Taxon t1 = taxa.get(new Qname("MX.65161"));
		assertEquals("Acrocordia cavata", t1.getScientificName());
		assertEquals("MX.iucnLC", t1.getLatestRedListEvaluation().getIucnStatus());
		assertEquals(" : MKV.habitatMl : null", t1.getLatestRedListEvaluation().getPrimaryHabitat().toString());
		Map<Qname, Occurrence> t1EvalOccurrences = t1.getLatestRedListEvaluation().getOccurrences().stream().collect(Collectors.toMap(Occurrence::getArea, o->o));
		assertEquals("null : ML.693 : MX.typeOfOccurrenceOccurs : null : 2020 : true : null : null", t1EvalOccurrences.get(new Qname("ML.693")).toString());
	}

	public static File getTestDataFolder() {
		URL url = EvaluationReaderTests.class.getResource("");
		return new File(url.getFile());
	}

}
