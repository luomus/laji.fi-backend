package fi.luomus.traitdb.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOEntity;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.TSV2InputRowConverter;
import fi.luomus.traitdb.services.TraitAPI.Method;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIAuthorizedRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIEndpoint;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIResource;

public class TraitAPIRows implements TraitAPIResource {

	public static final String PASS = "pass";
	public static final String ROWS_VALIDATION = "rows";
	public static final String HEADER_VALIDATION = "header";
	public static final String DATASET_ID = "datasetId";
	public static final int MAX_ALLOWED_ROWS = 10000;
	public static final int MAX_PAGE_SIZE = 1000;

	private final TraitAPIRequest req;
	private final TraitDAOEntity<InputRow> dao;

	public TraitAPIRows(TraitAPIRequest req) {
		this.req = req;
		this.dao = req.getDAO().rows();
	}

	@Override
	public TraitAPIEndpoint endpoint() {
		String endpoint = req.getEndpoint();
		if (Method.GET == req.getMethod()) {
			if ("{id}".equals(endpoint)) return getSingle(req.getId());
			if ("search".equals(endpoint)) return getSearch(req.getParams(), req.getPagingParameters().pageSize);
		}
		if (Method.POST == req.getMethod()) {
			if ("validate".equals(endpoint)) return validateSingleInsert(req.getJson());
			if ("validate-update/{id}".equals(endpoint)) return validateUpdate(req.getId(), req.getJson());
			if ("validate-delete/{id}".equals(endpoint)) return validateDelete(req.getId());
			if ("".equals(endpoint)) return insertSingle(req.getJson());
			if ("tsv2rows/validate".equals(endpoint)) return validateTSV(req.getLines());
			if ("tsv2rows".equals(endpoint)) return convertTSV(req.getLines());
			if ("multi/validate".equals(endpoint)) return validateInsertMulti(req.getArray());
			if ("multi".equals(endpoint)) return insertMulti(req.getArray());
		}
		if (Method.PUT == req.getMethod()) {
			if ("{id}".equals(endpoint)) return update(req.getId(), req.getJson());
		}
		if (Method.DELETE == req.getMethod()) {
			if ("{id}".equals(endpoint)) return delete(req.getId());
		}
		throw req.noEndpointException();
	}

	private TraitAPIAuthorizedRequest requirePermissions(Qname datasetId, TraitAPIEndpoint endpoint) throws IllegalAccessException {
		if (req.getDAO().permissions().get(datasetId).getUserIds().contains(req.getUserId())) {
			return new TraitAPIAuthorizedRequest(endpoint);
		}
		throw new IllegalAccessException("No permissions to dataset");
	}

	private TraitAPIAuthorizedRequest requirePermissions(JSONObject json, TraitAPIEndpoint endpoint) throws IllegalAccessException {
		InputRow row = new InputRow(json);
		if (row.getSubject() == null || row.getSubject().getDatasetId() == null) throw new IllegalAccessException("Must define dataset");
		return requirePermissions(row.getSubject().getDatasetId(), endpoint);
	}

	private TraitAPIAuthorizedRequest requirePermissionsUpdate(Qname id, JSONObject json, TraitAPIEndpoint endpoint) throws IllegalAccessException {
		requirePermissions(json, endpoint);
		InputRow existingRow = dao.get(id);
		return requirePermissions(existingRow.getSubject().getDatasetId(), endpoint);
	}

	private TraitAPIAuthorizedRequest requirePermissionsMulti(JSONArray jsonArray, TraitAPIEndpoint endpoint) throws IllegalAccessException {
		Set<Qname> datasetIds = new HashSet<>();
		for (JSONObject json : jsonArray.iterateAsObject()) {
			InputRow row = new InputRow(json);
			if (row.getSubject() == null || row.getSubject().getDatasetId() == null) throw new IllegalAccessException("Must define dataset");
			datasetIds.add(row.getSubject().getDatasetId());
		}
		for (Qname datasetId : datasetIds) {
			requirePermissions(datasetId, endpoint);
		}
		return new TraitAPIAuthorizedRequest(endpoint);
	}

	private void validateIdMatches(Qname id, InputRow inputRow) {
		if (inputRow.getSubject().getId() == null || !inputRow.getSubject().getId().equals(id)) throw new IllegalArgumentException("Id in path and model do not match");
	}

	private TraitAPIEndpoint delete(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				dao.validateDelete(id).requirePass();
				InputRow row = dao.get(id);
				dao.delete(id);
				req.getDAO().search().delete(row);
				return "ok";
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				InputRow existingRow = dao.get(id);
				return requirePermissions(existingRow.getSubject().getDatasetId(), this);
			}
		};
	}

	private TraitAPIEndpoint validateDelete(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.validateDelete(id);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				InputRow existingRow = dao.get(id);
				return requirePermissions(existingRow.getSubject().getDatasetId(), this);
			}
		};
	}

	private TraitAPIEndpoint update(Qname id, JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				InputRow inputRow = new InputRow(json);
				validateIdMatches(id, inputRow);
				inputRow.getSubject().setModifiedBy(req.getUserId());
				inputRow.getSubject().setModified(new Date());
				dao.validate(inputRow, false).requirePass();
				inputRow = dao.update(inputRow);
				req.getDAO().search().update(inputRow);
				return inputRow;
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissionsUpdate(id, json, this);
			}
		};
	}

	private TraitAPIEndpoint validateUpdate(Qname id, JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				InputRow inputRow = new InputRow(json);
				validateIdMatches(id, inputRow);
				inputRow.getSubject().setModifiedBy(req.getUserId());
				inputRow.getSubject().setModified(new Date());
				return dao.validate(inputRow, false);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissionsUpdate(id, json, this);
			}
		};
	}

	private TraitAPIEndpoint insertSingle(JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				InputRow inputRow = new InputRow(json);
				inputRow.getSubject().setCreatedBy(req.getUserId());
				dao.validate(inputRow, true).requirePass();
				InputRow row = dao.insert(inputRow);
				req.getDAO().search().update(row);
				return row;
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(json, this);
			}
		};
	}

	private TraitAPIEndpoint validateSingleInsert(JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				InputRow inputRow = new InputRow(json);
				inputRow.getSubject().setCreatedBy(req.getUserId());
				return dao.validate(inputRow, true);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(json, this);
			}
		};
	}

	private TraitAPIEndpoint insertMulti(JSONArray jsonArray) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				List<InputRow> rows = new ArrayList<>();
				List<JSONObject> jsonRows = jsonArray.iterateAsObject();
				if (jsonRows.size() > MAX_ALLOWED_ROWS) throw new IllegalArgumentException("Max allowed rows is " + MAX_ALLOWED_ROWS);
				for (JSONObject json : jsonRows) {
					InputRow row = new InputRow(json);
					row.getSubject().setCreatedBy(req.getUserId());
					rows.add(row);
					dao.validate(row, true).requirePass();
				}
				dao.insert(rows);
				req.getDAO().search().update(rows);
				return "ok";
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissionsMulti(jsonArray, this);
			}
		};
	}

	private TraitAPIEndpoint validateInsertMulti(JSONArray jsonArray) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				List<InputRow> rows = new ArrayList<>();
				List<JSONObject> jsonRows = jsonArray.iterateAsObject();
				if (jsonRows.size() > MAX_ALLOWED_ROWS) throw new IllegalArgumentException("Max allowed rows is " + MAX_ALLOWED_ROWS);
				for (JSONObject json : jsonRows) {
					InputRow row = new InputRow(json);
					row.getSubject().setCreatedBy(req.getUserId());
					rows.add(row);
				}
				boolean hasErrors = false;
				JSONArray rowErrors = new JSONArray();
				for (ValidationResponse vr : dao.validate(rows)) {
					rowErrors.appendObject(vr.toJSON());
					if (!vr.ok()) hasErrors = true;
				}
				JSONObject response = new JSONObject();
				response.setBoolean(PASS, !hasErrors);
				response.setArray(ROWS_VALIDATION, rowErrors);
				return response;
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissionsMulti(jsonArray, this);
			}
		};
	}

	private TraitAPIEndpoint convertTSV(List<String> lines) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				if (lines.isEmpty()) throw new IllegalArgumentException("Must have a header row");
				if (lines.size() > 10000) throw new IllegalArgumentException("Max allowed rows is " + MAX_ALLOWED_ROWS);
				TSV2InputRowConverter converter = new TSV2InputRowConverter(getDatasetParameter(), req.getDAO());
				converter.validateHeader(lines.get(0)).requirePass();
				for (ValidationResponse rowValidation : converter.validateRows(lines)) {
					rowValidation.requirePass();
				}
				return converter.convert(lines);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(getDatasetParameter(), this);
			}
		};
	}

	private TraitAPIEndpoint validateTSV(List<String> lines) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				if (lines.isEmpty()) throw new IllegalArgumentException("Must have a header row");
				if (lines.size() > 10000) throw new IllegalArgumentException("Max allowed rows is " + MAX_ALLOWED_ROWS);
				JSONObject response = new JSONObject();

				TSV2InputRowConverter converter = new TSV2InputRowConverter(getDatasetParameter(), req.getDAO());
				ValidationResponse headerValidation = converter.validateHeader(lines.get(0));

				boolean hasErrors = !headerValidation.ok();
				JSONArray rowsArray = new JSONArray();
				for (ValidationResponse rowValidation : converter.validateRows(lines)) {
					rowsArray.appendObject(rowValidation.toJSON());
					if (!rowValidation.ok()) hasErrors = true;
				}
				response.setBoolean(PASS, !hasErrors);
				response.setObject(HEADER_VALIDATION, headerValidation.toJSON());
				response.setArray(ROWS_VALIDATION, rowsArray);
				return response;
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(getDatasetParameter(), this);
			}
		};
	}

	private TraitAPIEndpoint getSingle(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.get(id);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				InputRow row = dao.get(id);
				return requirePermissions(row.getSubject().getDatasetId(), this);
			}
		};
	}

	private TraitAPIEndpoint getSearch(Map<String, List<String>> searchParams, int limit) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				if (limit > MAX_PAGE_SIZE) throw new IllegalArgumentException("Max pageSize is " + MAX_PAGE_SIZE);
				List<String> datasetId = searchParams.remove(DATASET_ID);
				searchParams.put("subject.datasetId", datasetId);
				return dao.search(searchParams, limit);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(getDatasetParameter(), this);
			}


		};
	}

	private Qname getDatasetParameter() {
		Qname datasetId = null;
		if (req.getParams().containsKey(DATASET_ID) && !req.getParams().get(DATASET_ID).isEmpty()) {
			if (req.getParams().get(DATASET_ID).size() > 1) throw new IllegalArgumentException("Must give only one datasetId");
			datasetId = new Qname(req.getParams().get(DATASET_ID).iterator().next());
		}
		if (!given(datasetId)) throw new IllegalArgumentException("datasetId is required parameter");
		return datasetId;
	}

	private boolean given(Qname id) {
		return id != null && id.isSet();
	}

}
