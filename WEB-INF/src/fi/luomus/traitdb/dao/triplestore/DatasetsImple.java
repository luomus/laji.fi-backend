package fi.luomus.traitdb.dao.triplestore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.Param;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.dao.db.DbDAO;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;

public class DatasetsImple extends TriplestoreBaseDAO<Dataset> {

	private static final String LICENSE = "MY.intellectualRightsCC-BY";
	private final DbDAO db;

	public DatasetsImple(Config config, DbDAO db) {
		super(config);
		this.db = db;
	}

	@Override
	public ValidationResponse validate(Dataset entity, boolean insert) throws UnsupportedOperationException {
		Map<String, String> errors = entity.validate(insert, null);
		for (Dataset existing : getAll()) {
			if (existing.getId().equals(entity.getId())) continue;
			if (existing.getName().equalsIgnoreCase(entity.getName())) {
				errors.put("name", "Dataset by this name already exists");
			}
		}
		return new ValidationResponse(errors);
	}

	@Override
	protected Class<Dataset> concreteClass() {
		return Dataset.class;
	}

	@Override
	public void delete(Qname id) throws EntityNotFoundException {
		if (!validateDelete(id).getErrors().isEmpty()) throw exception("delete", id, new IllegalStateException("Can not delete dataset " + id));
		Dataset dataset = new Dataset();
		dataset.setId(id);
		DatasetPermissions permissions = new DatasetPermissions();
		permissions.setDatasetId(id);
		db.upsert(permissions);
		db.delete(dataset);
		super.delete(id);
	}

	@Override
	protected void postInsertHook(Dataset entity) {
		db.insert(entity);
	}

	@Override
	public synchronized Dataset update(Dataset entity) throws EntityNotFoundException {
		db.update(entity);
		return super.update(entity);
	}

	@Override
	protected Model createModel(Qname id, Dataset entity) {
		Model model = new Model(id);
		model.setType("MY.collection");

		model.addStatementIfObjectGiven("MY.isPartOf", new Qname("HR.5897"));
		model.addStatementIfObjectGiven("MY.collectionType", new Qname("MY.collectionTypeTrait"));
		model.addStatementIfObjectGiven("MY.collectionQuality", new Qname("MY.collectionQualityEnum3"));
		model.addStatementIfObjectGiven("MZ.owner", new Qname("MOS.1016"));
		model.addStatementIfObjectGiven("MY.metadataStatus", new Qname("MY.metadataStatusComprehensive"));
		model.addStatementIfObjectGiven("MZ.dateCreated", DateUtils.getCurrentDateTime("yyyy-MM-dd'T'HH:mm:ssZ"));
		model.addStatementIfObjectGiven("MY.language", "english");

		// MZ.editor - these are not filled in at the moment; lets see if they are required by Kotka
		// MZ.creator

		model.addStatementIfObjectGiven("MY.collectionName", entity.getName(), "en");
		model.addStatementIfObjectGiven("MY.collectionName", entity.getName(), "fi");

		model.addStatementIfObjectGiven("MY.description", entity.getDescription(), "en");
		model.addStatementIfObjectGiven("MY.description", entity.getDescription(), "en");

		model.addStatementIfObjectGiven("MY.citation", entity.getCitation());
		model.addStatementIfObjectGiven("MY.intellectualOwner", entity.getIntellectualOwner());
		model.addStatementIfObjectGiven("MY.personResponsible", entity.getPersonResponsible());
		model.addStatementIfObjectGiven("MY.metadataCreator", entity.getPersonResponsible());
		model.addStatementIfObjectGiven("MY.contactEmail", entity.getContactEmail());
		model.addStatementIfObjectGiven("MY.institutionCode", entity.getInstitutionCode());

		model.addStatementIfObjectGiven("MY.methods", entity.getMethods(), "en");
		model.addStatementIfObjectGiven("MY.taxonomicCoverage", entity.getTaxonomicCoverage(), "en");
		model.addStatementIfObjectGiven("MY.temporalCoverage", entity.getTemporalCoverage(), "en");
		model.addStatementIfObjectGiven("MY.geographicCoverage", entity.getGeographicCoverage(), "en");
		model.addStatementIfObjectGiven("MY.coverageBasis", entity.getCoverageBasis(), "en");

		model.addStatementIfObjectGiven("MY.gbifDoi", entity.getGbifDOI());
		model.addStatementIfObjectGiven("MY.doi", entity.getFinbifDOI());

		for (String adId : entity.getAdditionalIdentifiers()) {
			model.addStatementIfObjectGiven("MY.additionalIdentifier", adId);
		}

		if (entity.isShareToGBIF()) {
			model.addStatementIfObjectGiven("MY.shareToGbif", id);
		}

		model.addStatementIfObjectGiven("MY.intellectualRights", new Qname(LICENSE));
		return model;
	}

	@Override
	protected List<Dataset> getAllActual() {
		Document doc = query("search", new Param("type", "MY.collection"), new Param("predicate", "MY.collectionType"), new Param("objectresource", "MY.collectionTypeTrait"));
		List<Dataset> datasets = new ArrayList<>();
		for (Node n : doc.getRootNode().getChildNodes()) {
			Dataset dataset = new Dataset();
			dataset.setId(getId(n));

			dataset.setName(getLiteral("MY.collectionName", "en", n));
			dataset.setDescription(getLiteral("MY.description", "en", n));

			dataset.setCitation(getLiteral("MY.citation", n));
			dataset.setIntellectualOwner(getLiteral("MY.intellectualOwner", n));
			dataset.setPersonResponsible(getLiteral("MY.personResponsible", n));
			dataset.setContactEmail(getLiteral("MY.contactEmail", n));
			dataset.setInstitutionCode(getLiteral("MY.institutionCode", n));

			dataset.setMethods(getLiteral("MY.methods", n));
			dataset.setTaxonomicCoverage(getLiteral("MY.taxonomicCoverage", n));
			dataset.setTemporalCoverage(getLiteral("MY.temporalCoverage", n));
			dataset.setGeographicCoverage(getLiteral("MY.geographicCoverage", n));
			dataset.setCoverageBasis(getLiteral("MY.coverageBasis", n));

			dataset.setGbifDOI(getLiteral("MY.gbifDoi", n));
			dataset.setFinbifDOI(getLiteral("MY.doi", n));

			for (Node adIdN : n.getChildNodes("MY.additionalIdentifier")) {
				dataset.getAdditionalIdentifiers().add(adIdN.getContents());
			}
			Collections.sort(dataset.getAdditionalIdentifiers());

			dataset.setShareToGBIF(n.hasChildNodes("MY.shareToGbif"));

			datasets.add(dataset);
		}
		db.getDatasets(datasets);
		return datasets;
	}

	@Override
	public ValidationResponse validateDelete(Qname id) {
		Map<String, String> errors = new HashMap<>();
		try {
			get(id);
		} catch (EntityNotFoundException e) {
			errors.put("id", "Not found: " + id);
			return new ValidationResponse(errors);
		}
		Map<String, List<String>> params = new HashMap<>();
		params.put("subject.datasetId", Utils.singleEntryList(id.toString()));
		int count = db.search(params, 1).size();
		if (count > 0) {
			errors.put("id", "Dataset is used. Can not delete before all data has been removed.");
		}
		return new ValidationResponse(errors);
	}

}
