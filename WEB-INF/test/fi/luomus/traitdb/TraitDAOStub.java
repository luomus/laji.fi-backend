package fi.luomus.traitdb;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.models.Enumeration;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.models.UnitOfMeasurement;

public class TraitDAOStub implements TraitDAO {

	@Override
	public TraitDAOEntity<Trait> traits() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public TraitDAOEntity<TraitGroup> groups() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public TraitDAOEntity<UnitOfMeasurement> units() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public TraitDAOEntity<Dataset> datasets() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public TraitDAOEntity<DatasetPermissions> permissions() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public TraitDAOEntity<InputRow> rows() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration getEnumeration(Qname id) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public void close() throws IOException {
		// Auto-generated method stub

	}

	@Override
	public SearchDAO search() {
		// Auto-generated method stub
		return null;
	}

	public static class TraitDAOEntityStub<T> implements TraitDAOEntity<T> {

		@Override
		public T get(Qname id) throws EntityNotFoundException {
			// Auto-generated method stub
			return null;
		}

		@Override
		public ValidationResponse validateDelete(Qname id) throws UnsupportedOperationException {
			// Auto-generated method stub
			return null;
		}

		@Override
		public ValidationResponse validate(T entity, boolean insert) throws UnsupportedOperationException {
			// Auto-generated method stub
			return null;
		}

		@Override
		public T insert(T entity) throws UnsupportedOperationException {
			// Auto-generated method stub
			return null;
		}

		@Override
		public void delete(Qname id) throws EntityNotFoundException, UnsupportedOperationException {
			// Auto-generated method stub

		}

		@Override
		public List<ValidationResponse> validate(Collection<T> entities) throws UnsupportedOperationException {
			// Auto-generated method stub
			return null;
		}

		@Override
		public void insert(Collection<T> entities) throws UnsupportedOperationException {
			// Auto-generated method stub

		}

		@Override
		public T update(T entity) throws EntityNotFoundException, UnsupportedOperationException {
			// Auto-generated method stub
			return null;
		}

		@Override
		public Collection<T> getAll() throws UnsupportedOperationException {
			// Auto-generated method stub
			return null;
		}

		@Override
		public Collection<T> search(Map<String, List<String>> searchParams, int limit) throws UnsupportedOperationException {
			// Auto-generated method stub
			return null;
		}

	}

}
