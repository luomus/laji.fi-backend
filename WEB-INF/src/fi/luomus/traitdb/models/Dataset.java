package fi.luomus.traitdb.models;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

@ClassDef(triplestorePrefix="HR")
public class Dataset extends AbstractBaseEntity implements Comparable<Dataset> {

	@FieldDef(order=1.1, etsOverride="datasetID", required=true)
	private Qname id;

	@FieldDef(order=1.2, length=200, etsOverride="datasetName", required=true)
	private String name;

	@FieldDef(order=1.3, length=2000, etsOverride="datasetDescription", required=true)
	private String description;

	@FieldDef(order=1.4, length=2000, etsOverride="bibliographicCitation", required=true)
	private String citation;

	@FieldDef(order=2.1, length=200, etsOverride="rightsHolder", required=true)
	private String intellectualOwner;

	@FieldDef(order=2.2, length=200, etsOverride="author", required=true)
	private String personResponsible;

	@FieldDef(order=2.3, length=200, required=true)
	private String contactEmail;

	@FieldDef(order=2.4, length=20, ignoreInSearch=true)
	private String institutionCode;

	@FieldDef(order=3.1, length=2000, etsOverride="measurementMethod", required=true)
	private String methods;

	@FieldDef(order=3.2, length=2000, ignoreInSearch=true, required=true)
	private String taxonomicCoverage;

	@FieldDef(order=3.3, length=2000, ignoreInSearch=true, required=true)
	private String temporalCoverage;

	@FieldDef(order=3.4, length=2000, ignoreInSearch=true, required=true)
	private String geographicCoverage;

	@FieldDef(order=3.5, length=2000, etsOverride="samplingProtocol", required=true)
	private String coverageBasis;

	@FieldDef(order=4.1, length=200)
	private String finbifDOI;

	@FieldDef(order=4.2, length=200)
	private String gbifDOI;

	@FieldDef(order=4.3, length=200)
	private List<String> additionalIdentifiers;

	@FieldDef(order=5.1, ignoreInSearch=true, required=true)
	private boolean published;

	@FieldDef(order=5.2, ignoreInSearch=true, required=true)
	private boolean shareToFinBIF;

	@FieldDef(order=5.3, ignoreInSearch=true, required=true)
	private boolean shareToGBIF;

	public Dataset() {
		super();
	}

	public Dataset(JSONObject json) throws ParseException {
		super(json);
	}

	public Qname getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getCitation() {
		return citation;
	}

	public String getIntellectualOwner() {
		return intellectualOwner;
	}

	public String getPersonResponsible() {
		return personResponsible;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public String getMethods() {
		return methods;
	}

	public String getTaxonomicCoverage() {
		return taxonomicCoverage;
	}

	public String getTemporalCoverage() {
		return temporalCoverage;
	}

	public String getGeographicCoverage() {
		return geographicCoverage;
	}

	public String getCoverageBasis() {
		return coverageBasis;
	}

	public String getFinbifDOI() {
		return finbifDOI;
	}

	public String getGbifDOI() {
		return gbifDOI;
	}

	public List<String> getAdditionalIdentifiers() {
		if (additionalIdentifiers == null) additionalIdentifiers = new ArrayList<>();
		return additionalIdentifiers;
	}

	public boolean isPublished() {
		return published;
	}

	public boolean isShareToFinBIF() {
		return shareToFinBIF;
	}

	public boolean isShareToGBIF() {
		return shareToGBIF;
	}

	public void setId(Qname id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCitation(String citation) {
		this.citation = citation;
	}

	public void setIntellectualOwner(String intellectualOwner) {
		this.intellectualOwner = intellectualOwner;
	}

	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public void setMethods(String methods) {
		this.methods = methods;
	}

	public void setTaxonomicCoverage(String taxonomicCoverage) {
		this.taxonomicCoverage = taxonomicCoverage;
	}

	public void setTemporalCoverage(String temporalCoverage) {
		this.temporalCoverage = temporalCoverage;
	}

	public void setGeographicCoverage(String geographicCoverage) {
		this.geographicCoverage = geographicCoverage;
	}

	public void setCoverageBasis(String coverageBasis) {
		this.coverageBasis = coverageBasis;
	}

	public void setFinbifDOI(String finbifDOI) {
		this.finbifDOI = finbifDOI;
	}

	public void setGbifDOI(String gbifDOI) {
		this.gbifDOI = gbifDOI;
	}

	public void setAdditionalIdentifiers(List<String> additionalIdentifiers) {
		this.additionalIdentifiers = additionalIdentifiers;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public void setShareToFinBIF(boolean shareToFinBIF) {
		this.shareToFinBIF = shareToFinBIF;
	}

	public void setShareToGBIF(boolean shareToGBIF) {
		this.shareToGBIF = shareToGBIF;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalIdentifiers == null) ? 0 : additionalIdentifiers.hashCode());
		result = prime * result + ((citation == null) ? 0 : citation.hashCode());
		result = prime * result + ((contactEmail == null) ? 0 : contactEmail.hashCode());
		result = prime * result + ((coverageBasis == null) ? 0 : coverageBasis.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((finbifDOI == null) ? 0 : finbifDOI.hashCode());
		result = prime * result + ((gbifDOI == null) ? 0 : gbifDOI.hashCode());
		result = prime * result + ((geographicCoverage == null) ? 0 : geographicCoverage.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((institutionCode == null) ? 0 : institutionCode.hashCode());
		result = prime * result + ((intellectualOwner == null) ? 0 : intellectualOwner.hashCode());
		result = prime * result + ((methods == null) ? 0 : methods.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((personResponsible == null) ? 0 : personResponsible.hashCode());
		result = prime * result + (published ? 1231 : 1237);
		result = prime * result + (shareToFinBIF ? 1231 : 1237);
		result = prime * result + (shareToGBIF ? 1231 : 1237);
		result = prime * result + ((taxonomicCoverage == null) ? 0 : taxonomicCoverage.hashCode());
		result = prime * result + ((temporalCoverage == null) ? 0 : temporalCoverage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dataset other = (Dataset) obj;
		if (additionalIdentifiers == null) {
			if (other.additionalIdentifiers != null)
				return false;
		} else if (!additionalIdentifiers.equals(other.additionalIdentifiers))
			return false;
		if (citation == null) {
			if (other.citation != null)
				return false;
		} else if (!citation.equals(other.citation))
			return false;
		if (contactEmail == null) {
			if (other.contactEmail != null)
				return false;
		} else if (!contactEmail.equals(other.contactEmail))
			return false;
		if (coverageBasis == null) {
			if (other.coverageBasis != null)
				return false;
		} else if (!coverageBasis.equals(other.coverageBasis))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (finbifDOI == null) {
			if (other.finbifDOI != null)
				return false;
		} else if (!finbifDOI.equals(other.finbifDOI))
			return false;
		if (gbifDOI == null) {
			if (other.gbifDOI != null)
				return false;
		} else if (!gbifDOI.equals(other.gbifDOI))
			return false;
		if (geographicCoverage == null) {
			if (other.geographicCoverage != null)
				return false;
		} else if (!geographicCoverage.equals(other.geographicCoverage))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (institutionCode == null) {
			if (other.institutionCode != null)
				return false;
		} else if (!institutionCode.equals(other.institutionCode))
			return false;
		if (intellectualOwner == null) {
			if (other.intellectualOwner != null)
				return false;
		} else if (!intellectualOwner.equals(other.intellectualOwner))
			return false;
		if (methods == null) {
			if (other.methods != null)
				return false;
		} else if (!methods.equals(other.methods))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (personResponsible == null) {
			if (other.personResponsible != null)
				return false;
		} else if (!personResponsible.equals(other.personResponsible))
			return false;
		if (published != other.published)
			return false;
		if (shareToFinBIF != other.shareToFinBIF)
			return false;
		if (shareToGBIF != other.shareToGBIF)
			return false;
		if (taxonomicCoverage == null) {
			if (other.taxonomicCoverage != null)
				return false;
		} else if (!taxonomicCoverage.equals(other.taxonomicCoverage))
			return false;
		if (temporalCoverage == null) {
			if (other.temporalCoverage != null)
				return false;
		} else if (!temporalCoverage.equals(other.temporalCoverage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Dataset [id=" + id + ", name=" + name + ", description=" + description + ", citation=" + citation + ", intellectualOwner=" + intellectualOwner
				+ ", personResponsible=" + personResponsible + ", contactEmail=" + contactEmail + ", institutionCode=" + institutionCode + ", methods=" + methods
				+ ", taxonomicCoverage=" + taxonomicCoverage + ", temporalCoverage=" + temporalCoverage + ", geographicCoverage=" + geographicCoverage + ", coverageBasis="
				+ coverageBasis + ", finbifDOI=" + finbifDOI + ", gbifDOI=" + gbifDOI + ", additionalIdentifiers=" + additionalIdentifiers + ", published=" + published
				+ ", shareToFinBIF=" + shareToFinBIF + ", shareToGBIF=" + shareToGBIF + "]";
	}

	@Override
	public int compareTo(Dataset o) {
		return this.getName().compareTo(o.getName());
	}

}
