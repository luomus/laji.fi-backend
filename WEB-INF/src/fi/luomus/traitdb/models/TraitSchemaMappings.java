package fi.luomus.traitdb.models;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonToMap;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.models.Enumeration.EnumerationValue;
import fi.luomus.traitdb.models.search.TraitSearchRow;
import fi.luomus.traitdb.models.search.TraitTaxon;

public class TraitSchemaMappings {

	public static class TraitSearchSubject {}
	public static class TraitSearchTrait {}
	public static class TraitSearchTraitGroup {}
	public static class TraitSearchDataset {}
	public static class HigherTaxa {}

	private final TraitDAO dao;

	public TraitSchemaMappings(TraitDAO dao) {
		this.dao = dao;
	}

	public Map<Class<?>, Map<String, Object>> getMapping() {
		Map<Class<?>, Map<String, Object>> mappings = new LinkedHashMap<>();
		mappings.put(TraitGroup.class, mapping(TraitGroup.class));
		mappings.put(Trait.class, mapping(Trait.class));
		mappings.put(TraitEnumerationValue.class, mapping(TraitEnumerationValue.class));
		mappings.put(UnitOfMeasurement.class, mapping(UnitOfMeasurement.class));
		mappings.put(Dataset.class, mapping(Dataset.class));
		mappings.put(DatasetPermissions.class, mapping(DatasetPermissions.class));
		mappings.put(InputRow.class, mapping(InputRow.class));
		mappings.put(Subject.class, mapping(Subject.class));
		mappings.put(TraitValue.class, mapping(TraitValue.class));

		mappings.put(TraitSearchRow.class, mapping(TraitSearchRow.class, true));
		mappings.put(TraitTaxon.class, mapping(TraitTaxon.class));
		mappings.put(TraitSearchSubject.class, mapping(Subject.class, true));
		mappings.put(TraitSearchTrait.class, mapping(Trait.class, true));
		mappings.put(TraitSearchTraitGroup.class, mapping(TraitGroup.class, true));
		mappings.put(TraitSearchDataset.class, mapping(Dataset.class, true));
		mappings.put(HigherTaxa.class, higherTaxaMapping());
		return mappings;
	}

	private Map<String, Object> higherTaxaMapping() {
		Map<String, Object> mapping = new LinkedHashMap<>();
		Map<String, Object> entry = new LinkedHashMap<>();
		entry.put("type", "string");
		for (Qname rank : TaxonToMap.HIGHER_RANKS) {
			mapping.put(rank.toString().replace(TaxonToMap.TAXON_RANK_PREFIX, ""), entry);
		}
		return mapping;
	}

	private Map<String, Object> mapping(Class<? extends AbstractBaseEntity> entityClass) {
		return mapping(entityClass, false);
	}

	private Map<String, Object> mapping(Class<? extends AbstractBaseEntity> entityClass, boolean onlySearch) {
		Map<String, Object> mapping = new LinkedHashMap<>();
		List<String> requiredFields = new ArrayList<>();
		for (Field f : AbstractBaseEntity.getSortedFields(entityClass, onlySearch)) {
			Map<String, Object> fieldMapping = new LinkedHashMap<>();
			Class<?> fieldType = f.getType();
			FieldDef def = f.getAnnotation(FieldDef.class);
			if (def.required()) requiredFields.add(f.getName());
			boolean isCollection = Collection.class.isAssignableFrom(fieldType);
			if (fieldType == Qname.class) {
				fieldMapping.put("type", "string");
				fieldMapping.put("desc", "Qname identifier");
			} else if (fieldType == String.class || fieldType == Integer.class || fieldType == Double.class || fieldType == boolean.class || fieldType == Boolean.class) {
				fieldMapping.put("type", fieldType.getSimpleName().toLowerCase().replace("double", "number"));
			} else if (fieldType == Date.class || fieldType == DateValue.class) {
				fieldMapping.put("type", "string");
				fieldMapping.put("format", "date");
				fieldMapping.put("example", "YYYY-MM-DD");
			} else if (isCollection) {
				fieldMapping.put("type", "array");
				Class<?> collectionType = (Class<?>) ((ParameterizedType) f.getGenericType()).getActualTypeArguments()[0];
				if (collectionType == String.class || collectionType == Qname.class) {
					Map<String, Object> itemSchema = new LinkedHashMap<>();
					itemSchema.put("type", "string");
					if (!def.enumeration().isEmpty()) addEnum(itemSchema, def);
					fieldMapping.put("item-schema", itemSchema);
				} else {
					fieldMapping.put("ref", collectionType.getSimpleName());
				}
			} else {
				String ref = onlySearch ? "TraitSearch"+fieldType.getSimpleName() : fieldType.getSimpleName();
				if (fieldType == TraitTaxon.class) ref = TraitTaxon.class.getSimpleName();
				if (f.getName() == "higherTaxa") ref = HigherTaxa.class.getSimpleName();
				fieldMapping.put("ref", ref);
			}
			if (!isCollection && !def.enumeration().isEmpty()) {
				addEnum(fieldMapping, def);
			}
			mapping.put(f.getName(), fieldMapping);
		}
		if (!requiredFields.isEmpty() && !onlySearch) {
			mapping.put("required-fields", requiredFields);
		}
		return mapping;
	}

	private void addEnum(Map<String, Object> itemSchema, FieldDef def) {
		List<String> enumValues = new ArrayList<>();
		for (EnumerationValue e : dao.getEnumeration(new Qname(def.enumeration()))) {
			enumValues.add(e.getId().toString());
		}
		itemSchema.put("enum", enumValues);
		itemSchema.put("x-enum-origin", def.enumeration());
	}

}
