package fi.luomus.lajitietokeskus.services;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;

@WebServlet(urlPatterns = {"/tiedotteet/*", "/aktuellt/*", "/news/*"})
public class NewsPageServlet extends BaseServlet {

	private static final long serialVersionUID = 8959019242635068829L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String id = getLastPart(req);
		String locale = getLocale(req);
		String location = "https://laji.fi/news/"+id;
		if ("en".equals(locale)) location = "https://laji.fi/en/news/"+id;
		if ("sv".equals(locale)) location = "https://laji.fi/sv/news/"+id;
		return redirectTo(location);
	}

}
