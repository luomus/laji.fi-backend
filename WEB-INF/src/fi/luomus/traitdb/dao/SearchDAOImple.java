package fi.luomus.traitdb.dao;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAO;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAO.PushError;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAO.WaitTimeoutException;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAOImple;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAOImple.IndexNames;
import fi.luomus.lajitietokeskus.taxonomy.dao.GBIFTaxonDAO;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.SearchDAO;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOException;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.TraitValue;
import fi.luomus.traitdb.models.search.Interpreter;
import fi.luomus.traitdb.models.search.TraitSearchRow;

public class SearchDAOImple implements AutoCloseable, SearchDAO {

	private static final long KEEP_DATASET_DAO_ALIVE_MS = 60*60*1000; // 1 hour

	private final ElasticSearchDAO searchDAO;
	private final TraitDAO traitDAO;
	private final ErrorReporter errorReporter;
	private final Interpreter interpreter;
	private Thread reprocessingThread;
	private ReprocessWorker reprocessWorker;
	private final Map<Qname, ElasticSearchDAO> datasetDAOMap = new HashMap<>();
	private final Map<Qname, Long> datasetDAOLastAccessed = new HashMap<>();
	private final ScheduledExecutorService cleanupScheduler;
	private final Object DAO_ACCESS_LOCK = new Object();
	private final Config config;

	public SearchDAOImple(TraitDAO dao, TaxonomyDAO taxonomyDAO, GBIFTaxonDAO gbifTaxonDAO, Config config, ErrorReporter errorReporter){
		this.traitDAO = dao;
		this.errorReporter = errorReporter;
		this.config = config;
		this.searchDAO = new ElasticSearchDAOImple(
				config.get("ES_address"),
				config.get("ES_cluster"),
				new IndexNames(IndexNames.INDEX_TYPE_TRAITS, "search-only", "search-only", "search-only"),
				errorReporter,
				null,
				config.productionMode());
		this.interpreter = new Interpreter(traitDAO, taxonomyDAO, gbifTaxonDAO);
		this.cleanupScheduler = Executors.newScheduledThreadPool(1);
		this.cleanupScheduler.scheduleAtFixedRate(this::cleanUp, 5, 5, TimeUnit.MINUTES);
	}

	private void cleanUp() {
		long currentTimestamp = System.currentTimeMillis();
		synchronized (DAO_ACCESS_LOCK) {
			Iterator<Map.Entry<Qname, Long>> iterator = datasetDAOLastAccessed.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<Qname, Long> entry = iterator.next();
				if (currentTimestamp - entry.getValue() >= KEEP_DATASET_DAO_ALIVE_MS) {
					System.out.println("ElasticSearch DAO for " + entry.getKey() + " is scheduled to close...");
					ElasticSearchDAO dao = datasetDAOMap.remove(entry.getKey());
					if (dao != null) {
						dao.close();
					}
					iterator.remove();
				}
			}
		}
	}

	@Override
	public void close() throws Exception {
		if (reprocessWorker != null) reprocessWorker.stop();
		if (reprocessingThread != null && reprocessingThread.isAlive()) {
			try {
				reprocessingThread.join(30*1000);
			} catch (Exception e) {}
		}
		if (searchDAO != null) searchDAO.close();
		for (ElasticSearchDAO dao : datasetDAOMap.values()) {
			dao.close();
		}
	}

	private ElasticSearchDAO getDao(Qname datasetId) {
		synchronized (DAO_ACCESS_LOCK) {
			ElasticSearchDAO dao = datasetDAOMap.get(datasetId);
			if (dao == null) {
				dao = new ElasticSearchDAOImple(
						config.get("ES_address"),
						config.get("ES_cluster"),
						indiciesFor(datasetId),
						errorReporter,
						null,
						config.productionMode());
				datasetDAOMap.put(datasetId, dao);
				datasetDAOLastAccessed.put(datasetId, System.currentTimeMillis());
			} else {
				datasetDAOLastAccessed.put(datasetId, System.currentTimeMillis());
			}
			return dao;
		}
	}

	@Override
	public JSONArray search(Map<String, List<String>> searchParams, int pageSize, int page) {
		List<String> aliases = null;
		try {
			aliases = resolveSearchAliases(searchParams);
		} catch (Exception e) {
			throw exception("search:resolve-aliases", searchParams, e);
		}
		if (aliases == null || aliases.isEmpty()) return new JSONArray();

		if (searchParams == null) searchParams = Collections.emptyMap();

		try {
			List<JSONObject> hits = searchDAO.search(aliases, searchParams, pageSize, page);
			JSONArray jsonArray = new JSONArray();
			hits.forEach(jsonArray::appendObject);
			return jsonArray;
		} catch (Exception e) {
			throw exception("search", aliases + " " + searchParams, e);
		}
	}

	@Override
	public long searchTotal(Map<String, List<String>> searchParams) {
		List<String> aliases = null;
		try {
			aliases = resolveSearchAliases(searchParams);
		} catch (Exception e) {
			throw exception("search total:resolve-aliases", searchParams, e);
		}
		if (aliases == null || aliases.isEmpty()) return 0;

		if (searchParams == null) searchParams = Collections.emptyMap();

		try {
			return searchDAO.searchTotal(aliases, searchParams);
		} catch (Exception e) {
			throw exception("search total", aliases + " " + searchParams, e);
		}
	}

	@Override
	public void download(Map<String, List<String>> searchParams, int limit, OutputStream out, String filename) {
		List<String> aliases = null;
		try {
			aliases = resolveSearchAliases(searchParams);
		} catch (Exception e) {
			throw exception("download:resolve-aliases", searchParams, e);
		}
		if (aliases == null || aliases.isEmpty()) return;

		if (searchParams == null) searchParams = Collections.emptyMap();

		try (ZipOutputStream zip = new ZipOutputStream(out)) {
			zip.setLevel(Deflater.BEST_COMPRESSION);
			zip.putNextEntry(new ZipEntry(filename+".tsv"));
			zip.write(headerLine());
			int page = 1;
			int currentTotal = 0;
			List<JSONObject> res;
			while (!(res = searchDAO.search(aliases, searchParams, 1000, page)).isEmpty()) {
				for (JSONObject rowJson : res) {
					try {
						zip.write(line(rowJson));
					} catch (Exception e) {
						zip.write(error(e));
						zip.flush();
						zip.finish();
						throw e;
					}
				}
				zip.flush();
				currentTotal += res.size();
				page++;
				if (currentTotal > limit) {
					zip.write(("WARNING: Aborted because limit of " + limit + " lines was reached! There would have been more result rows. Please limit your download to smaller subset.").getBytes());
					break;
				}
			}
			zip.finish();
		} catch (Exception e) {
			throw exception("download", aliases + " " + searchParams, e);
		}
	}

	private byte[] error(Exception e) {
		return ("ERROR OCCURRED!\n" + LogUtils.buildStackTrace(e, 3)).getBytes();
	}

	private byte[] headerLine() {
		return TraitSearchRow.getTSVHeader().getBytes();
	}

	private byte[] line(JSONObject json) {
		return new TraitSearchRow(json).toTSV(traitDAO).getBytes();
	}

	private List<String> resolveSearchAliases(Map<String, List<String>> searchParams) {
		if (searchParams.containsKey("dataset.id")) {
			// Performance tweak for searches that have dataset.id parameter -- only search from indices of those datasets
			List<String> aliases = new ArrayList<>();
			for (String datasetId : searchParams.get("dataset.id")) {
				try {
					Dataset dataset = traitDAO.datasets().get(new Qname(datasetId));
					if (dataset.isPublished()) {
						aliases.add(indiciesFor(dataset.getId()).alias);
					}
				} catch (EntityNotFoundException e) {}
			}
			return aliases;
		}
		return traitDAO.datasets().getAll().stream().filter(Dataset::isPublished).map(d->indiciesFor(d.getId()).alias).collect(Collectors.toList());
	}

	private final AtomicBoolean processing = new AtomicBoolean(false);

	@Override
	public synchronized void reprocessAllDatasetsBackground() {
		boolean isNotRunning = processing.compareAndSet(false, true);
		if (!isNotRunning) return;
		reprocessWorker = new ReprocessWorker(traitDAO.datasets().getAll());
		reprocessingThread = new Thread(reprocessWorker);
		reprocessingThread.start();
	}

	private class ReprocessWorker implements Runnable {

		boolean stopped = false;
		private final Collection<Dataset> datasets;

		public ReprocessWorker(Collection<Dataset> datasets) {
			this.datasets = datasets;
		}

		public ReprocessWorker(Dataset dataset) {
			this.datasets = Utils.singleEntryList(dataset);
		}

		@Override
		public void run() {
			System.out.println("Started " + ReprocessWorker.class.getSimpleName());
			try {
				for (Dataset dataset : datasets) {
					if (stopped) break;
					reprocessDataset(dataset);
				}
			} catch (Exception e) {
				errorReporter.report("reprocess", e);
			} finally {
				processing.set(false);
			}
			System.out.println("Exiting " + ReprocessWorker.class.getSimpleName());
		}

		public void stop() {
			stopped = true;
		}

	}

	@Override
	public void reprocessDatasetBackground(Dataset dataset) {
		boolean isNotRunning = processing.compareAndSet(false, true);
		if (!isNotRunning) return;
		reprocessWorker = new ReprocessWorker(dataset);
		reprocessingThread = new Thread(reprocessWorker);
		reprocessingThread.start();
	}

	private void reprocessDataset(Dataset dataset) throws Exception {
		try {
			long start = System.currentTimeMillis();
			System.out.println("Reprocessing " + dataset.getId() + " " + dataset.getName());
			ElasticSearchDAO dao = getDao(dataset.getId());
			if (!dataset.isPublished()) {
				System.out.println(" ...unpublished, delete all");
				dao.deleteAll();
				return;
			}
			dao.emptyTemp();
			pushTemp(dataset, dao);
			validateAllCompleted(dao);
			System.out.println("No errors for " + dataset.getId() + ".. switching temp to active");
			dao.switchTempToActiveIndex();
			dao.refreshSearchIndex();
			long took = System.currentTimeMillis() - start;
			System.out.println("Completed reprocessing " + dataset.getId() + " in " + (took/1000L) + " seconds");
		} catch (Exception e) {
			System.out.println("Failed reprocessing " + dataset.getId() + "!");
			throw exception("reprocess", dataset.getId(), e);
		}
	}

	private void pushTemp(Dataset dataset, ElasticSearchDAO dao) throws Exception {
		Map<String, List<String>> searchParams = new HashMap<>();
		searchParams.put("subject.datasetId", Utils.singleEntryList(dataset.getId().toString()));
		Collection<InputRow> rows = traitDAO.rows().search(searchParams, 10000); // TODO must implement paging or even better yet, streaming
		if (rows.size() >= 9999) errorReporter.report("Dataset " + dataset.getId() + " has exceeded temporary dev phase limit of 10000 traits for reprocess all");
		for (InputRow row : rows) {
			for (TraitValue value : row.getTraits()) {
				TraitSearchRow searchRow = interpreter.get(row.getSubject(), value);
				dao.pushTemp(searchRow);
			}
		}
	}

	@Override
	public void update(InputRow row) {
		update(Utils.singleEntryList(row));
	}

	@Override
	public void update(Collection<InputRow> rows) {
		if (rows.isEmpty()) return;
		Qname datasetId = rows.iterator().next().getSubject().getDatasetId();
		if (rows.stream().anyMatch(d->!d.getSubject().getDatasetId().equals(datasetId))) throw new IllegalArgumentException("Must only push data from one dataset at a time");
		if (!traitDAO.datasets().get(datasetId).isPublished()) return;
		try {
			ElasticSearchDAO dao = getDao(datasetId);
			for (InputRow row : rows) {
				for (TraitValue value : row.getTraits()) {
					TraitSearchRow searchRow = interpreter.get(row.getSubject(), value);
					dao.pushActive(searchRow);
				}
			}
			validateAllCompleted(dao);
		} catch (Exception e) {
			errorReporter.report("push " + rows.size() + " rows for dataset " + datasetId, e);
			throw exception("push", e);
		}
	}

	@Override
	public void delete(InputRow current) {
		try {
			ElasticSearchDAO dao = getDao(current.getSubject().getDatasetId());
			for (TraitValue value : current.getTraits()) {
				dao.delete(value.getId());
			}
			validateAllCompleted(dao);
		} catch (Exception e) {
			throw exception("delete", current, e);
		}
	}

	private void validateAllCompleted(ElasticSearchDAO dao) throws WaitTimeoutException, Exception {
		List<PushError> errors = dao.getPushErrors();
		if (!errors.isEmpty()) {
			PushError first = errors.get(0);
			throw new Exception("Push has " + errors.size() +" errors", first.getRootCause());
		}
	}

	private IndexNames indiciesFor(Qname datasetId) {
		return new IndexNames(IndexNames.INDEX_TYPE_TRAITS, "trait_"+datasetId+"_1", "trait_"+datasetId+"_2", "trait_"+datasetId);
	}

	private TraitDAOException exception(String method, Object param, Exception e) {
		return new TraitDAOException(SearchDAOImple.class, method, param == null ? "null" : param.toString(), e);
	}

	private TraitDAOException exception(String method, Exception e) {
		return new TraitDAOException(SearchDAOImple.class, method, null, e);
	}

}
