package fi.luomus.lajitietokeskus.taxonomy.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonSearch;
import fi.luomus.commons.taxonomy.TaxonSearch.MatchType;
import fi.luomus.commons.taxonomy.TaxonSearch.ResponseNameMode;
import fi.luomus.commons.taxonomy.TaxonSearchResponse;
import fi.luomus.commons.taxonomy.TaxonSearchResponse.Match;
import fi.luomus.lajitietokeskus.services.BaseServlet;

@WebServlet(urlPatterns = {"/api/taxon-search/*"})
public class TaxonomySearchApiServlet extends BaseServlet {

	private static final long serialVersionUID = -2255083547782078305L;

	public static final String QUERY = "query";
	public static final String ID = "id";
	public static final String TAXON_RANK = "taxonRank";
	public static final String SCIENTIFIC_NAME = "scientificName";
	public static final String SCIENTIFIC_NAME_AUTHORSHIP = "scientificNameAuthorship";
	public static final String VERNACULAR_NAME = "vernacularName";
	public static final String FINNISH = "finnish";
	public static final String CURSIVE_NAME = "cursiveName";
	public static final String SPECIES = "species";
	public static final String INFORMAL_GROUPS = "informalGroups";
	public static final String INFORMAL_GROUP_NAME = "name";
	public static final String KINGDOM_SCIENTIFIC_NAME = "kingdomScientificName";
	public static final String TYPE = "type";

	public static final String MATCHES = "matches";
	public static final String MATCHING_NAME = "matchingName";
	public static final String NAME_TYPE = "nameType";

	public static final String ONLY_SPECIES = "onlySpecies";
	public static final String ONLY_INVASIVE = "onlyInvasive";
	public static final String ONLY_FINNISH = "onlyFinnish";
	public static final String INCLUDE_HIDDEN = "includeHidden";
	public static final String CHECKLIST = "checklist";
	public static final String REQUIRED_TAXON_SET = "taxonSet";
	public static final String REQUIRED_INFORMAL_TAXON_GROUP = "informalTaxonGroup";
	public static final String EXCLUDED_INFORMAL_TAXON_GROUP = "excludedInformalTaxonGroup";
	public static final String EXCLUDED_NAME_TYPES = "excludeNameTypes";
	public static final String INCLUDED_NAME_TYPES = "includedNameTypes";
	public static final String INCLUDED_LANGUAGES = "includedLanguages";

	public static final String MATCH_TYPE = "matchType";
	public static final String LIMIT = "limit";
	public static final int DEFAULT_LIMIT = 10;
	public static final String OBSERVATION_MODE = "observationMode";

	public static final String ERROR = "error";
	public static final String ERROR_MESSAGE = "message";
	public static final String ERROR_NAME = INFORMAL_GROUP_NAME;
	public static final String STATUS_CODE = "statusCode";

	private static enum PayloadType { JOINED_LIST, SEPARATED }
	private static final String PAYLOAD_TYPE = "payloadType";
	private static final String TRUE = "true";
	private static final String FALSE = "false";
	public static final String EXACT_MATCHES = "exactMatches";
	public static final String PARTIAL_MATCHES = "partialMatches";
	public static final String LIKELY_MATCHES = "likelyMatches";
	private static final String NULL = "null";


	@Override
	protected boolean authorized(HttpServletRequest req) {
		return true;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			TaxonSearch taxonSearch = parseTaxonSearch(req);
			PayloadType payloadType = getPayloadType(req);
			TaxonSearchResponse response = getTaxonomyDAO().search(taxonSearch);
			JSONObject json = serialize(response, taxonSearch, payloadType);
			return jsonResponse(json);
		} catch (IllegalArgumentException | UnsupportedOperationException e) {
			return errorResponse(400, res, e);
		} catch (Throwable e) {
			getErrorReporter().report(e);
			while (e instanceof RuntimeException && e.getCause() != null) {
				e = e.getCause();
			}
			return errorResponse(500, res, e);
		}
	}

	private ResponseData errorResponse(int statusCode, HttpServletResponse res, Throwable e) {
		res.setStatus(statusCode);
		JSONObject json = new JSONObject();
		json.getObject(ERROR).setInteger(STATUS_CODE, statusCode);
		json.getObject(ERROR).setString(ERROR_NAME, e.getClass().getName());
		json.getObject(ERROR).setString(ERROR_MESSAGE, e.getMessage());
		return jsonResponse(json);
	}

	private JSONObject serialize(TaxonSearchResponse response, TaxonSearch search, PayloadType payloadType) {
		if (payloadType == PayloadType.JOINED_LIST) return serializeJoinedList(response, search);
		if (payloadType == PayloadType.SEPARATED) return serializeSeparated(response);
		throw new UnsupportedOperationException("Unknown payload type " + payloadType);
	}

	private JSONObject serializeSeparated(TaxonSearchResponse response) {
		JSONObject json = new JSONObject();
		for (Match m : response.getExactMatches()) {
			json.getArray(EXACT_MATCHES).appendObject(toMatch(m));
		}
		for (Match m : response.getPartialMatches()) {
			json.getArray(PARTIAL_MATCHES).appendObject(toMatch(m));
		}
		for (Match m : response.getLikelyMatches()) {
			json.getArray(LIKELY_MATCHES).appendObject(toMatch(m));
		}
		return json;
	}

	private JSONObject serializeJoinedList(TaxonSearchResponse response, TaxonSearch search) {
		JSONObject json = new JSONObject();

		List<Match> matches = new ArrayList<>();
		matches.addAll(response.getExactMatches());

		if (matches.size() > search.getLimit()) {
			matches = matches.subList(0, search.getLimit());
		} else {
			List<Match> nonExactMatches = new ArrayList<>();
			nonExactMatches.addAll(response.getPartialMatches());
			for (Match likelyMatch : response.getLikelyMatches()) {
				if (!nonExactMatches.contains(likelyMatch)) {
					nonExactMatches.add(likelyMatch);
				}
			}
			Collections.sort(nonExactMatches, TaxonSearchResponse.MATCH_COMPARATOR);
			matches.addAll(nonExactMatches);
		}

		if (matches.size() > search.getLimit()) {
			matches = matches.subList(0, search.getLimit());
		}

		for (Match match : matches) {
			json.getArray(MATCHES).appendObject(toJoinedListMatch(match));
		}

		return json;
	}

	private JSONObject toJoinedListMatch(Match match) {
		JSONObject json = toMatch(match);
		json.setString(TYPE, toTypeString(match.getMatchType()));
		return json;
	}

	private String toTypeString(MatchType matchType) {
		if (matchType == MatchType.EXACT) return EXACT_MATCHES;
		if (matchType == MatchType.LIKELY) return LIKELY_MATCHES;
		if (matchType == MatchType.PARTIAL) return PARTIAL_MATCHES;
		throw new UnsupportedOperationException("Unknown match type " + matchType);
	}

	private JSONObject toMatch(Match match) {
		JSONObject json = new JSONObject();
		json.setString(MATCHING_NAME, match.getMatchingName());
		json.setString(NAME_TYPE, match.getNameType().toString());
		if (match.getTaxon() == null) {
			return json;
		}
		Taxon taxon = match.getTaxon();
		json.setString(ID, taxon.getId().toString());
		json.setString(SCIENTIFIC_NAME, taxon.getScientificName());
		json.setString(SCIENTIFIC_NAME_AUTHORSHIP, taxon.getScientificNameAuthorship());
		json.setString(TAXON_RANK, qname(taxon.getTaxonRank()));
		json.setBoolean(CURSIVE_NAME, taxon.isCursiveName());
		json.setBoolean(FINNISH, taxon.isFinnish());
		json.setBoolean(SPECIES, taxon.isSpecies());
		json.setObject(VERNACULAR_NAME, serialize(taxon.getVernacularName()));
		for (InformalTaxonGroup group : match.getInformalGroups()) {
			json.getArray(INFORMAL_GROUPS).appendObject(serialize(group));
		}
		json.setString(KINGDOM_SCIENTIFIC_NAME, taxon.getKingdomScientificName());
		return json;
	}

	private JSONObject serialize(InformalTaxonGroup group) {
		JSONObject json = new JSONObject();
		json.setString(ID, group.getQname().toString());
		json.setObject(INFORMAL_GROUP_NAME, serialize(group.getName()));
		return json;
	}

	private JSONObject serialize(LocalizedText vernacularName) {
		JSONObject json = new JSONObject();
		for (Map.Entry<String, String> e : vernacularName.getAllTexts().entrySet()) {
			json.setString(e.getKey(), e.getValue());
		}
		return json;
	}

	private String qname(Qname qname) {
		if (qname == null) return null;
		return qname.toString();
	}

	private TaxonSearch parseTaxonSearch(HttpServletRequest req) {
		String searchword = req.getParameter("q");
		if (!given(searchword)) searchword = req.getParameter(QUERY);
		int limit = getLimit(req);
		Qname checklist = parseChecklist(req);
		Set<Qname> requiredInformalGroups = parseRequiredInformalGroups(req);
		Set<Qname> excludedInformalGroups = parseExcludedInformalGroups(req);
		Set<Qname> taxonSets = parseTaxonSets(req);
		MatchType[] matchTypes = parseMatchTypes(req);
		Boolean species = null;
		if (TRUE.equals(req.getParameter(ONLY_SPECIES))) {
			species = true;
		}
		if (req.getParameter(SPECIES) != null) {
			species = Boolean.valueOf(req.getParameter(SPECIES));
		}
		boolean onlyFinnish = TRUE.equals(req.getParameter(ONLY_FINNISH));
		boolean onlyInvasive = TRUE.equals(req.getParameter(ONLY_INVASIVE));
		boolean includeHidden = !FALSE.equals(req.getParameter(INCLUDE_HIDDEN));

		TaxonSearch taxonSearch = new TaxonSearch(searchword, limit, checklist)
				.setOnlyFinnish(onlyFinnish)
				.setSpecies(species)
				.setOnlyInvasive(onlyInvasive)
				.setIncludeHidden(includeHidden);

		for (Qname exludedNameType : parseExludedNameTypes(req)) {
			taxonSearch.addExlucedNameType(exludedNameType);
		}
		for (Qname includedNameType : parseIncludedNameTypes(req)) {
			taxonSearch.addIncludedNameType(includedNameType);
		}
		for (String locale : parseIncludedLanguages(req)) {
			taxonSearch.addIncludedLanguage(locale);
		}
		for (Qname q : requiredInformalGroups) {
			taxonSearch.addInformalTaxonGroup(q);
		}
		for (Qname q : excludedInformalGroups) {
			taxonSearch.addExcludedInformalTaxonGroup(q);
		}
		for (Qname q : taxonSets) {
			taxonSearch.addTaxonSet(q);
		}
		if (matchTypes.length > 0) {
			taxonSearch.setMatchTypes(matchTypes);
		}
		if (TRUE.equals(req.getParameter(OBSERVATION_MODE))) {
			taxonSearch.setResponseNameMode(ResponseNameMode.OBSERVATION);
		}
		return taxonSearch;
	}

	private Collection<Qname> parseExludedNameTypes(HttpServletRequest req) {
		if (req.getParameter(EXCLUDED_NAME_TYPES) == null) return Collections.emptyList();
		String[] types = req.getParameterValues(EXCLUDED_NAME_TYPES);
		Set<Qname> set = new HashSet<>();
		for (String type : types) {
			for (String typePart : type.split(Pattern.quote(","))) {
				if (given(typePart)) set.add(new Qname(typePart));
			}
		}
		return set;
	}

	private Collection<Qname> parseIncludedNameTypes(HttpServletRequest req) {
		if (req.getParameter(INCLUDED_NAME_TYPES) == null) return Collections.emptyList();
		String[] types = req.getParameterValues(INCLUDED_NAME_TYPES);
		Set<Qname> set = new HashSet<>();
		for (String type : types) {
			for (String typePart : type.split(Pattern.quote(","))) {
				if (given(typePart)) set.add(new Qname(typePart));
			}
		}
		return set;
	}

	private Collection<String> parseIncludedLanguages(HttpServletRequest req) {
		if (req.getParameter(INCLUDED_LANGUAGES) == null) return Collections.emptyList();
		String[] locales = req.getParameterValues(INCLUDED_LANGUAGES);
		Set<String> set = new HashSet<>();
		for (String locale : locales) {
			for (String part : locale.split(Pattern.quote(","))) {
				if (given(part)) set.add(part);
			}
		}
		return set;
	}

	private PayloadType getPayloadType(HttpServletRequest req) {
		String payload = req.getParameter(PAYLOAD_TYPE);
		if (payload == null) return PayloadType.JOINED_LIST;
		return PayloadType.valueOf(payload);
	}

	private MatchType[] parseMatchTypes(HttpServletRequest req) {
		if (req.getParameter(MATCH_TYPE) == null) return new MatchType[0];
		String[] types = req.getParameterValues(MATCH_TYPE);
		List<MatchType> list = new ArrayList<>();
		for (String type : types) {
			for (String typePart : type.split(Pattern.quote(","))) {
				if (given(typePart)) list.add(MatchType.valueOf(typePart));
			}
		}
		return list.toArray(new MatchType[list.size()]);
	}

	private Set<Qname> parseExcludedInformalGroups(HttpServletRequest req) {
		return getQnames(req, EXCLUDED_INFORMAL_TAXON_GROUP);
	}

	private Set<Qname> parseRequiredInformalGroups(HttpServletRequest req) {
		return getQnames(req, REQUIRED_INFORMAL_TAXON_GROUP);
	}

	private Set<Qname> getQnames(HttpServletRequest req, String param) {
		if (req.getParameter(param) == null) return Collections.emptySet();
		String[] values = req.getParameterValues(param);
		Set<Qname> set = new HashSet<>();
		for (String value : values) {
			for (String valuePart : value.split(Pattern.quote(","))) {
				if (given(valuePart)) set.add(new Qname(valuePart));
			}
		}
		return set;
	}

	private Set<Qname> parseTaxonSets(HttpServletRequest req) {
		return getQnames(req, REQUIRED_TAXON_SET);
	}

	public static Qname parseChecklist(HttpServletRequest req) {
		String checklistParameter = req.getParameter(CHECKLIST);
		if (!given(checklistParameter)) {
			return Taxon.MASTER_CHECKLIST;
		}
		if (checklistParameter.equals(NULL)) {
			return null;
		}
		return new Qname(checklistParameter);
	}

	private int getLimit(HttpServletRequest req) {
		String limit = req.getParameter(LIMIT);
		if (limit == null) return DEFAULT_LIMIT;
		try {
			int i = Integer.valueOf(limit);
			if (i > 1000) i = 1000;
			return i;
		} catch (Exception e) {
			return DEFAULT_LIMIT;
		}
	}

}
