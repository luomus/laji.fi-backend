package fi.luomus.traitdb.models;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.laji.CoordinateConverter;
import fi.laji.CoordinateConverterProj4jImple;
import fi.laji.DegreePoint;
import fi.laji.MetricPoint;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.Enumeration.EnumerationValue;

public class TSV2InputRowConverter {

	private static final Set<String> IGNORE = Utils.set(
			"id",
			"traitId",
			"value",
			"created",
			"createdBy",
			"datasteId",
			"modified",
			"modifiedBy",
			"");

	private static final Map<String, List<String>> SYNONYMS;
	static {
		SYNONYMS = new HashMap<>();
		SYNONYMS.put("scientificName", Utils.list("verbatimScientificName"));
		SYNONYMS.put("individualCount", Utils.list("count"));
		SYNONYMS.put("coordinateAccuracy", Utils.list("accuracy", "coordacc"));
		SYNONYMS.put("occurrenceRemarks", Utils.list("notes", "remarks"));
		SYNONYMS.put("ageYears", Utils.list("age"));
		SYNONYMS.put("yearBegin", Utils.list("year"));
		SYNONYMS.put("dateBegin", Utils.list("date"));
		SYNONYMS.put("functionalGroupName", Utils.list("group", "functionalgroup"));
		SYNONYMS.put("individualcount", Utils.list("count"));
		SYNONYMS.put("lat", Utils.list("latitude", "n", "y", "decimalLatitude"));
		SYNONYMS.put("lon", Utils.list("longitude", "e", "x", "decimalLongitude"));
		SYNONYMS.put("basisOfRecord", Utils.list("recordbasis"));
		SYNONYMS.put("finbifOccurrenceId", Utils.list("finbifid"));
		SYNONYMS.put("gbifOccurrenceId", Utils.list("gbifid"));
		SYNONYMS.put("otherOccurrenceId", Utils.list("occid"));
		SYNONYMS.put("taxonId", Utils.list("finbiftaxonid"));
		SYNONYMS.put("municipality", Utils.list("county"));
		SYNONYMS.put("locality", Utils.list("verbatimlocality"));
		SYNONYMS.put("locationIdentifiers", Utils.list("locid"));
		SYNONYMS.put("measurementDeterminedBy", Utils.list("measuredby"));
		SYNONYMS.put("measurementDeterminedDate", Utils.list("measureddate"));

	}
	private static final Map<String, Method> SUBJECT_FIELDS = fields(Subject.class);
	private static final Map<String, Method> TRAIT_VALUE_FIELDS = fields(TraitValue.class);

	public static boolean isReservedName(String field) {
		if (field == null) return false;
		field = cleanHeader(field);
		return SUBJECT_FIELDS.containsKey(field) || TRAIT_VALUE_FIELDS.containsKey(field);
	}

	public static Map<String, Method> fields(Class<?> entityClass) {
		Map<String, Method> fields = new LinkedHashMap<>();
		for (Method m : ReflectionUtil.getSetters(entityClass)) {
			String field = ReflectionUtil.cleanSetterFieldName(m);
			if (IGNORE.contains(field)) continue;
			fields.put(cleanHeader(field), m);
			if (SYNONYMS.containsKey(field)) {
				for (String synonym : SYNONYMS.get(field)) {
					fields.put(cleanHeader(synonym), m);
				}
			}
		}
		return fields;
	}

	private final TraitDAO dao;
	private final Qname datasetId;

	public TSV2InputRowConverter(Qname datasetId,  TraitDAO dao) {
		this.dao = dao;
		this.datasetId = datasetId;
	}

	public ValidationResponse validateHeader(String tsvHeader) {
		Map<String, String> errors = new LinkedHashMap<>();
		Map<Integer, String> headers = parts(tsvHeader);
		TraitHeader currentTrait = null;
		Set<String> alreadyLinkedFields = new HashSet<>();
		for (Map.Entry<Integer, String> e : headers.entrySet()) {
			String header = cleanHeader(e.getValue());
			if (!given(header)) continue;
			if (commentedOutHeader(header)) continue;
			if (!SUBJECT_FIELDS.containsKey(header) && !TRAIT_VALUE_FIELDS.containsKey(header)) {
				TraitHeader trait = new TraitHeader(header, dao);
				if (trait.traitId == null) {
					errors.put(e.getKey().toString(), "Unknown header value: " + e.getValue());
				} else {
					if (given(trait.error)) {
						errors.put(e.getKey().toString(), trait.error);
					}
					currentTrait = trait;
				}
			} else if (TRAIT_VALUE_FIELDS.containsKey(header)) {
				Method m = TRAIT_VALUE_FIELDS.get(header);
				if (m.getName().equals("setObjectTaxonLifeStage")) {
					if (currentTrait != null && currentTrait.traitId != null) {
						Trait t = dao.traits().get(currentTrait.traitId);
						if (!new Qname("MX.taxon").equals(t.getRange())) {
							errors.put(e.getKey().toString(), "Only available for taxon traits");
						}
					}
				}
			} else {
				Method m = SUBJECT_FIELDS.get(header);
				if (alreadyLinkedFields.contains(m.getName())) {
					errors.put(e.getKey().toString(), e.getValue() + " maps to field that is already present in the headers: " + ReflectionUtil.cleanSetterFieldName(m));
				}
				alreadyLinkedFields.add(m.getName());
			}
		}
		return new ValidationResponse(errors);
	}

	private boolean commentedOutHeader(String header) {
		return header.startsWith("#");
	}

	public List<ValidationResponse> validateRows(List<String> data) {
		List<ValidationResponse> results = new ArrayList<>();
		if (data.isEmpty()) throw new IllegalStateException("Must have header");
		if (data.size() == 1) return Collections.emptyList();
		Map<Integer, String> header = parts(data.get(0));
		data.stream().skip(1).map(line->validate(header, line)).forEach(results::add);
		return results;
	}

	private ValidationResponse validate(Map<Integer, String> header, String line) {
		Map<String, String> errors = new LinkedHashMap<>();
		convert(header, line, errors);
		return new ValidationResponse(errors);
	}

	public List<InputRow> convert(List<String> data) {
		if (data.isEmpty()) throw new IllegalStateException("Must have header");
		if (data.size() == 1) return Collections.emptyList();
		Map<Integer, String> header = parts(data.get(0));
		List<InputRow> rows = new ArrayList<>();
		data.stream().skip(1).map(line->convert(header, line, null)).forEach(rows::add);
		return rows;
	}

	private Map<Integer, String> parts(String tsv) {
		Map<Integer, String> header = new LinkedHashMap<>();
		String[] columns = tsv.split(Pattern.quote("\t"));
		for (int i = 0; i < columns.length; i++) {
			header.put(i, columns[i].trim());
		}
		return header;
	}

	private InputRow convert(Map<Integer, String> headers, String line, Map<String, String> errors) {
		InputRow row = new InputRow();
		Subject subject = new Subject();
		subject.setDatasetId(datasetId);
		TraitValue currentTraitValue = null;
		row.setSubject(subject);
		for (Map.Entry<Integer, String> part : parts(line).entrySet()) {
			String header = headers.get(part.getKey());
			if (header == null) {
				error(part.getKey(), new IllegalArgumentException("Row value without a header"), errors);
				continue;
			}
			if (commentedOutHeader(header)) continue;
			header = cleanHeader(header);
			String value = part.getValue();
			if (header.isEmpty() || !given(value)) continue;
			if (SUBJECT_FIELDS.containsKey(header)) {
				try {
					Method m = SUBJECT_FIELDS.get(header);
					m.invoke(subject, toValue(m, value));
				} catch (IllegalAccessException | InvocationTargetException e) {
					throw new IllegalStateException(header, e);
				} catch (IllegalArgumentException | java.text.ParseException e) {
					error(part.getKey(), e, errors);
				} catch (Exception e) {
					throw new IllegalStateException("Unknown error handling " + header +"="+value, e);
				}
				continue;
			}
			TraitHeader traitHeader = new TraitHeader(header, dao);
			if (traitHeader.traitId != null) {
				if (currentTraitValue != null) {
					row.addTrait(currentTraitValue);
				}
				currentTraitValue = new TraitValue();
				currentTraitValue.setValue(value);
				currentTraitValue.setTraitId(traitHeader.traitId);
				currentTraitValue.setMeasurementAccuracy(traitHeader.accuracy);
				currentTraitValue.setStatisticalMethod(traitHeader.statisticalMethod);
				currentTraitValue.setUnit(traitHeader.unitOfMeasurement);
				continue;
			}
			if (TRAIT_VALUE_FIELDS.containsKey(header) && currentTraitValue != null) {
				try {
					Method m = TRAIT_VALUE_FIELDS.get(header);
					m.invoke(currentTraitValue, toValue(m, value));
				} catch (IllegalArgumentException | java.text.ParseException e) {
					error(part.getKey(), e, errors);
				} catch (Exception e) {
					throw new IllegalStateException("Unknown error handling " + header +"="+value, e);
				}
			}
		}
		if (currentTraitValue != null) row.addTrait(currentTraitValue);
		convertCoordinates(row);
		return row;
	}

	private void error(Integer index, Exception e, Map<String, String> errors) {
		if (errors == null) {
			throw ValidationResponse.fail(e);
		}
		errors.put(String.valueOf(index), e.getMessage());
	}

	private Object toValue(Method setter, String value) throws ParseException, NoSuchFieldException, SecurityException {
		Class<?> paramClass = setter.getParameterTypes()[0];
		if (String.class == paramClass) {
			return value;
		} else if (Qname.class == paramClass) {
			if (value.startsWith("http")) return Qname.fromURI(value);
			return toQname(setter, value);
		} else if (Integer.class == paramClass) {
			return intV(value);
		} else if (Double.class == paramClass) {
			return doubleV(value);
		} else if (Boolean.TYPE == paramClass) {
			return "true".equalsIgnoreCase(value);
		} else if (DateValue.class == paramClass) {
			return toDateValue(value);
		} else if (Date.class == paramClass) {
			return toDate(value);
		}
		throw new IllegalStateException("No serialization for " + paramClass);
	}

	private Double doubleV(String value) {
		try {
			return Double.valueOf(value.replace(",", "."));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid double: " + value);
		}
	}

	private Integer intV(String value) {
		try {
			return Integer.valueOf(value);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid integer: " + value);
		}
	}

	private Qname toQname(Method setter, String value) throws NoSuchFieldException, SecurityException {
		FieldDef def = setter.getDeclaringClass().getDeclaredField(ReflectionUtil.cleanSetterFieldName(setter)).getAnnotation(FieldDef.class);
		if (def != null) {
			if (given(def.enumeration())) {
				String cleanedValue = clean(value);
				Enumeration enumeration = dao.getEnumeration(new Qname(def.enumeration()));
				for (EnumerationValue e : enumeration.getAll()) {
					String prefixed = prefixed(e.getName());
					String suffixed = suffixed(e.getName());
					if (clean(e.getName()).equals(cleanedValue)) return e.getId();
					if (prefixed != null && prefixed.equals(cleanedValue)) return e.getId();
					if (suffixed != null && suffixed.equals(cleanedValue)) return e.getId();
					if (e.getId().toString().equalsIgnoreCase(cleanedValue)) return e.getId();
				}
				throw new IllegalArgumentException("Unknown value: " + value + ". Should be one of: " + enumeration.getAll().stream().map(e->e.getName().replace(" ", "")).collect(Collectors.joining(", ")));
			}
		}
		return new Qname(value);
	}

	private String suffixed(String s) {
		if (!s.contains("-")) return null;
		try {
			s = clean(s);
			String suffixed = s.substring(s.indexOf("-")+1, s.length()).trim();
			if (given(suffixed)) return suffixed;
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	private String prefixed(String s) {
		if (!s.contains("-")) return null;
		try {
			s = clean(s);
			String prefixed = s.substring(0, s.indexOf("-")).trim();
			if (given(prefixed)) return prefixed;
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	private String clean(String s) {
		return s.toLowerCase().replace(" ", "").replace("_", "");
	}

	private Date toDate(String value) throws ParseException {
		if (value.contains(".")) return DateUtils.convertToDate(value);
		if (value.contains("-")) return DateUtils.convertToDate(value, "yyyy-MM-dd");
		throw new IllegalArgumentException("Unknown date format: " + value);
	}

	private DateValue toDateValue(String value) {
		DateValue v = null;
		if (value.contains(".")) v = DateUtils.convertToDateValue(value);
		if (value.contains("-")) v = DateUtils.convertToDateValue(value, "yyyy-MM-dd");
		if (v == null || v.hasEmptyFields()) throw new IllegalArgumentException("Unknown date format: " + value);
		return v;
	}

	private void convertCoordinates(InputRow row) {
		if (row.getSubject().getLat() != null && row.getSubject().getLon() != null) {
			DegreePoint converted = convertCoordinates(row.getSubject().getLat(), row.getSubject().getLon());
			row.getSubject().setLat(round(converted.getLat()));
			row.getSubject().setLon(round(converted.getLon()));
		}
		if (row.getSubject().getLatMin() != null && row.getSubject().getLonMin() != null) {
			DegreePoint converted = convertCoordinates(row.getSubject().getLatMin(), row.getSubject().getLonMin());
			row.getSubject().setLatMin(round(converted.getLat()));
			row.getSubject().setLonMin(round(converted.getLon()));
		}
		if (row.getSubject().getLatMax() != null && row.getSubject().getLonMax() != null) {
			DegreePoint converted = convertCoordinates(row.getSubject().getLatMax(), row.getSubject().getLonMax());
			row.getSubject().setLatMax(round(converted.getLat()));
			row.getSubject().setLonMax(round(converted.getLon()));
		}
	}

	private static final CoordinateConverter COORD_CONVERTER = new CoordinateConverterProj4jImple();

	private DegreePoint convertCoordinates(Double lat, Double lon) {
		if (looksLikeWGS84(lat, lon)) return new DegreePoint(lat, lon);
		try {
			if (looksLikeYKJ(lat, lon)) return COORD_CONVERTER.convertFromYKJ(new MetricPoint(lat, lon)).getWgs84();
			if (looksLikeEUREF(lat, lon)) return COORD_CONVERTER.convertFromEuref(new MetricPoint(lat, lon)).getWgs84();
		} catch (Exception e) {}
		return new DegreePoint(lat, lon);
	}

	private double round(Double lat) {
		return Utils.round(lat, 6);
	}

	private static boolean looksLikeYKJ(double lat, double lon) {
		return (lat >= 6400000 && lat <= 7910000 && lon >= 2900000 && lon <= 3990000);
	}

	private static boolean looksLikeEUREF(double lat, double lon) {
		return (lat >= 6300000 && lat <= 8100000 && lon >= -100000 && lon <= 990000);
	}

	private static boolean looksLikeWGS84(double lat, double lon) {
		return (lat >= -90 && lat <= 90 && lon >= -180 && lon <= 180);
	}

	private static class TraitHeader {
		private static final Qname STATISTICAL_METHOD_ENUM = new Qname("TDF.statisticalMethodEnum");
		private final Qname traitId;
		private final Qname unitOfMeasurement;
		private final Double accuracy;
		private final Qname statisticalMethod;
		private String error = "";

		public TraitHeader(String header, TraitDAO dao) {
			String trait = null;
			String unit = null;
			String accuracy = null;
			String statisticalMethod = null;
			if (header.contains("[") && header.contains("]")) {
				String[] parts = header.substring(header.indexOf("[") + 1, header.indexOf("]")).split(",");
				if (parts.length >= 1) {
					unit = parts[0].trim();
				}
				if (parts.length >= 2) {
					accuracy = parts[1].trim();
				}
				if (parts.length >= 3) {
					statisticalMethod = parts[2].trim();
				}
				trait = header.substring(0, header.indexOf("[")).trim();
			} else {
				trait = header;
			}
			trait = cleanHeader(trait);
			this.traitId = resolveTrait(trait, dao);
			boolean unitOfMeasurementError = false;
			if (given(unit)) {
				this.unitOfMeasurement = resolveUnit(unit, dao);
				if (this.unitOfMeasurement == null) {
					error("Unknown unit of measurement: " + unit);
					unitOfMeasurementError = true;
				}
			} else {
				this.unitOfMeasurement = null;
			}
			if (given(accuracy)) {
				this.accuracy = resolveAccuracy(accuracy);
				if (this.accuracy == null) error("Invalid accuracy: " + accuracy);
			} else {
				this.accuracy = null;
			}
			if (given(statisticalMethod)) {
				this.statisticalMethod = resolveStatisticalMethod(statisticalMethod, dao);
				if (this.statisticalMethod == null) error("Unknown statistical method: " + statisticalMethod);
			} else {
				this.statisticalMethod = null;
			}
			if (!unitOfMeasurementError) {
				if (this.traitId != null) {
					if (dao.traits().get(this.traitId).getBaseUnit() != null && this.unitOfMeasurement == null) {
						error("Unit of measurement is required for this trait");
					}
				}
			}
		}

		private Qname resolveStatisticalMethod(String statisticalMethod, TraitDAO dao) {
			Enumeration enums = dao.getEnumeration(STATISTICAL_METHOD_ENUM);
			for (EnumerationValue e : enums.getAll()) {
				if (e.getName().toLowerCase().startsWith(statisticalMethod)) return e.getId();
				if (e.getId().toString().equalsIgnoreCase(statisticalMethod)) return e.getId();
			}
			return null;
		}

		private Double resolveAccuracy(String accuracy) {
			try {
				return Double.valueOf(accuracy);
			} catch (Exception e) {
				return null;
			}
		}

		private Qname resolveUnit(String unit, TraitDAO dao) {
			for (UnitOfMeasurement um : dao.units().getAll()) {
				if (um.getUnit().equalsIgnoreCase(unit)) return um.getId();
				if (um.getId().toString().equalsIgnoreCase(unit)) return um.getId();
			}
			return null;
		}

		private void error(String error) {
			if (this.error.isEmpty()) {
				this.error = error;
			} else {
				this.error += "; " + error;
			}
		}

		private Qname resolveTrait(String traitString, TraitDAO dao) {
			for (Trait trait : dao.traits().getAll()) {
				if (cleanHeader(trait.getDataEntryName()).equals(traitString)) return trait.getId();
				if (cleanHeader(trait.getName()).equalsIgnoreCase(traitString)) return trait.getId();
				if (cleanHeader(trait.getId().toString()).equalsIgnoreCase(traitString)) return trait.getId();
			}
			return null;
		}
	}

	private static String cleanHeader(String s) {
		if (s == null) return "";
		return Utils.removeWhitespace(s.toLowerCase().replace("_", ""));
	}
	private static boolean given(String s) {
		return s != null && !s.isEmpty();
	}

}
