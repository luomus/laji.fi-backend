package fi.luomus.traitdb.models;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.models.Enumeration.EnumerationValue;

public class Enumeration implements Iterable<EnumerationValue>, Comparable<Enumeration> {

	public static class EnumerationValue {
		private final Qname id;
		private final String name;
		private EnumerationValue(Qname id, String name) {
			this.id = id;
			this.name = name;
		}
		public Qname getId() {
			return id;
		}
		public String getName() {
			return name;
		}
		@Override
		public String toString() {
			return "EnumerationValue [id=" + id + ", name=" + name + "]";
		}
	}

	private final Qname id;
	private final Map<Qname, EnumerationValue> values = new LinkedHashMap<>();

	public Enumeration(Qname id) {
		this.id = id;
	}

	public Qname getId() {
		return id;
	}

	public Enumeration add(Qname id, String name) {
		this.values.put(id, new EnumerationValue(id, name));
		return this;
	}

	@Override
	public Iterator<EnumerationValue> iterator() {
		return getAll().iterator();
	}

	public EnumerationValue get(Qname id) throws EntityNotFoundException {
		EnumerationValue v = values.get(id);
		if (v == null) throw new EntityNotFoundException(id);
		return v;
	}

	public Collection<EnumerationValue> getAll() {
		return Collections.unmodifiableCollection(values.values());
	}

	@Override
	public int compareTo(Enumeration o) {
		return this.id.compareTo(o.id);
	}

}
