package fi.luomus.lajitietokeskus;

import java.io.File;
import java.net.URL;

import fi.luomus.commons.utils.FileUtils;

public class TestUtil {

	public static String getContents(Class<?> testClass, String filename) throws Exception {
		URL url = testClass.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
