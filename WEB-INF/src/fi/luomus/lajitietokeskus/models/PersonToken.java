package fi.luomus.lajitietokeskus.models;

public class PersonToken {

	private final String targetSystem;
	private final String personId;

	public PersonToken(String targetSystem, String personId) {
		this.targetSystem = targetSystem;
		this.personId = personId;
	}

	public String getTargetSystem() {
		return targetSystem;
	}

	public String getPersonId() {
		return personId;
	}

}
