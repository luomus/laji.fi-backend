package fi.luomus.lajitietokeskus.models;

import fi.luomus.commons.taxonomy.TaxonomyDAO;

public class LajitietokeskusPageResponseData extends fi.luomus.commons.services.ResponseData {

	public LajitietokeskusPageResponseData(TaxonomyDAO taxonomyDAO) throws Exception {
		super();
		this.setData("checklists", taxonomyDAO.getChecklists());
		this.setData("publications", taxonomyDAO.getPublications());
		this.setData("persons", taxonomyDAO.getPersons());
	}


}
