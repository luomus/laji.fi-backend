package fi.luomus.lajitietokeskus.services;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.lajitietokeskus.dao.JSONSerializer;
import fi.luomus.lajitietokeskus.models.CMSContent;
import fi.luomus.lajitietokeskus.models.CMSPage;

@WebServlet(urlPatterns = {"/api/CMS/*"})
public class ApiCMSServlet extends BaseServlet {

	private static final long serialVersionUID = -9000727009723721411L;

	private static final Map<String, Integer> ROOTS = initRoots();

	private static Map<String, Integer> initRoots() {
		Map<String, Integer> roots = new LinkedHashMap<>();
		roots.put("fi", 41);
		roots.put("en", 43);
		roots.put("sv", 45);
		return roots;
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String id = getLastPart(req);
		if (!given(id)) {
			id = String.valueOf(ROOTS.get(getLocale(req)));
		}

		JSONObject responseObject = new JSONObject();
		CMSContent cmsContent = getDAO().getCMSContent();

		CMSPage page = cmsContent.getById(id);
		if (page != null) {
			responseObject.setObject("page", JSONSerializer.serialize(page));
			addChildren(responseObject, cmsContent, page);
			for (CMSPage parent : cmsContent.getBreadcrumb(page)) {
				responseObject.getArray("breadcrumb").appendObject(JSONSerializer.serializeSimple(parent));
			}
		}
		for (Map.Entry<String, Integer> e : ROOTS.entrySet()) {
			responseObject.getObject("roots").setInteger(e.getKey(), e.getValue());
		}

		return jsonResponse(responseObject);
	}

	private void addChildren(JSONObject responseObject, CMSContent cmsContent, CMSPage page) {
		for (CMSPage child : cmsContent.getChildren(page)) {
			JSONObject childObject = JSONSerializer.serializeSimple(child);
			addChildren(childObject, cmsContent, child);
			responseObject.getArray("children").appendObject(childObject);

		}
	}


}
