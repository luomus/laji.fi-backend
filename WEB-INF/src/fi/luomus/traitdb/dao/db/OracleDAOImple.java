package fi.luomus.traitdb.dao.db;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.db.connectivity.ConnectionDescription;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.Utils;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOException;
import fi.luomus.traitdb.models.AbstractBaseEntity;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Subject;
import fi.luomus.traitdb.models.TraitValue;

public class OracleDAOImple implements DbDAO {

	public static HikariDataSource initDataSource(ConnectionDescription desc) {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(desc.url());
		config.setUsername(desc.username());
		config.setPassword(desc.password());
		config.setDriverClassName(desc.driver());

		config.setAutoCommit(false); // transaction mode: all changes must be committed

		config.setConnectionTimeout(30000); // 30 seconds
		config.setMaximumPoolSize(10);
		config.setIdleTimeout(60000); // 1 minute
		config.setMaxLifetime(5*60000); // 5 minutes

		return new HikariDataSource(config);
	}

	private final HikariDataSource dataSource;

	public OracleDAOImple(Config config) {
		System.out.println("Create datasource TraitDB");
		this.dataSource = initDataSource(config.connectionDescription("TraitDB"));
	}

	@Override
	public void close() {
		if (this.dataSource != null) {
			dataSource.close();
			System.out.println("Closed datasource TraitDB");
		}
	}

	private Connection openConnection() throws SQLException {
		return dataSource.getConnection();
	}

	@Override
	public List<Dataset> getDatasets(List<Dataset> datasets) throws EntityNotFoundException {
		Map<Qname, Dataset> map = datasets.stream().collect(Collectors.toMap(Dataset::getId, Function.identity()));
		PreparedStatement p = null;
		ResultSet rs = null;
		try (Connection con = openConnection()) {
			p = con.prepareStatement("SELECT id, published, share_finbif FROM dataset");
			rs = p.executeQuery();
			while (rs.next()) {
				Qname id = new Qname(rs.getString(1));
				Dataset dataset = map.get(id);
				if (dataset != null) {
					dataset.setPublished(rs.getBoolean(2));
					dataset.setShareToFinBIF(rs.getBoolean(3));
				}
			}
		} catch (Exception e) {
			throw exception(Dataset.class, "getAll", e);
		} finally {
			Utils.close(p, rs);
		}
		return datasets;
	}

	@Override
	public void insert(Dataset dataset) {
		PreparedStatement p = null;
		try (Connection con = openConnection()) {
			p = con.prepareStatement("INSERT INTO dataset (id, published, share_finbif) VALUES (?,?,?)");
			p.setString(1, dataset.getId().toString());
			p.setInt(2, dataset.isPublished() ? 1 : 0);
			p.setInt(3, dataset.isShareToFinBIF() ? 1 : 0);
			p.execute();
			con.commit();
		} catch (Exception e) {
			throw exception(Dataset.class, "insert", dataset, e);
		} finally {
			Utils.close(p);
		}
	}

	@Override
	public void update(Dataset dataset) throws EntityNotFoundException {
		int i = 0;
		PreparedStatement p = null;
		try (Connection con = openConnection()) {
			p = con.prepareStatement("UPDATE dataset SET published = ?, share_finbif = ? WHERE id = ?");
			p.setInt(1, dataset.isPublished() ? 1 : 0);
			p.setInt(2, dataset.isShareToFinBIF() ? 1 : 0);
			p.setString(3, dataset.getId().toString());
			i = p.executeUpdate();
			con.commit();
		} catch (Exception e) {
			throw exception(Dataset.class, "update", dataset, e);
		} finally {
			Utils.close(p);
		}
		if (i != 1) throw new EntityNotFoundException(dataset.getId());
	}

	@Override
	public void delete(Dataset dataset) throws EntityNotFoundException {
		int i = 0;
		PreparedStatement p = null;
		try (Connection con = openConnection()) {
			p = con.prepareStatement("DELETE FROM dataset WHERE id = ?");
			p.setString(1, dataset.getId().toString());
			i = p.executeUpdate();
			con.commit();
		} catch (Exception e) {
			throw exception(Dataset.class, "delete", dataset, e);
		} finally {
			Utils.close(p);
		}
		if (i != 1) throw new EntityNotFoundException(dataset.getId());
	}

	@Override
	public DatasetPermissions getPermissions(Qname datasetId) throws EntityNotFoundException {
		PreparedStatement p = null;
		ResultSet rs = null;
		DatasetPermissions permissions = null;
		try (Connection con = openConnection()) {
			p = con.prepareStatement("SELECT user_id FROM datasetpermission WHERE dataset_id = ?");
			p.setString(1, datasetId.toString());
			rs = p.executeQuery();
			while (rs.next()) {
				if (permissions == null) {
					permissions = new DatasetPermissions();
					permissions.setDatasetId(datasetId);
				}
				String userId = rs.getString(1);
				if (!permissions.getUserIds().contains(userId)) permissions.getUserIds().add(userId);
			}
		} catch (Exception e) {
			throw exception(DatasetPermissions.class, "get", datasetId, e);
		} finally {
			Utils.close(p, rs);
		}
		if (permissions == null) return null;
		Collections.sort(permissions.getUserIds());
		return permissions;
	}

	@Override
	public void upsert(DatasetPermissions datasetPermissions) {
		PreparedStatement delStatement = null;
		PreparedStatement insertStatement = null;
		try (Connection con = openConnection()) {
			delStatement = con.prepareStatement("DELETE FROM datasetpermission WHERE dataset_id = ?");
			insertStatement = con.prepareStatement("INSERT INTO datasetpermission (dataset_id, user_id) VALUES (?, ?)");
			delStatement.setString(1, datasetPermissions.getDatasetId().toString());
			delStatement.execute();
			insertStatement.setString(1, datasetPermissions.getDatasetId().toString());
			for (String userId : datasetPermissions.getUserIds()) {
				insertStatement.setString(2, userId);
				insertStatement.execute();
			}
			con.commit();
		} catch (Exception e) {
			throw exception(DatasetPermissions.class, "upsert", datasetPermissions, e);
		} finally {
			Utils.close(delStatement);
			Utils.close(insertStatement);
		}
	}

	@Override
	public List<DatasetPermissions> getPermissions() {
		PreparedStatement p = null;
		ResultSet rs = null;
		Map<Qname, DatasetPermissions> permissions = new LinkedHashMap<>();
		try (Connection con = openConnection()) {
			p = con.prepareStatement("SELECT dataset_id, user_id FROM datasetpermission ORDER BY dataset_id, user_id");
			rs = p.executeQuery();
			while (rs.next()) {
				Qname datasetId = new Qname(rs.getString(1));
				String userid = rs.getString(2);
				if (!permissions.containsKey(datasetId)) {
					DatasetPermissions perm = new DatasetPermissions();
					perm.setDatasetId(datasetId);
					permissions.put(datasetId, perm);
				}
				permissions.get(datasetId).getUserIds().add(userid);
			}
		} catch (Exception e) {
			throw exception(DatasetPermissions.class, "getAll", e);
		} finally {
			Utils.close(p, rs);
		}
		permissions.values().forEach(dp->Collections.sort(dp.getUserIds()));
		return new ArrayList<>(permissions.values());
	}

	public static final String SUBJECT_INSERT_SQL = insertSQL(Subject.class);
	public static final String TRAIT_VALUE_INSERT_SQL = insertSQL(TraitValue.class, "subject_id");

	@Override
	public void insert(Collection<InputRow> rows) {
		PreparedStatement subjectInsert = null;
		PreparedStatement traitValueInsert = null;
		ResultSet subjectKeysRs = null;
		ResultSet traitValueKeysRs = null;
		try (Connection con = openConnection()) {
			subjectInsert = con.prepareStatement(SUBJECT_INSERT_SQL, new String[]{"id", "created"});
			traitValueInsert = con.prepareStatement(TRAIT_VALUE_INSERT_SQL, new String[]{"id"});
			for (InputRow row : rows) {
				values(true, subjectInsert, row.getSubject());
				subjectInsert.addBatch();
			}
			int[] res = subjectInsert.executeBatch();
			for (int i = 0; i < res.length; i++) {
				if (res[i] < 0) throw new SQLException("Subject batch row " + i +" failed with error code " + res[i]);
			}
			subjectKeysRs = subjectInsert.getGeneratedKeys();
			for (InputRow row : rows) {
				subjectKeysRs.next();
				long id = subjectKeysRs.getLong(1);
				Date created = new Date(subjectKeysRs.getDate(2).getTime());
				row.getSubject().setId(rowid(id));
				row.getSubject().setCreated(created);
				for (TraitValue v : row.getTraits()) {
					values(true, traitValueInsert, v, id);
					traitValueInsert.addBatch();
				}
			}
			res = traitValueInsert.executeBatch();
			traitValueKeysRs = traitValueInsert.getGeneratedKeys();
			for (int i = 0; i < res.length; i++) {
				if (res[i] < 0) throw new SQLException("Trait value batch row " + i +" failed with error code " + res[i]);
			}
			for (InputRow row : rows) {
				for (TraitValue v : row.getTraits()) {
					traitValueKeysRs.next();
					long id = traitValueKeysRs.getLong(1);
					v.setId(rowid(id));
				}
			}
			con.commit();
		} catch (Exception e) {
			throw exception(InputRow.class, "insertMulti", e);
		} finally {
			Utils.close(traitValueInsert, traitValueKeysRs);
			Utils.close(subjectInsert, subjectKeysRs);
		}
	}

	private void values(boolean insert, PreparedStatement p, AbstractBaseEntity object, Object...additionalValues) throws SQLException {
		int i = 1;
		for (Map.Entry<String, Method> e : ReflectionUtil.getGettersMap(object.getClass()).entrySet()) {
			if (e.getKey().equals("id") || e.getKey().equals("created")) continue;
			if (!insert) {
				if (e.getKey().equals("datasetId") || e.getKey().equals("createdBy")) continue;
			}
			try {
				Object value = e.getValue().invoke(object);
				i = setValue(p, i, value);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
				throw new IllegalStateException(e.getKey(), ex);
			} catch (IllegalStateException exx) {
				throw new IllegalStateException(e.getKey());
			}
		}
		for (Object value : additionalValues) {
			i = setValue(p, i, value);
		}
	}

	private int setValue(PreparedStatement p, int i, Object value) throws SQLException {
		if (value == null) {
			p.setString(i++, null);
		} else if (value instanceof String || value instanceof Qname) {
			p.setString(i++, value.toString());
		} else if (value instanceof Integer) {
			p.setInt(i++, (int)value);
		} else if (value instanceof Double) {
			p.setDouble(i++, (double)value);
		} else if (value instanceof Long) {
			p.setLong(i++, (long)value);
		} else if (value instanceof Date) {
			p.setDate(i++, new java.sql.Date(((Date)value).getTime()));
		} else if (value instanceof DateValue) {
			p.setDate(i++, new java.sql.Date(DateUtils.convertToDate((DateValue)value).getTime()));
		} else if (value instanceof Boolean) {
			p.setInt(i++, ((boolean)value) ? 1 : 0);
		} else {
			throw new IllegalStateException();
		}
		return i;
	}

	private static String insertSQL(Class<? extends AbstractBaseEntity> entityClass, String...additionalFields) {
		StringBuilder b = new StringBuilder();
		b.append("INSERT INTO ").append(entityClass.getSimpleName().toLowerCase()).append(" ( ");
		List<String> fields = new ArrayList<>(ReflectionUtil.getGettersMap(entityClass).keySet());
		for (String field : additionalFields) {
			fields.add(field);
		}
		fields.removeIf(f->f.equals("id") || f.equals("created"));
		Iterator<String> i = fields.iterator();
		StringBuilder b2 = new StringBuilder();
		while (i.hasNext()) {
			b.append(camelcaseToUnderscore(i.next()));
			b2.append("?");
			if (i.hasNext()) {
				b.append(",");
				b2.append(",");
			}
		}
		b.append(" ) VALUES ( ").append(b2).append(" ) ");
		return b.toString();
	}

	public static final String SUBJECT_UPDATE_SQL = updateSQL(Subject.class);
	public static final String TRAIT_VALUE_UPDATE_SQL = updateSQL(TraitValue.class);

	@Override
	public void update(InputRow row) throws EntityNotFoundException {
		PreparedStatement subjectUpdate = null;
		PreparedStatement traitValueInsert = null;
		PreparedStatement traitValueUpdate = null;
		ResultSet traitValueKeysRs = null;
		try (Connection con = openConnection()) {
			subjectUpdate = con.prepareStatement(SUBJECT_UPDATE_SQL);
			traitValueUpdate = con.prepareStatement(TRAIT_VALUE_UPDATE_SQL);
			traitValueInsert = con.prepareStatement(TRAIT_VALUE_INSERT_SQL, new String[]{"id"});
			values(false, subjectUpdate, row.getSubject(), rowid(row.getSubject().getId()));
			subjectUpdate.execute();
			boolean hadInserts = false;
			boolean hadUpdates = false;
			for (TraitValue v : row.getTraits()) {
				if (v.getId() == null) {
					values(true, traitValueInsert, v, rowid(row.getSubject().getId()));
					traitValueInsert.addBatch();
					hadInserts = true;
				} else {
					values(false, traitValueUpdate, v, rowid(v.getId()));
					traitValueUpdate.addBatch();
					hadUpdates = true;
				}
			}
			if (hadInserts) {
				int[] res = traitValueInsert.executeBatch();
				for (int i = 0; i < res.length; i++) {
					if (res[i] < 0) throw new SQLException("Trait value insert batch row " + i +" failed with error code " + res[i]);
				}
				traitValueKeysRs = traitValueInsert.getGeneratedKeys();
				for (TraitValue v : row.getTraits()) {
					if (v.getId() != null) continue;
					traitValueKeysRs.next();
					long id = traitValueKeysRs.getLong(1);
					v.setId(rowid(id));
				}
			}
			if (hadUpdates) {
				int[] res = traitValueUpdate.executeBatch();
				for (int i = 0; i < res.length; i++) {
					if (res[i] < 0) throw new SQLException("Trait value update batch row " + i +" failed with error code " + res[i]);
				}
			}
			con.commit();
		} catch (Exception e) {
			throw exception(InputRow.class, "update", row, e);
		} finally {
			Utils.close(traitValueInsert, traitValueKeysRs);
			Utils.close(traitValueUpdate);
			Utils.close(subjectUpdate);
		}
	}

	private static String updateSQL(Class<? extends AbstractBaseEntity> entityClass) {
		StringBuilder b = new StringBuilder();
		b.append("UPDATE ").append(entityClass.getSimpleName().toLowerCase()).append(" SET ");
		List<String> fields = new ArrayList<>(ReflectionUtil.getGettersMap(entityClass).keySet());
		fields.removeIf(f->f.equals("id") || f.equals("datasetId") || f.equals("created") || f.equals("createdBy"));
		Iterator<String> i = fields.iterator();
		while (i.hasNext()) {
			b.append(camelcaseToUnderscore(i.next())).append("=?");
			if (i.hasNext()) {
				b.append(", ");
			}
		}
		b.append(" WHERE id = ?");
		return b.toString();
	}

	@Override
	public void delete(InputRow row) throws EntityNotFoundException {
		int i = 0;
		PreparedStatement p = null;
		try (Connection con = openConnection()) {
			p = con.prepareStatement("DELETE FROM subject WHERE id = ?");
			p.setLong(1, rowid(row.getSubject().getId()));
			i = p.executeUpdate();
			con.commit();
		} catch (Exception e) {
			throw exception(InputRow.class, "delete", row.getSubject().getId(), e);
		} finally {
			Utils.close(p);
		}
		if (i != 1) throw new EntityNotFoundException(row.getSubject().getId());
	}

	@Override
	public List<InputRow> search(Map<String, List<String>> searchParams, int limit) {
		if (searchParams.isEmpty()) throw new IllegalArgumentException("Must provide some search parameters");
		List<InputRow> rows = new ArrayList<>();
		InputRow current = null;
		PreparedStatement p = null;
		ResultSet rs = null;
		try (Connection con = openConnection()) {
			p = con.prepareStatement(searchSQL(searchParams, limit));
			int i = 1;
			for (Map.Entry<String, List<String>> e : searchParams.entrySet()) {
				for (String value : e.getValue()) {
					i = setSearchValue(p, i, e.getKey(), value);
				}
			}
			rs = p.executeQuery();
			while (rs.next()) {
				Index ix = new Index();
				Subject subject = parseSubject(rs, ix);
				TraitValue value = parseTraitValue(rs, ix);
				if (current == null || !current.getSubject().equals(subject)) {
					InputRow row = new InputRow(subject);
					rows.add(row);
					current = row;
				}
				current.addTrait(value);
			}
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (Exception e) {
			throw exception(InputRow.class, "search", searchParams, e);
		} finally {
			Utils.close(p, rs);
		}
		return rows;
	}

	private int setSearchValue(PreparedStatement p, int i, String searchField, String value) throws NoSuchMethodException, SQLException {
		if (value == null) return i;
		if (searchField.endsWith(".id")) {
			p.setLong(i, rowid(value));
			return i++;
		}
		Class<?> fieldClass = null;
		if (searchField.startsWith("subject.")) {
			fieldClass = Subject.class;
			searchField = searchField.replace("subject.", "");
		} else {
			fieldClass = TraitValue.class;
			searchField = searchField.replace("traits.", "");
		}
		Method m = ReflectionUtil.getGetter(searchField, fieldClass);
		Class<?> valueClass = m.getReturnType();
		try {
			if (valueClass == String.class || valueClass == Qname.class) {
				p.setString(i++, value);
				return i;
			}
			if (valueClass == DateValue.class || valueClass == Date.class) {
				p.setDate(i++, new java.sql.Date(DateUtils.convertToDate(value, "yyyy-MM-dd").getTime()));
				return i;
			}
			if (valueClass == Integer.class) {
				p.setInt(i++, Integer.valueOf(value));
				return i;
			}
			if (valueClass == Double.class) {
				p.setDouble(i++, Double.valueOf(value.replace(",", ".")));
				return i;
			}
			if (valueClass == Boolean.class || valueClass == boolean.class) {
				p.setInt(i++, value.equals("true") ? 1 : 0);
				return i;
			}
		} catch (Exception e) {
			throw new IllegalStateException(searchField + " " + i, e);
		}
		throw new IllegalStateException(searchField);
	}

	private static class Index {
		int i = 1;
	}

	private TraitValue parseTraitValue(ResultSet rs, Index ix) {
		TraitValue value = new TraitValue();
		for (Method setter : ReflectionUtil.getSetters(TraitValue.class)) {
			try {
				setter.invoke(value, getValue(setter, rs, ix));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new IllegalStateException(setter.getName(), e);
			}
		}
		return value;
	}

	private Subject parseSubject(ResultSet rs, Index ix) {
		Subject subject = new Subject();
		for (Method setter : ReflectionUtil.getSetters(Subject.class)) {
			try {
				setter.invoke(subject, getValue(setter, rs, ix));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new IllegalStateException(setter.getName(), e);
			}
		}
		return subject;
	}

	private Object getValue(Method setter, ResultSet rs, Index ix) {
		int i = ix.i;
		ix.i++;
		try {
			if (setter.getName().equals("setId")) {
				return rowid(rs.getLong(i));
			}
			Class<?> valueClass = setter.getParameterTypes()[0];
			if (valueClass == String.class) {
				return rs.getString(i);
			}
			if (valueClass == Qname.class) {
				String s = rs.getString(i);
				if (s == null) return null;
				return new Qname(s);
			}
			if (valueClass == DateValue.class) {
				String s = rs.getString(i);
				if (s == null) return null;
				return DateUtils.convertToDateValue(new Date(rs.getDate(i).getTime()));
			}
			if (valueClass == Date.class) {
				String s = rs.getString(i);
				if (s == null) return null;
				return new Date(rs.getDate(i).getTime());
			}
			if (valueClass == Integer.class) {
				String s = rs.getString(i);
				if (s == null) return null;
				return Integer.valueOf(s);
			}
			if (valueClass == Double.class) {
				String s = rs.getString(i);
				if (s == null) return null;
				return Double.valueOf(s);
			}
			if (valueClass == Boolean.class || valueClass == boolean.class) {
				return rs.getInt(i) > 0;
			}
		} catch (Exception e) {
			throw new IllegalStateException(setter.getName() + " " + i, e);
		}
		throw new IllegalStateException(setter.getName());
	}

	public static String searchSQL(Map<String, List<String>> searchParams, int limit) {
		StringBuilder b = new StringBuilder();
		b.append("SELECT " );
		Set<String> allowedFields = new HashSet<>();
		Iterator<String> subjectFields = ReflectionUtil.getSettersMap(Subject.class).keySet().iterator();
		while (subjectFields.hasNext()) {
			String field = subjectFields.next();
			b.append("subject.").append(camelcaseToUnderscore(field)).append(",");
			allowedFields.add("subject."+field);
		}
		Iterator<String> valueFields = ReflectionUtil.getSettersMap(TraitValue.class).keySet().iterator();
		while (valueFields.hasNext()) {
			String field = valueFields.next();
			b.append("traits.").append(camelcaseToUnderscore(field));
			allowedFields.add("traits."+field);
			if (valueFields.hasNext()) b.append(",");
		}
		b.append(" FROM subject JOIN traitvalue traits ON subject.id = traits.subject_id WHERE 1=1");
		for (Map.Entry<String, List<String>> e : searchParams.entrySet()) {
			if (allowedFields.contains(e.getKey())) {
				if (e.getValue() == null || e.getValue().isEmpty()) {
					b.append(" AND ").append(camelcaseToUnderscore(e.getKey())).append(" IS NULL");
				} else if (e.getKey().equals("subject.created")) {
					b.append(" AND TRUNC(").append(camelcaseToUnderscore(e.getKey())).append(")");
					in(b, e.getValue());
				} else {
					b.append(" AND ").append(camelcaseToUnderscore(e.getKey()));
					in(b, e.getValue());
				}
			} else {
				throw new IllegalArgumentException("Invalid search parameter " + e.getKey());
			}
		}
		b.append(" ORDER BY subject.id, traits.id ");
		b.append("FETCH FIRST ").append(limit).append(" ROWS ONLY");
		return b.toString();
	}

	private static void in(StringBuilder b, List<String> values) {
		b.append(" IN(");
		for (int i = 0; i < values.size(); i++) {
			if (i > 0) {
				b.append(",");
			}
			b.append("?");
		}
		b.append(")");
	}

	private Qname rowid(long id) {
		return new Qname("TD." + id);
	}

	private long rowid(Qname id) {
		return rowid(id.toString());
	}

	private long rowid(String id) {
		try {
			return Long.valueOf(id.replace("TD.", ""));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid id");
		}
	}

	private static String camelcaseToUnderscore(String field) {
		return fi.luomus.lajitietokeskus.util.Utils.camelcaseToUnderscore(field);
	}

	private TraitDAOException exception(Class<?> entityClass, String method, Exception e) {
		return new TraitDAOException(entityClass, method, null, e);
	}

	private TraitDAOException exception(Class<?> entityClass, String method, Object param, Exception e) {
		return new TraitDAOException(entityClass, method, param == null ? "null" : param.toString(), e);
	}

}
