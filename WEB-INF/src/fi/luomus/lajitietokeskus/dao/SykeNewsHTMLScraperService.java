package fi.luomus.lajitietokeskus.dao;

import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.models.ExternalNewsItem;
import fi.luomus.lajitietokeskus.models.NewsItem;

public class SykeNewsHTMLScraperService {

	public static List<NewsItem> scrape(String html) throws Exception {
		String newsTable = Utils.list(html.split(Pattern.quote("<table ")))
				.stream()
				.filter(table->Utils.removeWhitespace(table).contains("<td>Julkaistu</td>"))
				.findFirst()
				.orElse(null);
		if (newsTable == null) throw new Exception("Can't find Julkaistu-table");

		newsTable = "<table " + newsTable.substring(0, newsTable.indexOf("</table>")) + "</table>";

		Document doc = Jsoup.parse(newsTable);
		Elements rows = doc.select("table tr:not(.header-row)"); // Select all rows except the header row

		List<NewsItem> news = new ArrayList<>();

		for (Element row : rows) {
			String link = row.select("a[href]").attr("href");
			String title = row.select("a[href]").text().trim();
			String date = row.select("td:last-child").text().trim();
			news.add(newsItem(link, title, date));
		}
		return news;
	}

	private static NewsItem newsItem(String link, String title, String date) throws Exception {
		String id = link.replace("/fi-FI/content/", "");
		Timestamp posted = toTimestamp(date);
		URL url = new URL("https://www.syke.fi"+link);
		return new ExternalNewsItem(id, posted, title, "syke.fi", url);
	}

	private static Timestamp toTimestamp(String s) throws ParseException {
		return new Timestamp(DateUtils.convertToDate(s).getTime());
	}

}
