package fi.luomus.traitdb.models;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

@ClassDef(triplestorePrefix="TDF")
public class TraitEnumerationValue extends AbstractBaseEntity implements Comparable<TraitEnumerationValue> {

	@FieldDef(order=1, required=true)
	private Qname id;

	@FieldDef(order=2, length=50, required=true)
	private String dataEntryName;

	@FieldDef(order=3, length=200, required=true)
	private String name;

	@FieldDef(order=4, length=2000, required=true)
	private String description;

	private int order;

	public TraitEnumerationValue() {
		super();
	}

	public TraitEnumerationValue(JSONObject json) throws ParseException {
		super(json);
	}

	public Qname getId() {
		return id;
	}

	public String getDataEntryName() {
		return dataEntryName;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getOrder() {
		return order;
	}

	public void setId(Qname id) {
		this.id = id;
	}

	public void setDataEntryName(String dataEntryName) {
		this.dataEntryName = dataEntryName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataEntryName == null) ? 0 : dataEntryName.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + order;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TraitEnumerationValue other = (TraitEnumerationValue) obj;
		if (dataEntryName == null) {
			if (other.dataEntryName != null)
				return false;
		} else if (!dataEntryName.equals(other.dataEntryName))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (order != other.order)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TraitEnumerationValue [id=" + id + ", dataEntryName=" + dataEntryName + ", name=" + name + ", description=" + description + ", order=" + order + "]";
	}

	@Override
	public int compareTo(TraitEnumerationValue o) {
		return Integer.compare(this.getOrder(), o.getOrder());
	}

}
