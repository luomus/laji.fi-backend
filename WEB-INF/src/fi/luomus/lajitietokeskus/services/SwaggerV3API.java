package fi.luomus.lajitietokeskus.services;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.SingleObjectCache.CacheLoader;
import fi.luomus.lajitietokeskus.util.SwaggerV3APIDescriptionGenerator;
import fi.luomus.traitdb.models.TraitSchemaMappings;

@WebServlet(urlPatterns = {"/api/openapi-v3.json"})
public class SwaggerV3API extends BaseServlet {

	private static final long serialVersionUID = -2716137359580153152L;

	private SingleObjectCache<JSONObject> cache = new SingleObjectCache<>(loader(), 1, TimeUnit.HOURS);

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return jsonResponse(cache.get());
	}

	private CacheLoader<JSONObject> loader() {
		return new CacheLoader<JSONObject>() {
			@Override
			public JSONObject load() {
				Map<Class<?>, Map<String, Object>> taxonMappings = getTaxonomyDAO().getTaxonPropertyMapping();
				Map<Class<?>, Map<String, Object>> traitMappings = new TraitSchemaMappings(getTraitDAO()).getMapping();
				return SwaggerV3APIDescriptionGenerator.generateSwaggerDescription(taxonMappings, traitMappings);
			}
		};
	}

}
