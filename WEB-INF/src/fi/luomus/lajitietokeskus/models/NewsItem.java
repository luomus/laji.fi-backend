package fi.luomus.lajitietokeskus.models;

import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.safety.Whitelist;

import fi.luomus.commons.utils.Utils;

public class NewsItem implements Comparable<NewsItem> {

	public static final String ALLOWED_TAGS = "p, a, img, b, strong, i, em, ul, ol, li, table, tr, th, td, tbody, thead, caption, hr, h1, h2, h3, h4, h5, h6, blockquote, pre, code, article, address, section, header, footer, figure, figcaption, dl, dt, dd, iframe, time, span, link";
	private static final Whitelist WHITELIST;
	private static final Document.OutputSettings OUTPUT_SETTINGS = new Document.OutputSettings().prettyPrint(false).escapeMode(EscapeMode.xhtml);

	static {
		WHITELIST = Whitelist.none()
				.addAttributes("img", "src", "alt", "style", "width", "height", "id", "class", "lang")
				.addAttributes("a", "href", "target", "id", "class", "itemprop", "lang")
				.addAttributes("table", "id", "class", "lang")
				.addAttributes("h1", "id", "class", "itemprop", "lang")
				.addAttributes("h2", "id", "class", "itemprop", "lang")
				.addAttributes("h3", "id", "class", "itemprop", "lang")
				.addAttributes("h4", "id", "class", "itemprop", "lang")
				.addAttributes("h5", "id", "class", "itemprop", "lang")
				.addAttributes("h6", "id", "class", "itemprop", "lang")
				.addAttributes("ol", "style", "id", "class", "lang")
				.addAttributes("ul", "style", "id", "class", "lang")
				.addAttributes("pre", "id", "class", "lang")
				.addAttributes("p", "id", "class", "itemprop", "itemscope", "itemtype", "lang")
				.addAttributes("figure", "id", "class", "lang")
				.addAttributes("iframe", "id", "class", "src", "style", "width", "height", "frameborder", "scrolling", "lang")
				.addAttributes("article", "id", "class", "itemscope", "itemtype", "lang")
				.addAttributes("address", "id", "class", "itemprop", "lang")
				.addAttributes("header", "id", "class", "lang")
				.addAttributes("footer", "id", "class", "lang")
				.addAttributes("time", "id", "class", "datetime", "itemprop", "lang")
				.addAttributes("span", "id", "class", "itemprop", "itemscope", "itemtype", "lang")
				.addAttributes("link", "href", "target", "id", "class", "itemprop", "lang");
		for (String tag : ALLOWED_TAGS.split(Pattern.quote(","))) {
			tag = tag.trim();
			WHITELIST.addTags(tag);
		}
	}

	private final String id;
	private final Timestamp posted;
	private final Timestamp modified;
	private final String author;
	private final String title;
	private final String content;
	private final String tag;
	private String featuredImage;
	private String featuredImageCaption;
	private String featuredImageAlt;
	private List<String> tags;

	public NewsItem(String id, Timestamp posted, Timestamp modified, String author, String title, String content, String tag) {
		this.id = id;
		this.posted = posted;
		this.modified = modified;
		this.author = author;
		this.title = title;
		this.content = htmlContent(content);
		this.tag = tag;
	}

	private String htmlContent(String content) {
		if (content == null) return "";
		content = Jsoup.clean(content, "", WHITELIST, OUTPUT_SETTINGS)
				.replace("<p></p>", "")
				.replace("<p>&#xa0;</p>", "")
				.replace("\t", "")
				.replace("\n", " ")
				.replace("\r", "")
				.replace("http://", "https://")
				.trim();
		while (content.contains("  ")) {
			content = content.replace("  ", " ");
		}
		return content;
	}

	public Timestamp getPosted() {
		return posted;
	}

	public Timestamp getModified() {
		return modified;
	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	@Override
	public int compareTo(NewsItem other) {
		int c = other.getPosted().compareTo(this.getPosted());
		if (c != 0) return c;
		return this.getId().compareTo(other.getId());
	}

	public String getId() {
		return id;
	}

	public boolean hasBeenModified() {
		return !this.getPosted().equals(this.getModified());
	}

	public String getTag() {
		return tag;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof NewsItem) {
			return this.getId().equals(((NewsItem)o).getId());
		}
		throw new UnsupportedOperationException("Can only compare to other " + this.getClass().getName());
	}

	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}

	@Override
	public String toString() {
		return Utils.debugS(id, tag, title);
	}

	public URL getExternalUrl() {
		throw new UnsupportedOperationException();
	}

	public boolean isExternalNewsItem() {
		return false;
	}

	public String getFeaturedImage() {
		return featuredImage;
	}

	public String getFeaturedImageCaption() {
		return featuredImageCaption;
	}

	public String getFeaturedImageAlt() {
		return featuredImageAlt;
	}

	public NewsItem setFeaturedImage(String imageUrl, String caption, String alt) {
		this.featuredImage = imageUrl;
		this.featuredImageCaption = caption;
		this.featuredImageAlt = alt;
		return this;
	}

	public NewsItem addTag(String tag) {
		if (tags == null) tags = new ArrayList<>();
		tags.add(tag);
		return this;
	}

	public List<String> getTags() {
		if (tags == null) return Collections.emptyList();
		return tags;
	}

}
