package fi.luomus.lajitietokeskus.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CMSContent {

	public static class CMSContentNode {
		private final CMSPage page;
		private String parentId = null;
		private final List<String> childIds = new ArrayList<>();
		public CMSContentNode(CMSPage page) {
			this.page = page;
		}
		public CMSPage getPage() {
			return page;
		}
		public String getParentId() {
			return parentId;
		}
		public void setParentId(String parentId) {
			this.parentId = parentId;
		}
		public List<String> getChildIds() {
			return childIds;
		}
		public void addChild(String childId) {
			this.childIds.add(childId);
		}
		public boolean hasParent() {
			return parentId != null;
		}
	}

	private final Map<String, CMSContentNode> contents = new HashMap<>();
	private boolean loadingFailed = false;

	public CMSPage getById(String id) {
		if (contents.containsKey(id)) {
			return contents.get(id).getPage();
		}
		return null;
	}

	public List<CMSPage> getBreadcrumb(CMSPage page) {
		List<CMSPage> breadcrumb = getBreadcrumb(page.getId());
		Collections.reverse(breadcrumb);
		return breadcrumb;
	}

	private List<CMSPage> getBreadcrumb(String id) {
		List<CMSPage> breadCrumb = new ArrayList<>();
		CMSContentNode node = contents.get(id);
		if (node.hasParent()) {
			CMSContentNode parent = contents.get(node.getParentId());
			breadCrumb.add(parent.getPage());
			breadCrumb.addAll(getBreadcrumb(node.getParentId()));
		}
		return breadCrumb;
	}

	public List<CMSPage> getChildren(CMSPage page) {
		List<CMSPage> children = new ArrayList<>();
		CMSContentNode node = contents.get(page.getId());
		for (String childId : node.getChildIds()) {
			children.add(contents.get(childId).getPage());
		}
		Collections.sort(children, new Comparator<CMSPage>() {
			@Override
			public int compare(CMSPage o1, CMSPage o2) {
				return Integer.compare(o1.getMenuOrder(), o2.getMenuOrder());
			}});
		return children;
	}

	public void add(CMSPage page) {
		contents.put(page.getId(), new CMSContentNode(page));
	}

	public CMSContentNode getTreeNode(String id) {
		return contents.get(id);
	}

	public boolean contains(String postId) {
		return getById(postId) != null;
	}

	public CMSContent loadingFailed() {
		loadingFailed = true;
		return this;
	}

	public boolean successfullyLoaded() {
		return !loadingFailed;
	}
}
