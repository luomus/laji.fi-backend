package fi.luomus.traitdb.dao.triplestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.Param;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.TraitGroup;

public class TraitGroupsImple extends TriplestoreBaseDAO<TraitGroup> {

	private final TraitDAO dao;

	public TraitGroupsImple(Config config, TraitDAO dao) {
		super(config);
		this.dao = dao;
	}

	@Override
	public ValidationResponse validate(TraitGroup entity, boolean insert) {
		Map<String, String> errors = entity.validate(insert, null);
		for (TraitGroup existing : getAll()) {
			if (existing.getId().equals(entity.getId())) continue;
			if (existing.getName().equalsIgnoreCase(entity.getName())) {
				errors.put("name", "Group by this name already exists");
			}
		}
		return new ValidationResponse(errors);
	}

	@Override
	protected Class<TraitGroup> concreteClass() {
		return TraitGroup.class;
	}

	@Override
	protected Model createModel(Qname id, TraitGroup entity) {
		Model model = new Model(id);
		model.setType("TDF.traitGroup");
		model.addStatementIfObjectGiven("TDF.name", entity.getName());
		model.addStatementIfObjectGiven("TDF.description", entity.getDescription());
		return model;
	}

	@Override
	protected List<TraitGroup> getAllActual() {
		Document doc = query("search", new Param("type", "TDF.traitGroup"));
		List<TraitGroup> groups = new ArrayList<>();
		for (Node n : doc.getRootNode().getChildNodes()) {
			TraitGroup group = new TraitGroup();
			group.setId(getId(n));
			group.setName(getLiteral("TDF.name", n));
			group.setDescription(getLiteral("TDF.description", n));
			groups.add(group);
		}
		return groups;
	}

	@Override
	public ValidationResponse validateDelete(Qname id) {
		Map<String, String> errors = new HashMap<>();
		try {
			get(id);
		} catch (EntityNotFoundException e) {
			errors.put("id", "Not found: " + id);
			return new ValidationResponse(errors);
		}
		boolean used = dao.traits().getAll().stream().anyMatch(t->t.getGroup().equals(id));
		if (used) {
			errors.put("id", "Trait group has traits. Can not delete before all traits have been removed.");
		}
		return new ValidationResponse(errors);
	}

}
