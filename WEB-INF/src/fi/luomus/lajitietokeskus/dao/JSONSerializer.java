package fi.luomus.lajitietokeskus.dao;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.lajitietokeskus.models.CMSPage;
import fi.luomus.lajitietokeskus.models.NewsItem;

public class JSONSerializer {

	public static JSONObject serialize(NewsItem newsItem) {
		JSONObject jsonObject = new JSONObject();
		if (newsItem.isExternalNewsItem()) {
			jsonObject.setBoolean("external", true);
			jsonObject.setString("externalURL", newsItem.getExternalUrl().toString());
		} else {
			if (!(newsItem instanceof CMSPage)) {
				jsonObject.setBoolean("external", false);
			}
			jsonObject.setString("content", newsItem.getContent());
		}
		jsonObject.setString("title", newsItem.getTitle());
		if (newsItem.getFeaturedImage() != null) {
			jsonObject.getObject("featuredImage").setString("url", newsItem.getFeaturedImage());
			if (newsItem.getFeaturedImageCaption() != null) {
				jsonObject.getObject("featuredImage").setString("caption", newsItem.getFeaturedImageCaption());
			}
			if (newsItem.getFeaturedImageAlt() != null) {
				jsonObject.getObject("featuredImage").setString("alt", newsItem.getFeaturedImageAlt());
			}
		}
		for (String tag : newsItem.getTags()) {
			jsonObject.getArray("tags").appendString(tag);
		}
		jsonObject.setString("author", newsItem.getAuthor());
		jsonObject.setString("posted", String.valueOf(newsItem.getPosted().getTime()));
		if (newsItem.getPosted().getTime() != newsItem.getModified().getTime()) {
			jsonObject.setString("modified", String.valueOf(newsItem.getModified().getTime()));
		}
		jsonObject.setString("tag", newsItem.getTag());
		jsonObject.setString("id", newsItem.getId());
		return jsonObject;
	}

	public static JSONObject serializeSimple(CMSPage page) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.setString("title", page.getTitle());
		jsonObject.setString("id", page.getId());
		return jsonObject;
	}
}
