package fi.luomus.lajitietokeskus.taxonomy.services;

import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.lajitietokeskus.services.BaseServlet;

@WebServlet(urlPatterns = {"/taxonomy/*", "/taksonomia/*", "/taxonomi/*", "/taxon/*", "/taksoni/*"})
public class TaxonomyTreePageServlet extends BaseServlet {

	private static final Qname DEFAULT_ROOT_QNAME = new Qname("MX.53761");

	private static final long serialVersionUID = -2167340480903530068L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Qname id = getQname(req);
		if (id == null) id = DEFAULT_ROOT_QNAME;
		String locale = getLocale(req);
		String location = "https://laji.fi/taxon/"+id;
		if ("en".equals(locale)) location = "https://laji.fi/en/taxon/"+id;
		if ("sv".equals(locale)) location = "https://laji.fi/sv/taxon/"+id;
		return redirectTo(location);
	}

	private Qname getQname(HttpServletRequest req) {
		String path = req.getPathInfo();
		if (path == null || path.equals("/")) {
			return null;
		}
		path = path.toUpperCase().replace(".CSV", "");
		String[] pathParts = path.split(Pattern.quote("/"));
		for (String part : pathParts) {
			if (part.startsWith("MX.")) {
				return new Qname(part);
			}
		}
		return null;
	}

}