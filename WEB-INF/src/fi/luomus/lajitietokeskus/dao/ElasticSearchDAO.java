package fi.luomus.lajitietokeskus.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.traitdb.models.search.TraitSearchRow;

public interface ElasticSearchDAO extends AutoCloseable {

	public class WaitTimeoutException extends Exception {
		private static final long serialVersionUID = -4565929196051502663L;
		private final Map<Qname, Object> pushedData;
		public WaitTimeoutException(Map<Qname, Object> pushedData) {
			this.pushedData = pushedData;
		}
		@Override
		public String getMessage() {
			return pushedData.keySet().toString();
		}
	}

	/**
	 * Push taxon to temp index
	 * @param taxon
	 */
	void push(Taxon taxon);

	/**
	 * Push trait data to ACTIVE index
	 * @param row
	 */
	void pushActive(TraitSearchRow row);

	/**
	 * Push trait data to TEMP index
	 * @param row
	 */
	void pushTemp(TraitSearchRow row);

	/**
	 * Delete from ACTIVE index
	 * @param id
	 */
	void delete(Qname id);

	/**
	 * Drop all indices
	 * @throws Exception
	 */
	void deleteAll() throws Exception;

	/**
	 * Will wait until all pushed items have been attempted (results in success or failure)
	 */
	void waitForPushCompletion() throws WaitTimeoutException;

	/**
	 * Get all push errors since the creation of the DAO object (will not clear)
	 * Will perform waitForPushCompletion()
	 * @return
	 * @throws WaitTimeoutException
	 */
	List<PushError> getPushErrors() throws WaitTimeoutException;

	/**
	 * Switch temp index to active index
	 * @throws Exception
	 */
	void switchTempToActiveIndex() throws Exception;

	/**
	 * Empty temp index
	 * @throws Exception
	 */
	void emptyTemp() throws Exception;


	/**
	 * Get from ACTIVE index
	 * @param id
	 * @return null if not found
	 * @throws Exception
	 */
	JSONObject get(Qname id) throws Exception;

	/**
	 * Search from aliases
	 * @param aliases
	 * @param searchParams
	 * @param pageSize
	 * @param page
	 * @return
	 */
	List<JSONObject> search(Collection<String> aliases, Map<String, List<String>> searchParams, int pageSize, int page);

	/**
	 * Get total count from aliases
	 * @param aliases
	 * @param searchParams
	 * @return total count
	 */
	long searchTotal(List<String> aliases, Map<String, List<String>> searchParams);

	void refreshSearchIndex();

	/**
	 * Close must be called when dao is no longer needed
	 */
	@Override
	void close();

	public static class PushError {
		private final Qname id;
		private final String errorMessage;
		private final Object data;
		private final Exception exception;
		public PushError(Qname id, String errorMessage, Object data, Exception cause) {
			this.id = id;
			this.errorMessage = errorMessage;
			this.data = data;
			this.exception = cause;
		}
		public Qname getId() {
			return id;
		}
		public String getErrorMessage() {
			return errorMessage;
		}
		public Object getData() {
			return data;
		}
		public Exception getRootCause() {
			return exception;
		}
		@Override
		public String toString() {
			return "PushError [id=" + id + ", errorMessage=" + errorMessage + ", data=" + data + ", exception=" + exception + "]";
		}
	}
}
