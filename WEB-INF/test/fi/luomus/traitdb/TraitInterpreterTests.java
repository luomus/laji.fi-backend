package fi.luomus.traitdb;

import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonSearch;
import fi.luomus.commons.taxonomy.TaxonSearch.MatchType;
import fi.luomus.commons.taxonomy.TaxonSearchResponse;
import fi.luomus.commons.taxonomy.TaxonSearchResponse.Match;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.TaxonomyDAOStub;
import fi.luomus.lajitietokeskus.taxonomy.dao.GBIFTaxonDAO;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.Enumeration;
import fi.luomus.traitdb.models.Subject;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.models.TraitValue;
import fi.luomus.traitdb.models.UnitOfMeasurement;
import fi.luomus.traitdb.models.search.Interpreter;
import fi.luomus.traitdb.models.search.TraitSearchRow;

public class TraitInterpreterTests {

	private Interpreter interpreter;
	private TraitDAO dao;
	private TaxonomyDAO taxonomyDAO;
	private GBIFTaxonDAO gbifTaxonDAO;

	@Before
	public void init() {
		dao = createFakeTraitDAO();
		taxonomyDAO = new TaxonomyDAOStub() {
			@Override
			public TaxonSearchResponse search(TaxonSearch taxonSearch) {
				TaxonSearchResponse response = new TaxonSearchResponse(taxonSearch);
				if (taxonSearch.getSearchword().equalsIgnoreCase("Bubo bubo")) {
					Taxon taxon = new Taxon(new Qname("MX.123"), null);
					taxon.setScientificName("Bubo bubo");
					taxon.setTaxonRank(new Qname("MX.species"));
					taxon.setSecureLevel(new Qname("MX.secureLevelKM10"));
					response.getExactMatches().add(new Match(taxon, "Bubo bubo", MatchType.EXACT, new Qname("MX.scientificName"), "fi", "Bubo bubo"));
				}
				return response;
			}
		};
		gbifTaxonDAO = new GBIFTaxonDAOStub() {
			@Override
			public GBIFTaxon search(String scientificName, String kingdom) {
				if (scientificName.equalsIgnoreCase("Bubo bubo")) {
					GBIFTaxon t = new GBIFTaxon(123);
					t.setScientificName("Bubo bubo");
					t.setTaxonRank(new Qname("MX.species"));
					t.getHigherTaxa().put(new Qname("MX.kingdom"), "Animalia");
					return t;
				}
				return null;
			}
		};
		interpreter = new Interpreter(dao, taxonomyDAO, gbifTaxonDAO);
	}

	public static TraitDAOStub createFakeTraitDAO() {
		return new TraitDAOStub() {
			@Override
			public TraitDAOEntity<Trait> traits() {
				return new TraitDAOEntityStub<Trait>() {
					@Override
					public Collection<Trait> getAll() {
						return Utils.list(
								get(new Qname("TDF.123_m")),
								get(new Qname("TDF.123_mm")),
								get(new Qname("TDF.taxon_trait")));
					}
					@Override
					public Trait get(Qname id) throws EntityNotFoundException {
						Trait t = new Trait();
						t.setId(id);
						t.setGroup(new Qname("TDF.567"));

						if ("TDF.123_m".equals(id.toString())) {
							t.setBaseUnit(new Qname("TDF.umM"));
							t.setDataEntryName("length_of_something");
							t.setName("Some length");
							t.setRange(new Qname("xsd:decimal"));
						}
						if ("TDF.123_mm".equals(id.toString())) {
							t.setBaseUnit(new Qname("TDF.umMM"));
							t.setDataEntryName("small_length");
							t.setName("Smaller length");
							t.setRange(new Qname("xsd:decimal"));
						}
						if ("TDF.123_cm".equals(id.toString())) {
							t.setBaseUnit(new Qname("TDF.umCM"));
							t.setRange(new Qname("xsd:decimal"));
						}
						if ("TDF.taxon_trait".equals(id.toString())) {
							t.setRange(new Qname("MX.taxon"));
							t.setName("Best friends with this other taxon");
							t.setDataEntryName("best_friend");
						}
						return t;
					}
				};
			}
			@Override
			public TraitDAOEntity<TraitGroup> groups() {
				return new TraitDAOEntityStub<TraitGroup>() {
					@Override
					public TraitGroup get(Qname id) throws EntityNotFoundException {
						TraitGroup g = new TraitGroup();
						g.setId(id);
						g.setName("Trait Group Name");
						return g;
					}
				};
			}
			@Override
			public TraitDAOEntity<UnitOfMeasurement> units() {
				return new TraitDAOEntityStub<UnitOfMeasurement>() {
					@Override
					public Collection<UnitOfMeasurement> getAll() {
						return Utils.list(
								get(new Qname("TDF.umMM")),
								get(new Qname("TDF.umCM")),
								get(new Qname("TDF.umM"))
								);
					}
					@Override
					public UnitOfMeasurement get(Qname id) throws EntityNotFoundException {
						UnitOfMeasurement u = new UnitOfMeasurement();
						u.setId(id);
						if ("TDF.umMM".equals(id.toString())) {
							u.setUnit("mm");
							u.setConversionFactor(0.001);
							u.setBase(new Qname("TDF.umM"));
						}
						if ("TDF.umCM".equals(id.toString())) {
							u.setUnit("cm");
							u.setConversionFactor(0.01);
							u.setBase(new Qname("TDF.umM"));
						}
						if ("TDF.umM".equals(id.toString())) {
							u.setUnit("m");
							u.setConversionFactor(1.0);
							u.setBase(id);
						}
						return u;
					}
				};
			}
			@Override
			public TraitDAOEntity<Dataset> datasets() {
				return new TraitDAOEntityStub<Dataset>() {
					@Override
					public Dataset get(Qname id) throws EntityNotFoundException {
						Dataset d = new Dataset();
						d.setId(id);
						d.setName("Datset name for " + id);
						return d;
					}
				};
			}
			@Override
			public Enumeration getEnumeration(Qname id) {
				if (id.equals(new Qname("TDF.statisticalMethodEnum"))) {
					Enumeration e = new Enumeration(new Qname("TDF.statisticalMethodEnum"));
					e.add(new Qname("TDF.statisticalMethodMin"), "Minimum");
					e.add(new Qname("TDF.statisticalMethodMax"), "Maximum");
					return e;
				}
				if (id.equals(new Qname("TDF.typeEnum"))) {
					Enumeration e = new Enumeration(new Qname("TDF.typeEnum"));
					e.add(new Qname("TDF.typeIndividual"), "Individual");
					e.add(new Qname("TDF.typePopulationGroup"), "Population group");
					e.add(new Qname("TDF.typeTaxon"), "Taxon");
					return e;
				}
				if (id.equals(new Qname("MY.lifeStages"))) {
					Enumeration e = new Enumeration(new Qname("MY.lifeStages"));
					e.add(new Qname("MY.lifeStageAdult"), "A - adult");
					return e;
				}
				if (id.equals(new Qname("TDF.unitOfMeasurementEnum"))) {
					Enumeration e = new Enumeration(new Qname("TDF.unitOfMeasurementEnum"));
					for (UnitOfMeasurement u : this.units().getAll()) {
						e.add(u.getId(), u.getUnit());
					}
					return e;
				}
				throw new EntityNotFoundException(id);
			}
		};
	}

	@Test
	public void test_basics() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");
		traitValue.setWarnings(true);
		traitValue.setMeasurementRemarks("remarks");
		traitValue.setReference("ref");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\"\n" +
				"  },\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.123\"\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Trait Group Name\"\n" +
				"  },\n" +
				"  \"value\": \"a\",\n" +
				"  \"originalValue\": \"a\",\n" +
				"  \"warnings\": true,\n" +
				"  \"measurementRemarks\": \"remarks\",\n" +
				"  \"reference\": \"ref\",\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.123\",\n" +
				"    \"name\": \"Datset name for HR.123\"\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				searchRow.toJSON().beautify());
	}

	@Test
	public void test_unit_of_mes_conversions__no_trait_unit() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("7,5");
		traitValue.setUnit(new Qname("TDF.umM"));
		traitValue.setMeasurementAccuracy(0.5);
		traitValue.setStatisticalMethod(new Qname("TDF.statisticalMethodAvg"));

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\"\n" +
				"  },\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.123\"\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Trait Group Name\"\n" +
				"  },\n" +
				"  \"statisticalMethod\": \"TDF.statisticalMethodAvg\",\n" +
				"  \"value\": \"7.5\",\n" +
				"  \"originalValue\": \"7,5\",\n" +
				"  \"originalValueNumeric\": 7.5,\n" +
				"  \"originalUnit\": \"TDF.umM\",\n" +
				"  \"originalMeasurementAccuracy\": 0.5,\n" +
				"  \"warnings\": false,\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.123\",\n" +
				"    \"name\": \"Datset name for HR.123\"\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				searchRow.toJSON().beautify());
	}

	@Test
	public void test_unit_of_mes_conversions__same_trait_unit() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123_m"));
		traitValue.setValue("7,5");
		traitValue.setUnit(new Qname("TDF.umM"));
		traitValue.setMeasurementAccuracy(0.5);

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\"\n" +
				"  },\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.123_m\",\n" +
				"    \"dataEntryName\": \"length_of_something\",\n" +
				"    \"name\": \"Some length\",\n" +
				"    \"baseUnit\": \"TDF.umM\",\n" +
				"    \"range\": \"xsd:decimal\"\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Trait Group Name\"\n" +
				"  },\n" +
				"  \"value\": \"7.5\",\n" +
				"  \"valueNumeric\": 7.5,\n" +
				"  \"unit\": \"TDF.umM\",\n" +
				"  \"measurementAccuracy\": 0.5,\n" +
				"  \"originalValue\": \"7,5\",\n" +
				"  \"originalValueNumeric\": 7.5,\n" +
				"  \"originalUnit\": \"TDF.umM\",\n" +
				"  \"originalMeasurementAccuracy\": 0.5,\n" +
				"  \"warnings\": false,\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.123\",\n" +
				"    \"name\": \"Datset name for HR.123\"\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				searchRow.toJSON().beautify());
	}

	@Test
	public void test_unit_of_mes_conversions__conversion_m_to_mm() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123_mm"));
		traitValue.setValue("7,5");
		traitValue.setUnit(new Qname("TDF.umM"));
		traitValue.setMeasurementAccuracy(0.5);

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\"\n" +
				"  },\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.123_mm\",\n" +
				"    \"dataEntryName\": \"small_length\",\n" +
				"    \"name\": \"Smaller length\",\n" +
				"    \"baseUnit\": \"TDF.umMM\",\n" +
				"    \"range\": \"xsd:decimal\"\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Trait Group Name\"\n" +
				"  },\n" +
				"  \"value\": \"7500.0\",\n" +
				"  \"valueNumeric\": 7500,\n" +
				"  \"unit\": \"TDF.umMM\",\n" +
				"  \"measurementAccuracy\": 500,\n" +
				"  \"originalValue\": \"7,5\",\n" +
				"  \"originalValueNumeric\": 7.5,\n" +
				"  \"originalUnit\": \"TDF.umM\",\n" +
				"  \"originalMeasurementAccuracy\": 0.5,\n" +
				"  \"warnings\": false,\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.123\",\n" +
				"    \"name\": \"Datset name for HR.123\"\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				searchRow.toJSON().beautify());
	}

	@Test
	public void test_unit_of_mes_conversions__conversion_cm_to_mm() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123_mm"));
		traitValue.setValue("750");
		traitValue.setUnit(new Qname("TDF.umCM"));
		traitValue.setMeasurementAccuracy(50.0);

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\"\n" +
				"  },\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.123_mm\",\n" +
				"    \"dataEntryName\": \"small_length\",\n" +
				"    \"name\": \"Smaller length\",\n" +
				"    \"baseUnit\": \"TDF.umMM\",\n" +
				"    \"range\": \"xsd:decimal\"\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Trait Group Name\"\n" +
				"  },\n" +
				"  \"value\": \"7500.0\",\n" +
				"  \"valueNumeric\": 7500,\n" +
				"  \"unit\": \"TDF.umMM\",\n" +
				"  \"measurementAccuracy\": 500,\n" +
				"  \"originalValue\": \"750\",\n" +
				"  \"originalValueNumeric\": 750,\n" +
				"  \"originalUnit\": \"TDF.umCM\",\n" +
				"  \"originalMeasurementAccuracy\": 50,\n" +
				"  \"warnings\": false,\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.123\",\n" +
				"    \"name\": \"Datset name for HR.123\"\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				searchRow.toJSON().beautify());
	}

	@Test
	public void test_unit_of_mes_conversions__conversion_mm_to_cm() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123_cm"));
		traitValue.setValue("7500");
		traitValue.setUnit(new Qname("TDF.umMM"));
		traitValue.setMeasurementAccuracy(500.0);

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\"\n" +
				"  },\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.123_cm\",\n" +
				"    \"baseUnit\": \"TDF.umCM\",\n" +
				"    \"range\": \"xsd:decimal\"\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Trait Group Name\"\n" +
				"  },\n" +
				"  \"value\": \"750.0\",\n" +
				"  \"valueNumeric\": 750,\n" +
				"  \"unit\": \"TDF.umCM\",\n" +
				"  \"measurementAccuracy\": 50,\n" +
				"  \"originalValue\": \"7500\",\n" +
				"  \"originalValueNumeric\": 7500,\n" +
				"  \"originalUnit\": \"TDF.umMM\",\n" +
				"  \"originalMeasurementAccuracy\": 500,\n" +
				"  \"warnings\": false,\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.123\",\n" +
				"    \"name\": \"Datset name for HR.123\"\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				searchRow.toJSON().beautify());
	}

	@Test
	public void dateConversions_only_begin_date() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setDateBegin(DateUtils.convertToDateValue("15.5.2000"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("2000-05-15", subject.getDateBegin().toString("yyyy-MM-dd"));
		assertEquals("2000-05-15", subject.getDateEnd().toString("yyyy-MM-dd"));
		assertEquals(15, searchRow.getDay().intValue());
		assertEquals(5, searchRow.getMonth().intValue());
		assertEquals(2000, searchRow.getYear().intValue());
		assertEquals(515, searchRow.getSubject().getSeasonBegin().intValue());
		assertEquals(515, searchRow.getSubject().getSeasonEnd().intValue());
		assertEquals(2000, searchRow.getSubject().getYearBegin().intValue());
		assertEquals(2000, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("2000-05-15", searchRow.getEventDate());
	}

	@Test
	public void dateConversions_begin_and_end_date() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setDateBegin(DateUtils.convertToDateValue("15.5.2000"));
		subject.setDateEnd(DateUtils.convertToDateValue("18.5.2000"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("2000-05-15", subject.getDateBegin().toString("yyyy-MM-dd"));
		assertEquals("2000-05-18", subject.getDateEnd().toString("yyyy-MM-dd"));
		assertEquals(null, searchRow.getDay());
		assertEquals(5, searchRow.getMonth().intValue());
		assertEquals(2000, searchRow.getYear().intValue());
		assertEquals(515, searchRow.getSubject().getSeasonBegin().intValue());
		assertEquals(518, searchRow.getSubject().getSeasonEnd().intValue());
		assertEquals(2000, searchRow.getSubject().getYearBegin().intValue());
		assertEquals(2000, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("2000-05-15/2000-05-18", searchRow.getEventDate());
	}

	@Test
	public void dateConversions_begin_and_end_date_2() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setDateBegin(DateUtils.convertToDateValue("15.5.2000"));
		subject.setDateEnd(DateUtils.convertToDateValue("18.6.2000"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("2000-05-15", subject.getDateBegin().toString("yyyy-MM-dd"));
		assertEquals("2000-06-18", subject.getDateEnd().toString("yyyy-MM-dd"));
		assertEquals(null, searchRow.getDay());
		assertEquals(null, searchRow.getMonth());
		assertEquals(2000, searchRow.getYear().intValue());
		assertEquals(515, searchRow.getSubject().getSeasonBegin().intValue());
		assertEquals(618, searchRow.getSubject().getSeasonEnd().intValue());
		assertEquals(2000, searchRow.getSubject().getYearBegin().intValue());
		assertEquals(2000, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("2000-05-15/2000-06-18", searchRow.getEventDate());
	}

	@Test
	public void dateConversions_begin_and_end_date_3() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setDateBegin(DateUtils.convertToDateValue("15.5.2000"));
		subject.setDateEnd(DateUtils.convertToDateValue("18.6.2001"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("2000-05-15", subject.getDateBegin().toString("yyyy-MM-dd"));
		assertEquals("2001-06-18", subject.getDateEnd().toString("yyyy-MM-dd"));
		assertEquals(null, searchRow.getDay());
		assertEquals(null, searchRow.getMonth());
		assertEquals(null, searchRow.getYear());
		assertEquals(null, searchRow.getSubject().getSeasonBegin());
		assertEquals(null, searchRow.getSubject().getSeasonEnd());
		assertEquals(2000, searchRow.getSubject().getYearBegin().intValue());
		assertEquals(2001, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("2000-05-15/2001-06-18", searchRow.getEventDate());
	}

	@Test
	public void dateConversions_only_end_date() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setDateEnd(DateUtils.convertToDateValue("18.6.2001"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals(null, subject.getDateBegin());
		assertEquals("2001-06-18", subject.getDateEnd().toString("yyyy-MM-dd"));
		assertEquals(null, searchRow.getDay());
		assertEquals(null, searchRow.getMonth());
		assertEquals(null, searchRow.getYear());
		assertEquals(null, searchRow.getSubject().getSeasonBegin());
		assertEquals(null, searchRow.getSubject().getSeasonEnd());
		assertEquals(null, searchRow.getSubject().getYearBegin());
		assertEquals(2001, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("/2001-06-18", searchRow.getEventDate());
	}

	@Test
	public void dateConversions_only_begin_year() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setYearBegin(2000);

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals(null, subject.getDateBegin());
		assertEquals(null, subject.getDateEnd());
		assertEquals(null, searchRow.getDay());
		assertEquals(null, searchRow.getMonth());
		assertEquals(2000, searchRow.getYear().intValue());
		assertEquals(null, searchRow.getSubject().getSeasonBegin());
		assertEquals(null, searchRow.getSubject().getSeasonEnd());
		assertEquals(2000, searchRow.getSubject().getYearBegin().intValue());
		assertEquals(2000, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("2000", searchRow.getEventDate());
	}

	@Test
	public void dateConversions_begin_and_end_year() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setYearBegin(2000);
		subject.setYearEnd(2005);

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals(null, subject.getDateBegin());
		assertEquals(null, subject.getDateEnd());
		assertEquals(null, searchRow.getDay());
		assertEquals(null, searchRow.getMonth());
		assertEquals(null, searchRow.getYear());
		assertEquals(null, searchRow.getSubject().getSeasonBegin());
		assertEquals(null, searchRow.getSubject().getSeasonEnd());
		assertEquals(2000, searchRow.getSubject().getYearBegin().intValue());
		assertEquals(2005, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("2000/2005", searchRow.getEventDate());
	}

	@Test
	public void dateConversions_only_end_year() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setYearEnd(2005);

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals(null, subject.getDateBegin());
		assertEquals(null, subject.getDateEnd());
		assertEquals(null, searchRow.getDay());
		assertEquals(null, searchRow.getMonth());
		assertEquals(null, searchRow.getYear());
		assertEquals(null, searchRow.getSubject().getSeasonBegin());
		assertEquals(null, searchRow.getSubject().getSeasonEnd());
		assertEquals(null, searchRow.getSubject().getYearBegin());
		assertEquals(2005, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("/2005", searchRow.getEventDate());
	}

	@Test
	public void dateConversions_conflicting_year_and_dates__dates_preferred() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setDateBegin(DateUtils.convertToDateValue("5.5.2000"));
		subject.setDateEnd(DateUtils.convertToDateValue("30.8.2000"));
		subject.setYearBegin(1959);
		subject.setYearEnd(2024);

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("2000-05-05", subject.getDateBegin().toString("yyyy-MM-dd"));
		assertEquals("2000-08-30", subject.getDateEnd().toString("yyyy-MM-dd"));
		assertEquals(null, searchRow.getDay());
		assertEquals(null, searchRow.getMonth());
		assertEquals(2000, searchRow.getYear().intValue());
		assertEquals(505, searchRow.getSubject().getSeasonBegin().intValue());
		assertEquals(830, searchRow.getSubject().getSeasonEnd().intValue());
		assertEquals(2000, searchRow.getSubject().getYearBegin().intValue());
		assertEquals(2000, searchRow.getSubject().getYearEnd().intValue());
		assertEquals("2000-05-05/2000-08-30", searchRow.getEventDate());
	}

	@Test
	public void coordinates() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setDateBegin(DateUtils.convertToDateValue("5.5.2000"));
		subject.setDateEnd(DateUtils.convertToDateValue("30.8.2000"));
		subject.setYearBegin(1959);
		subject.setYearEnd(2024);

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals(null, searchRow.getGeodeticDatum());

		subject.setLat(60.0);
		subject.setLon(35.2);
		searchRow = interpreter.get(subject, traitValue);
		assertEquals("WGS84", searchRow.getGeodeticDatum());
	}

	@Test
	public void subject_taxon__from_scientificName() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setScientificName("Bubo bubo");
		subject.setKingdom("Animalia");

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"TraitTaxon [id=http://tun.fi/MX.123, taxonomicOrder=null, taxonRank=species, scientificName=Bubo bubo, cursiveName=true, author=null, higherTaxa=null, iucnStatus=null, primaryHabitat=null, habitatSpecifiers=null, sensitive=true]",
				searchRow.getSubjectFinBIFTaxon().toString());

		assertEquals(""
				+ "TraitTaxon [id=gbif:123, taxonomicOrder=null, taxonRank=species, scientificName=Bubo bubo, cursiveName=true, author=null, higherTaxa={kingdom=Animalia}, iucnStatus=null, primaryHabitat=null, habitatSpecifiers=null, sensitive=false]",
				searchRow.getSubjectGBIFTaxon().toString());

		assertEquals(null, searchRow.getObjectFinBIFTaxon());
		assertEquals(null, searchRow.getObjectGBIFTaxon());
	}

	@Test
	public void object_taxon__from_scientificName() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.taxon_trait"));
		traitValue.setValue("Bubo bubo");
		traitValue.setObjectTaxonLifeStage(new Qname("MY.lifeStageEgg"));

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\"\n" +
				"  },\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.taxon_trait\",\n" +
				"    \"dataEntryName\": \"best_friend\",\n" +
				"    \"name\": \"Best friends with this other taxon\",\n" +
				"    \"range\": \"MX.taxon\"\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Trait Group Name\"\n" +
				"  },\n" +
				"  \"value\": \"Bubo bubo\",\n" +
				"  \"originalValue\": \"Bubo bubo\",\n" +
				"  \"objectTaxonLifeStage\": \"MY.lifeStageEgg\",\n" +
				"  \"objectTaxonVerbatim\": \"Bubo bubo\",\n" +
				"  \"objectFinBIFTaxon\": {\n" +
				"    \"id\": \"http://tun.fi/MX.123\",\n" +
				"    \"taxonRank\": \"species\",\n" +
				"    \"scientificName\": \"Bubo bubo\",\n" +
				"    \"cursiveName\": true,\n" +
				"    \"higherTaxa\": {\n" +
				"      \n" +
				"    },\n" +
				"    \"sensitive\": true\n" +
				"  },\n" +
				"  \"objectGBIFTaxon\": {\n" +
				"    \"id\": \"gbif:123\",\n" +
				"    \"taxonRank\": \"species\",\n" +
				"    \"scientificName\": \"Bubo bubo\",\n" +
				"    \"cursiveName\": true,\n" +
				"    \"higherTaxa\": {\n" +
				"      \"kingdom\": \"Animalia\"\n" +
				"    },\n" +
				"    \"sensitive\": false\n" +
				"  },\n" +
				"  \"warnings\": false,\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.123\",\n" +
				"    \"name\": \"Datset name for HR.123\"\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				searchRow.toJSON().beautify());
	}

	@Test
	public void securing_based_on_subject_taxon() throws Exception {
		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setScientificName("Bubo bubo");

		subject.setLat(52.1411414);
		subject.setLon(33.2252252);
		subject.setCoordinateAccuracy(3);
		subject.setLocality("supersecret locality");
		subject.setMunicipality("small municipality");
		subject.setCountry("No need to hide");
		subject.setOccurrenceRemarks("supersecret notes");

		TraitValue traitValue = new TraitValue();
		traitValue.setId(new Qname("TD.2"));
		traitValue.setTraitId(new Qname("TDF.123"));
		traitValue.setValue("a");

		TraitSearchRow searchRow = interpreter.get(subject, traitValue);

		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\",\n" +
				"    \"scientificName\": \"Bubo bubo\",\n" +
				"    \"lat\": 52.5,\n" +
				"    \"lon\": 33.5,\n" +
				"    \"coordinateAccuracy\": 100000,\n" +
				"    \"country\": \"No need to hide\",\n" +
				"    \"municipality\": \"hidden\",\n" +
				"    \"locality\": \"hidden\",\n" +
				"    \"occurrenceRemarks\": \"hidden\"\n" +
				"  },\n" +
				"  \"geodeticDatum\": \"WGS84\",\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.123\"\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Trait Group Name\"\n" +
				"  },\n" +
				"  \"value\": \"a\",\n" +
				"  \"originalValue\": \"a\",\n" +
				"  \"subjectFinBIFTaxon\": {\n" +
				"    \"id\": \"http://tun.fi/MX.123\",\n" +
				"    \"taxonRank\": \"species\",\n" +
				"    \"scientificName\": \"Bubo bubo\",\n" +
				"    \"cursiveName\": true,\n" +
				"    \"higherTaxa\": {\n" +
				"      \n" +
				"    },\n" +
				"    \"sensitive\": true\n" +
				"  },\n" +
				"  \"subjectGBIFTaxon\": {\n" +
				"    \"id\": \"gbif:123\",\n" +
				"    \"taxonRank\": \"species\",\n" +
				"    \"scientificName\": \"Bubo bubo\",\n" +
				"    \"cursiveName\": true,\n" +
				"    \"higherTaxa\": {\n" +
				"      \"kingdom\": \"Animalia\"\n" +
				"    },\n" +
				"    \"sensitive\": false\n" +
				"  },\n" +
				"  \"warnings\": false,\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.123\",\n" +
				"    \"name\": \"Datset name for HR.123\"\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				searchRow.toJSON().beautify());
	}

	@Test
	public void testSecureMax() {
		assertEquals(Double.valueOf(61), Interpreter.secureMax(60.2));
		assertEquals(Double.valueOf(60), Interpreter.secureMax(60.0));
		assertEquals(Double.valueOf(-11), Interpreter.secureMax(-11.5));
		assertEquals(Double.valueOf(-11), Interpreter.secureMax(-11.0));
		assertEquals(Double.valueOf(0.0), Interpreter.secureMax(0.0));
	}

	@Test
	public void testSecureMin() {
		assertEquals(Double.valueOf(60), Interpreter.secureMin(60.2));
		assertEquals(Double.valueOf(60), Interpreter.secureMin(60.0));
		assertEquals(Double.valueOf(-11), Interpreter.secureMin(-11.5));
		assertEquals(Double.valueOf(-11), Interpreter.secureMin(-11.0));
		assertEquals(Double.valueOf(0.0), Interpreter.secureMin(0.0));
	}

	@Test
	public void testSecureRound() {
		assertEquals(Double.valueOf(50.5), Interpreter.secureRound(50.2));
		assertEquals(Double.valueOf(50.5), Interpreter.secureRound(50.5));
		assertEquals(Double.valueOf(-6.5), Interpreter.secureRound(-6.9));
		assertEquals(Double.valueOf(-5.5), Interpreter.secureRound(-5.1));
		assertEquals(Double.valueOf(0.0), Interpreter.secureRound(0.0));
	}

}
