package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.lajitietokeskus.taxonomy.dao.GBIFTaxonDAOImple;

public class GBIFTaxonDAOTests {

	private GBIFTaxonDAOImple dao;

	@Before
	public void init() {
		dao = new GBIFTaxonDAOImple();
	}

	@After
	public void close() throws Exception {
		if (dao != null) dao.close();
	}

	@Test
	public void nameSearch() {
		assertEquals("" +
				"GBIFTaxon [id=1427067, taxonRank=MX.species, scientificName=Calopteryx splendens, author=(Harris, 1780) , higherTaxa={MX.kingdom=Animalia, MX.phylum=Arthropoda, MX.class=Insecta, MX.order=Odonata, MX.family=Calopterygidae, MX.genus=Calopteryx}]",
				dao.search("Calopteryx splendens", "Animalia").toString());
		assertEquals(null, dao.search("Calopteryx splendens", "Plantae"));
		assertEquals(1427067, dao.search("Calopteryx splendens", null).getId().intValue());

		assertEquals("" +
				"GBIFTaxon [id=7215856, taxonRank=MX.subspecies, scientificName=Calopteryx splendens splendens, author=, higherTaxa={MX.kingdom=Animalia, MX.phylum=Arthropoda, MX.class=Insecta, MX.order=Odonata, MX.family=Calopterygidae, MX.genus=Calopteryx}]",
				dao.search("Calopteryx splendens splendens", "animalia").toString());
	}

	@Test
	public void getById() {
		assertEquals("" +
				"GBIFTaxon [id=1, taxonRank=MX.kingdom, scientificName=Animalia, author=, higherTaxa={MX.kingdom=Animalia}]",
				dao.get(1).toString());

		assertEquals(null, dao.get(Integer.MAX_VALUE));

		assertEquals("" +
				"GBIFTaxon [id=1427007, taxonRank=MX.genus, scientificName=Calopteryx, author=Leach, 1815, higherTaxa={MX.kingdom=Animalia, MX.phylum=Arthropoda, MX.class=Insecta, MX.order=Odonata, MX.family=Calopterygidae, MX.genus=Calopteryx}]",
				dao.get(1427007).toString());
	}

}
