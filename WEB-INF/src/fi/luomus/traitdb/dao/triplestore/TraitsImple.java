package fi.luomus.traitdb.dao.triplestore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.Param;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.dao.TraitDAOImple;
import fi.luomus.traitdb.models.TSV2InputRowConverter;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitEnumerationValue;

public class TraitsImple extends TriplestoreBaseDAO<Trait> {

	public TraitDAOImple dao;

	public TraitsImple(Config config, TraitDAOImple dao) {
		super(config);
		this.dao = dao;
	}

	@Override
	public ValidationResponse validate(Trait entity, boolean insert) {
		Map<String, String> errors = entity.validate(insert, dao);
		if (!errors.isEmpty()) return new ValidationResponse(errors);

		try {
			dao.groups().get(entity.getGroup());
		} catch (EntityNotFoundException e) {
			errors.put("group", "Unknown group " + entity.getGroup());
		}

		if (TSV2InputRowConverter.isReservedName(entity.getDataEntryName())) {
			errors.put("dataEntryName", "This is a reserved term and can not be used as a trait name");
		}
		if (TSV2InputRowConverter.isReservedName(entity.getName())) {
			errors.put("name", "This is a reserved term and can not be used as a trait name");
		}

		for (Trait existing : getAll()) {
			if (existing.getId().equals(entity.getId())) continue;
			if (existing.getName().equalsIgnoreCase(entity.getName())) {
				errors.put("name", "Trait by this name already exists");
			}
			if (existing.getDataEntryName().equalsIgnoreCase(entity.getDataEntryName())) {
				errors.put("dataEntryName", "Trait by this short name already exists");
			}
		}

		Set<String> enumDataEntryNames = new HashSet<>();
		Set<String> enumNames = new HashSet<>();
		int i = 0;
		for (TraitEnumerationValue enumValue : entity.getEnumerations()) {
			i++;
			if (enumDataEntryNames.contains(enumValue.getDataEntryName().toLowerCase())) {
				errors.put("enumerations ["+i+"] dataEntryName", "Enumeration by this short name already exists");
			}
			if (enumNames.contains(enumValue.getName().toLowerCase())) {
				errors.put("enumerations ["+i+"] name", "Enumeration by this name already exists");
			}
			enumDataEntryNames.add(enumValue.getDataEntryName().toLowerCase());
			enumDataEntryNames.add(enumValue.getName().toLowerCase());
		}
		return new ValidationResponse(errors);
	}

	@Override
	protected Class<Trait> concreteClass() {
		return Trait.class;
	}

	@Override
	public synchronized Trait insert(Trait entity) {
		if (!entity.getEnumerations().isEmpty()) {
			int i = 0;
			for (TraitEnumerationValue enumValue : entity.getEnumerations()) {
				enumValue.setOrder(i++);
			}
			dao.traitEnumerations().insert(entity.getEnumerations());
		}
		return super.insert(entity);
	}

	@Override
	public synchronized Trait update(Trait entity) throws EntityNotFoundException {
		if (!entity.getEnumerations().isEmpty()) {
			int i = 0;
			for (TraitEnumerationValue enumValue : entity.getEnumerations()) {
				enumValue.setOrder(i++);
			}
			List<TraitEnumerationValue> update = new ArrayList<>();
			List<TraitEnumerationValue> insert = new ArrayList<>();
			Trait current = get(entity.getId());
			for (TraitEnumerationValue enumValue : entity.getEnumerations()) {
				Optional<TraitEnumerationValue> existing = current.getEnumerations().stream().filter(c->c.getDataEntryName().equals(enumValue.getDataEntryName())).findFirst();
				if (existing.isPresent()) {
					if (!existing.get().equals(enumValue)) {
						update.add(enumValue);
					}
				} else {
					insert.add(enumValue);
				}
			}
			dao.traitEnumerations().insert(insert);
			for (TraitEnumerationValue e : update) {
				dao.traitEnumerations().update(e);
			}
		}
		return super.update(entity);
	}

	@Override
	protected Model createModel(Qname id, Trait entity) {
		Model model = new Model(id);
		model.setType("TDF.trait");
		model.addStatementIfObjectGiven(new Predicate("TDF.partOfGroup"), entity.getGroup());
		model.addStatementIfObjectGiven(new Predicate("TDF.dataEntryName"), entity.getDataEntryName());
		model.addStatementIfObjectGiven(new Predicate("TDF.name"), entity.getName());
		model.addStatementIfObjectGiven(new Predicate("TDF.description"), entity.getDescription());
		model.addStatementIfObjectGiven(new Predicate("TDF.exampleValues"), entity.getExampleValues());
		model.addStatementIfObjectGiven(new Predicate("TDF.baseUnit"), entity.getBaseUnit());
		model.addStatementIfObjectGiven(new Predicate("rdfs:range"), entity.getRange());
		model.addStatementIfObjectGiven(new Predicate("TDF.reference"), entity.getReference());
		for (String extId : entity.getIdentifiers()) {
			model.addStatementIfObjectGiven(new Predicate("TDF.externalIdentifier"), extId);
		}
		for (TraitEnumerationValue enumVal : entity.getEnumerations()) {
			model.addStatementIfObjectGiven(new Predicate("TDF.hasEnumeration"), enumVal.getId());
		}
		return model;
	}

	@Override
	protected List<Trait> getAllActual() {
		Document doc = query("search", new Param("type", "TDF.trait"));
		List<Trait> traits = new ArrayList<>();
		for (Node n : doc.getRootNode().getChildNodes()) {
			Trait trait = new Trait();
			trait.setId(getId(n));
			trait.setGroup(getResource("TDF.partOfGroup", n));
			trait.setDataEntryName(getLiteral("TDF.dataEntryName", n));
			trait.setName(getLiteral("TDF.name", n));
			trait.setDescription(getLiteral("TDF.description", n));
			trait.setExampleValues(getLiteral("TDF.exampleValues", n));
			trait.setBaseUnit(getResource("TDF.baseUnit", n));
			trait.setRange(getResource("rdfs:range", n));
			trait.setReference(getLiteral("TDF.reference", n));
			for (Node exIdN : n.getChildNodes("TDF.externalIdentifier")) {
				trait.getIdentifiers().add(exIdN.getContents());
			}
			Collections.sort(trait.getIdentifiers());
			for (Node enumN : n.getChildNodes("TDF.hasEnumeration")) {
				Qname enumId = getResource(enumN);
				trait.getEnumerations().add(dao.traitEnumerations().get(enumId));
			}
			Collections.sort(trait.getEnumerations());
			traits.add(trait);
		}
		return traits;
	}

	@Override
	public ValidationResponse validateDelete(Qname id) {
		Map<String, String> errors = new HashMap<>();
		try {
			get(id);
		} catch (EntityNotFoundException e) {
			errors.put("id", "Not found: " + id);
			return new ValidationResponse(errors);
		}
		Map<String, List<String>> params = new HashMap<>();
		params.put("traits.traitId", Utils.singleEntryList(id.toString()));
		int count = dao.rows().search(params, 1).size();
		if (count > 0) {
			errors.put("id", "Trait is used. Can not delete.");
		}
		return new ValidationResponse(errors);
	}

}
