package fi.luomus.traitdb.dao.db;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOEntity;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.DatasetPermissions;

public class DatasetPermissionsImple implements TraitDAOEntity<DatasetPermissions> {

	private final Map<Qname, DatasetPermissions> CACHE = new ConcurrentHashMap<>();
	private final DbDAO db;
	private final TraitDAO dao;

	public DatasetPermissionsImple(DbDAO db, TraitDAO dao) {
		this.db = db;
		this.dao = dao;
	}

	@Override
	public DatasetPermissions get(Qname datasetId) throws EntityNotFoundException {
		DatasetPermissions p = CACHE.computeIfAbsent(datasetId, load());
		if (p == null) {
			p = new DatasetPermissions();
			p.setDatasetId(datasetId);
		}
		return p;
	}

	private Function<Qname, DatasetPermissions> load() {
		return datasetId -> {
			return db.getPermissions(datasetId);
		};
	}

	@Override
	public ValidationResponse validate(DatasetPermissions entity, boolean insert) throws UnsupportedOperationException {
		Map<String, String> errors = entity.validate(insert, dao);
		if (!errors.isEmpty()) return new ValidationResponse(errors);
		try {
			dao.datasets().get(entity.getDatasetId());
		} catch (EntityNotFoundException e) {
			errors.put("datasetId", "Unknown dataset " + entity.getDatasetId());
		}
		return new ValidationResponse(errors);
	}

	@Override
	public DatasetPermissions insert(DatasetPermissions entity) {
		return upsert(entity);
	}

	@Override
	public DatasetPermissions update(DatasetPermissions entity) throws EntityNotFoundException, UnsupportedOperationException {
		return upsert(entity);
	}

	private DatasetPermissions upsert(DatasetPermissions entity) {
		Set<String> unique = new HashSet<>(entity.getUserIds());
		entity.getUserIds().clear();
		entity.getUserIds().addAll(unique);
		Collections.sort(entity.getUserIds());
		db.upsert(entity);
		CACHE.put(entity.getDatasetId(), entity);
		return entity;
	}

	@Override
	public Collection<DatasetPermissions> getAll() throws UnsupportedOperationException {
		List<DatasetPermissions> all = db.getPermissions();
		for (DatasetPermissions p : all) {
			CACHE.put(p.getDatasetId(), p);
		}
		return all;
	}

	@Override
	public void delete(Qname id) throws EntityNotFoundException, UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<ValidationResponse> validate(Collection<DatasetPermissions> entities) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void insert(Collection<DatasetPermissions> entities) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<DatasetPermissions> search(Map<String, List<String>> searchParams, int limit) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public ValidationResponse validateDelete(Qname id) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

}
