package fi.luomus.lajitietokeskus.models;

import java.net.URL;
import java.sql.Timestamp;

public class ExternalNewsItem extends NewsItem {

	private final URL externalUrl;
		
	public ExternalNewsItem(String id, Timestamp posted, String title, String tag, URL url) {
		super(tag+"-"+id, posted, posted, null, title, null, tag);
		this.externalUrl = url;
	}

	@Override
	public URL getExternalUrl() {
		return externalUrl;
	}

	@Override
	public boolean isExternalNewsItem() {
		return true;
	}
	
}
