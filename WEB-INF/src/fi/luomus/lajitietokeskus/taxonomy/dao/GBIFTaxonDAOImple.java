package fi.luomus.lajitietokeskus.taxonomy.dao;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.http.HttpClientService.NotFoundException;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.URIBuilder;

public class GBIFTaxonDAOImple implements GBIFTaxonDAO, AutoCloseable {

	private final Cached<SearchTerm, Integer> searchCache = new Cached<>(searchLoader(), 3, TimeUnit.DAYS, 20000);
	private final Cached<Integer, GBIFTaxon> taxonCache = new Cached<>(taxonLoader(), 3, TimeUnit.DAYS, 20000);
	private HttpClientService client;

	private HttpClientService getClient() {
		if (client == null) {
			synchronized (this) {
				if (client == null) {
					client = new HttpClientService();
				}
			}
		}
		return client;
	}

	@Override
	public GBIFTaxon get(Integer id) {
		if (id == null) return null;
		return taxonCache.get(id);
	}

	@Override
	public GBIFTaxon search(String scientificName, String kingdom) {
		Integer id = searchCache.get(new SearchTerm(scientificName, kingdom));
		return get(id);
	}

	private CacheLoader<Integer, GBIFTaxon> taxonLoader() {
		return new CacheLoader<Integer, GBIFTaxonDAO.GBIFTaxon>() {

			@Override
			public GBIFTaxon load(Integer key) {
				if (key == null) return null;
				try {
					JSONObject res = getClient().contentAsJson(new HttpGet("https://api.gbif.org/v1/species/"+key));
					GBIFTaxon taxon = new GBIFTaxon(key);
					taxon.setScientificName(res.getString("canonicalName"));
					taxon.setAuthor(res.getString("authorship"));
					taxon.setTaxonRank(toRank(res.getString("rank")));
					for (Qname rank : TaxonToMap.HIGHER_RANKS) {
						String plainRank = plainRank(rank);
						if (res.hasKey(plainRank)) {
							taxon.getHigherTaxa().put(rank, res.getString(plainRank));
						}
					}
					return taxon;
				} catch (NotFoundException nf) {
					return null;
				} catch (Exception e) {
					throw new RuntimeException("GBIF taxon load failed", e);
				}
			}

			private Qname toRank(String plainRank) {
				return new Qname("MX."+(plainRank.toLowerCase()));
			}

			private String plainRank(Qname taxonRank) {
				if (taxonRank == null) return null;
				return taxonRank.toString().replace(TaxonToMap.TAXON_RANK_PREFIX, "");
			}
		};
	}

	private CacheLoader<SearchTerm, Integer> searchLoader() {
		return new CacheLoader<GBIFTaxonDAOImple.SearchTerm, Integer>() {

			@Override
			public Integer load(SearchTerm key) {
				try {
					JSONObject res = getClient().contentAsJson(new HttpGet(q(key)));
					return findMatch(res, key);
				} catch (Exception e) {
					throw new RuntimeException("GBIF taxon load failed", e);
				}
			}

			private URI q(SearchTerm key) throws URISyntaxException {
				URIBuilder uri = new URIBuilder("https://api.gbif.org/v1/species/search");
				uri.addParameter("datasetKey", "d7dddbf4-2cf0-4f39-9b2a-bb099caae36c"); // GBIF backbone
				uri.addParameter("nameType", "SCIENTIFIC");
				uri.addParameter("status", "ACCEPTED");
				uri.addParameter("q", key.scientificName);
				return uri.getURI();
			}

			private Integer findMatch(JSONObject res, SearchTerm key) {
				for (JSONObject match : res.getArray("results").iterateAsObject()) {
					String canocialName = match.getString("canonicalName");
					String kingdom = match.getString("kingdom");
					if (canocialName.equalsIgnoreCase(key.scientificName)) {
						if (given(key.kingdom)) {
							if (kingdom.equalsIgnoreCase(key.kingdom)) {
								return match.getInteger("key");
							}
						} else {
							return match.getInteger("key");
						}
					}
				}
				return null;
			}
		};
	}

	protected boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private static final class SearchTerm {
		private final String scientificName;
		private final String kingdom;
		private SearchTerm(String scientificName, String kingdom) {
			this.scientificName = scientificName;
			this.kingdom = kingdom;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((kingdom == null) ? 0 : kingdom.hashCode());
			result = prime * result + ((scientificName == null) ? 0 : scientificName.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SearchTerm other = (SearchTerm) obj;
			if (kingdom == null) {
				if (other.kingdom != null)
					return false;
			} else if (!kingdom.equals(other.kingdom))
				return false;
			if (scientificName == null) {
				if (other.scientificName != null)
					return false;
			} else if (!scientificName.equals(other.scientificName))
				return false;
			return true;
		}
	}

	@Override
	public void close() throws Exception {
		HttpClientService client = getClient();
		if (client != null) client.close();
	}

}
