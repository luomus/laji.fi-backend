package fi.luomus.lajitietokeskus;

import java.io.File;
import java.io.FileNotFoundException;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;

public class TestConfig {

	public static Config getConfig() {
		try {
			String base = System.getenv("CATALINA_HOME");
			if (base == null) base = "C:/apache-tomcat";
			File file = new File(base);
			if (!file.exists()) file = new File(System.getProperty("user.home"));
			String fullPath = file.getAbsolutePath() + File.separator + "app-conf" + File.separator + "lajitietokeskus.properties";
			System.out.println("Using test config " + fullPath);
			return new ConfigReader(fullPath);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

}
