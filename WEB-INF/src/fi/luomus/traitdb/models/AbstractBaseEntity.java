package fi.luomus.traitdb.models;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;

public abstract class AbstractBaseEntity {

	public static class ParseException extends RuntimeException {
		private static final long serialVersionUID = -8496797772924152640L;
		public ParseException(String message, Exception e) {
			super(message, e);
		}
	}

	protected static final String YYYY_MM_DD = "yyyy-MM-dd";

	protected boolean onlySearchFields() { return false; }

	public AbstractBaseEntity() {}

	public AbstractBaseEntity(JSONObject json) throws ParseException {
		super();
		try {
			Map<String, Method> setters = ReflectionUtil.getSettersMap(this.getClass());
			for (String key : json.getKeys()) {
				Method setter = setters.get(key);
				if (setter == null) continue;
				if (json.isNull(key)) continue;
				Class<?> paramClass = setter.getParameterTypes()[0];
				if (String.class == paramClass) {
					String v = json.getString(key);
					if (!given(v)) continue;
					setter.invoke(this, v);
				} else if (Qname.class == paramClass) {
					String v = json.getString(key);
					if (!given(v)) continue;
					setter.invoke(this, new Qname(v));
				} else if (Integer.class == paramClass) {
					setter.invoke(this, json.getInteger(key));
				} else if (Double.class == paramClass) {
					setter.invoke(this, json.getDouble(key));
				} else if (Boolean.TYPE == paramClass) {
					setter.invoke(this, json.getBoolean(key));
				} else if (DateValue.class == paramClass) {
					String v = json.getString(key);
					if (!given(v)) continue;
					setter.invoke(this, toDateValue(v));
				} else if (Date.class == paramClass) {
					String v = json.getString(key);
					if (!given(v)) continue;
					setter.invoke(this, toDate(v));
				} else if (AbstractBaseEntity.class.isAssignableFrom(paramClass)) {
					setter.invoke(this, toObject(json, key, paramClass));
				} else if (Collection.class.isAssignableFrom(paramClass)) {
					setter.invoke(this, toList(json, key, setter));
				} else if (Map.class.isAssignableFrom(paramClass)) {
					setter.invoke(this, toMap(json, key));
				} else throw new IllegalArgumentException("No serialization for " + paramClass);
			}
		} catch (Exception e) {
			throw new ParseException("JSON deserialization failed: " + e.getMessage(), e);
		}
	}

	private Object toMap(JSONObject json, String key) {
		LinkedHashMap<String, String> map = new LinkedHashMap<>();
		for (String mapKey : json.getObject(key).getKeys()) {
			map.put(mapKey, json.getObject(key).getString(mapKey));
		}
		return map;
	}

	private ArrayList<Object> toList(JSONObject json, String key, Method setter) throws Exception {
		ParameterizedType type = (ParameterizedType) setter.getGenericParameterTypes()[0];
		Class<?> listValueClass = (Class<?>) type.getActualTypeArguments()[0];

		ArrayList<Object> list = new ArrayList<>();

		if (listValueClass == Qname.class || listValueClass == String.class) {
			Constructor<?> constructor = listValueClass.getConstructor(String.class);
			for (String value : json.getArray(key).iterateAsString()) {
				list.add(constructor.newInstance(value));
			}
			return list;
		}
		if (AbstractBaseEntity.class.isAssignableFrom(listValueClass)) {
			Constructor<?> constructor = listValueClass.getConstructor(JSONObject.class);
			for (JSONObject listValueJsonObject : json.getArray(key).iterateAsObject()) {
				list.add(constructor.newInstance(listValueJsonObject));
			}
			return list;
		}
		throw new IllegalArgumentException("No list serialization for " + listValueClass);
	}

	private Object toObject(JSONObject json, String key, Class<?> paramClass) throws Exception {
		return paramClass.getConstructor(JSONObject.class).newInstance(json.getObject(key));
	}

	public JSONObject toJSON() {
		return toJSON(onlySearchFields());
	}

	private JSONObject toJSON(boolean onlySearch) {
		JSONObject json = new JSONObject();
		String currentField = "";
		try {
			for (Method getter : getGetters(onlySearch)) {
				Object value = getter.invoke(this);
				if (value == null) continue;
				String fieldName = ReflectionUtil.cleanGetterFieldName(getter);
				currentField = fieldName;
				Class<?> returnClass = getter.getReturnType();
				if (String.class == returnClass) {
					json.setString(fieldName, value.toString());
				} else if (Qname.class == returnClass) {
					json.setString(fieldName, value.toString());
				} else if (Integer.class == returnClass) {
					json.setInteger(fieldName, (int)value);
				} else if (Double.class == returnClass) {
					json.setDouble(fieldName, (double)value);
				} else if (Boolean.TYPE == returnClass) {
					json.setBoolean(fieldName, (boolean)value);
				} else if (DateValue.class == returnClass) {
					json.setString(fieldName, toDateString((DateValue)value));
				} else if (Date.class == returnClass) {
					json.setString(fieldName, toDateString((Date)value));
				} else if (AbstractBaseEntity.class.isAssignableFrom(returnClass)) {
					json.setObject(fieldName, ((AbstractBaseEntity)value).toJSON(onlySearch));
				} else if (Collection.class.isAssignableFrom(returnClass)) {
					Collection<?> col = (Collection<?>) value;
					if (col.isEmpty()) continue;
					JSONArray array = json.getArray(fieldName);
					for (Object o : col) {
						if (o instanceof AbstractBaseEntity) {
							array.appendObject(((AbstractBaseEntity)o).toJSON(onlySearch));
						} else if (o instanceof Qname) {
							array.appendString(((Qname)o).toString());
						} else if (o instanceof String) {
							array.appendString(o.toString());
						} else throw new IllegalArgumentException("No array serialization for " + o.getClass());
					}
				} else if (Map.class.isAssignableFrom(returnClass)) {
					JSONObject dict = json.getObject(fieldName);
					for (Map.Entry<?, ?> e : ((Map<?, ?>)value).entrySet()) {
						dict.setString(e.getKey().toString(), e.getValue().toString());
					}
				} else throw new IllegalArgumentException("No serialization for " + returnClass);
			}
		} catch (Exception e) {
			throw new IllegalStateException("JSON serialization failed: " + currentField, e);
		}
		return json;
	}

	private static final Map<Class<?>, List<Method>> GETTERS = new HashMap<>();
	private static final Map<Class<?>, List<Method>> SEARCH_GETTERS = new HashMap<>();

	protected List<Method> getGetters(boolean onlySearch) throws NoSuchMethodException {
		Map<Class<?>, List<Method>> getterMap = onlySearch ? SEARCH_GETTERS : GETTERS;
		List<Method> getters = getterMap.get(this.getClass());
		if (getters == null) {
			getters = initGetters(onlySearch);
			getterMap.put(this.getClass(), getters);
		}
		return getters;
	}

	private List<Method> initGetters(boolean onlySearch) throws NoSuchMethodException {
		List<Method> getters = new ArrayList<>();
		for (Field field : getSortedFields(onlySearch)) {
			getters.add(ReflectionUtil.getGetter(field.getName(), this.getClass()));
		}
		return getters;
	}

	private Method getGetter(Field field) throws NoSuchMethodException {
		return ReflectionUtil.getGetter(field.getName(), this.getClass());
	}

	private static final Map<Class<?>, List<Field>> FIELDS = new HashMap<>();
	private static final Map<Class<?>, List<Field>> SEARCH_FIELDS = new HashMap<>();

	private List<Field> getSortedFields(boolean onlySearch) {
		return getSortedFields(this.getClass(), onlySearch);
	}

	protected static List<Field> getSortedFields(Class<? extends AbstractBaseEntity> entityClass, boolean onlySearch) {
		Map<Class<?>, List<Field>> fieldMap = onlySearch ? SEARCH_FIELDS : FIELDS;
		List<Field> fields = fieldMap.get(entityClass);
		if (fields == null) {
			fields = initSortedFields(entityClass, onlySearch);
			fieldMap.put(entityClass, fields);
		}
		return fields;
	}

	private static List<Field> initSortedFields(Class<? extends AbstractBaseEntity> entityClass, boolean onlySearch) {
		List<Field> fields = new ArrayList<>();
		for (Field field : entityClass.getDeclaredFields()) {
			if (!field.isAnnotationPresent(FieldDef.class)) continue;
			if (onlySearch) {
				if (field.getAnnotation(FieldDef.class).ignoreInSearch()) continue;
			}
			fields.add(field);
		}
		Collections.sort(fields, new Comparator<Field>() {
			@Override
			public int compare(Field f1, Field f2) {
				double order1 = f1.getAnnotation(FieldDef.class).order();
				double order2 = f2.getAnnotation(FieldDef.class).order();
				return Double.compare(order1, order2);
			}
		});
		return fields;
	}

	private Object toDate(String v) throws java.text.ParseException {
		return DateUtils.convertToDate(v, YYYY_MM_DD);
	}

	private String toDateString(Date v) {
		return DateUtils.format(v, YYYY_MM_DD);
	}

	private Object toDateValue(String v) {
		return DateUtils.convertToDateValue(v, YYYY_MM_DD);
	}

	private String toDateString(DateValue v) {
		return DateUtils.format(v, YYYY_MM_DD);
	}

	private boolean given(String s) {
		return s != null && !s.trim().isEmpty();
	}

	/**
	 *
	 * @param insert true = insert, false = update
	 * @param dao
	 * @return error messages as map: field: error; empty map if no errors
	 */
	public Map<String, String> validate(boolean insert, TraitDAO dao) {
		Map<String, String> errors = new LinkedHashMap<>();
		for (Field field : getSortedFields(false)) {
			try {
				Object value = getGetter(field).invoke(this);
				validate(insert, errors, field, null, value, dao);
			} catch (Exception e) {
				throw new IllegalStateException("Validation of " + this.getClass().getSimpleName() + " failed", e);
			}
		}
		return errors;
	}

	private void validate(boolean insert, Map<String, String> errors, Field field, String fieldName, Object value, TraitDAO dao) {
		if (fieldName == null) fieldName  = field.getName();
		FieldDef def = field.getAnnotation(FieldDef.class);
		if (field.getName().equals("id") && insert) {
			if (given(value)) error("Id set before insert", fieldName, errors);
			return;
		}
		if (value != null && Collection.class.isAssignableFrom(value.getClass())) {
			int i = -1;
			for (Object colVal : (Collection<?>)value) {
				i++;
				if (AbstractBaseEntity.class.isAssignableFrom(colVal.getClass())) {
					for (Map.Entry<String, String> e : ((AbstractBaseEntity)colVal).validate(insert, dao).entrySet()) {
						if (!insert && e.getKey().equals("id")) continue;
						error(e.getValue(),  field.getName() + "[" + i + "]" + e.getKey(), errors);
					}
				} else {
					validate(insert, errors, field, field.getName() + "[" + i + "]", colVal, dao);
				}
			}
			return;
		}
		if (def.required() && !given(value)) error("Required field", fieldName, errors);
		if (value != null && def.length() > 0) {
			if (value.toString().length() > def.length()) error("Too long value, maximum is " + def.length(), fieldName, errors);
		}
		if (value != null && given(def.enumeration())) {
			Enumeration enumeration = dao.getEnumeration(new Qname(def.enumeration()));
			try {
				enumeration.get(new Qname(value.toString()));
			} catch (EntityNotFoundException e) {
				error("Unknown value " + value, fieldName, errors);
			}
		}
		if (value != null && Subject.class == value.getClass()) {
			errors.putAll(((Subject)value).validate(insert, dao));
		}
	}

	private boolean given(Object value) {
		return value != null && !value.toString().isEmpty();
	}

	private void error(String error, String field, Map<String, String> errors) {
		if (errors.containsKey(field)) {
			error = errors.get(field) + "; " + error;
		}
		errors.put(field, error);
	}

	@Override
	public abstract boolean equals(Object obj);

	@Override
	public abstract int hashCode();

	@Override
	public abstract String toString();

}