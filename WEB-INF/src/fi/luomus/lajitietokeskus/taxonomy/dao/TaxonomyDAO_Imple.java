package fi.luomus.lajitietokeskus.taxonomy.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;

import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.RedListEvaluationGroup;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfProperties;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.AdministrativeStatusContainer;
import fi.luomus.commons.taxonomy.HabitatOccurrenceCounts.HabitatOccurrenceCount;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple.InfiniteTaxonLoopException;
import fi.luomus.commons.taxonomy.InformalTaxonGroupContainer;
import fi.luomus.commons.taxonomy.Occurrences;
import fi.luomus.commons.taxonomy.Occurrences.Occurrence;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.commons.taxonomy.TaxonSearch;
import fi.luomus.commons.taxonomy.TaxonSearchDAOSQLQueryImple;
import fi.luomus.commons.taxonomy.TaxonSearchDataSourceDefinition;
import fi.luomus.commons.taxonomy.TaxonSearchResponse;
import fi.luomus.commons.taxonomy.TaxonomyDAOBaseImple;
import fi.luomus.commons.taxonomy.TripletToImageHandlers;
import fi.luomus.commons.taxonomy.iucn.Evaluation;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.taxonomy.dao.BoldService.BoldRecordTaxon;
import fi.luomus.lajitietokeskus.taxonomy.dao.ElasticSearchPushService.AlreadyRunningException;
import fi.luomus.triplestore.dao.TriplestoreDAO;
import fi.luomus.triplestore.dao.TriplestoreDAOConst;
import fi.luomus.triplestore.dao.TriplestoreDAOImple;
import fi.luomus.triplestore.taxonomy.dao.IucnDAOImple;
import fi.luomus.triplestore.taxonomy.iucn.model.EvaluationTarget;
import fi.luomus.triplestore.taxonomy.iucn.model.EvaluationYear;

public class TaxonomyDAO_Imple extends TaxonomyDAOBaseImple implements AutoCloseable {

	public static final int TAXON_IMAGE_COUNT = 36;

	private static final String SEPARATOR = "\u001F";

	private static final Qname NOT_EVALUATED = new Qname("MX.typeOfOccurrenceNotEvaluated");
	private static final Qname BASED_ON_OCCURRENCES = new Qname("MX.typeOfOccurrenceOccursBasedOnOccurrences");
	private static final Qname FINBIF_MASTER_CHECKLIST = new Qname("MR.1");

	private static final class CompleteableInMemoryTaxonContainerImple extends InMemoryTaxonContainerImple {
		public CompleteableInMemoryTaxonContainerImple(
				InformalTaxonGroupContainer informalTaxonGroupContainer,
				InformalTaxonGroupContainer redListEvaluationGroupContainer,
				AdministrativeStatusContainer administrativeStatusContainer) {
			super(
					informalTaxonGroupContainer,
					redListEvaluationGroupContainer,
					administrativeStatusContainer);
		}
		private boolean loadingCompleted = false;
		public boolean isCompleted() {
			return loadingCompleted;
		}
		public void setLoadingCompleted() {
			loadingCompleted = true;
		}
	}

	private CompleteableInMemoryTaxonContainerImple taxonContainer;
	private final Cached<TaxonSearch, TaxonSearchResponse> cachedTaxonSearches;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private final HikariDataSource dataSource;
	private final ElasticSearchPushService esService;
	private final BoldService boldService;
	private final ErrorReporter errorReporter;
	private final Config config;
	private final TaxonToMap taxonMapper;
	private static final Object LOCK = new Object();

	public TaxonomyDAO_Imple(Config config, ErrorReporter errorReporter) {
		super(config);
		this.config = config;
		this.dataSource = TaxonSearchDataSourceDefinition.initDataSource(config.connectionDescription("Taxonomy"));
		this.taxonContainer = null;
		this.cachedTaxonSearches = new Cached<>(
				new TaxonSearchLoader(), 12, TimeUnit.HOURS, taxonSearchCacheSize());
		this.taxonMapper = initTaxonMapper();
		this.esService = new ElasticSearchPushService(config, errorReporter, this, taxonMapper);
		this.boldService = new BoldService(config, errorReporter);
		this.errorReporter = errorReporter;
		startNightlyTasks();
		startWeeklyTasks();
	}

	@Override
	public void close() {
		if (this.dataSource != null) dataSource.close();
		try {
			if (scheduler != null) {
				scheduler.shutdownNow();
			}
		} catch (Exception e) {}
	}

	private int taxonSearchCacheSize() {
		if (config.productionMode()) return 150000;
		return 1000;
	}

	private TaxonToMap initTaxonMapper() {
		try {
			TriplestoreDAO triplestoreDAO = createTriplestoreDAO(dataSource, errorReporter);
			RdfProperties evaluationProperties = triplestoreDAO.getProperties(Evaluation.EVALUATION_CLASS);
			if (!evaluationProperties.hasProperty(Evaluation.HABITAT)) {
				RdfProperties habitatObject = triplestoreDAO.getProperties(Evaluation.HABITAT_OBJECT_CLASS);
				evaluationProperties.addProperty(habitatObject.getProperty(Evaluation.HABITAT));
			}
			return new TaxonToMap(this, evaluationProperties, getAreas());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static TriplestoreDAO createTriplestoreDAO(HikariDataSource dataSource, ErrorReporter errorReporter) {
		TriplestoreDAOConst.SCHEMA = "ltkm_luonto";
		return new TriplestoreDAOImple(dataSource, TriplestoreDAO.SYSTEM_USER, errorReporter);
	}

	private static IucnDAOImple createIucnDAO(TaxonContainer taxonContainer, HikariDataSource dataSource, Config config, ErrorReporter errorReporter) {
		TriplestoreDAO triplestoreDAO = createTriplestoreDAO(dataSource, errorReporter);
		IucnDAOImple iucnDAO = new IucnDAOImple(config, limitedTaxonLoad(config), triplestoreDAO, taxonContainer, errorReporter);
		return iucnDAO;
	}

	private void startNightlyTasks() {
		long repeatPeriod = TimeUnit.DAYS.toSeconds(1);
		long initialDelay = calculateInitialDelayTill(5);
		scheduler.scheduleAtFixedRate(
				nightyTasks(),
				initialDelay, repeatPeriod, TimeUnit.SECONDS);
	}

	private void startWeeklyTasks() {
		long repeatPeriod = TimeUnit.DAYS.toSeconds(1) * 7;
		long initialDelay = calculateInitialDelayTill(1);
		scheduler.scheduleAtFixedRate(
				weeklyTasks(),
				initialDelay, repeatPeriod, TimeUnit.SECONDS);
	}

	public Runnable nightyTasks() {
		return new Runnable() {
			@Override
			public void run() {
				try {
					reloadTriplets();
					cachedTaxonSearches.invalidateAll();
					taxonContainer = null;
					getTaxonContainer();
					pushToElasticSearch();
				} catch (AlreadyRunningException e) {
					errorReporter.report("Scheduled nightly taxon refresh: Elastic push was already running!");
				} catch (Exception e) {
					errorReporter.report("Nightly scheduler", e);
				}
			}

			private void pushToElasticSearch() throws Exception {
				completeLoading();
				esService.pushAll();
			}
		};
	}

	private Runnable weeklyTasks() {
		return new Runnable() {
			@Override
			public void run() {
				boldService.reloadFeed();
			}
		};
	}

	private long calculateInitialDelayTill(int hour) {
		ZonedDateTime now = ZonedDateTime.now();
		ZonedDateTime nextRun = now.withHour(hour).withMinute(0).withSecond(0);
		if (now.compareTo(nextRun) > 0)
			nextRun = nextRun.plusDays(1);

		Duration duration = Duration.between(now, nextRun);
		return duration.getSeconds();
	}

	public void completeLoading() throws Exception {
		synchronized (LOCK) {
			System.out.println("Laji-backend: Filling additional taxon information...");
			CompleteableInMemoryTaxonContainerImple taxonContainer = getTaxonContainer();
			if (taxonContainer.isCompleted()) {
				System.out.println("Laji-backend: Filling additional taxon information already done!");
				return;
			}
			SimpleTransactionConnection con = null;
			try {
				con = new SimpleTransactionConnection(dataSource.getConnection());
				Set<String> possiblyLimitedIds = getLimitedIdsBasedOnMode(config);
				addIucnRedListGroups(taxonContainer, con);
				addEvaluations(taxonContainer);
				addHabitats(taxonContainer, con, possiblyLimitedIds);
				addBiogeographicalProvinceOccurrences(taxonContainer, con, possiblyLimitedIds);
				addObservationCounts(taxonContainer);
				taxonContainer.generateInheritedOccurrencesAndHabitats();
				limitHabitatCountsToTop(10, taxonContainer);
				addImages(taxonContainer, con, possiblyLimitedIds);
				addBoldRecords(taxonContainer);
				taxonContainer.setLoadingCompleted();
				System.out.println("Laji-backend: Additional taxon information added!");
			} catch (Exception e) {
				System.out.println("Laji-backend: Filling additional taxon information failed! Clearing taxon data!");
				taxonContainer = null;
				if (e instanceof TaxonLoadingException) throw e;
				throw new TaxonLoadingException("Complete loading failed", e);
			}
			finally {
				if (con != null) con.release();
			}
		}
	}

	private void limitHabitatCountsToTop(int top, CompleteableInMemoryTaxonContainerImple taxonContainer) {
		for (Taxon t : taxonContainer.getAll()) {
			if (t.hasHabitatOccurrenceCounts()) {
				t.getHabitatOccurrenceCounts().retainTop(top);
			}
		}
	}

	private void completeLoadingEvaluations() {
		IucnDAOImple iucnDAO = createIucnDAO(getTaxonContainer(), dataSource, config, errorReporter);
		System.out.println("Laji-backend: Completing IUCN evaluations ... ");

		List<Evaluation> incompleted = getTaxonContainer().getAll().stream().flatMap(t->t.getRedListEvaluations().stream()).filter(e->e.isIncompletelyLoaded()).collect(Collectors.toList());
		AtomicInteger i = new AtomicInteger(0);
		incompleted.parallelStream().forEach(evaluation -> {
			if (i.incrementAndGet() % 10000 == 0) System.out.println("Laji-backend: ... " + i + " / "+incompleted.size());
			try {
				iucnDAO.completeLoading(evaluation);
			} catch (Exception e) {
				errorReporter.report("Could not load " + evaluation.getId() + " for taxon " + evaluation.getSpeciesQname(), e);
			}
		});
		System.out.println("Laji-backend: IUCN evaluations completed!");
	}

	public ElasticSearchPushService getElasticSearchPushService() {
		return esService;
	}

	@Override
	public Taxon getTaxon(Qname qname) {
		return getTaxonContainer().getTaxon(qname);
	}

	private static Set<String> getLimitedIdsBasedOnMode(Config config) {
		if (limitedTaxonLoad(config)) {
			return generateLimitedIds(config);
		}
		return Collections.emptySet();
	}

	private static Set<String> generateLimitedIds(Config config) {
		try (HttpClientService client = new HttpClientService()) {
			Set<String> ids = new HashSet<>();
			ids.addAll(getLimitedIds(config, client, "MX.272980")); // niinijalokirsikäs (multiple secondary habitats)
			ids.addAll(getLimitedIds(config, client, "MX.59808")); //  kangashietakoi (primary habitat has several habitatSpecificTypes)
			ids.addAll(getLimitedIds(config, client, "MX.34567")); // talitiainen (have to have this)
			ids.addAll(getLimitedIds(config, client, "MX.231375")); // sokeritoukka (has evaluations in limited iucn dao evaluation mode)
			return ids;
		} catch (Exception e) {
			throw new RuntimeException("Getting limited taxon load taxon ids from taxon api", e);
		}
	}

	private static Collection<String> getLimitedIds(Config config, HttpClientService client, String childId) throws ClientProtocolException, IOException, URISyntaxException {
		Collection<String> ids = new ArrayList<>();
		ids.add(childId);
		URIBuilder uri = new URIBuilder(config.get("TaxonomyAPIURL") + "/"+childId+"/parents").addParameter("selectedFields", "id");
		JSONArray response = new JSONArray(client.contentAsString(new HttpGet(uri.getURI())));
		for (JSONObject o : response.iterateAsObject()) {
			ids.add(o.getString("id"));
		}
		return ids;
	}

	private static class TaxonContainerLoader {
		private final Config config;
		private final ErrorReporter errorReporter;
		private final TaxonomyDAO_Imple taxonomyDAO;
		private final File tripletFile;

		public TaxonContainerLoader(Config config, ErrorReporter errorReporter, TaxonomyDAO_Imple taxonomyDAO, File tripletFile) {
			this.config = config;
			this.errorReporter = errorReporter;
			this.taxonomyDAO = taxonomyDAO;
			this.tripletFile = tripletFile;
		}

		public CompleteableInMemoryTaxonContainerImple load() {
			try {
				System.out.println("Laji-backend: Starting to create taxon container...");
				CompleteableInMemoryTaxonContainerImple taxonContainer = new CompleteableInMemoryTaxonContainerImple(
						new InformalTaxonGroupContainer(taxonomyDAO.getInformalTaxonGroupsForceReload()),
						new InformalTaxonGroupContainer(taxonomyDAO.getRedListEvaluationGroupsForceReload()),
						new AdministrativeStatusContainer(taxonomyDAO.getAdministrativeStatusesForceReload()));
				loadTaxa(taxonContainer, tripletFile);
				loadObservationCounts(taxonContainer);
				taxonContainer.setHasMediaFilter(Collections.emptySet());
				List<InfiniteTaxonLoopException> ex = taxonContainer.generateTaxonomicOrders();
				for (InfiniteTaxonLoopException e : ex) {
					errorReporter.report(e);
					e.printStackTrace();
				}
				System.out.println("Laji-backend: Taxon container creation done");
				return taxonContainer;
			} catch (Exception e) {
				errorReporter.report(e);
				throw new RuntimeException(e);
			}
		}

		private void loadObservationCounts(CompleteableInMemoryTaxonContainerImple taxonContainer) {
			if (config.developmentMode()) return;
			int i = 0;
			while (i < 5) {
				try {
					taxonomyDAO.addObservationCountsNeededForTaxonSearch(taxonContainer);
					return;
				} catch (Exception e) {
					System.out.println("Laji-backend: ... loading limited taxon observation counts FAILED! Retrying in 30 sec ...");
					try { Thread.sleep(1000*30); } catch (InterruptedException e1) { }
					i++;
				}
			}
			try {
				taxonomyDAO.addObservationCountsNeededForTaxonSearch(taxonContainer);
			} catch (Exception e) {
				errorReporter.report("Taxon loading: failed to load occurrence counts -- taxon name search sorting does not work as it should", e);
				e.printStackTrace();
			}
		}

		private void loadTaxa(InMemoryTaxonContainerImple taxonContainer, File tripletFile) throws Exception {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(tripletFile), "UTF-8"), 1024*1024)) {
				System.out.println("Laji-backend: Reading triplets from " + tripletFile.getAbsolutePath() + " ... ");
				int i = 0;
				String line = null;
				while ((line = reader.readLine()) != null) {
					addTaxonInformation(line, taxonContainer);
					i++;
					if (i % 100000 == 0 || i == 1) {
						System.out.println("Laji-backend: ... taxon triplet row " + i);
					}
				}
			}
		}

		private void addTaxonInformation(String line, InMemoryTaxonContainerImple taxonContainer) {
			String[] parts = line.split(SEPARATOR, -1);
			Qname taxonId = new Qname(parts[0]);
			Qname predicate = new Qname(parts[1]);
			Qname object = new Qname(parts[2]);

			String resourceliteral = parts[3];
			if (resourceliteral.isEmpty()) resourceliteral = null;

			String locale = parts[4];
			if (locale.isEmpty()) locale = null;

			Qname context = new Qname(parts[5]);
			if (!context.isSet()) context = null;

			taxonContainer.handle(taxonId, predicate, object, resourceliteral, locale, context);
		}

	}

	private class TaxonSearchLoader implements Cached.CacheLoader<TaxonSearch, TaxonSearchResponse> {
		@Override
		public TaxonSearchResponse load(TaxonSearch key) {
			try {
				return uncachedTaxonSearch(key);
			} catch (Exception e) {
				throw exceptionAndReport("Taxon search with terms " + key.toString(), e);
			}
		}
	}

	private TaxonSearchResponse uncachedTaxonSearch(TaxonSearch taxonSearch) throws Exception {
		return new TaxonSearchDAOSQLQueryImple(this, dataSource, "ltkm_luonto").search(taxonSearch);
	}

	public RuntimeException exceptionAndReport(String message, Exception e) {
		errorReporter.report(message, e);
		return new RuntimeException(message, e);
	}

	@Override
	public TaxonSearchResponse search(TaxonSearch taxonSearch) throws Exception {
		return cachedTaxonSearches.get(taxonSearch);
	}

	@Override
	public CompleteableInMemoryTaxonContainerImple getTaxonContainer() {
		if (taxonContainer == null) {
			synchronized (LOCK) {
				if (taxonContainer == null) {
					File tripletFile = getTripletFile();
					if (!tripletFile.exists()) {
						tripletFile = reloadTriplets();
					}
					taxonContainer = new TaxonContainerLoader(config, errorReporter, this, tripletFile).load();
				}
			}
		}
		return taxonContainer;
	}

	public void clearTaxonContainer() {
		taxonContainer = null;
	}

	public File reloadTriplets() {
		synchronized (LOCK) {
			File finalFile = tripletFile();
			File backupFile = new File(tempFolder(), "taxon_triplets_BACKUP.txt");
			File tempFile = new TaxonTripletLoader(config, errorReporter, dataSource, this).load();
			if (finalFile.exists()) {
				if (backupFile.exists()) {
					backupFile.delete();
				}
				finalFile.renameTo(backupFile);
			}
			tempFile.renameTo(finalFile);
			System.out.println("Laji-backend: Taxon triplets saved to " + finalFile.getAbsolutePath());
			return finalFile;
		}
	}

	private File getTripletFile() {
		File tripletFile = tripletFile();
		if (tripletFile.exists()) return tripletFile;
		synchronized (LOCK) {
			if (tripletFile.exists()) return tripletFile;
			tripletFile = reloadTriplets();
			return tripletFile;
		}
	}

	private File tripletFile() {
		return new File(tempFolder(), "taxon_triplets.txt");
	}

	private File tempFolder() {
		return new File(config.baseFolder() + config.get("StorageFolder"));
	}

	private class TaxonTripletLoader {

		private final HikariDataSource dataSource;
		private final ErrorReporter errorReporter;
		private final TaxonomyDAO_Imple taxonomyDAO;
		private final Set<String> limitToIds;
		private final Set<String> limitToChecklistIds;

		public TaxonTripletLoader(Config config, ErrorReporter errorReporter, HikariDataSource dataSource, TaxonomyDAO_Imple taxonomyDAO) {
			this.dataSource = dataSource;
			this.errorReporter = errorReporter;
			this.taxonomyDAO = taxonomyDAO;
			this.limitToIds = getLimitedIdsBasedOnMode(config);
			this.limitToChecklistIds = getLimitedToChecklistIdsBasedOnMode(config);
		}

		public File load() {
			TransactionConnection con = null;
			try {
				System.out.println("Laji-backend: Starting to load taxon triplets...");
				con = new SimpleTransactionConnection(dataSource.getConnection());
				File f = loadUsing(con);
				System.out.println("Laji-backend: Taxon triplets loading done");
				return f;
			} catch (Exception e) {
				errorReporter.report(e);
				throw new RuntimeException(e);
			}
			finally {
				if (con != null) con.release();
			}

		}

		private  Set<String> getLimitedToChecklistIdsBasedOnMode(Config config) {
			if (config.productionMode()) {
				return getPublicChecklistIds();
			}
			return Utils.set(FINBIF_MASTER_CHECKLIST.toString(), "MR.84", "MR.344");
		}

		private Set<String> getPublicChecklistIds() {
			try {
				return taxonomyDAO.getChecklists().values().stream()
						.filter(c->c.isPublic())
						.map(c->c.getQname().toString())
						.collect(Collectors.toSet());
			} catch (Exception e) {
				throw new RuntimeException("Get public checklist", e);
			}
		}

		private File loadUsing(TransactionConnection con) throws Exception {
			PreparedStatement p = null;
			ResultSet rs = null;
			File tempFile = new File(tempFolder(), "taxon-triplets-"+DateUtils.getFilenameDatetime()+".txt");
			tempFile.getParentFile().mkdirs();
			try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile), "UTF-8"), 1024*1024)) {
				String sql = getTaxonLoadSQL();
				p = con.prepareStatement(sql);
				System.out.println("Laji-backend: Executing taxon triplet query...");
				rs = p.executeQuery();
				rs.setFetchSize(4001);
				System.out.println("Laji-backend: Received first taxon triplet query response, going through result set...");
				int i = 0;
				while (rs.next()) {
					write(rs, writer);
					i++;
					if (i % 100000 == 0 || i == 1) {
						System.out.println("Laji-backend: ... taxon triplet row " + i);
					}
				}
				return tempFile;
			} catch (Exception e) {
				if (tempFile.exists()) {
					try { tempFile.delete(); } catch (Exception e2) {}
				}
				throw e;
			} finally {
				Utils.close(p, rs);
			}
		}

		private String getTaxonLoadSQL() {
			StringBuilder sql = new StringBuilder()
					.append(" SELECT     s.subjectname, s.predicatename, s.objectname, s.resourceliteral, s.langcodefk, s.contextname ")
					.append(" FROM       rdf_statementview s ")
					.append(" LEFT JOIN  rdf_statementview s2 on s.subjectname = s2.subjectname AND s2.predicatename = 'MX.nameAccordingTo' ")
					.append(" WHERE      s.subjectname IN (     ")
					.append(" 				SELECT distinct subjectname     ")
					.append(" 				FROM   rdf_statementview     ")
					.append(" 				WHERE  predicatename = 'rdf:type' ")
					.append(" 				AND    objectname = 'MX.taxon' ");
			if (!limitToIds.isEmpty()) {
				sql.append(" 				AND subjectname IN ( ");
				Utils.toCommaSeperatedStatement(sql, limitToIds, true);
				sql.append(" 				) ");
			}
			sql.append(" ) ");
			sql.append(" AND (s2.objectname IS NULL OR s2.objectname IN ( ");
			Utils.toCommaSeperatedStatement(sql, limitToChecklistIds, true);
			sql.append(" )) " );
			sql.append(" ORDER BY s.subjectname ");
			return sql.toString();
		}

		private void write(ResultSet rs, BufferedWriter writer) throws SQLException, IOException {
			String taxonId = s(rs, 1);
			String predicate = s(rs, 2);
			String object = s(rs, 3);
			String resourceliteral = s(rs, 4);
			String locale = s(rs, 5);
			String context = s(rs, 6);
			w(writer, taxonId);
			w(writer, predicate);
			w(writer, object);
			w(writer, resourceliteral);
			w(writer, locale);
			w(writer, context);
			writer.write('\n');
		}

		private void w(BufferedWriter writer, String s) throws IOException {
			writer.write(s);
			writer.write(SEPARATOR);
		}

		private String s(ResultSet rs, int i) throws SQLException {
			String s = rs.getString(i);
			if (s == null) return "";
			return s.replace(SEPARATOR, "").replace("\n", "").replace("\r", "");
		}

	}

	public Map<Class<?>, Map<String, Object>> getTaxonPropertyMapping() {
		return taxonMapper.getMappings();
	}

	private static final String YES = "YES";
	private static final String LIMITED_TAXON_LOAD = "LIMITED_TAXON_LOAD";

	private static boolean limitedTaxonLoad(Config config) {
		return config.defines(LIMITED_TAXON_LOAD) && YES.equals(config.get(LIMITED_TAXON_LOAD));
	}

	private void addHabitats(InMemoryTaxonContainerImple taxonContainer, TransactionConnection con, Set<String> possiblyLimitedIds) throws SQLException {
		System.out.println("Laji-backend: ... loading habitats ...");
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT 	taxon.subjectname AS taxonId, ");
			sql.append(" 			habitatId.objectname AS habitatId,  ");
			sql.append(" 			habitatId.predicatename AS type,  ");
			sql.append(" 			habitat.objectname AS habitat,  ");
			sql.append(" 			sortOrder.resourceliteral AS sortOrder, ");
			sql.append(" 			habitatSpecificType.objectname AS habitatSpecificType ");
			sql.append(" FROM 		rdf_statementview taxon ");
			sql.append(" JOIN 		rdf_statementview habitatId ON taxon.subjectname = habitatId.subjectname AND habitatId.predicatename IN ('MKV.primaryHabitat', 'MKV.secondaryHabitat') ");
			sql.append(" JOIN 		rdf_statementview habitat ON habitatId.objectname = habitat.subjectname AND habitat.predicatename = 'MKV.habitat' ");
			sql.append(" LEFT JOIN rdf_statementview sortOrder ON habitatId.objectname = sortOrder.subjectname AND sortOrder.predicatename = 'sortOrder' ");
			sql.append(" LEFT JOIN	rdf_statementview habitatSpecificType ON habitatId.objectname = habitatSpecificType.subjectname AND habitatSpecificType.predicatename = 'MKV.habitatSpecificType' ");
			sql.append(" WHERE 	taxon.predicatename = 'rdf:type' ");
			sql.append(" AND		taxon.objectname = 'MX.taxon' ");
			if (!possiblyLimitedIds.isEmpty()) {
				sql.append(" AND taxon.subjectname IN (");
				Utils.toCommaSeperatedStatement(sql, possiblyLimitedIds, true);
				sql.append(")");
			}
			sql.append(" ORDER BY	taxonId, habitatId, type ");
			p = con.prepareStatement(sql.toString());
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			HabitatObject habitatObject = new HabitatObject(new Qname(""), null, -1);
			while (rs.next()) {
				// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypeP
				// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypeH
				// MX.59808	MKV.383407	MKV.primaryHabitat		MKV.habitatMkk	0	MKV.habitatSpecificTypePAK
				// MX.59808	MKV.383408	MKV.secondaryHabitat	MKV.habitatIu	0
				Qname taxonId = new Qname(rs.getString(1));
				Qname habitatId = new Qname(rs.getString(2));
				String type = rs.getString(3);
				Qname habitat = new Qname(rs.getString(4));
				int order = intV(rs.getString(5));
				Qname habitatSpecificType = new Qname(rs.getString(6));

				if (habitatId.equals(habitatObject.getId())) {
					habitatObject.addHabitatSpecificType(habitatSpecificType);
				} else {
					habitatObject = new HabitatObject(habitatId, habitat, order);
					if (habitatSpecificType.isSet()) {
						habitatObject.addHabitatSpecificType(habitatSpecificType);
					}
					if (!taxonContainer.hasTaxon(taxonId)) continue;
					Taxon taxon = taxonContainer.getTaxon(taxonId);
					if (type.equals("MKV.primaryHabitat")) {
						taxon.setPrimaryHabitat(habitatObject);
					} else {
						taxon.addSecondaryHabitat(habitatObject);
					}
				}
			}
		} finally {
			Utils.close(p, rs);
		}
		System.out.println("Laji-backend: ... habitats loaded ...");
	}

	private int intV(String string) {
		if (string == null) return 0;
		try {
			return Integer.valueOf(string);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	private void addBiogeographicalProvinceOccurrences(TaxonContainer taxonContainer, TransactionConnection con, Set<String> possiblyLimitedIds) throws SQLException {
		System.out.println("Laji-backend: ... loading biogeographical occurences ...");
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT  occurrence.subjectname, taxon.objectname, area.objectname, status.objectname, year.resourceliteral, notes.resourceliteral, specimenURI.resourceliteral ");
			sql.append(" FROM  rdf_statementview occurrence ");
			sql.append(" JOIN  rdf_statementview area ON (occurrence.subjectname = area.subjectname AND area.predicatename = 'MO.area') ");
			sql.append(" JOIN  rdf_statementview areaType ON (area.objectname = areaType.subjectname AND areaType.predicatename = 'ML.areaType') ");
			sql.append(" JOIN  rdf_statementview taxon ON (occurrence.subjectname = taxon.subjectname AND taxon.predicatename = 'MO.taxon') ");
			sql.append(" JOIN  rdf_statementview status ON (occurrence.subjectname = status.subjectname AND status.predicatename = 'MO.status') ");
			sql.append(" LEFT JOIN  rdf_statementview year ON (occurrence.subjectname = year.subjectname AND year.predicatename = 'MO.year') ");
			sql.append(" LEFT JOIN  rdf_statementview notes ON (occurrence.subjectname = notes.subjectname AND notes.predicatename = 'MO.notes') ");
			sql.append(" LEFT JOIN  rdf_statementview specimenURI ON (occurrence.subjectname = specimenURI.subjectname AND specimenURI.predicatename = 'MO.specimenURI') ");
			sql.append(" WHERE occurrence.predicatename = 'rdf:type' ");
			sql.append(" AND   occurrence.objectname = 'MO.occurrence' ");
			sql.append(" AND   areaType.objectname = 'ML.biogeographicalProvince' ");
			if (!possiblyLimitedIds.isEmpty()) {
				sql.append(" AND taxon.objectname IN (");
				Utils.toCommaSeperatedStatement(sql, possiblyLimitedIds, true);
				sql.append(")");
			}
			p = con.prepareStatement(sql.toString());
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				Qname occurrenceId = new Qname(rs.getString(1));
				Qname taxonId = new Qname(rs.getString(2));
				if (!taxonContainer.hasTaxon(taxonId)) continue;
				Qname area = new Qname(rs.getString(3));
				Qname status = new Qname(rs.getString(4));
				Integer year = iVal(rs.getString(5));
				String notes = rs.getString(6);
				String specimenURI = rs.getString(7);

				Taxon taxon = taxonContainer.getTaxon(taxonId);
				taxon.getOccurrences().setOccurrence(occurrenceId, area, status);
				if (year != null) taxon.getOccurrences().getOccurrence(area).setYear(year);
				if (notes != null) taxon.getOccurrences().getOccurrence(area).setNotes(notes);
				if (specimenURI != null) taxon.getOccurrences().getOccurrence(area).setSpecimenURI(toUri(specimenURI));
			}
		} finally {
			Utils.close(p, rs);
		}
		System.out.println("Laji-backend: ... biogeographical occurences loaded ...");
	}

	private Integer iVal(String string) {
		if (string == null) return null;
		try {
			return Integer.valueOf(string);
		} catch (Exception e) {
			return null;
		}
	}

	private URI toUri(String specimenURI) {
		if (specimenURI == null) return null;
		try {
			return new URI(specimenURI);
		} catch (Exception e) {
			return null;
		}
	}

	public void addObservationCountsNeededForTaxonSearch(InMemoryTaxonContainerImple taxonContainer) throws Exception {
		System.out.println("Laji-backend: ... loading limited taxon observation counts ...");
		JSONObject response = getObservationCountData();
		addObservationCountsNeededForTaxonSearch(taxonContainer, response);
		System.out.println("Laji-backend: ... taxon limited observation counts loaded ...");
	}

	private void addObservationCountsNeededForTaxonSearch(InMemoryTaxonContainerImple taxonContainer, JSONObject response) {
		response.getArray("results").iterateAsObject().forEach(counts -> {
			Qname taxonId = Qname.fromURI(counts.getString("taxonId"));
			if (!taxonContainer.hasTaxon(taxonId)) return;
			Taxon taxon = taxonContainer.getTaxon(taxonId);
			taxon.setExplicitObservationCountFinland(counts.getInteger("countFinland"));
		});
	}

	private void addObservationCounts(InMemoryTaxonContainerImple taxonContainer) throws Exception {
		System.out.println("Laji-backend: ... loading taxon observation counts ...");
		JSONObject response = getObservationCountData();
		addObservationCounts(taxonContainer, response);
		System.out.println("Laji-backend: ... taxon observation counts loaded ...");
	}

	private void addObservationCounts(InMemoryTaxonContainerImple taxonContainer, JSONObject response) {
		response.getArray("results").iterateAsObject().forEach(counts -> {
			Qname taxonId = Qname.fromURI(counts.getString("taxonId"));
			if (!taxonContainer.hasTaxon(taxonId)) return;
			Taxon taxon = taxonContainer.getTaxon(taxonId);
			taxon.setExplicitObservationCount(counts.getInteger("count"));
			taxon.setExplicitObservationCountFinland(counts.getInteger("countFinland"));
			if (counts.hasKey("biogeographicalProvinceCounts")) {
				JSONObject provinceCounts = counts.getObject("biogeographicalProvinceCounts");
				for (String areaQname : provinceCounts.getKeys()) {
					int count = provinceCounts.getInteger(areaQname);
					addBiogeographicalProvinceCount(areaQname, count, taxon);
				}
			}
			if (counts.hasKey("habitatOccurrenceCounts")) {
				JSONObject habitatCounts = counts.getObject("habitatOccurrenceCounts");
				for (String habitat : habitatCounts.getKeys()) {
					int count = habitatCounts.getInteger(habitat);
					addHabitatCount(habitat, count, taxon);
				}
			}
		});
	}

	private void addHabitatCount(String habitat, int count, Taxon taxon) {
		if (count < 1) return;
		if (!validHabitat(habitat)) return;
		taxon.getHabitatOccurrenceCounts().setCount(
				new HabitatOccurrenceCount(habitat, localized(habitat))
				.setOccurrenceCount(count));
	}

	private boolean validHabitat(String habitat) {
		if (habitat == null) return false;
		habitat = habitat.trim();
		if (habitat.length() < 5) return false;
		return true;
	}

	private LocalizedText localized(String habitat) {
		if (habitat.startsWith("http://tun.fi")) {
			return localize(Qname.fromURI(habitat));
		}
		return localizedAsPlain(habitat);
	}

	private LocalizedText localizedAsPlain(String habitat) {
		return new LocalizedText().set("fi", habitat).set("en", habitat).set("sv", habitat);
	}

	private LocalizedText localize(Qname habitat) {
		try {
			LocalizedText localizedText = getLabels(habitat);
			if (localizedText.isEmpty()) return localizedAsPlain(habitat.toString());
			return localizedText;
		} catch (Exception e) {
			return localizedAsPlain(habitat.toString());
		}
	}

	private void addBiogeographicalProvinceCount(String areaQname, int count, Taxon taxon) {
		if (count < 1) return;
		Qname areaId = new Qname(areaQname);
		Occurrences occurrences = taxon.getOccurrences();
		Occurrence occurrence = occurrences.getOccurrence(areaId);
		if (occurrence == null) {
			occurrence = new Occurrence(null, areaId, BASED_ON_OCCURRENCES);
			occurrences.setOccurrence(occurrence);
		} else {
			if (occurrence.getStatus().equals(NOT_EVALUATED)) {
				occurrence.setStatus(BASED_ON_OCCURRENCES);
			}
		}
		occurrence.setOccurrenceCount(count);
	}

	private JSONObject getObservationCountData() throws Exception {
		try (HttpClientService client = new HttpClientService()) {
			String url = config.get("DwURL");
			System.out.println("Laji backend calling " + url);
			return client.contentAsJson(new HttpGet(url));
		}
	}

	private void addImages(InMemoryTaxonContainerImple taxonContainer, TransactionConnection con, Set<String> possiblyLimitedIds) throws SQLException {
		System.out.println("Laji-backend: ... loading taxon media ...");
		Set<Qname> taxaWithExplicitMedia = setImagesToTaxa(taxonContainer, con, possiblyLimitedIds);
		System.out.println("Laji-backend: ... media filter ...");
		taxonContainer.setHasMediaFilter(taxaWithMediaFilter(taxonContainer, taxaWithExplicitMedia));
		System.out.println("Laji-backend: ... choose media ...");
		chooseMedia(taxonContainer);
		System.out.println("Laji-backend: ... taxon media done! ...");
	}

	private void chooseMedia(InMemoryTaxonContainerImple taxonContainer) {
		List<Taxon> roots = getRoots(taxonContainer.getAll());
		for (Taxon root : roots) {
			chooseMedia(root);
		}
	}

	private List<Taxon> getRoots(Collection<Taxon> all) {
		List<Taxon> roots = new ArrayList<>();
		for (Taxon t : all) {
			if (!t.hasParent() && t.hasChildren()) {
				roots.add(t);
			}
		}
		return roots;
	}

	private void chooseMedia(Taxon taxon) {
		if (!taxon.hasChildren()) {
			taxon.sortAndRetainTopMultimedia(TAXON_IMAGE_COUNT);
			return;
		}

		taxon.getChildren().forEach(c->chooseMedia(c));

		if (taxon.isSpecies()) {
			chooseSpeciesMedia(taxon);
		} else {
			chooseHigherTaxaMedia(taxon);
		}
	}

	private void chooseSpeciesMedia(Taxon taxon) {
		taxon.sortAndRetainTopMultimedia(TAXON_IMAGE_COUNT);
		int count = taxon.getMultimedia().size();
		if (count >= TAXON_IMAGE_COUNT) return;

		List<Taxon> childrenWithMediaSorted = getChildrenWithMediaSortedByMostCommon(taxon);
		if (childrenWithMediaSorted.isEmpty()) return;

		int neededCount = TAXON_IMAGE_COUNT - count;
		Map<Taxon, List<Image>> selectedImages = chooseSpeciesMedia(childrenWithMediaSorted, neededCount);

		selectedImages.values().forEach(l-> l.stream().forEach(taxon::addMultimedia));
	}

	private Map<Taxon, List<Image>> chooseSpeciesMedia(List<Taxon> childrenWithMediaSorted, int neededCount) {
		Map<Taxon, List<Image>> selectedImages = new LinkedHashMap<>();
		int i = 0;
		while (neededCount > 0) {
			boolean added = false;
			for (Taxon child : childrenWithMediaSorted) {
				if (child.getMultimedia().size() <= i) continue;
				selectedImages.computeIfAbsent(child, k -> new ArrayList<>()).add(child.getMultimedia().get(i));
				neededCount--;
				added = true;
				if (neededCount <= 0) return selectedImages;
			}
			if (!added) break;
			i++;
		}
		return selectedImages;
	}

	private void chooseHigherTaxaMedia(Taxon taxon) {
		List<Taxon> children = getChildrenWithMediaSortedByMostCommon(taxon);
		if (children.isEmpty()) {
			taxon.sortAndRetainTopMultimedia(TAXON_IMAGE_COUNT);
			return;
		}

		Optional<Image> selfPrimary = taxon.getMultimedia().stream().filter(i->i.isPrimaryForTaxon()).findFirst();

		int neededCount = selfPrimary.isPresent() ? TAXON_IMAGE_COUNT - 1 : TAXON_IMAGE_COUNT;
		List<Image> selectedImages = chooseHigherTaxaMedia(children, neededCount);

		if (selectedImages.size() < neededCount) {
			taxon.getMultimedia().stream()
			.filter(i-> selfPrimary.isPresent() ? !i.equals(selfPrimary.get()) : true)
			.forEach(selectedImages::add);
		}
		taxon.clearMultimedia();
		if (selfPrimary.isPresent()) taxon.addMultimedia(selfPrimary.get());
		selectedImages.stream().limit(neededCount).forEach(taxon::addMultimedia);
	}

	private List<Image> chooseHigherTaxaMedia(List<Taxon> children, int neededCount) {
		Map<Taxon, Integer> howManyImagesPerChild = calculateHowManyImagesPerChild(children, neededCount);
		List<Image> childMedia = new ArrayList<>();
		for (Taxon child : children) {
			Integer count = howManyImagesPerChild.get(child);
			if (count == null) continue;
			childMedia.addAll(chooseHigherTaxaMedia(child, count));
		}
		return childMedia;
	}

	private Map<Taxon, Integer> calculateHowManyImagesPerChild(List<Taxon> children, int neededCount) {
		Map<Taxon, Double> ratios = new HashMap<>();
		for (Taxon child : children) {
			ratios.put(child, imageRatio(child));
		}
		double ratioSum = 0;
		for (Double ratio : ratios.values()) {
			ratioSum += ratio;
		}
		Map<Taxon, Integer> imageCounts = new HashMap<>();
		for (Taxon child : children) {
			double ratio = ratios.get(child);
			imageCounts.put(child, imageCount(ratio, ratioSum, neededCount));
		}

		while (sum(imageCounts.values()) > neededCount) {
			if (allCountsOne(imageCounts.values())) return imageCounts;
			Taxon top = getTaxonWithMostImages(imageCounts);
			int newCount = imageCounts.get(top) - 1;
			if (newCount < 1) return imageCounts;
			imageCounts.put(top, newCount);
		}
		return imageCounts;
	}

	private Double imageRatio(Taxon child) {
		double obsRatio = child.getObservationCountFinland() > 0 ? Math.log10(child.getObservationCountFinland()) : 0;
		double speciesRatio = child.getCountOfFinnishSpecies() > 0 ? Math.log(child.getCountOfFinnishSpecies()) : 0;
		return obsRatio + (speciesRatio * 10);
	}

	private Integer imageCount(double ratio, double ratioSum, int neededCount) {
		int i = (int) Math.ceil((ratio / ratioSum) * neededCount);
		if (i < 1) return 1;
		return i;
	}

	private int sum(Collection<Integer> values) {
		return values.stream().mapToInt(Integer::intValue).sum();
	}

	private boolean allCountsOne(Collection<Integer> values) {
		return values.stream().allMatch(i -> i == 1);
	}

	private Taxon getTaxonWithMostImages(Map<Taxon, Integer> imageCounts) {
		List<Taxon> orderByCount = new ArrayList<>(imageCounts.keySet());
		orderByCount.sort((o1, o2) -> {
			int c = Integer.compare(imageCounts.get(o2), imageCounts.get(o1));
			return (c != 0) ? c : o1.compareTo(o2);
		});
		return orderByCount.get(0);
	}

	private List<Image> chooseHigherTaxaMedia(Taxon child, int count) {
		Map<Taxon, List<Image>> byTaxon = new LinkedHashMap<>();
		child.getMultimedia().stream()
		.filter(i->i.getTaxon().isSpecies())
		.forEach(i -> byTaxon.computeIfAbsent(i.getTaxon(), k -> new ArrayList<>()).add(i));

		List<Image> selected = new ArrayList<>();
		while (true) {
			boolean added = false;
			for (Taxon taxon : byTaxon.keySet()) {
				if (byTaxon.get(taxon).isEmpty()) continue;
				selected.add(byTaxon.get(taxon).remove(0));
				added = true;
				if (selected.size() >= count) return selected;
			}
			if (!added) break;
		}
		return selected;
	}

	private List<Taxon> getChildrenWithMediaSortedByMostCommon(Taxon taxon) {
		return taxon.getChildren().stream()
				.filter(Taxon::hasMultimedia)
				.sorted(Comparator
						.comparingInt(Taxon::getObservationCountFinland)
						.reversed()
						.thenComparing(Taxon::compareTo))
				.collect(Collectors.toList());
	}

	private Set<Qname> taxaWithMediaFilter(InMemoryTaxonContainerImple taxonContainer, Set<Qname> taxaWithExplicitMedia) {
		Set<Qname> taxaWithMediaIncludingParents = new HashSet<>();
		for (Qname taxonId : taxaWithExplicitMedia) {
			if (!taxonContainer.hasTaxon(taxonId)) continue;
			taxaWithMediaIncludingParents.add(taxonId);
			Taxon taxon = taxonContainer.getTaxon(taxonId);
			while (taxon.hasParent()) {
				if (taxonContainer.hasTaxon(taxon.getParentQname())) {
					taxon = taxon.getParent();
					taxaWithMediaIncludingParents.add(taxon.getId());
				}
			}
		}
		return taxaWithMediaIncludingParents;
	}

	private Set<Qname> setImagesToTaxa(InMemoryTaxonContainerImple taxonContainer, TransactionConnection con, Set<String> possiblyLimitedIds) throws SQLException {
		Set<Qname> taxaWithExplicitMedia = new HashSet<>();

		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT	subjectname, predicatename, objectname, resourceliteral, langcodefk ");
			sql.append(" FROM rdf_statementview WHERE subjectname IN ( ");
			sql.append("     SELECT DISTINCT s.subjectname ");
			sql.append("     FROM 	rdf_statementview s ");
			sql.append("     JOIN	rdf_statementview s2 on (s.subjectname = s2.subjectname and s2.predicatename = 'MM.taxonURI') ");
			sql.append("     JOIN	rdf_statementview s3 on (s.subjectname = s3.subjectname and s3.predicatename = 'MM.fullURL') ");
			sql.append("     WHERE	s.predicatename = 'rdf:type' AND s.objectname = 'MM.image' ");
			if (!possiblyLimitedIds.isEmpty()) {
				sql.append(" AND s2.objectname IN ( ");
				Utils.toCommaSeperatedStatement(sql, possiblyLimitedIds, true);
				sql.append(" ) ");
			}
			sql.append(" ) ");
			sql.append(" ORDER BY subjectname, predicatename ");
			p = con.prepareStatement(sql.toString());
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			String prevImageId = "";
			Image image = null;
			List<Qname> taxonIdsToAddTheImage = new ArrayList<>();
			while (rs.next()) {
				String imageId = rs.getString(1);
				String predicate = rs.getString(2);
				String objectname = rs.getString(3);
				String resourceliteral = rs.getString(4);
				String langcode = rs.getString(5);
				if (imageChanges(prevImageId, imageId)) {
					addToTaxa(taxonContainer, image, taxonIdsToAddTheImage);
					taxaWithExplicitMedia.addAll(taxonIdsToAddTheImage);
					prevImageId = imageId;
					image = new Image(new Qname(imageId));
					taxonIdsToAddTheImage.clear();
				}
				addStatementToImage(image, taxonIdsToAddTheImage, predicate, objectname, resourceliteral, langcode);
			}
			addToTaxa(taxonContainer, image, taxonIdsToAddTheImage);
		} finally {
			Utils.close(p, rs);
		}
		return taxaWithExplicitMedia;
	}

	private void addEvaluations(InMemoryTaxonContainerImple taxonContainer) throws Exception {
		IucnDAOImple iucnDAO = createIucnDAO(taxonContainer, dataSource, config, errorReporter);
		iucnDAO.makeSureEvaluationDataIsLoaded();
		Collection<EvaluationTarget> targets = iucnDAO.getIUCNContainer().getTargets();
		int latestLocked = getLatestLocked(iucnDAO.getEvaluationYears());
		taxonContainer.setLatestLockedRedListEvaluationYear(latestLocked);
		for (EvaluationTarget target : targets) {
			for (Evaluation evaluation : target.getEvaluations()) {
				if (evaluation.getEvaluationYear() != latestLocked) continue;
				if (!evaluation.isReady()) continue;
				Qname taxonId = new Qname(evaluation.getSpeciesQname());
				if (!taxonContainer.hasTaxon(taxonId)) continue;
				Taxon taxon = taxonContainer.getTaxon(taxonId);
				if (notFromMasterChecklist(taxon)) continue;
				taxon.addRedListEvaluation(evaluation);
			}
		}
		completeLoadingEvaluations();
	}

	private int getLatestLocked(List<EvaluationYear> evaluationYears) {
		int max = 0;
		for (EvaluationYear y : evaluationYears) {
			if (y.isLocked()) {
				max = Math.max(max, y.getYear());
			}
		}
		return max;
	}

	private boolean notFromMasterChecklist(Taxon taxon) {
		return !FINBIF_MASTER_CHECKLIST.equals(taxon.getChecklist());
	}

	private void addIucnRedListGroups(InMemoryTaxonContainerImple taxonContainer, TransactionConnection con) throws Exception {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			String sql = "" +
					" SELECT subjectname, predicatename, objectname FROM rdf_statementview " +
					" WHERE predicatename IN ( 'MVL.includesInformalTaxonGroup', 'MVL.includesTaxon' ) ";
			p = con.prepareStatement(sql);
			rs = p.executeQuery();
			rs.setFetchSize(4001);
			while (rs.next()) {
				Qname evaluationGroupId = new Qname(rs.getString(1));
				String predicate = rs.getString(2);
				Qname object = new Qname(rs.getString(3));
				if ("MVL.includesInformalTaxonGroup".equals(predicate)) {
					addRedListEvaluationGroupsOfInformalTaxonGroup(evaluationGroupId, object, taxonContainer);
				} else {
					taxonContainer.addRedListEvaluationGroupOfTaxon(evaluationGroupId, object);
				}
			}
		} finally {
			Utils.close(p, rs);
		}
	}

	private void addRedListEvaluationGroupsOfInformalTaxonGroup(Qname evaluationGroupId, Qname informalTaxonGroupId, InMemoryTaxonContainerImple taxonContainer) throws Exception {
		RedListEvaluationGroup g = getRedListEvaluationGroups().get(evaluationGroupId.toString());
		if (g == null) return;

		taxonContainer.addRedListEvaluationGroupOfInformalTaxonGroup(evaluationGroupId, informalTaxonGroupId);

		for (Qname evaluationGroupParentId : g.getParents()) {
			addRedListEvaluationGroupsOfInformalTaxonGroup(evaluationGroupParentId, informalTaxonGroupId, taxonContainer);
		}
	}

	private static final TripletToImageHandlers TRIPLET_TO_IMAGE_HANDLERS = new TripletToImageHandlers();

	private void addStatementToImage(Image image, List<Qname> taxonIdsToAddTheImage, String predicate, String objectname, String resourceliteral, String locale) {
		TRIPLET_TO_IMAGE_HANDLERS
		.getHandler(new Qname(predicate))
		.setToImage(new Qname(objectname), resourceliteral, locale, image, taxonIdsToAddTheImage);
	}

	private void addToTaxa(InMemoryTaxonContainerImple taxonContainer, Image image, List<Qname> taxonIdsToAddTheImage) {
		if (image == null) return;
		addLicenseName(image);
		for (Qname taxonId : taxonIdsToAddTheImage) {
			if (taxonContainer.hasTaxon(taxonId)) {
				Taxon taxon = taxonContainer.getTaxon(taxonId);
				if (taxonIdsToAddTheImage.size() == 1) {
					image.setTaxon(taxon);
					taxon.addMultimedia(image);
				} else {
					taxon.addCopyForSelf(image);
				}
			}
		}
	}

	private void addLicenseName(Image image) {
		if (image.getLicenseId() == null) return;
		LocalizedText localized = getLicenseFullnames().get(image.getLicenseId().toString());
		if (localized != null) {
			image.setLicenseFullname(localized);
		}
	}

	private boolean imageChanges(String prevImageId, String imageId) {
		return !prevImageId.equals(imageId);
	}

	private void addBoldRecords(CompleteableInMemoryTaxonContainerImple taxonContainer) throws TaxonLoadingException {
		System.out.println("Laji-backend: ... loading bold records ...");
		Map<String, List<BoldRecordTaxon>> recordsByName = boldService.load();
		System.out.println("Laji-backend: ... setting bold records to taxa ...");
		for (Taxon taxon : taxonContainer.getAll()) {
			addBoldRecords(recordsByName, taxon);
		}
		System.out.println("Laji-backend: ... bold records done! ...");
	}

	private void addBoldRecords(Map<String, List<BoldRecordTaxon>> recordsByName, Taxon taxon) {
		if (taxon.isSynonym()) return;
		List<BoldRecordTaxon> recordTaxa = recordsByName.get(taxon.getScientificName());
		if (recordTaxa != null) {
			for (BoldRecordTaxon recordTaxon : recordTaxa) {
				if (higherTaxonomyMatches(recordTaxon, taxon)) {
					taxon.setBold(recordTaxon.getRecords());
					break;
				}
			}
		}
		for (Taxon synonym : taxon.getSynonyms()) {
			addSynonymBoldRecords(recordsByName, synonym, taxon);
		}
	}

	private void addSynonymBoldRecords(Map<String, List<BoldRecordTaxon>> recordsByName, Taxon synonym, Taxon synonymParent) {
		List<BoldRecordTaxon> recordTaxa = recordsByName.get(synonym.getScientificName());
		if (recordTaxa == null) return;
		for (BoldRecordTaxon recordTaxon : recordTaxa) {
			if (higherTaxonomyMatches(recordTaxon, synonymParent)) {
				synonym.setBold(recordTaxon.getRecords());
				return;
			}
		}
	}

	private boolean higherTaxonomyMatches(BoldRecordTaxon recordTaxon, Taxon taxon) {
		if (matches(recordTaxon.getFamilyName(), taxon.getScientificNameOfRank("family"))) return true;
		if (matches(recordTaxon.getClassName(), taxon.getScientificNameOfRank("class"))) return true;
		if (matches(recordTaxon.getPhylumName(), taxon.getScientificNameOfRank("phylum"))) return true;
		return false;
	}

	private boolean matches(String recordSciName, String taxonScientificNameOfRank) {
		if (recordSciName == null) return false;
		return recordSciName.equalsIgnoreCase(taxonScientificNameOfRank);
	}

	public static class TaxonLoadingException extends Exception {
		private static final long serialVersionUID = -8951482144722153435L;
		public TaxonLoadingException(String message, Exception cause) {
			super(message, cause);
		}
	}

}
