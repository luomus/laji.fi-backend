package fi.luomus.traitdb.dao.triplestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.Enumeration;

public class TriplestoreEnumerationsImple extends TriplestoreBaseDAO<Enumeration> {

	private final TraitDAO dao;

	public TriplestoreEnumerationsImple(Config config, TraitDAO dao) {
		super(config);
		this.dao = dao;
	}

	private HttpClientService getProductionTriplestoreClient() {
		try {
			return new HttpClientService(productionTriplestoreURL(), config.get("TriplestoreUsername"), config.get("TriplestorePassword"));
		} catch (Exception e) {
			throw new RuntimeException("Triplestore not properly configured", e);
		}
	}

	private String productionTriplestoreURL() {
		return config.get("TriplestoreURL");
	}

	@Override
	public ValidationResponse validate(Enumeration entity, boolean insert) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	protected Class<Enumeration> concreteClass() {
		return Enumeration.class;
	}

	@Override
	protected Model createModel(Qname id, Enumeration entity) {
		throw new UnsupportedOperationException();
	}

	private static final boolean ALLWAYS_PRODUCTION_TRIPLESTORE = true;
	private static final boolean TRAIT_CONFIGURED_TRIPLESTORE = false;

	@Override
	protected List<Enumeration> getAllActual() {
		List<Enumeration> all = new ArrayList<>();

		// These are fetched from the configured TraitTriplestore -- local and staging = triplestore test, on production -> triplestore production
		all.add(get("TDF.typeEnum", TRAIT_CONFIGURED_TRIPLESTORE));
		all.add(get("TDF.statisticalMethodEnum", TRAIT_CONFIGURED_TRIPLESTORE));
		all.add(get("TDF.rangeEnum", TRAIT_CONFIGURED_TRIPLESTORE));
		all.add(getUnitsOfMeasurement());

		// These are always fetched from production triplestore, even on local and staging
		all.add(get("MY.sexes", ALLWAYS_PRODUCTION_TRIPLESTORE));
		all.add(get("MY.lifeStages", ALLWAYS_PRODUCTION_TRIPLESTORE));
		all.add(get("MY.recordBases", ALLWAYS_PRODUCTION_TRIPLESTORE));
		all.add(get("MZ.intellectualRightsEnum", ALLWAYS_PRODUCTION_TRIPLESTORE));
		all.add(get("MKV.habitatEnum", ALLWAYS_PRODUCTION_TRIPLESTORE));
		all.add(get("MKV.habitatSpecificTypeEnum", ALLWAYS_PRODUCTION_TRIPLESTORE));
		all.add(get("MX.iucnStatuses", ALLWAYS_PRODUCTION_TRIPLESTORE));
		return all;
	}

	private Enumeration getUnitsOfMeasurement() {
		Enumeration enumeration = new Enumeration(new Qname("TDF.unitOfMeasurementEnum"));
		dao.units().getAll().forEach(u->enumeration.add(u.getId(), u.getUnit()));
		return enumeration;
	}

	private Enumeration get(String id, boolean fromProduction) {
		Document doc = query(id, fromProduction);
		Map<Integer, Qname> order = new TreeMap<>();
		for (Node n : doc.getRootNode().getChildNodes().iterator().next().getChildNodes()) {
			if (n.getName().startsWith("rdf:_")) {
				Integer o = Integer.valueOf(n.getName().replace("rdf:_", ""));
				Qname enumValueId = getResource(n);
				order.put(o, enumValueId);
			}
		}
		Enumeration enumeration = new Enumeration(new Qname(id));
		for (Qname enumValueId : order.values()) {
			doc = query(enumValueId.toString(), fromProduction);
			String name = getName(doc);
			if (name != null) {
				enumeration.add(enumValueId, name);
			}
		}
		return enumeration;
	}

	protected Document query(String id, boolean fromProduction) {
		try (HttpClientService client = fromProduction ? getProductionTriplestoreClient() : getTriplestoreClient()) {
			URIBuilder uri = new URIBuilder((fromProduction ? productionTriplestoreURL() : triplestoreURL()) + "/" + id);
			uri.addParameter("format", "rdfxml");
			return client.contentAsDocument(new HttpGet(uri.getURI()));
		} catch (Exception e) {
			throw exception("enum query", id, e);
		}
	}

	private String getName(Document doc) {
		// Prefer label without locale, then en and lastly fi
		String nameFi = null;
		String nameUnk = null;
		for (Node n : doc.getRootNode().getChildNodes().iterator().next().getChildNodes("rdfs:label")) {
			if (n.getContents() == null || n.getContents().isEmpty()) continue;
			if (n.hasAttribute("xml:lang")) {
				String locale = n.getAttribute("xml:lang");
				if ("en".equals(locale)) return cleanEnumName(n.getContents());
				if ("fi".equals(locale)) nameFi = n.getContents();
			} else {
				nameUnk = n.getContents();
			}
		}
		return nameUnk != null ? cleanEnumName(nameUnk) : cleanEnumName(nameFi);
	}

	private String cleanEnumName(String name) {
		if (name.contains("(")) {
			return name.substring(0, name.indexOf("(")).trim();
		}
		return name.trim();
	}

	@Override
	public ValidationResponse validateDelete(Qname id) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

}
