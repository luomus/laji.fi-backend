package fi.luomus.lajitietokeskus.util;

public class Utils {

	public static String camelcaseToUnderscore(String input) {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (int i = 0; i < input.length(); i++) {
			char currentChar = input.charAt(i);
			if (Character.isUpperCase(currentChar)) {
				if (!first) {
					result.append('_');
				}
				result.append(Character.toLowerCase(currentChar));
			} else {
				result.append(currentChar);
			}
			first = false;
		}
		return result.toString();
	}

}
