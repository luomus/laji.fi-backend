package fi.luomus.lajitietokeskus.dao;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.lajitietokeskus.models.ExternalNewsItem;
import fi.luomus.lajitietokeskus.models.NewsItem;

public class LuomusNewsService {

	public static List<NewsItem> parse(JSONObject json) throws Exception {
		List<NewsItem> news = new ArrayList<>();
		for (JSONObject i : json.getArray("nodes").iterateAsObject()) {
			JSONObject newsJson = i.getObject("node");
			String id = newsJson.getString("nid");
			String link = newsJson.getString("url");
			String title = newsJson.getString("title");
			String date = newsJson.getString("published_at");
			news.add(newsItem(id, link, title, date));
		}
		return news;
	}

	private static NewsItem newsItem(String id, String link, String title, String date) throws MalformedURLException, ParseException {
		Timestamp posted = toTimestamp(date);
		URL url = new URL(link);
		return new ExternalNewsItem(id, posted, title, "luomus", url);
	}

	private static Timestamp toTimestamp(String s) throws ParseException {
		return new Timestamp(DateUtils.convertToDate(s).getTime());
	}

}
