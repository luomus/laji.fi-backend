package fi.luomus.traitdb.models.search;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.traitdb.models.AbstractBaseEntity;
import fi.luomus.traitdb.models.FieldDef;

public class TraitTaxon extends AbstractBaseEntity {

	@FieldDef(order=1.1, etsOverride="taxonID")
	private String id;

	@FieldDef(order=1.2, etsOverride="skip")
	private Integer taxonomicOrder;

	@FieldDef(order=2)
	private String taxonRank;

	@FieldDef(order=3.1)
	private String scientificName;

	@FieldDef(order=3.2, etsOverride="skip")
	private boolean cursiveName;

	@FieldDef(order=3.3)
	private String author;

	@FieldDef(order=6, etsOverride="skip")
	private Map<String, String> higherTaxa;

	@FieldDef(order=8, enumeration="MX.iucnStatuses")
	private Qname iucnStatus;

	@FieldDef(order=9.1, enumeration="MKV.habitatEnum")
	private Qname primaryHabitat;

	@FieldDef(order=9.2, enumeration="MKV.habitatSpecificTypeEnum")
	private List<Qname> habitatSpecifiers;

	@FieldDef(order=10)
	private boolean sensitive = false;

	public TraitTaxon() {
		super();
	}

	public TraitTaxon(JSONObject json) {
		super(json);
	}

	public String getId() {
		return id;
	}

	public String getTaxonRank() {
		return taxonRank;
	}

	public String getScientificName() {
		return scientificName;
	}

	public boolean isCursiveName() {
		return cursiveName;
	}

	public String getAuthor() {
		return author;
	}

	public Map<String, String> getHigherTaxa() {
		if (higherTaxa ==  null) higherTaxa = new LinkedHashMap<>();
		return higherTaxa;
	}

	public Qname getIucnStatus() {
		return iucnStatus;
	}

	public Qname getPrimaryHabitat() {
		return primaryHabitat;
	}

	public List<Qname> getHabitatSpecifiers() {
		if (habitatSpecifiers ==  null) habitatSpecifiers = new ArrayList<>();
		return habitatSpecifiers;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTaxonRank(String taxonRank) {
		this.taxonRank = taxonRank;
	}

	public void setScientificName(String scientificName) {
		this.scientificName = scientificName;
	}

	public void setCursiveName(boolean cursiveName) {
		this.cursiveName = cursiveName;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setHigherTaxa(Map<String, String> higherTaxa) {
		this.higherTaxa = higherTaxa;
	}

	public void setIucnStatus(Qname iucnStatus) {
		this.iucnStatus = iucnStatus;
	}

	public void setPrimaryHabitat(Qname primaryHabitat) {
		this.primaryHabitat = primaryHabitat;
	}

	public void setHabitatSpecifiers(List<Qname> habitatSpecifiers) {
		this.habitatSpecifiers = habitatSpecifiers;
	}

	public Integer getTaxonomicOrder() {
		return taxonomicOrder;
	}

	public void setTaxonomicOrder(Integer taxonomicOrder) {
		this.taxonomicOrder = taxonomicOrder;
	}

	public boolean isSensitive() {
		return sensitive;
	}

	public void setSensitive(boolean sensitive) {
		this.sensitive = sensitive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + (cursiveName ? 1231 : 1237);
		result = prime * result + ((habitatSpecifiers == null) ? 0 : habitatSpecifiers.hashCode());
		result = prime * result + ((higherTaxa == null) ? 0 : higherTaxa.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((iucnStatus == null) ? 0 : iucnStatus.hashCode());
		result = prime * result + ((primaryHabitat == null) ? 0 : primaryHabitat.hashCode());
		result = prime * result + ((scientificName == null) ? 0 : scientificName.hashCode());
		result = prime * result + (sensitive ? 1231 : 1237);
		result = prime * result + ((taxonRank == null) ? 0 : taxonRank.hashCode());
		result = prime * result + ((taxonomicOrder == null) ? 0 : taxonomicOrder.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TraitTaxon other = (TraitTaxon) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (cursiveName != other.cursiveName)
			return false;
		if (habitatSpecifiers == null) {
			if (other.habitatSpecifiers != null)
				return false;
		} else if (!habitatSpecifiers.equals(other.habitatSpecifiers))
			return false;
		if (higherTaxa == null) {
			if (other.higherTaxa != null)
				return false;
		} else if (!higherTaxa.equals(other.higherTaxa))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (iucnStatus == null) {
			if (other.iucnStatus != null)
				return false;
		} else if (!iucnStatus.equals(other.iucnStatus))
			return false;
		if (primaryHabitat == null) {
			if (other.primaryHabitat != null)
				return false;
		} else if (!primaryHabitat.equals(other.primaryHabitat))
			return false;
		if (scientificName == null) {
			if (other.scientificName != null)
				return false;
		} else if (!scientificName.equals(other.scientificName))
			return false;
		if (sensitive != other.sensitive)
			return false;
		if (taxonRank == null) {
			if (other.taxonRank != null)
				return false;
		} else if (!taxonRank.equals(other.taxonRank))
			return false;
		if (taxonomicOrder == null) {
			if (other.taxonomicOrder != null)
				return false;
		} else if (!taxonomicOrder.equals(other.taxonomicOrder))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TraitTaxon [id=" + id + ", taxonomicOrder=" + taxonomicOrder + ", taxonRank=" + taxonRank + ", scientificName=" + scientificName + ", cursiveName=" + cursiveName
				+ ", author=" + author + ", higherTaxa=" + higherTaxa + ", iucnStatus=" + iucnStatus + ", primaryHabitat=" + primaryHabitat + ", habitatSpecifiers=" + habitatSpecifiers
				+ ", sensitive=" + sensitive + "]";
	}

}
