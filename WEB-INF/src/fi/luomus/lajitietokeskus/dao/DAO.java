package fi.luomus.lajitietokeskus.dao;

import fi.luomus.lajitietokeskus.models.CMSContent;
import fi.luomus.lajitietokeskus.models.News;
import fi.luomus.lajitietokeskus.models.PersonToken;

public interface DAO {

	News getNews() throws Exception;

	CMSContent getCMSContent() throws Exception;

	/**
	 * Get system id (KE.123) or api user (email address)
	 * @param accessToken
	 * @return
	 * @throws IllegalArgumentException if access token is unknown
	 */
	String getApiUser(String accessToken) throws IllegalArgumentException;

	/**
	 * Get person's info based on person token
	 * @param personToken
	 * @return
	 * @throws IllegalArgumentException if person token is unknown
	 */
	PersonToken getPersonToken(String personToken) throws IllegalArgumentException;

}

