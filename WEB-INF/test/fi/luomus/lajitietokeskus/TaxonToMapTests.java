package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import fi.luomus.commons.containers.AdministrativeStatus;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.Content;
import fi.luomus.commons.containers.ContentContextDescription;
import fi.luomus.commons.containers.ContentGroups;
import fi.luomus.commons.containers.ContentGroups.ContentGroup;
import fi.luomus.commons.containers.ContentGroups.ContentVariable;
import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.LocalizedURL;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.ObjectLiteral;
import fi.luomus.commons.containers.rdf.ObjectResource;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfProperties;
import fi.luomus.commons.containers.rdf.RdfProperty;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.taxonomy.AdministrativeStatusContainer;
import fi.luomus.commons.taxonomy.BoldRecords;
import fi.luomus.commons.taxonomy.HabitatOccurrenceCounts.HabitatOccurrenceCount;
import fi.luomus.commons.taxonomy.Occurrences.Occurrence;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.commons.taxonomy.iucn.EndangermentObject;
import fi.luomus.commons.taxonomy.iucn.Evaluation;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.Utils;
import fi.luomus.java.tests.commons.taxonomy.TaxonContainerStub;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonToMap;

public class TaxonToMapTests {

	public static class FakeTaxonDao extends TaxonomyDAOStub {
		@Override
		public ContentGroups getContentGroups() {
			ContentVariable v1 = new ContentVariable(new Qname("MX.var1"), new LocalizedText().set("fi", "Muuttuja 1 otsikko").set("en", "Variable 1 title"));
			ContentVariable v2 = new ContentVariable(new Qname("MX.var2"), new LocalizedText().set("fi", "Muuttuja 2 otsikko").set("en", "Variable 2 title"));
			List<ContentVariable> g1Variables = Utils.list(v1, v2);
			ContentGroup g1 = new ContentGroup(new Qname("groupid1"), new LocalizedText().set("fi", "Ryhmä 1 otsikko").set("en", "Group 1 title"), g1Variables);

			ContentVariable v3 = new ContentVariable(new Qname("MX.var3"), new LocalizedText().set("fi", "Muuttuja 3 otsikko").set("en", "Variable 3 title"));
			ContentVariable v4 = new ContentVariable(new Qname("MX.speciesCardAuthors"), new LocalizedText().set("fi", "Tekstin laatijat").set("en", "Authors"));
			List<ContentVariable> g2Variables = Utils.list(v3, v4);
			ContentGroup g2 = new ContentGroup(new Qname("groupid2"), new LocalizedText().set("fi", "Ryhmä 2 otsikko").set("en", "Group 2 title"), g2Variables);

			List<ContentGroup> groups = Utils.list(g1, g2);
			return new ContentGroups(groups);
		}

		@Override
		public Map<String, ContentContextDescription> getContentContextDescriptions() {
			Map<String, ContentContextDescription> map = new HashMap<>();
			Qname contextId = new Qname("somecontext");
			map.put(contextId.toString(), new ContentContextDescription(contextId, new LocalizedText().set("fi", "Kontekstin otsikko").set("en", "Context title")));
			return map;
		}

		@Override
		public TaxonContainer getTaxonContainer() {
			return new TestTaxonContainer();
		}

		@Override
		public Taxon getTaxon(Qname taxonId) {
			return getTaxonContainer().getTaxon(taxonId);
		}

	}

	public static class TestTaxonContainer extends TaxonContainerStub {

		@Override
		public int getLatestLockedRedListEvaluationYear() throws UnsupportedOperationException {
			return 2019;
		}

		@Override
		public Taxon getTaxon(Qname qname)  {
			if ("MX.4".equals(qname.toString())) {
				return new Taxon(qname, this).setTaxonRank(new Qname("MX.family")).setScientificName("Faml");
			}
			if ("MX.3".equals(qname.toString())) {
				return new Taxon(qname, this).setTaxonRank(new Qname("MX.genus")).setScientificName("Something").setScientificNameAuthorship("L.");
			}
			Taxon t = new Taxon(qname, this).setTaxonRank(new Qname("MX.species"));
			if (!qname.toString().startsWith("MX.123")) {
				t.setScientificName("Sciname").setScientificNameAuthorship("L.");
			}
			if (qname.toString().equals("MX.123")) {
				t.getVernacularName().set("en", "Potato virus (IX)");
			}
			if ("MX.1".equals(qname.toString())) {
				t.getVernacularName().set("fi", "verfi");
				t.getVernacularName().set("sv", "versv");
				t.getVernacularName().set("en", "veren");
			}
			if ("MX.2".equals(qname.toString())) {
				t.getVernacularName().set("fi", "verfi");
			}
			if (qname.toString().startsWith("MX.123_")) {
				// synonym
				BoldRecords bold = new BoldRecords();
				bold.setPublicRecords(2);
				t.setBold(bold);
			}
			return t;
		}

		@Override
		public Set<Qname> getChildren(Qname qname) {
			if ("MX.3".equals(qname.toString())) {
				return Utils.set(new Qname("MX.1"));
			}
			return Collections.emptySet();
		}

		@Override
		public boolean hasTaxon(Qname qname)  { return true; }

		@Override
		public Qname getSynonymParent(Qname synonymId) {
			if (synonymId == null) return null;
			if (synonymId.toString().toLowerCase().contains("synonym")) return new Qname("MX.1");
			if (synonymId.toString().equals("MX.123_Basionym")) return new Qname("MX.1");
			return null;
		}

		@Override
		public Set<Qname> getRedListEvaluationGroupsOfInformalTaxonGroup(Qname informalTaxonGroupId) {
			if (informalTaxonGroupId.toString().equals("MVL.61")) {
				return Utils.set(new Qname("XXX.1"), new Qname("XXX.2"));
			}
			return Collections.emptySet();
		}

		@Override
		public Set<Qname> orderInformalTaxonGroups(Set<Qname> informalTaxonGroups) {
			List<Qname> sorted = new ArrayList<>(informalTaxonGroups);
			Collections.sort(sorted);
			return new LinkedHashSet<>(sorted);
		}

		@Override
		public Set<Qname> orderRedListEvaluationGroups(Set<Qname> groups) {
			List<Qname> sorted = new ArrayList<>(groups);
			Collections.sort(sorted);
			return new LinkedHashSet<>(sorted);
		}

		@Override
		public Set<Qname> orderAdministrativeStatuses(Set<Qname> statuses) {
			Map<String, AdministrativeStatus> administrativeStatuses = new LinkedHashMap<>();
			administrativeStatuses.put("MX.finlex160_1997_appendix4_specialInterest_2021", new AdministrativeStatus(new Qname("MX.finlex160_1997_appendix4_specialInterest_2021"), new LocalizedText()));
			administrativeStatuses.put("MX.finlex160_1997_appendix2b", new AdministrativeStatus(new Qname("MX.finlex160_1997_appendix2b"), new LocalizedText()));
			administrativeStatuses.put("MX.gameBird", new AdministrativeStatus(new Qname("MX.gameBird"), new LocalizedText()));
			administrativeStatuses.put("MX.cropWildRelative", new AdministrativeStatus(new Qname("MX.cropWildRelative"), new LocalizedText()));
			return new AdministrativeStatusContainer(administrativeStatuses).orderAdministrativeStatuses(statuses);
		}

	}

	public static Map<String, Area> getHardCodedAreas() {
		Map<String, Area> areas = new HashMap<>();
		Area a1 = new Area(new Qname("AREA.1"), new LocalizedText().set("fi", "b"), new Qname("sometype"));
		Area a2 = new Area(new Qname("AREA.2"), new LocalizedText().set("fi", "a"), new Qname("sometype"));
		areas.put(a1.getQname().toString(), a1);
		areas.put(a2.getQname().toString(), a2);
		return areas;
	}

	public static RdfProperties getHardCodedEvaluationProperties() {
		RdfProperties properties = new RdfProperties();
		properties.addProperty(new RdfProperty(new Qname(Evaluation.EVALUATION_YEAR)));
		properties.addProperty(new RdfProperty(new Qname(Evaluation.RED_LIST_STATUS)));
		properties.addProperty(new RdfProperty(new Qname(Evaluation.EXTERNAL_IMPACT)));
		properties.addProperty(new RdfProperty(new Qname(Evaluation.CRITERIA_FOR_STATUS)));
		properties.addProperty(new RdfProperty(new Qname(Evaluation.POSSIBLY_RE)));
		properties.addProperty(new RdfProperty(new Qname(Evaluation.REASON_FOR_STATUS_CHANGE)));
		properties.addProperty(new RdfProperty(new Qname(Evaluation.LAST_SIGHTING_NOTES)));
		properties.getProperty(Evaluation.REASON_FOR_STATUS_CHANGE).setMaxOccurs(Integer.MAX_VALUE);

		RdfProperty habitat = new RdfProperty(new Qname(Evaluation.HABITAT), new Qname("MKV.habitatEnum"));
		List<RdfProperty> habitatRange = new ArrayList<>();
		habitatRange.add(new RdfProperty(new Qname("K")));
		habitatRange.add(new RdfProperty(new Qname("Ka")).setAltParent(new Qname("K")));
		habitatRange.add(new RdfProperty(new Qname("Kb")).setAltParent(new Qname("K")));
		habitatRange.add(new RdfProperty(new Qname("Kba")).setAltParent(new Qname("Kb")));
		habitatRange.add(new RdfProperty(new Qname("Kbb")).setAltParent(new Qname("Kb")));
		habitatRange.add(new RdfProperty(new Qname("A")));
		habitatRange.add(new RdfProperty(new Qname("B")));
		// C intentionally left missing
		habitat.getRange().setRangeValues(habitatRange);
		properties.addProperty(habitat);
		return properties;
	}

	private TaxonToMap mapper() {
		return new TaxonToMap(new FakeTaxonDao(), getHardCodedEvaluationProperties(), getHardCodedAreas());
	}

	public static Taxon createTestData() throws Exception {
		Taxon taxon = new Taxon(new Qname("MX.1"), new TestTaxonContainer());
		taxon.setParentQname(new Qname("MX.3"));
		taxon.setTaxonRank(new Qname("MX.species"));
		taxon.setChecklist(new Qname("MR.1"));
		taxon.addExternalLink(new LocalizedURL(new URI("http://link.com"), "en"));
		taxon.setScientificName("Pyrpys pyrpys");
		taxon.addVernacularName("fi", "talitiainen");
		taxon.addAlternativeVernacularName("fi", "talitintti");
		taxon.addAlternativeVernacularName("fi", "tilitantti");
		taxon.addVernacularName("ru", "rusintti");
		taxon.addVernacularName("se", "samisintti");
		taxon.addColloquialVernacularName("fi", "tintti");

		taxon.getOccurrences().setOccurrence(null, new Qname("AREA.2"), new Qname("STATUS.2"));
		taxon.getOccurrences().getOccurrence(new Qname("AREA.2")).setThreatened(true);

		taxon.getOccurrences().setOccurrence(null, new Qname("AREA.1"), new Qname("STATUS.1"));
		taxon.getOccurrences().getOccurrence(new Qname("AREA.1")).setNotes("occ notes");
		taxon.getOccurrences().getOccurrence(new Qname("AREA.1")).setYear(1982);
		taxon.getOccurrences().getOccurrence(new Qname("AREA.1")).setSpecimenURI(new URI("http://tun.fi/A.1"));
		taxon.getOccurrences().getOccurrence(new Qname("AREA.1")).setOccurrenceCount(1000);

		taxon.getDescriptions().addText(new Qname("somecontext"), new Qname("MX.speciesCardAuthors"), "Teppo Testaaja", "fi");
		taxon.getDescriptions().addText(new Qname("somecontext"), new Qname("MX.var1"), "var1 arvo", "fi");
		taxon.getDescriptions().addText(new Qname("somecontext"), new Qname("MX.var1"), "var1 value", "en");
		taxon.getDescriptions().addText(new Qname("somecontext"), new Qname("MX.var1"), "var1 på svenska", "sv");
		taxon.getDescriptions().addText(new Qname("somecontext"), new Qname("MX.var1"), "var1 русский", "ru");
		taxon.getDescriptions().addText(new Qname("somecontext"), new Qname("MX.var1"), "var1 sami", "se");
		taxon.getDescriptions().addText(new Qname("somecontext"), new Qname("MX.var3"), "var3 arvo", "fi");
		taxon.getDescriptions().addText(new Qname("eol:api"), new Qname("MX.var3"), "var3 arvo", "fi");

		for (Method method : taxon.addSynonyms().getClass().getMethods()) {
			if (method.getName().startsWith("add")) {
				method.invoke(taxon.addSynonyms(), new Qname("MX.123_"+method.getName().replace("add", "")));
			}
		}
		taxon.addSynonyms().addHeterotypicSynonym(new Qname("MX.onemore"));

		taxon.addTaxonConceptId(Qname.fromURI("http://taxonid.org/FOO"));
		taxon.addAdditionalId("http://eol.org/XYZ123");

		Image image = new Image(new Qname("MM.123"), Content.DEFAULT_DESCRIPTION_CONTEXT, "http://img.com/1.jgp");
		image.setCaption("captiontext");
		image.setLicenseId(new Qname("MZ.intellectualRightsARR"));
		image.setTaxonDescriptionCaption(new LocalizedText().set("en", "taxon caption"));
		image.setLicenseFullname(new LocalizedText().set("fi", "Kaikki oikeudet pidätetään"));
		image.addKeyword("k1").addKeyword("k2").addKeyword("primary");
		image.addPrimaryForTaxon(taxon.getId());
		image.addSex(new Qname("MY.sexF"));
		image.setType(new Qname("MM.typeEnumLive"));
		image.setTaxon(taxon);
		taxon.addMultimedia(image);

		taxon.addEditor(new Qname("MA.1"));
		taxon.addExpert(new Qname("MA.2"));

		taxon.setPrimaryHabitat(new HabitatObject(new Qname("MKV.1"), new Qname("Kba"), 0));
		taxon.getPrimaryHabitat().addHabitatSpecificType(new Qname("type1"));
		taxon.getPrimaryHabitat().addHabitatSpecificType(new Qname("type2"));
		taxon.getPrimaryHabitat().addHabitatSpecificType(new Qname("type3"));

		taxon.addSecondaryHabitat(new HabitatObject(new Qname("MKV.2"), new Qname("C"), 2));
		taxon.addSecondaryHabitat(new HabitatObject(new Qname("MKV.3"), new Qname("B"), 1));
		taxon.addSecondaryHabitat(new HabitatObject(new Qname("MKV.4"), new Qname("A"), 0));
		taxon.getSecondaryHabitats().get(0).addHabitatSpecificType(new Qname("type1"));

		taxon.getHabitatOccurrenceCounts().setCount(new HabitatOccurrenceCount("MZ.habitatZ", new LocalizedText().set("fi", "zetamaasto").set("en", "habitat-z")).setOccurrenceCount(10));

		Model evaluationModel = new Model(new Qname("ABC.1"));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.state"), new ObjectResource("MKV.stateReady")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.redListStatus"), new ObjectResource("MX.iucnLC")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.lastModifiedBy"), new ObjectResource("MA.5")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.lastModified"), new ObjectLiteral("2017-02-06")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.editNotes"), new ObjectLiteral("Merkitty valmiiksi; 06.02.2017")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.evaluationYear"), new ObjectLiteral("2019")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.evaluatedTaxon"), new ObjectResource("MX.123")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.primaryHabitat"), new ObjectResource("MKV.1234")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.redListIndexCorrection"), new ObjectResource("MX.iucnNT")));

		evaluationModel.addStatement(new Statement(new Predicate("MKV.publication"), new ObjectResource("MP.1")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.publication"), new ObjectResource("MP.2")));

		evaluationModel.addStatement(new Statement(new Predicate("MKV.reasonForStatusChange"), new ObjectResource("MKV.reason1")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.reasonForStatusChange"), new ObjectResource("MKV.reason2")));

		evaluationModel.addStatement(new Statement(new Predicate("MKV.borderGain"), new ObjectLiteral("true")));

		Evaluation evaluation = Evaluation.createNonPredicateCheckingEvaluation(evaluationModel);
		evaluation.setPrimaryHabitat(new HabitatObject(new Qname("MKV.1234"), new Qname("MKV.habitatMk"), 1));
		evaluation.addSecondaryHabitat(new HabitatObject(new Qname("MKV.1235"), new Qname("MKV.habitatI"), 1));
		evaluation.addSecondaryHabitat(
				new HabitatObject(new Qname("MKV.1236"), new Qname("MKV.habitatMtl"), 2)
				.addHabitatSpecificType(new Qname("MKV.habitatSpecificTypePAK"))
				.addHabitatSpecificType(new Qname("MKV.habitatSpecificTypeCA")));
		evaluation.addThreat(new EndangermentObject(new Qname("T.1"), new Qname("MKV.threatTA"), 1));
		evaluation.addThreat(new EndangermentObject(new Qname("T.2"), new Qname("MKV.threatTB"), 2));
		evaluation.addEndangermentReason(new EndangermentObject(new Qname("E.1"), new Qname("MKV.end1"), 1));
		Occurrence o1 = new Occurrence(new Qname("O.1"), new Qname("AREA.1"), new Qname("MX.typeOfOccurrenceStablePopulation"));
		Occurrence o2 = new Occurrence(new Qname("O.2"), new Qname("AREA.2"), new Qname("MX.typeOfOccurrenceStablePopulation"));
		o1.setThreatened(true);
		evaluation.addOccurrence(o1);
		evaluation.addOccurrence(o2);
		taxon.addRedListEvaluation(evaluation);

		taxon.setRedListStatus2019Finland(new Qname("MX.iucnLC"));

		taxon.addInformalTaxonGroup(new Qname("MVL.61"));

		taxon.addTaxonSet(new Qname("MX.taxonSetSome"));
		taxon.addTaxonSet(new Qname("MX.taxonSetOther"));

		taxon.addInvasiveSpeciesMainGroup(new Qname("FOO.BAR"));
		taxon.addInvasiveSpeciesMainGroup(new Qname("BAR.FOO"));

		taxon.setExplicitObservationCount(5);

		taxon.addAdministrativeStatus(new Qname("MX.gameBird"));
		taxon.addAdministrativeStatus(new Qname("MX.cropWildRelative"));
		taxon.addAdministrativeStatus(new Qname("MX.finlex160_1997_appendix2b"));
		taxon.addAdministrativeStatus(new Qname("MX.finlex160_1997_appendix4_specialInterest_2021"));

		BoldRecords bold = new BoldRecords();
		bold.setPublicRecords(4);
		bold.setSpecimens(4);
		bold.setBarcodes(1);
		bold.addToBins("BOLD:XXX");
		bold.addToBins("BOLD:YYY");
		taxon.setBold(bold);
		return taxon;
	}

	@Test
	public void test() throws Exception {
		TaxonToMap mapper = mapper();

		Taxon taxon = createTestData();
		taxon.setTaxonomicOrder(1L);

		Map<String, Object> map = mapper.map(taxon);

		assertEquals("MX.1", map.get("id"));
		assertEquals("MX.species", map.get("taxonRank"));
		assertEquals(1L, map.get("taxonomicOrder"));
		assertEquals("Pyrpys pyrpys", map.get("scientificName"));
		assertEquals("{fi=talitiainen, ru=rusintti, se=samisintti}", map.get("vernacularName").toString());

		Object basionyms = map.get("basionyms");
		assertTrue(basionyms instanceof ArrayList);
		ArrayList<?> basList = (ArrayList<?>) basionyms;
		assertEquals(1, basList.size());
		Object basionym = basList.get(0);
		assertTrue(basionym instanceof LinkedHashMap);
		Map<?, ?> basinymData = (Map<?,?>) basionym;
		assertEquals("MX.123_Basionym", basinymData.get("id"));
		assertEquals(null, basinymData.get("synonymOf"));

		String expected = "{fi=[talitintti, tilitantti]}";
		assertEquals(expected, map.get("alternativeVernacularName").toString());
		assertEquals("{fi=[tintti]}", map.get("colloquialVernacularName").toString());

		expected = "[{area=AREA.1, notes=occ notes, occurrenceCount=1000, specimenURI=http://tun.fi/A.1, status=STATUS.1, year=1982}, {area=AREA.2, status=STATUS.2, threatened=true}]";
		assertEquals(expected, map.get("occurrences").toString());

		assertEquals("[{locale=en, uri=http://link.com}]", map.get("externalLinks").toString());

		expected = "[{id=eol:api, groups=[{group=groupid2, title={en=Group 2 title, fi=Ryhmä 2 otsikko}, variables=[{variable=MX.var3, title={en=Variable 3 title, fi=Muuttuja 3 otsikko}, content={fi=var3 arvo}}]}]}, {id=somecontext, title={en=Context title, fi=Kontekstin otsikko}, groups=[{group=groupid1, title={en=Group 1 title, fi=Ryhmä 1 otsikko}, variables=[{variable=MX.var1, title={en=Variable 1 title, fi=Muuttuja 1 otsikko}, content={fi=var1 arvo, en=var1 value, ru=var1 русский, se=var1 sami, sv=var1 på svenska}}]}, {group=groupid2, title={en=Group 2 title, fi=Ryhmä 2 otsikko}, variables=[{variable=MX.var3, title={en=Variable 3 title, fi=Muuttuja 3 otsikko}, content={fi=var3 arvo}}]}], speciesCardAuthors={variable=MX.speciesCardAuthors, title={en=Authors, fi=Tekstin laatijat}, content={fi=Teppo Testaaja}}}]";
		assertEquals(expected, map.get("descriptions").toString());

		assertEquals("[taxonid:FOO]", map.get("taxonConceptIds").toString());
		assertEquals("[http://eol.org/XYZ123]", map.get("additionalIds").toString());

		expected = "" +
				"[{caption=captiontext, fullURL=http://img.com/1.jgp, id=MM.123, keywords=[k1, k2, primary], licenseFullname={fi=Kaikki oikeudet pidätetään}, licenseId=MZ.intellectualRightsARR, sex=[MY.sexF], sortOrder=1, source=default, taxon={id=MX.1, scientificName=Pyrpys pyrpys, vernacularName={fi=talitiainen, ru=rusintti, se=samisintti}, taxonRank=MX.species, cursiveName=true, bold={barcodes=1, binCount=2, bins=[BOLD:XXX, BOLD:YYY], publicRecords=4, specimens=4}, hasBold=true}, taxonDescriptionCaption={en=taxon caption}, type=MM.typeEnumLive, primaryForTaxon=true}]";
		assertEquals(expected, map.get("multimedia").toString());

		assertEquals("[MA.1]", map.get("taxonEditor").toString());
		assertEquals("[MA.2]", map.get("taxonExpert").toString());

		assertEquals("MX.3", map.get("isPartOf"));
		assertEquals("MX.3", map.get("isPartOfNonHidden"));
		assertEquals("[MX.3]", map.get("parents").toString());
		assertEquals("[MX.3]", map.get("nonHiddenParents").toString());
		assertEquals("[MX.3, MX.1]", map.get("parentsIncludeSelf").toString());
		assertEquals("[MX.3, MX.1]", map.get("nonHiddenParentsIncludeSelf").toString());

		assertEquals("" +
				"{genus={id=MX.3, scientificName=Something, scientificNameAuthorship=L., taxonRank=MX.genus, cursiveName=true, hasBold=false}, species={id=MX.1, scientificName=Pyrpys pyrpys, vernacularName={fi=talitiainen, ru=rusintti, se=samisintti}, taxonRank=MX.species, cursiveName=true, bold={barcodes=1, binCount=2, bins=[BOLD:XXX, BOLD:YYY], publicRecords=4, specimens=4}, hasBold=true}}",
				map.get("parent").toString());

		assertEquals(
				"[K, Kb, Kba, K[], K[type1], K[type2], K[type3], K[type1,type2], K[type1,type3], K[type2,type1], K[type2,type3], K[type3,type1], K[type3,type2], K[type1,type2,type3], K[type1,type3,type2], K[type2,type1,type3], K[type2,type3,type1], K[type3,type1,type2], K[type3,type2,type1], Kb[], Kb[type1], Kb[type2], Kb[type3], Kb[type1,type2], Kb[type1,type3], Kb[type2,type1], Kb[type2,type3], Kb[type3,type1], Kb[type3,type2], Kb[type1,type2,type3], Kb[type1,type3,type2], Kb[type2,type1,type3], Kb[type2,type3,type1], Kb[type3,type1,type2], Kb[type3,type2,type1], Kba[], Kba[type1], Kba[type2], Kba[type3], Kba[type1,type2], Kba[type1,type3], Kba[type2,type1], Kba[type2,type3], Kba[type3,type1], Kba[type3,type2], Kba[type1,type2,type3], Kba[type1,type3,type2], Kba[type2,type1,type3], Kba[type2,type3,type1], Kba[type3,type1,type2], Kba[type3,type2,type1]]",
				map.get("primaryHabitatSearchStrings").toString()); // taxon has Kb1[type1, type2, type3] so it matches to for example Kb1[type1] or K or K[type2, type3]

		assertEquals(
				"[A, B, C, K, Kb, Kba, A[], A[type1], K[], K[type1], K[type2], K[type3], K[type1,type2], K[type1,type3], K[type2,type1], K[type2,type3], K[type3,type1], K[type3,type2], K[type1,type2,type3], K[type1,type3,type2], K[type2,type1,type3], K[type2,type3,type1], K[type3,type1,type2], K[type3,type2,type1], Kb[], Kb[type1], Kb[type2], Kb[type3], Kb[type1,type2], Kb[type1,type3], Kb[type2,type1], Kb[type2,type3], Kb[type3,type1], Kb[type3,type2], Kb[type1,type2,type3], Kb[type1,type3,type2], Kb[type2,type1,type3], Kb[type2,type3,type1], Kb[type3,type1,type2], Kb[type3,type2,type1], Kba[], Kba[type1], Kba[type2], Kba[type3], Kba[type1,type2], Kba[type1,type3], Kba[type2,type1], Kba[type2,type3], Kba[type3,type1], Kba[type3,type2], Kba[type1,type2,type3], Kba[type1,type3,type2], Kba[type2,type1,type3], Kba[type2,type3,type1], Kba[type3,type1,type2], Kba[type3,type2,type1]]",
				map.get("anyHabitatSearchStrings").toString());

		String expectedEvaluation = "" +
				"{evaluationYear=2019, redListStatus=MX.iucnLC, reasonForStatusChange=[MKV.reason1, MKV.reason2], primaryHabitat={habitat=MKV.habitatMk, id=MKV.1234, order=1}, secondaryHabitats=[{habitat=MKV.habitatI, id=MKV.1235, order=1}, {habitat=MKV.habitatMtl, habitatSpecificTypes=[MKV.habitatSpecificTypeCA, MKV.habitatSpecificTypePAK], id=MKV.1236, order=2}], primaryHabitatSearchStrings=[MKV.habitatMk], anyHabitatSearchStrings=[MKV.habitatI, MKV.habitatMk, MKV.habitatMtl, MKV.habitatMtl[MKV.habitatSpecificTypeCA], MKV.habitatMtl[MKV.habitatSpecificTypePAK], MKV.habitatMtl[], MKV.habitatMtl[MKV.habitatSpecificTypeCA,MKV.habitatSpecificTypePAK], MKV.habitatMtl[MKV.habitatSpecificTypePAK,MKV.habitatSpecificTypeCA]], endangermentReasons=[MKV.end1], primaryEndangermentReason=MKV.end1, threats=[MKV.threatTA, MKV.threatTB], primaryThreat=MKV.threatTA, occurrences=[{area=AREA.2, id=O.2, status=MX.typeOfOccurrenceStablePopulation, year=2019}, {area=AREA.1, id=O.1, status=MX.typeOfOccurrenceStablePopulation, threatened=true, year=2019}], threatenedAtArea=[AREA.1], calculatedRedListIndex=0, calculatedCorrectedRedListIndex=1, correctedStatusForRedListIndex=MX.iucnNT}";

		assertEquals(expectedEvaluation, map.get("latestRedListEvaluation").toString());
		assertTrue((boolean)map.get("hasLatestRedListEvaluation"));

		assertEquals("{habitat=Kba, habitatSpecificTypes=[type1, type2, type3], id=MKV.1, order=0}", map.get("primaryHabitat").toString());
		assertEquals("[{habitat=A, habitatSpecificTypes=[type1], id=MKV.4, order=0}, {habitat=B, id=MKV.3, order=1}, {habitat=C, id=MKV.2, order=2}]", map.get("secondaryHabitats").toString());

		assertEquals("{status=MX.iucnLC, year=2019}", map.get("latestRedListStatusFinland").toString());

		assertEquals("[MX.taxonSetOther, MX.taxonSetSome]", map.get("taxonSets").toString());

		assertEquals(5, map.get("observationCount"));
		assertEquals(0, map.get("observationCountFinland"));
		assertEquals(0, map.get("observationCountInvasiveFinland"));

		assertEquals("" +
				"[{habitat={en=habitat-z, fi=zetamaasto}, id=mz.habitatz, occurrenceCount=10}]",
				map.get("habitatOccurrenceCounts").toString());

		assertEquals("[MX.finlex160_1997_appendix4_specialInterest_2021, MX.finlex160_1997_appendix2b, MX.gameBird, MX.cropWildRelative]", map.get("administrativeStatuses").toString());
		assertEquals("MX.threatenedStatusStatutoryProtected", map.get("threatenedStatus").toString());

		assertEquals("{barcodes=1, binCount=2, bins=[BOLD:XXX, BOLD:YYY], publicRecords=4, specimens=4}", map.get("bold").toString());
		assertEquals("true", map.get("hasBold").toString());
		assertEquals("" +
				"[{id=MX.123_Synonym, taxonRank=MX.species, cursiveName=true, bold={binCount=0, publicRecords=2}, hasBold=true}]",
				map.get("synonyms").toString());
	}

	@Test
	public void test_obscounts_2() throws Exception {
		TaxonToMap mapper = mapper();

		Taxon taxon = createTestData();
		taxon.setTaxonomicOrder(1L);
		taxon.setExplicitObservationCount(1);
		taxon.setExplicitObservationCountFinland(1);
		taxon.addAdministrativeStatus(new Qname("MX.nationallySignificantInvasiveSpecies"));

		Map<String, Object> map = mapper.map(taxon);
		map = mapper.map(taxon);

		assertEquals(1, map.get("observationCountFinland"));
		assertEquals(1, map.get("observationCountInvasiveFinland"));
	}

	@Test
	public void nonLatestEval() throws Exception {
		TaxonToMap mapper = mapper();

		Taxon taxon = new Taxon(new Qname("MX.1"), new TestTaxonContainer());

		Model evaluationModel = new Model(new Qname("ABC.1"));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.state"), new ObjectResource("MKV.stateReady")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.evaluationYear"), new ObjectLiteral("2015")));
		evaluationModel.addStatement(new Statement(new Predicate("MKV.evaluatedTaxon"), new ObjectResource("MX.123")));
		Evaluation evaluation = Evaluation.createNonPredicateCheckingEvaluation(evaluationModel);
		taxon.addRedListEvaluation(evaluation);

		Map<String, Object> map = mapper.map(taxon);

		assertNull(map.get("latestRedListEvaluation"));
		assertFalse((boolean)map.get("hasLatestRedListEvaluation"));
	}

	@Test
	public void synonymsHaveSynonymParentInTheModel() throws Exception {
		TaxonToMap mapper = mapper();

		Taxon taxon = new Taxon(new Qname("MX.somesynonym"), new TestTaxonContainer());

		Map<String, Object> map = mapper.map(taxon);

		String synonymParentInfo = map.get("synonymOf").toString();
		assertEquals(
				"{id=MX.1, scientificName=Sciname, scientificNameAuthorship=L., vernacularName={en=veren, fi=verfi, sv=versv}, taxonRank=MX.species, cursiveName=true, hasBold=false}",
				synonymParentInfo.toString());
	}

	@Test
	public void taxonDescriptionLinking() throws Exception {
		TaxonToMap mapper = mapper();
		assertEquals("foobar [foo] bar", mapper.replaceTaxonLinks("foobar [foo] bar"));

		assertEquals(a("MX.1"), mapper.replaceTaxonLinks("[MX.1]"));
		assertEquals(a("MX.1"), mapper.replaceTaxonLinks(" [MX.1]"));
		assertEquals(a("MX.1"), mapper.replaceTaxonLinks("[MX.1] "));
		assertEquals(a("MX.1"), mapper.replaceTaxonLinks(" [MX.1] "));
		assertEquals("foo "+a("MX.1"), mapper.replaceTaxonLinks("foo [MX.1]"));
		assertEquals("foo "+a("MX.1")+" bar", mapper.replaceTaxonLinks("foo [MX.1] bar"));
		assertEquals(a("MX.1") + " bar", mapper.replaceTaxonLinks("[MX.1] bar"));
		assertEquals(
				"looks same as "+a("MX.1")+" and " + a("MX.2") + " and don't confuse with " + a("MX.3"),
				mapper.replaceTaxonLinks("looks same as [MX.1] and [MX.2] and don't confuse with [MX.3]"));

		assertEquals("[Mustikka]", mapper.replaceTaxonLinks("[Mustikka]"));
		assertEquals("foo [Mustikka] bar", mapper.replaceTaxonLinks("foo [Mustikka] bar"));
		assertEquals("foo [MX] bar", mapper.replaceTaxonLinks("foo [MX] bar"));
		assertEquals("foo [MX.] bar", mapper.replaceTaxonLinks("foo [MX.] bar"));

		assertEquals("foo [[MX]] bar", mapper.replaceTaxonLinks("foo [[MX]] bar"));

		assertEquals("foo [MX.1 [MX.2]] bar", mapper.replaceTaxonLinks("foo [MX.1 [MX.2]] bar"));

		assertEquals(a("MX.1")+a("MX.2"), mapper.replaceTaxonLinks("[MX.1][MX.2]"));
		assertEquals(a("MX.1"), mapper.replaceTaxonLinks("[MX.1]"));
		assertEquals(a("MX.2"), mapper.replaceTaxonLinks("[MX.2]"));
		assertEquals(a("MX.4"), mapper.replaceTaxonLinks("[MX.4]"));
		assertEquals(a("MX.123"), mapper.replaceTaxonLinks("[MX.123]"));
	}

	private String a(String qname) {
		String name = "<em>Sciname</em>";
		if (qname.equals("MX.3")) name = "<em>Something</em>";
		if (qname.equals("MX.4")) name = "Faml";
		if (qname.equals("MX.123")) name = "Potato virus (IX)";
		return "<a href=\"http://tun.fi/"+qname+"\" target=\"_blank\">"+name+"</a>";
	}

}
