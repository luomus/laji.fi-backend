package fi.luomus.traitdb.models;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

@ClassDef(triplestorePrefix="TDF")
public class UnitOfMeasurement extends AbstractBaseEntity implements Comparable<UnitOfMeasurement> {

	@FieldDef(order=1, required=true)
	private Qname id;

	@FieldDef(order=2, required=true)
	private String unit;

	@FieldDef(order=3, enumeration="TDF.unitOfMeasurementEnum", required=true)
	private Qname base;

	@FieldDef(order=4, required=true)
	private Double conversionFactor;

	@FieldDef(order=5, required=true)
	private Boolean isBaseUnit;

	private int order;

	public UnitOfMeasurement( ) {
		super();
	}

	public UnitOfMeasurement(JSONObject json) throws ParseException {
		super(json);
	}

	public Qname getId() {
		return id;
	}

	public String getUnit() {
		return unit;
	}

	public Qname getBase() {
		return base;
	}

	public Double getConversionFactor() {
		return conversionFactor;
	}

	public boolean getIsBaseUnit() {
		return isBaseUnit;
	}

	public int getOrder() {
		return order;
	}

	public void setId(Qname id) {
		this.id = id;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setBase(Qname base) {
		this.base = base;
	}

	public void setConversionFactor(Double conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	public void setIsBaseUnit(boolean isBaseUnit) {
		this.isBaseUnit = isBaseUnit;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + ((conversionFactor == null) ? 0 : conversionFactor.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (isBaseUnit ? 1231 : 1237);
		result = prime * result + order;
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnitOfMeasurement other = (UnitOfMeasurement) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (conversionFactor == null) {
			if (other.conversionFactor != null)
				return false;
		} else if (!conversionFactor.equals(other.conversionFactor))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isBaseUnit != other.isBaseUnit)
			return false;
		if (order != other.order)
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UnitOfMeasurement [id=" + id + ", unit=" + unit + ", base=" + base + ", conversionFactor=" + conversionFactor + ", isBaseUnit=" + isBaseUnit + ", order=" + order
				+ "]";
	}

	@Override
	public int compareTo(UnitOfMeasurement o) {
		return Integer.compare(this.getOrder(), o.getOrder());
	}

}
