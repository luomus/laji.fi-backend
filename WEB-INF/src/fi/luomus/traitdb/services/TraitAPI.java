package fi.luomus.traitdb.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.lajitietokeskus.models.PersonToken;
import fi.luomus.lajitietokeskus.services.BaseServlet;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.ValidationFailedException;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.AbstractBaseEntity;
import fi.luomus.traitdb.models.AbstractBaseEntity.ParseException;

@WebServlet(urlPatterns = {"/api/trait/*"})
public class TraitAPI extends BaseServlet {

	private final static long serialVersionUID = -991416676772303817L;

	public static final int DEFAULT_PAGE_SIZE = 10;
	public static final String PAGE_SIZE = "pageSize";
	public static final String PAGE = "page";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String PERSON_TOKEN = "personToken";
	public static final String OK = "ok";
	public static final String STACKTRACE = "stacktrace";
	public static final String MESSAGE = "message";
	public static final String STATUS = "status";

	public static class UnknownEntityException extends RuntimeException {
		private final static long serialVersionUID = -5356330480518529930L;
		public UnknownEntityException(String path, Method method) {
			super("Unknown entity: " + method + " " + path);
		}
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			return response(request(req, res, Method.GET).resource().endpoint().authorize().execute());
		} catch (Exception e) {
			return exception(e, res);
		}
	}

	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			return response(request(req, res, Method.POST).resource().endpoint().authorize().execute());
		} catch (Exception e) {
			return exception(e, res);
		}
	}

	@Override
	protected ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			return response(request(req, res, Method.PUT).resource().endpoint().authorize().execute());
		} catch (Exception e) {
			return exception(e, res);
		}
	}

	@Override
	protected ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			return response(request(req, res, Method.DELETE).resource().endpoint().authorize().execute());
		} catch (Exception e) {
			return exception(e, res);
		}
	}

	public static enum Method { GET, POST, PUT, DELETE }

	public static class PagingParameters {
		public int pageSize;
		public int page;
		private PagingParameters(int pageSize, int page) {
			this.pageSize = pageSize;
			this.page = page;
		}
	}
	public static class TraitAPIRequest {

		private final String userId;
		private final TraitDAO dao;
		private final Method method;
		private final String resource;
		private final String endpoint;
		private final Qname id;
		private final String body;
		private final Map<String, List<String>> parameters;
		private final PagingParameters pagingParameters;
		private final HttpServletResponse res;

		public TraitAPIRequest(String userId, TraitDAO dao, HttpServletRequest req, HttpServletResponse res, Method method) throws IOException {
			this.userId = userId;
			this.dao = dao;
			this.method = method;
			this.res = res;

			String[] uriParts = req.getRequestURI().split("/");
			int apiIndex = apiIndex(uriParts, req);
			this.id = id(req);
			this.resource = resource(req, method, uriParts, apiIndex);
			this.endpoint = endpoint(req, method, id, uriParts, apiIndex);

			this.body = readBody(req);

			this.parameters = new LinkedHashMap<>();
			this.pagingParameters = parameters(req);
		}

		private String readBody(HttpServletRequest req) throws IOException {
			StringBuilder stringBuilder = new StringBuilder();
			try (BufferedReader bufferedReader = req.getReader()) {
				char[] charBuffer = new char[128];
				int bytesRead;
				while ((bytesRead = bufferedReader.read(charBuffer)) != -1) {
					stringBuilder.append(charBuffer, 0, bytesRead);
				}
			}
			return stringBuilder.toString();
		}

		private PagingParameters parameters(HttpServletRequest req) {
			int pageSize = DEFAULT_PAGE_SIZE;
			int page = 1;
			Map<String, String[]> paramMap = req.getParameterMap();
			for (Map.Entry<String, String[]> entry : paramMap.entrySet()) {
				String paramName = entry.getKey();
				String[] paramValues = entry.getValue();
				if (paramValues.length > 0) {
					if (paramName.equals(ACCESS_TOKEN)) continue;
					if (paramName.equals(PERSON_TOKEN)) continue;
					if (paramName.equals(PAGE_SIZE)) {
						try {
							pageSize = Integer.parseInt(paramValues[0]);
							if (pageSize < 1) throw new IllegalArgumentException();
						} catch (Exception e) {
							throw new IllegalArgumentException("Invalid pageSize: " + paramValues[0]);
						}
						continue;
					}
					if (paramName.equals(PAGE)) {
						try {
							page = Integer.parseInt(paramValues[0]);
							if (page < 1) throw new IllegalArgumentException();
						} catch (Exception e) {
							throw new IllegalArgumentException("Invalid page: " + paramValues[0]);
						}
						continue;
					}
					if (!this.parameters.containsKey(paramName)) {
						this.parameters.put(paramName, new ArrayList<>());
					}
					for (String paramValue : paramValues) {
						this.parameters.get(paramName).add(paramValue);
					}
				}
			}
			return new PagingParameters(pageSize, page);
		}

		private String endpoint(HttpServletRequest req, Method method, Qname id, String[] uriParts, int apiIndex) {
			String endpoint = "";
			try {
				StringBuilder endpointBuilder = new StringBuilder();
				for (int i = apiIndex + 3; i < uriParts.length; i++) {
					endpointBuilder.append(uriParts[i]).append("/");
				}
				String endpointStr = endpointBuilder.toString();
				endpoint = endpointStr.endsWith("/") ? endpointStr.substring(0, endpointStr.length() - 1) : endpointStr;
			} catch (ArrayIndexOutOfBoundsException e) {
				throw new UnknownEntityException(req.getRequestURI(), method);
			}
			if (id != null && endpoint.contains(id.toString())) {
				return endpoint.replace(id.toString(), "{id}");
			}
			return endpoint;
		}

		private String resource(HttpServletRequest req, Method method, String[] uriParts, int apiIndex) {
			try {
				return uriParts[apiIndex + 2];
			} catch (ArrayIndexOutOfBoundsException e) {
				throw new UnknownEntityException(req.getRequestURI(), method);
			}
		}

		private int apiIndex(String[] uriParts, HttpServletRequest req) {
			int apiIndex = -1;
			for (int i = 0; i < uriParts.length; i++) {
				if (uriParts[i].equals("api")) {
					apiIndex = i;
					break;
				}
			}
			if (apiIndex == -1) {
				throw new UnknownEntityException(req.getRequestURI(), method);
			}
			return apiIndex;
		}

		private Qname id(HttpServletRequest req) {
			String path = req.getPathInfo();
			if (path == null || path.equals("/")) {
				return null;
			}
			String idpart = path.substring(path.lastIndexOf("/")+1);
			if (idpart.contains(".")) {
				return new Qname(idpart);
			}
			return null;
		}

		private TraitAPIResource resource() throws UnknownEntityException {
			if ("search".equals(resource)) return new TraitAPISearch(this);
			if ("rows".equals(resource)) return new TraitAPIRows(this);
			if ("datasets".equals(resource)) return new TraitAPIDatasets(this);
			if ("dataset-permissions".equals(resource)) return new TraitAPIDatasetPermissions(this);
			if ("trait-groups".equals(resource)) return new TraitAPITraitGroups(this);
			if ("traits".equals(resource)) return new TraitAPITraits(this);
			if ("units".equals(resource)) return new TraitAPIUnits(this);
			throw new UnknownEntityException(resource, method);
		}

		public TraitDAO getDAO() {
			return dao;
		}

		public Method getMethod() {
			return method;
		}

		public String getEndpoint() {
			return endpoint;
		}

		public Qname getId() {
			return id;
		}

		public JSONArray getArray() throws ParseException {
			try {
				return new JSONArray(body);
			} catch (Exception e) {
				throw new ParseException("invalid json array: " + e.getMessage(), e);
			}
		}

		public JSONObject getJson() throws ParseException {
			try {
				return new JSONObject(body);
			} catch (Exception e) {
				throw new ParseException("invalid json: " + e.getMessage(), e);
			}
		}

		public List<String> getLines() {
			List<String> lines = new ArrayList<>();
			StringBuilder lineBuilder = new StringBuilder();
			for (int i = 0; i < body.length(); i++) {
				char c = body.charAt(i);
				if (c == '\n') {
					lines.add(lineBuilder.toString().trim());
					lineBuilder.setLength(0);
				} else if (c != '\r') {
					lineBuilder.append(c);
				}
			}
			if (lineBuilder.length() > 0) {
				lines.add(lineBuilder.toString().trim());
			}
			return lines;
		}

		public UnknownEntityException noEndpointException() {
			return new TraitAPI.UnknownEntityException(resource + "/" + endpoint, method);
		}

		public Map<String, List<String>> getParams() {
			return parameters;
		}

		public PagingParameters getPagingParameters() {
			return pagingParameters;
		}

		public String getUserId() {
			return userId;
		}

		public HttpServletResponse getResponse() {
			return res;
		}
	}

	interface TraitAPIResource {

		TraitAPIEndpoint endpoint();

	}

	interface TraitAPIEndpoint {

		TraitAPIAuthorizedRequest authorize() throws IllegalAccessException;

		Object execute() throws Exception;

	}

	static class TraitAPIAuthorizedRequest {

		private final TraitAPIEndpoint endpoint;

		public TraitAPIAuthorizedRequest(TraitAPIEndpoint endpoint) {
			this.endpoint = endpoint;
		}

		public Object execute() throws Exception {
			return endpoint.execute();
		}

	}

	private TraitAPIRequest request(HttpServletRequest req, HttpServletResponse res, Method method) throws IOException, IllegalAccessException {
		return new TraitAPIRequest(userId(req), getTraitDAO(), req, res, method);
	}

	private String userId(HttpServletRequest req) throws IllegalAccessException {
		String apiUser = apiUser(req);
		String personToken = param(PERSON_TOKEN, req);
		if (personToken != null) {
			return personId(apiUser, personToken);
		}
		return apiUser;
	}

	private String personId(String apiUser, String personToken) throws IllegalAccessException {
		PersonToken tokenInfo = null;
		try {
			tokenInfo = getDAO().getPersonToken(personToken);
		} catch (IllegalArgumentException e) {
			throw new IllegalAccessException("Invalid personToken");
		}
		return getPersonId(tokenInfo, apiUser);
	}

	private String apiUser(HttpServletRequest req) throws IllegalAccessException {
		String accessToken = param("access_token", req);
		if (accessToken == null) accessToken = req.getHeader("Authorization");
		if (accessToken == null) throw new IllegalAccessException("Must provide access_token parameter or Authorization header");
		String apiUser = null;
		try {
			apiUser = getDAO().getApiUser(accessToken);
		} catch (IllegalArgumentException e) {
			throw new IllegalAccessException("Invalid access_token");
		}
		return apiUser;
	}

	private String getPersonId(PersonToken tokenInfo, String apiUser) throws IllegalAccessException {
		if (tokenInfo == null) return null;
		if (!apiUser.equals(tokenInfo.getTargetSystem().toString()))
			throw new IllegalAccessException("Person token is not provided for the same system that is calling this API");
		return tokenInfo.getPersonId();
	}

	private String param(String param, HttpServletRequest req) {
		if (!req.getParameterMap().containsKey(param)) {
			return null;
		}
		String[] vals = req.getParameterValues(param);
		if (vals.length != 1) return null;
		return vals[0];
	}

	private ResponseData response(Object entity) throws Exception {
		if (entity == null) throw new IllegalStateException("Null response");
		if (Collection.class.isAssignableFrom(entity.getClass())) {
			JSONArray array = new JSONArray();
			for (Object e : (Collection<?>)entity) {
				array.appendObject(((AbstractBaseEntity)e).toJSON());
			}
			return jsonResponse(array);
		}
		if (entity instanceof ValidationResponse) {
			return jsonResponse(((ValidationResponse)entity).toJSON());
		}
		if (entity instanceof AbstractBaseEntity) {
			return jsonResponse(((AbstractBaseEntity) entity).toJSON());
		}
		if (entity instanceof String) {
			return jsonResponse(new JSONObject().setString(OK, entity.toString()));
		}
		if (entity instanceof JSONArray) {
			return jsonResponse((JSONArray)entity);
		}
		if (entity instanceof JSONObject) {
			return jsonResponse((JSONObject)entity);
		}
		if (entity instanceof ResponseData) {
			return (ResponseData)entity;
		}
		throw new IllegalStateException("Unknown response " + entity.getClass());
	}

	private ResponseData exception(Exception e, HttpServletResponse res) throws Exception {
		if (e instanceof EntityNotFoundException) {
			return error(404, res, e);
		}
		if (e instanceof UnknownEntityException) {
			return error(404, res, e);
		}
		if (e instanceof IllegalAccessException) {
			return error(403, res, e);
		}
		if (e instanceof IllegalArgumentException) {
			return error(400, res, e);
		}
		if (e instanceof ValidationFailedException) {
			return error(422, res, e);
		}
		if (e instanceof ParseException) {
			return error(400, res, e);
		}
		if (e instanceof ServletException) {
			throw e;
		}
		getErrorReporter().report(e);
		return error(500, res, e);
	}

	private ResponseData error(int status, HttpServletResponse res, Exception e) {
		res.setStatus(status);
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.setInteger(STATUS, status);
		jsonResponse.setString(MESSAGE, e.getMessage());
		jsonResponse.setString(STACKTRACE, LogUtils.buildStackTrace(e, 10));
		return jsonResponse(jsonResponse);
	}

	@Override
	protected void handleException(Exception e, HttpServletRequest req, HttpServletResponse res) throws ServletException {
		if (e instanceof ServletException) throw (ServletException)e;
		super.handleException(e, req, res);
	}

}
