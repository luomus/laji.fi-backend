package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.luomus.lajitietokeskus.models.NewsItem;

public class NewsCmsContentCleanTests {

	@Test
	public void test() {
		String content = "testing";
		NewsItem news = create(content);
		assertEquals("testing", news.getContent());
	}

	@Test
	public void test_2() {
		String content = "" +
				"<p>Blaa noin 15 minuuttia</p><p><a href=\"https://link.link.com/S/0CAB67CA3888F325\" target=\"_blank\" rel=\"noopener noreferrer\">Vieraslajikysely</a></p><p>Kysely sulkeutuu <strong>31.8.2020 mennessä</strong>. (Jee). <br /><br />Kyselyn tuloksia ... sähköposti etunimi.sukunimi@ymparisto.fi</p><p>&nbsp;</p>";
		String expected = "" +
				"<p>Blaa noin 15 minuuttia</p><p><a href=\"https://link.link.com/S/0CAB67CA3888F325\" target=\"_blank\">Vieraslajikysely</a></p><p>Kysely sulkeutuu <strong>31.8.2020 mennessä</strong>. (Jee). Kyselyn tuloksia ... sähköposti etunimi.sukunimi@ymparisto.fi</p>";
		NewsItem news = create(content);
		assertEquals(expected, news.getContent());
	}

	private NewsItem create(String content) {
		return new NewsItem("id", null, null, "author", "title", content, "tag");
	}

}
