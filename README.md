Backends used by api.laji.fi (trais,, taxon search, cms, news).
This system also performs nightly taxon loading to Elastic Search for other /taxa endpoints.
This system implements trait database backend API and background tasks.

Laji.fi API
===========
##Taxon search API

* /api/taxon-search

### For example

* /api/taxon-search?q=susi&limit=1000
* /api/taxon-search?q=susi&payloadType=SEPARATED&matchType=EXACT,PARTIAL
* /api/taxon-search?q=pacific&informalTaxonGroup=MVL1,MVL2&onlySpecies=true

### Parameters

* q - The search word.
* checklist=MR.123|null - Default: Master checklist MR.1; "null" searches for orphan taxa (not on any checklist).
* matchType=EXACT,PARTIAL,LIKELY - Default: All match types; EXACT = exact matches, PARTIAL = partially matching, LIKELY = fuzzy matching.
* payloadType=JOINED_LIST|SEPARATED - Default: JOINED_LIST; Joined list is a sorted list of all matches (exact matches always first); SEPARATED returns match types in separate arrays (sorted internally).
* excludeNameTypes=MX.hasMisappliedName,MX.hasMisspelledName - Default: all name types are included. List name types you want to ignore.
* limit - Default: 10; When using payloadType=JOINED_LIST this is the size of the list; When using SEPARATED this is the size of each separate array.
* observationMode=true - Default: false. If observationMode is set, " sp." is catenated to higher taxa scientific name matches. 
* informalTaxonGroup=MVL.1,MVL.2 - Filter results to include only certain informal taxon group(s).
* excludedInformalTaxonGroup=MVL.1,MVL.2 - Filter results to exclude certain informal taxon group(s).
* taxonSet=MX.someSet,MX.otherSet - Filter results to include only taxa that are part of certain taxon set(s).
* Deprecated: onlySpecies=true - Filter to include only species (and subspecies).
* species=false|true - Filter to include only species and subspecies (true) or only higher taxa (false). Default: all. 
* onlyFinnish=true - Filter to include only finnish taxa. Default: false.
* onlyInvasive=true - Filter to include only invasive species. Default: false.
* includeHidden=false - Filter to exclude hidden taxa. Default: true.

### Error handling:
Will return status code 400 or 500 and JSON-object in payload with model:
~~~ 
{
	error: {
		statusCode: int,
		name: "string",
		message: "string"
	}
}
~~~ 

##News API

* /api/news
* /api/news/{id}
* /api/news/tags

###Parameters
 * locale [fi,sv,en]
 * tag

### For example
* /api/news (defaults to locale fi)
* /api/news?locale=sv
* /api/news?locale=sv&tag=syke.fi
* /api/news/1
* /api/news/tags

### Error handling
* Will return something else than HTTP 200


-----------------------

##CMS API

* /api/CMS/{id}?locale={fi,sv,en}

### For example
* /api/CMS (defaults to locale fi, returns front page)
* /api/CMS?locale=sv (returns front page for locale sv)
* /api/CMS/1  (returns page with given id, locale has no effect)

### Error handling
* Will return something else than HTTP 200


-----------------------

##ElasticSearch

* GET api/es-load-status

### Returns
* 200 "ok" if all ok
* 500 and text/plain message if previous load attempt failed

* POST api/es-load-start?secret=xxx

Starts to push all taxon data to ElasticSearch



Dev environment
============
1. Clone git repo to <catalina.base>/webapps/lajitietokeskus
2. Add project to IDE
3. Set class output folder to /WEB-INF/classes
4. Include <catalina.base>/lib/servlet-api.jar and <catalina.base>/bin/tomcat-juli.jar to build path
5. Follow deployment instructions 1-2


Deployment
============

1. Place lajitietokeskus.properties config file to <catalina.base>/app-conf/
2. Place ojdbc6.jar, sqljdbc4.jar, sqljdbc4.jar and mysql-connector-java-5.1.35-bin.jar to <catalina.base>/lib
3. Deploy lajitietokeskus.war


#### lajitietokeskus.properties example
~~~ 
SystemID = lajitietokeskus

DevelopmentMode = NO
StagingMode = YES
ProductionMode = NO
#LIMITED_TAXON_LOAD = YES

#All other folders are relative to basefolder
BaseFolder = .../tomcat/

DataFolder         = webapps/lajitietokeskus/data
StorageFolder      = application-output/lajitietokeskus/storage
LanguageFileFolder = webapps/lajitietokeskus/locales
LanguageFiles = locale
SupportedLanguages = fi,en,sv

ErrorReporting_SMTP_Host = localhost
ErrorReporting_SMTP_Username =
ErrorReporting_SMTP_Password =
ErrorReporting_SMTP_SendTo = xxx, xxx
ErrorReporting_SMTP_SendFrom = xxx
ErrorReporting_SMTP_Subject = Laji.fi Staging Error Report

MySQLRead_DBdriver = com.mysql.jdbc.Driver
MySQLRead_DBurl = jdbc:mysql://
MySQLRead_DBusername = 
MySQLRead_DBpassword = 

InvasiveMySQLRead_DBdriver = com.mysql.jdbc.Driver
InvasiveMySQLRead_DBurl = jdbc:mysql://
InvasiveMySQLRead_DBusername = 
InvasiveMySQLRead_DBpassword = 

IucnMySQLRead_DBdriver = com.mysql.jdbc.Driver
IucnMySQLRead_DBurl = jdbc:mysql://
IucnMySQLRead_DBusername = 
IucnMySQLRead_DBpassword = 

Taxonomy_DBdriver = oracle.jdbc.OracleDriver
Taxonomy_DBurl = jdbc:oracle:thin:@
Taxonomy_DBusername = 
Taxonomy_DBpassword = 

TraitDB_DBdriver = oracle.jdbc.OracleDriver
TraitDB_DBurl = jdbc:oracle:thin:@
TraitDB_DBusername = 
TraitDB_DBpassword = 

TraitTriplestoreURL = https://.../triplestore
TraitTriplestoreUsername = 
TraitTriplestorePassword =

TriplestoreURL = https://.../triplestore
TriplestoreUsername = 
TriplestorePassword = 

# These are for access_token and personToken validation so use apitest or api prod as appropriate
ApiBaseURL = https://apitest.laji.fi/v0
ApiKey = 

DwURL = https://.../taxon-obs-count

# This is only for dev environment to limit taxon load
TaxonomyAPIURL = ../api/taxa 

ES_address = 
ES_cluster = 
ES_push_secret = 

FinBIFBibURL = https:// 

BOLD_feed_URL = http://...

~~~

Triplestore.jar
============
Laji-backend uses Triplestore functionality. To generate triplestore.jar include the following Triplestore packages:
* fi.luomus.triplestore.dao
* fi.luomus.triplestore.taxonomy.dao
* fi.luomus.triplestore.taxonomy.iucn.model
* fi.luomus.triplestore.taxonomy.models

It is important no to include any Servlets (service packages) -- otherwise they would start running when deployed on server.



