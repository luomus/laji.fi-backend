package fi.luomus.lajitietokeskus.models;

import java.sql.Timestamp;

public class CMSPage extends NewsItem {

	private final int menuOrder;
	private final Integer copyNumber;

	public CMSPage(String id, Timestamp posted, Timestamp modified, String author, String title, String content, int menuOrder, Integer copyNumber) {
		super(id, posted, modified, author, title, content, null);
		this.menuOrder = menuOrder;
		this.copyNumber = copyNumber;
	}

	public int getMenuOrder() {
		return menuOrder;
	}

	public Integer getCopyNumber() {
		return copyNumber;
	}

}
