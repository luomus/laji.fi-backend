package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.lajitietokeskus.dao.LuomusNewsService;
import fi.luomus.lajitietokeskus.models.NewsItem;

public class LuomusNewsServiceTests {

	@Test
	public void parsingNews() throws Exception {
		JSONObject json = new JSONObject(getContents("luomus_news.json"));
		List<NewsItem> news = LuomusNewsService.parse(json);
		assertEquals(1, news.size());

		NewsItem n1 = news.get(0);
		assertEquals("Suomen eliölajien luettelon vuosiversio 2024 on julkaistu", n1.getTitle());
		assertEquals("luomus", n1.getTag());
		assertEquals("luomus-40338", n1.getId());
		assertEquals("2025-01-15 00:00:00.0", n1.getPosted().toString());
		assertEquals("https://www.helsinki.fi/fi/luomus/uutiset/suomen-eliolajien-luettelon-vuosiversio-2024-julkaistu", n1.getExternalUrl().toString());
	}

	private String getContents(String filename) throws Exception {
		return TestUtil.getContents(LuomusNewsServiceTests.class, filename);
	}

}
