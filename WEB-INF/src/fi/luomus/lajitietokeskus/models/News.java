package fi.luomus.lajitietokeskus.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class News {

	Map<String, TreeSet<NewsItem>> newsItems = new HashMap<>();
	Map<String, NewsItem> newsItemsById = new HashMap<>();
	private boolean loadingFailed = false;

	public NewsItem getById(String id) {
		return newsItemsById.get(id);
	}

	public java.util.Collection<NewsItem> getForLocale(String locale) {
		if (!newsItems.containsKey(locale)) return Collections.emptyList();
		return newsItems.get(locale);
	}

	public synchronized void addForLocale(String locale, NewsItem newsItem) {
		if (!newsItems.containsKey(locale)) {
			newsItems.put(locale, new TreeSet<NewsItem>());
		}
		newsItems.get(locale).add(newsItem);
		newsItemsById.put(newsItem.getId(), newsItem);
	}

	public java.util.Collection<String> getTagsForLocale(String locale) {
		Set<String> tags = new TreeSet<>();
		for (NewsItem i : getForLocale(locale)) {
			tags.add(i.getTag());
		}
		return tags;
	}

	public java.util.Collection<String> getTags() {
		Set<String> tags = new TreeSet<>();
		for (NewsItem i : newsItemsById.values()) {
			tags.add(i.getTag());
		}
		return tags;
	}

	public List<NewsItem> get(String locale, List<String> tags) {
		List<NewsItem> items = new ArrayList<>(getForLocale(locale));
		if (tags.isEmpty()) return items;
		return items.stream().filter(n->tags.contains(n.getTag())).collect(Collectors.toList());
	}

	public News loadingFailed() {
		loadingFailed = true;
		return this;
	}

	public boolean successfullyLoaded() {
		return !loadingFailed;
	}

}
