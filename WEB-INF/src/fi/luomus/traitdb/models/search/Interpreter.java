package fi.luomus.traitdb.models.search;

import java.util.List;
import java.util.stream.Collectors;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonSearch;
import fi.luomus.commons.taxonomy.TaxonSearchResponse;
import fi.luomus.commons.taxonomy.TaxonSearchResponse.Match;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.lajitietokeskus.taxonomy.dao.GBIFTaxonDAO;
import fi.luomus.lajitietokeskus.taxonomy.dao.GBIFTaxonDAO.GBIFTaxon;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonToMap;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.models.Subject;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.models.TraitValue;
import fi.luomus.traitdb.models.UnitOfMeasurement;

public class Interpreter {

	private static final Qname SCIENTIFIC_NAME = new Qname("MX.scientificName");
	private static final Qname INDIVIDUAL = new Qname("TDF.typeIndividual");
	private static final String YYYY_MM_DD = "yyyy-MM-dd";
	private static final String WGS84 = "WGS84";
	public static final Qname LICENSE = new Qname("MZ.intellectualRightsCC-BY-4.0");
	private static final Qname RANGE_TAXON = new Qname("MX.taxon");
	private static final String GBIF_PREFIX = "gbif:";
	private static final String TAXON_QNAME_PREFIX = "MX.";
	private static final String TAXON_URI_PREFIX = new Qname(TAXON_QNAME_PREFIX).toURI();

	private final TraitDAO traitDAO;
	private final TaxonomyDAO taxonomyDAO;
	private final GBIFTaxonDAO gbifTaxonDAO;

	public Interpreter(TraitDAO traitDAO, TaxonomyDAO taxonomyDAO, GBIFTaxonDAO gbifTaxonDAO) {
		this.traitDAO = traitDAO;
		this.taxonomyDAO = taxonomyDAO;
		this.gbifTaxonDAO = gbifTaxonDAO;
	}

	public TraitSearchRow get(Subject subject, TraitValue traitValue) throws Exception {
		TraitSearchRow row = new TraitSearchRow();
		row.setId(traitValue.getId());
		row.setSubject(subject);

		dates(subject, row);
		geodeticDatum(subject, row);
		subjectTaxa(subject, row);
		traitValue(traitValue, row);
		objectTaxa(traitValue, row);
		row.setWarnings(traitValue.isWarnings());
		row.setMeasurementRemarks(traitValue.getMeasurementRemarks());
		row.setReference(traitValue.getReference());
		individualCount(row);
		row.setDataset(traitDAO.datasets().get(subject.getDatasetId()));
		row.setLicense(LICENSE);
		secure(row);
		return row;
	}

	private void secure(TraitSearchRow row) {
		if (row.getSubjectFinBIFTaxon() == null) return;
		if (!row.getSubjectFinBIFTaxon().isSensitive()) return;
		row.getSubject().setLat(secureRound(row.getSubject().getLat()));
		row.getSubject().setLon(secureRound(row.getSubject().getLon()));
		row.getSubject().setLatMin(secureMin(row.getSubject().getLatMin()));
		row.getSubject().setLatMax(secureMax(row.getSubject().getLatMax()));
		row.getSubject().setLonMin(secureMin(row.getSubject().getLonMin()));
		row.getSubject().setLonMax(secureMax(row.getSubject().getLonMax()));
		row.getSubject().setCoordinateAccuracy(100000);
		row.getSubject().setLocality(row.getSubject().getLocality() == null ? null : "hidden");
		row.getSubject().setMunicipality(row.getSubject().getMunicipality() == null ? null : "hidden");
		row.getSubject().setOccurrenceRemarks(row.getSubject().getOccurrenceRemarks() == null ? null : "hidden");
	}

	public static Double secureMax(Double maxCoord) {
		if (maxCoord == null) return null;
		return Math.ceil(maxCoord);
	}

	public static Double secureMin(Double minCoord) {
		if (minCoord == null) return null;
		if (minCoord >= 0) {
			return Math.floor(minCoord);
		}
		return Math.ceil(minCoord);
	}

	public static Double secureRound(Double coord) {
		if (coord == null)  return null;
		if (coord == 0.0) return coord;
		return Math.round(coord - 0.5) + 0.5;
	}

	private void geodeticDatum(Subject subject, TraitSearchRow row) {
		if (given(subject.getLat())|| given(subject.getLon())) {
			row.setGeodeticDatum(WGS84);
		}
	}

	private void individualCount(TraitSearchRow row) {
		if (INDIVIDUAL.equals(row.getSubject().getType()) && row.getSubject().getIndividualCount() == null) {
			row.getSubject().setIndividualCount(1);
		}
	}

	private void objectTaxa(TraitValue traitValue, TraitSearchRow row) throws Exception {
		Qname taxonId = getTaxonId(traitValue.getValue());
		Integer gbifTaxonId = getGBIFTaxonId(traitValue.getValue());
		if (given(taxonId) || given(gbifTaxonId)) {
			ResolvedTaxa resolvedTaxa = resolveTaxon(taxonId, gbifTaxonId, null, null);
			row.setObjectFinBIFTaxon(resolvedTaxa.getFinBIFTaxon());
			row.setObjectGBIFTaxon(resolvedTaxa.getGBIFTaxon());
			row.setObjectTaxonVerbatim(traitValue.getValue());
		} else {
			Trait trait = traitDAO.traits().get(traitValue.getTraitId());
			if (RANGE_TAXON.equals(trait.getRange())) {
				ResolvedTaxa resolvedTaxa = resolveTaxon(null, null, traitValue.getValue(), null);
				row.setObjectFinBIFTaxon(resolvedTaxa.getFinBIFTaxon());
				row.setObjectGBIFTaxon(resolvedTaxa.getGBIFTaxon());
				row.setObjectTaxonVerbatim(traitValue.getValue());
			}
		}
		row.setObjectTaxonLifeStage(traitValue.getObjectTaxonLifeStage());
	}

	private void traitValue(TraitValue traitValue, TraitSearchRow row) {
		Trait trait = traitDAO.traits().get(traitValue.getTraitId());
		TraitGroup group = traitDAO.groups().get(trait.getGroup());
		row.setTrait(trait);
		row.setTraitGroup(group);

		row.setStatisticalMethod(traitValue.getStatisticalMethod());

		row.setOriginalValue(traitValue.getValue());
		row.setOriginalUnit(traitValue.getUnit());

		Double originalNumeric = getNumeric(traitValue.getValue());
		Double originalAccuracy = traitValue.getMeasurementAccuracy();
		UnitOfMeasurement originalUnit = getUnit(traitValue.getUnit());
		UnitOfMeasurement traitUnit = getUnit(trait.getBaseUnit());

		row.setOriginalValueNumeric(originalNumeric);
		row.setOriginalMeasurementAccuracy(originalAccuracy);

		if (given(traitUnit) && given(originalUnit)) {
			row.setValueNumeric(convertNumeric(originalNumeric, originalUnit, traitUnit));
			row.setMeasurementAccuracy(convertNumeric(originalAccuracy, originalUnit, traitUnit));
			row.setUnit(traitUnit.getId());
		}
		row.setValue(given(row.getValueNumeric()) ? row.getValueNumeric().toString() : given(row.getOriginalValueNumeric()) ? row.getOriginalValueNumeric().toString() : row.getOriginalValue());
	}

	private void subjectTaxa(Subject subject, TraitSearchRow row) throws Exception {
		ResolvedTaxa subjectTaxa = resolveTaxon(subject.getTaxonId(), subject.getGbifTaxonId(), subject.getScientificName(), subject.getKingdom());
		row.setSubjectFinBIFTaxon(subjectTaxa.getFinBIFTaxon());
		row.setSubjectGBIFTaxon(subjectTaxa.getGBIFTaxon());
	}

	private void dates(Subject subject, TraitSearchRow row) {
		if (subject.getDateBegin() != null) {
			if (subject.getDateEnd() == null) subject.setDateEnd(subject.getDateBegin());
			DateValue begin = subject.getDateBegin();
			DateValue end = subject.getDateEnd();
			row.setYear(year(begin, end));
			row.setMonth(month(begin, end));
			row.setDay(day(begin, end));
			if (subject.getSeasonBegin() == null && subject.getSeasonEnd() == null) {
				if (end.getYearAsInt() - begin.getYearAsInt() <= 1) {
					subject.setSeasonBegin(season(begin));
					subject.setSeasonEnd(season(end));
					if (end.getYearAsInt() != begin.getYearAsInt()) {
						if (subject.getSeasonBegin() <= subject.getSeasonEnd()) {
							subject.setSeasonBegin(null);
							subject.setSeasonEnd(null);
						}
					}
				}
			}
			subject.setYearBegin(begin.getYearAsInt());
			subject.setYearEnd(end.getYearAsInt());
		} else if (subject.getDateEnd() != null) {
			subject.setYearEnd(subject.getDateEnd().getYearAsInt());
		} else if (subject.getYearBegin() != null) {
			if (subject.getYearEnd() == null) subject.setYearEnd(subject.getYearBegin());
			if (row.getYear() == null) {
				if (subject.getYearBegin() == subject.getYearEnd()) {
					row.setYear(subject.getYearBegin());
				}
			}
		}
		row.setEventDate(eventDate(subject));
	}

	private String eventDate(Subject subject) {
		if (subject.getDateBegin() != null) {
			if (subject.getDateBegin().equals(subject.getDateEnd())) {
				return DateUtils.format(subject.getDateBegin(), YYYY_MM_DD);
			}
			return DateUtils.format(subject.getDateBegin(), YYYY_MM_DD) + "/" + DateUtils.format(subject.getDateEnd(), YYYY_MM_DD);
		}
		if (subject.getDateEnd() != null) {
			return "/"+DateUtils.format(subject.getDateEnd(), YYYY_MM_DD);
		}
		if (subject.getYearBegin() != null) {
			if (subject.getYearBegin() == subject.getYearEnd()) {
				return subject.getYearBegin().toString();
			}
			return subject.getYearBegin() + "/" + subject.getYearEnd();
		}
		if (subject.getYearEnd() != null) {
			return "/" + subject.getYearEnd();
		}
		return null;
	}

	private Integer season(DateValue date) {
		String month = date.getMonth();
		String day = date.getDay();
		if (day.length() < 2) day = "0" + day;
		return Integer.valueOf(month + day);
	}

	private Integer day(DateValue begin, DateValue end) {
		if (begin.getDayAsInt() == end.getDayAsInt()) {
			return begin.getDayAsInt();
		}
		return null;
	}

	private Integer month(DateValue begin, DateValue end) {
		if (begin.getMonthAsInt() == end.getMonthAsInt()) {
			return begin.getMonthAsInt();
		}
		return null;
	}

	private Integer year(DateValue begin, DateValue end) {
		if (begin.getYearAsInt() == end.getYearAsInt()) {
			return begin.getYearAsInt();
		}
		return null;
	}

	private Double convertNumeric(Double originalNumeric, UnitOfMeasurement originalUnit, UnitOfMeasurement traitUnit) {
		if (!given(originalNumeric)) return null;
		if (!given(originalUnit)) return null;
		if (!given(traitUnit)) return null;
		if (originalUnit.equals(traitUnit)) return originalNumeric;

		if (!given(originalUnit.getBase())) return null;
		UnitOfMeasurement baseUnit = traitDAO.units().get(originalUnit.getBase());
		Double baseValue = null;
		if (originalUnit.equals(baseUnit)) {
			baseValue = originalNumeric;
		} else {
			if (!given(originalUnit.getConversionFactor())) return null;
			baseValue = originalNumeric * originalUnit.getConversionFactor();
		}
		if (traitUnit.equals(baseUnit)) return baseValue;
		if (!given(traitUnit.getConversionFactor())) return null;
		return baseValue / traitUnit.getConversionFactor();
	}

	private UnitOfMeasurement getUnit(Qname unitId) {
		if (!given(unitId)) return null;
		return traitDAO.units().get(unitId);
	}

	private Integer getGBIFTaxonId(String value) {
		value = value.toLowerCase();
		if (value.startsWith(GBIF_PREFIX)) {
			try {
				return Integer.valueOf(value.replace(GBIF_PREFIX, ""));
			} catch (NumberFormatException e) {}
		}
		return null;
	}

	private Qname getTaxonId(String value) {
		if (value.startsWith(TAXON_QNAME_PREFIX)) {
			return new Qname(value);
		}
		if (value.startsWith(TAXON_URI_PREFIX)) {
			return Qname.fromURI(value);
		}
		return null;
	}

	private Double getNumeric(String value) {
		value = value.toLowerCase();
		if (value.contains("e")) return null; // Scientific notation
		try {
			Double doubleValue = Double.valueOf(value.replace(",", "."));
			if (doubleValue.longValue() >= Integer.MAX_VALUE) return null;
			return doubleValue;
		} catch (NumberFormatException e) {
			return null;
		}
	}

	private static class ResolvedTaxa {
		private final TraitTaxon taxon;
		private final TraitTaxon gbifTaxon;
		private ResolvedTaxa(TraitTaxon taxon, TraitTaxon gbifTaxon) {
			this.taxon = taxon;
			this.gbifTaxon = gbifTaxon;
		}
		public TraitTaxon getFinBIFTaxon() {
			return taxon;
		}
		public TraitTaxon getGBIFTaxon() {
			return gbifTaxon;
		}
	}

	private ResolvedTaxa resolveTaxon(Qname taxonId, Integer gbifTaxonId, String scientificName, String kingdom) throws Exception {
		TraitTaxon finbif = null;
		TraitTaxon gbif =  null;
		if (given(taxonId)) {
			finbif = getTaxon(taxonId);
		}
		if (given(gbifTaxonId)) {
			gbif = getGBIFTaxon(gbifTaxonId);
		}
		if (finbif != null && gbif != null) return new ResolvedTaxa(finbif, gbif);
		if (finbif != null || gbif != null) {
			if (gbif != null) {
				scientificName = gbif.getScientificName();
				kingdom = gbif.getHigherTaxa().get("kingdom");
			} else if (finbif != null) {
				scientificName = finbif.getScientificName();
				kingdom = finbif.getHigherTaxa().get("kingdom");
			}
		}
		if (!given(scientificName)) return new ResolvedTaxa(null, null);

		if (finbif == null) {
			TaxonSearchResponse res = taxonomyDAO.search(new TaxonSearch(scientificName).onlyExact());
			List<Match> filteredMatches = onlyScientificNames(res);
			if (given(kingdom)) {
				for (Match match : filteredMatches) {
					if (kingdom.equalsIgnoreCase(match.getTaxon().getScientificNameOfRank("kingdom"))) {
						finbif = toTraitTaxon(match.getTaxon());
						break;
					}
				}
			}
			if (finbif == null && filteredMatches.size() == 1) {
				finbif = toTraitTaxon(res.getExactMatches().get(0).getTaxon());
			}
		}
		if (gbif == null) {
			GBIFTaxon taxon = gbifTaxonDAO.search(scientificName, kingdom);
			if (taxon != null) {
				gbif = toTraitTaxon(taxon);
			}
		}
		return new ResolvedTaxa(finbif, gbif);
	}

	private List<Match> onlyScientificNames(TaxonSearchResponse res) {
		return res.getExactMatches().stream().filter(m->m.getNameType().equals(SCIENTIFIC_NAME)).collect(Collectors.toList());
	}

	private TraitTaxon getGBIFTaxon(Integer id) {
		if (id == null) return null;
		GBIFTaxon taxon = gbifTaxonDAO.get(id);
		if (taxon == null) return null;
		return toTraitTaxon(taxon);
	}

	private TraitTaxon getTaxon(Qname taxonId) throws Exception {
		if (!taxonomyDAO.getTaxonContainer().hasTaxon(taxonId)) return null;
		Taxon taxon = taxonomyDAO.getTaxonContainer().getTaxon(taxonId);
		if (taxon.getSynonymParent() != null) {
			taxon = taxon.getSynonymParent();
		}
		if (Taxon.MASTER_CHECKLIST.equals(taxon.getChecklist())) {
			return toTraitTaxon(taxon);
		}
		return null;
	}

	private TraitTaxon toTraitTaxon(GBIFTaxon taxon) {
		TraitTaxon traitT = new TraitTaxon();

		traitT.setId(GBIF_PREFIX+taxon.getId());
		traitT.setTaxonRank(plainRank(taxon.getTaxonRank()));
		traitT.setScientificName(taxon.getScientificName());
		traitT.setCursiveName(Taxon.shouldCursive(taxon.getTaxonRank()));
		traitT.setAuthor(taxon.getAuthor());
		for (Qname rank : TaxonToMap.HIGHER_RANKS) {
			String name = taxon.getHigherTaxa().get(rank);
			if (given(name)) traitT.getHigherTaxa().put(plainRank(rank), name);
		}
		return traitT;
	}

	private TraitTaxon toTraitTaxon(Taxon taxon) {
		TraitTaxon traitT = new TraitTaxon();
		traitT.setId(taxon.getId().toURI());
		traitT.setTaxonRank(plainRank(taxon.getTaxonRank()));
		traitT.setScientificName(taxon.getScientificName());
		traitT.setCursiveName(taxon.isCursiveName());
		traitT.setAuthor(taxon.getScientificNameAuthorship());

		for (Qname rank : TaxonToMap.HIGHER_RANKS) {
			String name = taxon.getScientificNameOfRank(rank);
			if (given(name)) traitT.getHigherTaxa().put(plainRank(rank), name);
		}

		if (taxon.getLatestRedListStatusFinland() != null) {
			traitT.setIucnStatus(taxon.getLatestRedListStatusFinland().getStatus());
		}

		if (taxon.getPrimaryHabitat() != null) {
			traitT.setPrimaryHabitat(taxon.getPrimaryHabitat().getHabitat());
			for (Qname specifier : taxon.getPrimaryHabitat().getHabitatSpecificTypes()) {
				traitT.getHabitatSpecifiers().add(specifier);
			}
		}

		traitT.setSensitive(taxon.isSensitive());

		return traitT;
	}

	private String plainRank(Qname taxonRank) {
		if (taxonRank == null) return null;
		return taxonRank.toString().replace(TAXON_QNAME_PREFIX, "");
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private boolean given(Double d) {
		return d != null;
	}

	private boolean given(Integer i) {
		return i != null;
	}

	private boolean given(Qname qname) {
		return qname !=  null && qname.isSet();
	}

	private boolean given(UnitOfMeasurement u) {
		return u != null;
	}

}
