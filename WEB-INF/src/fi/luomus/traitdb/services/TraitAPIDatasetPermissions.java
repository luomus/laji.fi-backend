package fi.luomus.traitdb.services;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOEntity;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.services.TraitAPI.Method;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIAuthorizedRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIEndpoint;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIResource;

public class TraitAPIDatasetPermissions implements TraitAPIResource {

	private final TraitAPIRequest req;
	private final TraitDAOEntity<DatasetPermissions> dao;

	public TraitAPIDatasetPermissions(TraitAPIRequest req) {
		this.req = req;
		this.dao = req.getDAO().permissions();
	}

	@Override
	public TraitAPIEndpoint endpoint() {
		String endpoint = req.getEndpoint();
		if (Method.GET == req.getMethod()) {
			if ("".equals(endpoint)) return getUsersPermissions();
			if ("{id}".equals(endpoint)) return getSingleDatasetsPermissions(req.getId());
		}
		if (Method.POST == req.getMethod()) {
			if ("validate-update/{id}".equals(endpoint)) return validate(req.getId(), req.getJson());
		}
		if (Method.PUT == req.getMethod()) {
			if ("{id}".equals(endpoint)) return update(req.getId(), req.getJson());
		}
		throw req.noEndpointException();
	}

	private TraitAPIEndpoint update(Qname id, JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				DatasetPermissions permissions = new DatasetPermissions(json);
				validateIdMatches(id, permissions);
				dao.validate(permissions, false).requirePass();
				return dao.update(permissions);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(id, this);
			}
		};
	}

	private TraitAPIEndpoint validate(Qname datesetId, JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				DatasetPermissions permissions = new DatasetPermissions(json);
				validateIdMatches(datesetId, permissions);
				return dao.validate(permissions, true);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(datesetId, this);
			}
		};
	}

	private TraitAPIEndpoint getSingleDatasetsPermissions(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.get(id);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return new TraitAPIAuthorizedRequest(this);
			}
		};
	}

	private TraitAPIEndpoint getUsersPermissions() {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				JSONArray array = new JSONArray();
				dao.getAll().stream().filter(p->p.getUserIds().contains(req.getUserId())).map(p->p.toJSON()).forEach(array::appendObject);
				return array;
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return new TraitAPIAuthorizedRequest(this);
			}
		};
	}

	private TraitAPIAuthorizedRequest requirePermissions(Qname id, TraitAPIEndpoint endpoint) throws IllegalAccessException {
		if (req.getDAO().permissions().get(id).getUserIds().contains(req.getUserId())) {
			return new TraitAPIAuthorizedRequest(endpoint);
		}
		throw new IllegalAccessException("No permissions to dataset");
	}

	private void validateIdMatches(Qname datasetId, DatasetPermissions permissions) {
		if (permissions.getDatasetId() == null || !permissions.getDatasetId().equals(datasetId)) throw new IllegalArgumentException("Id in path and model do not match");
	}
}
