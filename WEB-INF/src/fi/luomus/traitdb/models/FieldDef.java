package fi.luomus.traitdb.models;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface FieldDef {

	double order();
	boolean required() default false;
	int length() default -1;
	String enumeration() default "";
	String etsOverride() default "";
	boolean ignoreInSearch() default false;

}
