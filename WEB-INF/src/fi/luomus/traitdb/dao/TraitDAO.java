package fi.luomus.traitdb.dao;

import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.models.Enumeration;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.models.UnitOfMeasurement;

public interface TraitDAO extends AutoCloseable {

	public static class ValidationFailedException extends RuntimeException {
		private static final long serialVersionUID = 8648658516877252616L;
		public ValidationFailedException(String message) {
			super(message);
		}
	}

	public static class EntityNotFoundException extends RuntimeException {
		private static final long serialVersionUID = -2710280470102207693L;
		public EntityNotFoundException(Qname id) {
			super("Not found: " + id);
		}
		public EntityNotFoundException(Collection<Qname> ids) {
			super("not found: " + ids.stream().map(id->id.toString()).collect(Collectors.joining(", ")));
		}
	}

	public static class TraitDAOException extends RuntimeException {
		private static final long serialVersionUID = -7303188318360032358L;
		public TraitDAOException(Class<?> entity, String method, String params, Exception e) {
			super(message(entity, method, params), e);
		}
		private static String message(Class<?> entity, String method, String params) {
			if (params != null) {
				return "Issues with " + entity.getSimpleName() + " " + method + " " + params;
			}
			return "Issues with " + entity.getSimpleName() + " " + method;
		}
	}

	public static class ValidationResponse {

		public static final String ERRORS = "errors";
		private final Map<String, String> errors;

		public ValidationResponse(Map<String, String> errors) {
			this.errors = errors;
		}

		public Map<String, String> getErrors() {
			return errors;
		}

		public boolean ok() {
			return errors.isEmpty();
		}

		public ValidationResponse requirePass() throws ValidationFailedException {
			if (ok()) return this;
			throw new ValidationFailedException(errors.toString());
		}

		@Override
		public String toString() {
			return errors.toString();
		}

		public JSONObject toJSON() {
			JSONObject json = new JSONObject();
			json.setBoolean("pass", ok());
			for (Map.Entry<String, String> error : errors.entrySet()) {
				json.getObject(ERRORS).setString(error.getKey(), error.getValue());
			}
			return json;
		}

		public static ValidationFailedException fail(Exception e) {
			return new ValidationFailedException(e.getMessage());
		}
	}

	public interface TraitDAOEntity<T> {
		/**
		 * Get by id
		 * @param id
		 * @return entity by id
		 * @throws EntityNotFoundException
		 */
		T get(Qname id) throws EntityNotFoundException;

		/**
		 * Validate if entity by this id can be deleted
		 * @param id
		 * @return
		 * @throws UnsupportedOperationException if not implemented
		 */
		ValidationResponse validateDelete(Qname id) throws UnsupportedOperationException;

		/**
		 * Validate insert or update of entity
		 * @param entity
		 * @param insert true=insert, false=update
		 * @return results of validation
		 * @throws UnsupportedOperationException if storing is not implemented
		 */
		ValidationResponse validate(T entity, boolean insert) throws UnsupportedOperationException;

		/**
		 * Insert one
		 * @param entity
		 * @return the stored entity with id set
		 * @throws UnsupportedOperationException if storing is not implemented
		 */
		T insert(T entity) throws UnsupportedOperationException;

		/**
		 * Delete entity by id
		 * @param id
		 * @throws EntityNotFoundException
		 * @throws UnsupportedOperationException if storing is not implemented
		 */
		void delete(Qname id) throws EntityNotFoundException, UnsupportedOperationException;

		/**
		 * Validate multiple entities for insert or update
		 * @param entities
		 * @return
		 * @throws UnsupportedOperationException if multi storing is not implemented
		 */
		List<ValidationResponse> validate(Collection<T> entities) throws UnsupportedOperationException;

		/**
		 * Insert multiple
		 * @param entities
		 * @throws UnsupportedOperationException if multi storing is not implemented
		 */
		void insert(Collection<T> entities) throws UnsupportedOperationException;

		/**
		 * Update by id
		 * @param entity
		 * @return updated entity
		 * @throws UnsupportedOperationException if storing is not implemented
		 * @throws EntityNotFoundException
		 */
		T update(T entity) throws EntityNotFoundException, UnsupportedOperationException;

		/**
		 * Get all
		 * @return entities
		 * @throws UnsupportedOperationException if not implemented
		 */
		Collection<T> getAll() throws UnsupportedOperationException;

		/**
		 * Filtered search
		 * @param searchParams fieldname : required value - all given parameters must match at least one of the values
		 * @params limit
		 * @throws UnsupportedOperationException if searching is not implemented
		 * @return matching entities
		 */
		Collection<T> search(Map<String, List<String>> searchParams, int limit) throws UnsupportedOperationException;
	}

	public interface SearchDAO {
		/**
		 * Search published trait data
		 * @param searchParams for each key at least one value has to match
		 * @param pageSize
		 * @param page
		 * @return
		 */
		JSONArray search(Map<String, List<String>> searchParams, int pageSize, int page);
		/**
		 * Get number of total matches from published trait data
		 * @param searchParams for each key at least one value has to match
		 * @return total matches
		 */
		long searchTotal(Map<String, List<String>> searchParams);
		/**
		 * Add traits as TSV to out
		 * @param params - for each key at least one value has to match
		 * @param limit
		 * @param out
		 * @param filename
		 * @return filename without prefix
		 */
		void download(Map<String, List<String>> params, int limit, OutputStream out, String filename);
		/**
		 * Update intput row to search index
		 * @param row
		 */
		void update(InputRow row);
		/**
		 * Update input rows to search index
		 * @param rows
		 */
		void update(Collection<InputRow> rows);
		/**
		 * Delete input row from search index
		 * @param row
		 */
		void delete(InputRow row);
		/**
		 * Reprocess all published datasets to search index
		 */
		void reprocessAllDatasetsBackground();
		/**
		 * Reprocess dataset to search index (removing index if unpublished)
		 * @param dataset
		 */
		void reprocessDatasetBackground(Dataset dataset);
	}

	TraitDAOEntity<Trait> traits();
	TraitDAOEntity<TraitGroup> groups();
	TraitDAOEntity<UnitOfMeasurement> units();
	TraitDAOEntity<Dataset> datasets();
	TraitDAOEntity<DatasetPermissions> permissions();
	TraitDAOEntity<InputRow> rows();
	Enumeration getEnumeration(Qname id);
	SearchDAO search();

}