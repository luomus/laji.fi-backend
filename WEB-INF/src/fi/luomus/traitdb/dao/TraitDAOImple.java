package fi.luomus.traitdb.dao;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.lajitietokeskus.taxonomy.dao.GBIFTaxonDAO;
import fi.luomus.traitdb.dao.db.DatasetPermissionsImple;
import fi.luomus.traitdb.dao.db.InputRowsImple;
import fi.luomus.traitdb.dao.db.OracleDAOImple;
import fi.luomus.traitdb.dao.triplestore.DatasetsImple;
import fi.luomus.traitdb.dao.triplestore.TraitEnumerationValuesImple;
import fi.luomus.traitdb.dao.triplestore.TraitGroupsImple;
import fi.luomus.traitdb.dao.triplestore.TraitsImple;
import fi.luomus.traitdb.dao.triplestore.TriplestoreEnumerationsImple;
import fi.luomus.traitdb.dao.triplestore.UnitsOfMeasurementImple;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.models.Enumeration;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitEnumerationValue;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.models.UnitOfMeasurement;

public class TraitDAOImple implements TraitDAO {

	private final TraitDAOEntity<Trait> traits;
	private final TraitDAOEntity<TraitEnumerationValue> traitEnumerations;
	private final TraitDAOEntity<TraitGroup> groups;
	private final TraitDAOEntity<UnitOfMeasurement> units;
	private final TraitDAOEntity<Dataset> datasets;
	private final TraitDAOEntity<InputRow> rows;
	private final TraitDAOEntity<DatasetPermissions> permissions;
	private final TraitDAOEntity<Enumeration> enumerations;
	private final OracleDAOImple dbDAO;
	private final SearchDAOImple searchDAO;

	public TraitDAOImple(Config config, TaxonomyDAO taxonomyDAO, GBIFTaxonDAO gbifTaxonDAO, ErrorReporter errorReporter) {
		this.dbDAO = new OracleDAOImple(config);
		this.traits = new TraitsImple(config, this);
		this.traitEnumerations = new  TraitEnumerationValuesImple(config);
		this.groups = new TraitGroupsImple(config, this);
		this.units = new UnitsOfMeasurementImple(config);
		this.enumerations = new TriplestoreEnumerationsImple(config, this);
		this.datasets = new DatasetsImple(config, dbDAO);
		this.permissions = new DatasetPermissionsImple(dbDAO, this);
		this.rows = new InputRowsImple(this, dbDAO);
		this.searchDAO = new SearchDAOImple(this, taxonomyDAO, gbifTaxonDAO, config, errorReporter);
	}

	@Override
	public TraitDAOEntity<Trait> traits() {
		return traits;
	}

	public TraitDAOEntity<TraitEnumerationValue> traitEnumerations() {
		return traitEnumerations;
	}

	@Override
	public TraitDAOEntity<TraitGroup> groups() {
		return groups;
	}

	@Override
	public TraitDAOEntity<UnitOfMeasurement> units() {
		return units;
	}

	@Override
	public TraitDAOEntity<Dataset> datasets() {
		return datasets;
	}

	@Override
	public TraitDAOEntity<InputRow> rows() {
		return rows;
	}

	@Override
	public TraitDAOEntity<DatasetPermissions> permissions() {
		return permissions;
	}

	@Override
	public Enumeration getEnumeration(Qname id) {
		return enumerations.get(id);
	}

	@Override
	public void close() {
		try { if (dbDAO !=  null) dbDAO.close(); } catch (Exception e) {}
		try { if (searchDAO != null) searchDAO.close(); } catch (Exception e) {}
	}

	@Override
	public SearchDAO search() {
		return searchDAO;
	}

}
