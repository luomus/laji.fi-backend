package fi.luomus.traitdb.models;

import java.util.ArrayList;
import java.util.List;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class DatasetPermissions extends AbstractBaseEntity {

	@FieldDef(order=1, required=true)
	private Qname datasetId;

	@FieldDef(order=2, length=100)
	private List<String> userIds;

	public DatasetPermissions() {
		super();
	}

	public DatasetPermissions(JSONObject json) throws ParseException {
		super(json);
	}

	public Qname getDatasetId() {
		return datasetId;
	}

	public List<String> getUserIds() {
		if (userIds == null) userIds = new ArrayList<>();
		return userIds;
	}

	public void setDatasetId(Qname datasetId) {
		this.datasetId = datasetId;
	}

	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datasetId == null) ? 0 : datasetId.hashCode());
		result = prime * result + ((userIds == null) ? 0 : userIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatasetPermissions other = (DatasetPermissions) obj;
		if (datasetId == null) {
			if (other.datasetId != null)
				return false;
		} else if (!datasetId.equals(other.datasetId))
			return false;
		if (userIds == null) {
			if (other.userIds != null)
				return false;
		} else if (!userIds.equals(other.userIds))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DatasetPermissions [datasetId=" + datasetId + ", userIds=" + userIds + "]";
	}

}
