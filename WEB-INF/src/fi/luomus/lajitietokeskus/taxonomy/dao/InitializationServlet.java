package fi.luomus.lajitietokeskus.taxonomy.dao;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.lajitietokeskus.services.BaseServlet;

@WebServlet(name="ThreadStarterServlet", loadOnStartup=1, urlPatterns={"/init"})
public class InitializationServlet extends BaseServlet {

	private static final long serialVersionUID = -2720017552039635737L;

	@Override
	protected void applicationInit() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println(" === Laji BACKEND STARTING UP  === ");
				try {
					getTaxonomyDAO().getTaxon(new Qname("MX.1"));
				} catch (Exception e) {
					getErrorReporter().report("init servlet", e);
				}
			}

		});
		t.setName("Initialization Thread");
		t.start();
	}

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return status404(res);
	}

}
