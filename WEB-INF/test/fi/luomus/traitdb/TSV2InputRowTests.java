package fi.luomus.traitdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;
import fi.luomus.lajitietokeskus.TestUtil;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Subject;
import fi.luomus.traitdb.models.TSV2InputRowConverter;
import fi.luomus.traitdb.models.TraitValue;

public class TSV2InputRowTests {

	private TSV2InputRowConverter converter;
	private TraitDAO dao = TraitInterpreterTests.createFakeTraitDAO();

	@Before
	public void init() {
		converter = new TSV2InputRowConverter(new Qname("HR.123"), dao);
	}

	@Test
	public void methodMapping() {
		assertEquals("" +
				"[ageyears, age, author, basisofrecord, recordbasis, coordinateaccuracy, accuracy, coordacc, country, datasetid, datebegin, date, dateend, elevation, finbifoccurrenceid, finbifid, functionalgroupname, group, functionalgroup, gbifoccurrenceid, gbifid, gbiftaxonid, habitat, highergeography, individualcount, count, kingdom, lat, latitude, n, y, decimallatitude, latmax, latmin, lifestage, locality, verbatimlocality, locationidentifiers, locid, lon, longitude, e, x, decimallongitude, lonmax, lonmin, measurementdeterminedby, measuredby, measurementdetermineddate, measureddate, municipality, county, occurrenceremarks, notes, remarks, otheroccurrenceid, occid, othertaxonid, scientificname, verbatimscientificname, seasonbegin, seasonend, sex, taxonid, finbiftaxonid, type, yearbegin, year, yearend]",
				TSV2InputRowConverter.fields(Subject.class).keySet().toString());

		assertEquals("" +
				"[measurementaccuracy, measurementremarks, objecttaxonlifestage, reference, statisticalmethod, unit, warnings]",
				TSV2InputRowConverter.fields(TraitValue.class).keySet().toString());
	}

	@Test
	public void validateHeaders() {
		assertEquals("{}", converter.validateHeader(Utils.toTSV("")).getErrors().toString());
		assertEquals("{0=Unknown header value: foobar, 1=Unknown header value: barfoo}", converter.validateHeader(Utils.toTSV("foobar", "barfoo")).getErrors().toString());
		assertEquals("{}", converter.validateHeader(Utils.toTSV("scientificName", "latitude", "measurement_remarks")).getErrors().toString());
		assertEquals("{}", converter.validateHeader(Utils.toTSV("some length[mm]", "small_length[m]")).getErrors().toString());
		assertEquals("{0=Unit of measurement is required for this trait}", converter.validateHeader(Utils.toTSV(" small_length        ")).getErrors().toString());
		assertEquals("{0=Unknown header value: small_length[}", converter.validateHeader(Utils.toTSV("small_length[")).getErrors().toString());
		assertEquals("{0=Unit of measurement is required for this trait}", converter.validateHeader(Utils.toTSV("small_length[]")).getErrors().toString());
		assertEquals("{}", converter.validateHeader(Utils.toTSV("best_friend[]")).getErrors().toString());
		assertEquals("{}", converter.validateHeader(Utils.toTSV("small_length[mm, 1, min]")).getErrors().toString());
		assertEquals("{0=Unit of measurement is required for this trait}", converter.validateHeader(Utils.toTSV("small_length[, 1, min]")).getErrors().toString());
		assertEquals("{0=Unit of measurement is required for this trait}", converter.validateHeader(Utils.toTSV("small_length[, , min  ]")).getErrors().toString());
		assertEquals("{0=Unit of measurement is required for this trait}", converter.validateHeader(Utils.toTSV("small_length[, , , ]")).getErrors().toString());
		assertEquals("{0=Unknown unit of measurement: maximum}", converter.validateHeader(Utils.toTSV("small_length[maximum, , , ]")).getErrors().toString());
		assertEquals("{0=Unit of measurement is required for this trait}", converter.validateHeader(Utils.toTSV("small_length[, , , maximum]")).getErrors().toString());
		assertEquals("{0=Unknown unit of measurement: foobar; Invalid accuracy: a; Unknown statistical method: barfoo}", converter.validateHeader(Utils.toTSV("small_length[foobar, a, barfoo]")).getErrors().toString());
		assertEquals("{1=basisOfRecord maps to field that is already present in the headers: basisOfRecord, 2=BASISOFRECORD maps to field that is already present in the headers: basisOfRecord, 3=BASIS OF RECORD maps to field that is already present in the headers: basisOfRecord}",
				converter.validateHeader(Utils.toTSV("basis_of_record", "basisOfRecord", "BASISOFRECORD", "BASIS OF RECORD")).getErrors().toString());
	}

	@Test
	public void parseAndValidateRows() throws Exception {
		List<String> data = new ArrayList<>();

		try {
			converter.validateRows(data);
		} catch (IllegalStateException e) {
			assertEquals("Must have header", e.getMessage());
		}

		try {
			converter.convert(data);
			fail();
		} catch (IllegalStateException e) {
			assertEquals("Must have header", e.getMessage());
		}

		data.add(Utils.toTSV("type", "scientific_name", "#ignored", "x", "y", "small_length[mm, 1, min]", "measurementRemarks", "warnings", "some length[m]", "date", "best_friend", "ObjectTaxonLifeStage"));

		assertEquals("{}", converter.validateHeader(data.get(0)).toString());
		assertEquals("[]", converter.validateRows(data).toString());
		assertEquals("[]", converter.convert(data).toString());

		data.add(Utils.toTSV("PopulationGroup", "Aves", "ignored", "26.858243", "63.671112", "28,5", "notes about small length", "true", "1.1", "15.2.2004")); // coord is wg84
		data.add(Utils.toTSV("Taxon", "Parus major", "ignored", "3493153", "7063331", "", "", "", "", "", "Canis lupus", "ADULT")); // coord is ykj
		data.add(Utils.toTSV("Taxon", "Parus major", "ignored", "492984", "7060373", "A", "", "", "", "", "Canis lupus", "FOOBAR")); // coord is euref

		assertEquals("[{}, {}, {11=Unknown value: FOOBAR. Should be one of: A-adult}]", converter.validateRows(data).toString());

		data.set(3, Utils.toTSV("Taxon", "Parus major", "ignored", "492984", "7060373", "A", "", "", "", "", "Canis lupus", "ADULT"));

		assertEquals("[{}, {}, {}]", converter.validateRows(data).toString());

		assertEquals(getContents("tsv_generated_rows_expected.json"), actual(converter.convert(data)));
	}

	@Test
	public void validate_taxon_trait_header() {
		List<String> data = new ArrayList<>();
		data.add(Utils.toTSV("type", "scientific_name", "small_length[mm]", "ObjectTaxonLifeStage", "best_friend", "ObjectTaxonLifeStage"));
		data.add(Utils.toTSV("Taxon", "Aves", "28,5", "ADULT", "Canis lupus", "ADULT"));

		assertEquals("{3=Only available for taxon traits}", converter.validateHeader(data.get(0)).toString());
		assertEquals("[{}]", converter.validateRows(data).toString());
	}

	private String actual(List<InputRow> rows) {
		JSONArray array = new JSONArray();
		rows.stream().map(r->r.toJSON()).forEach(array::appendObject);
		return new JSONObject().setArray("beautified", array).beautify();
	}

	private String getContents(String filename) throws Exception {
		return TestUtil.getContents(TSV2InputRowTests.class, filename);
	}
}
