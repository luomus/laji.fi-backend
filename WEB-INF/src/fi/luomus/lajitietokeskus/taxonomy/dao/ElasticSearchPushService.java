package fi.luomus.lajitietokeskus.taxonomy.dao;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonomyDAO;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAO;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAOImple;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAO.PushError;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAO.WaitTimeoutException;
import fi.luomus.lajitietokeskus.dao.ElasticSearchDAOImple.IndexNames;

public class ElasticSearchPushService {

	private static final int LAST_LOAD_ERROR_28_HOURS = 1000 * 60 * 60 * 28;

	public static IndexNames generateIndexNames(String prefix, Qname checklistVersion) {
		String checklistPart = "_" + checklistVersion.toString().toLowerCase().replace(".", "_");
		return new IndexNames(
				IndexNames.INDEX_TYPE_TAXA,
				prefix + checklistPart + "_1", // index 1
				prefix + checklistPart + "_2", // index 2
				prefix + checklistPart); // alias
	}

	public static final Qname CURRENT_CHECKLIST_VERSION = new Qname("current");

	private final Config config;
	private final ErrorReporter errorReporter;
	private final TaxonomyDAO taxonomyDAO;
	private final Map<Qname, String> errors = new HashMap<>();
	private final TaxonToMap taxonMapper;
	private static final Object LOCK = new Object();
	private boolean running = false;
	private long lastCompleted = System.currentTimeMillis();

	public ElasticSearchPushService(Config config, ErrorReporter errorReporter, TaxonomyDAO taxonomyDAO, TaxonToMap taxonMapper) {
		this.config = config;
		this.errorReporter = errorReporter;
		this.taxonomyDAO = taxonomyDAO;
		this.taxonMapper = taxonMapper;
	}

	public static class AlreadyRunningException extends Exception {
		private static final long serialVersionUID = 1381215804740007898L;
	}

	public boolean isRunning() {
		return running;
	}

	public void pushAll() throws AlreadyRunningException {
		if (running) throw new AlreadyRunningException();
		synchronized (LOCK) {
			if (running) throw new AlreadyRunningException();
			running = true;
			errors.clear();
			try {
				System.out.println("-- ES push started --");
				pushAllChecklistVersions();
				if (!errors.isEmpty()) {
					String summary = getErrorSummary();
					System.err.println("Push caused following errors:\n" + summary);
					errorReporter.report(summary);
				}
				System.out.println("-- ES push end --");
			} finally {
				lastCompleted = System.currentTimeMillis();
				running = false;
			}
		}
	}

	private void pushAllChecklistVersions() {
		try {
			pushCurrent();
			pushRedListEvaluationData(2015, new Qname("MR.425"));
			pushRedListEvaluationData(2019, new Qname("MR.424"));
			pushRedListEvaluationData(2020, new Qname("MR.484"));
		} catch (Throwable e) {
			// should not throw any - all exceptions are catched and reported
			e.printStackTrace();
		}
	}

	private void pushRedListEvaluationData(int year, Qname checklistVersion) {
		Collection<Taxon> taxa;
		try {
			taxa = new EvaluationReader(taxonomyDAO.getTaxonContainer(), new File(config.dataFolder())).readEvaluationData(year, checklistVersion);
		} catch (Throwable e) {
			errors.put(checklistVersion, "Generating evaluation data for year " + year + " failed:\n" + LogUtils.buildStackTrace(e));
			errorReporter.report(e);
			return;
		}

		push(checklistVersion, taxa);
	}

	private void emptyTemp(Qname checklistVersion, ElasticSearchDAO dao) {
		try {
			dao.emptyTemp();
		} catch (Throwable emptyTempException) {
			errors.put(checklistVersion, "Empty temp:\n" + LogUtils.buildStackTrace(emptyTempException));
			errorReporter.report(emptyTempException);
		}
	}

	private void switchTempToActive(Qname checklistVersion, ElasticSearchDAO dao) {
		try {
			dao.switchTempToActiveIndex();
		} catch (Throwable indexSwitchException) {
			errors.put(checklistVersion, "Switching temp to active:\n" + LogUtils.buildStackTrace(indexSwitchException));
			errorReporter.report(indexSwitchException);
		}
	}

	private void pushCurrent() {
		Collection<Taxon> currentTaxa = null;
		try {
			currentTaxa = taxonomyDAO.getTaxonContainer().getAll();
		} catch (Throwable taxaLoadException) {
			errors.put(CURRENT_CHECKLIST_VERSION, "Loading taxa failed:\n" + LogUtils.buildStackTrace(taxaLoadException));
			errorReporter.report(taxaLoadException);
			return;
		}
		push(CURRENT_CHECKLIST_VERSION, currentTaxa);
	}

	public boolean hasErrors() {
		if (isRunning()) {
			return false;
		}
		long diff = System.currentTimeMillis() - lastCompleted;
		if (diff > LAST_LOAD_ERROR_28_HOURS) {
			errors.put(CURRENT_CHECKLIST_VERSION, "Last refreshed " + (diff/1000/60/60) + " hours ago!" );
		}
		return !errors.isEmpty();
	}

	public String getErrorSummary() {
		if (!hasErrors()) return "ok";
		StringBuilder b = new StringBuilder();
		for (String error : errors.values()) {
			b.append(error).append("\n\n");
		}
		return b.toString();
	}

	private void push(Qname checklistVersion, Collection<Taxon> taxa) {
		System.out.println("Pushing " + checklistVersion + " (" + taxa.size() + " taxa)");
		ElasticSearchDAO dao = null;
		try {
			long start = System.currentTimeMillis();
			dao = openDao(checklistVersion);
			emptyTemp(checklistVersion, dao);
			push(taxa, dao);
			try {
				boolean hasErrors = handleErrors(dao.getPushErrors(), checklistVersion);
				if (!hasErrors) {
					System.out.println("No errors for " + checklistVersion + ". Switching temp to active...");
					switchTempToActive(checklistVersion, dao);
				} else {
					System.out.println("There were errors for " + checklistVersion + ". Will not switching temp to active!");
				}
			} catch (WaitTimeoutException e) {
				errors.put(checklistVersion, "Timeout when pushing " + checklistVersion + ":\n" + e.getMessage());
				errorReporter.report(e);
			}
			long took = System.currentTimeMillis() - start;
			System.out.println("Pushing " + checklistVersion + " took " + (took/1000L) + " seconds");
		} catch (Throwable unknownException) {
			errors.put(checklistVersion, "Unknown error occurred when loading " + checklistVersion + ":\n" + LogUtils.buildStackTrace(unknownException));
			errorReporter.report(unknownException);
		} finally {
			if (dao != null) dao.close();
		}
	}

	private void push(Collection<Taxon> taxa, ElasticSearchDAO dao) {
		int i = 1;
		for (Taxon taxon : taxa) {
			if (i % 10000 == 0) System.out.println(" ... " + i + "/" + taxa.size());
			dao.push(taxon);
			i++;
		}
	}

	public ElasticSearchDAO openDao(Qname checklistVersion) {
		return new ElasticSearchDAOImple(
				config.get("ES_address"),
				config.get("ES_cluster"),
				generateIndexNames("taxon", checklistVersion),
				errorReporter,
				taxonMapper,
				true
				);
	}

	private boolean handleErrors(List<PushError> pushErrors, Qname checklistVersion) {
		if (!pushErrors.isEmpty()) {
			errors.put(checklistVersion, generateErrorSummary(checklistVersion, pushErrors));
			return true;
		}
		return false;
	}

	private String generateErrorSummary(Qname checklistVersion, List<PushError> pushErrors) {
		StringBuilder b = new StringBuilder();
		b.append(checklistVersion.toString()).append( " loading encountered errors for ").append(pushErrors.size()).append(" taxa:\n");
		generateErrorListingSummary(b, pushErrors);
		return b.toString();
	}

	private void generateErrorListingSummary(StringBuilder b, List<PushError> pushErrors) {
		int c = 0;
		for (PushError error : pushErrors) {
			b.append(error.getId()).append(": ").append(error.getErrorMessage()).append("\n").append("data: ").append(error.getData());
			if (c++ > 10) {
				b.append(" ...\n");
				break;
			}
		}
	}

}
