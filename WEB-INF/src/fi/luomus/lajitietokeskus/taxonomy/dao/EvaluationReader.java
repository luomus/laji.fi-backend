package fi.luomus.lajitietokeskus.taxonomy.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple.InfiniteTaxonLoopException;
import fi.luomus.commons.taxonomy.Occurrences.Occurrence;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.commons.taxonomy.iucn.EndangermentObject;
import fi.luomus.commons.taxonomy.iucn.Evaluation;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

public class EvaluationReader {

	public static final Qname BIOTA_QNAME = new Qname("MX.37600");
	public static final Qname FINBIF_MASTER_CHECKLIST = new Qname("MR.1");

	private final InMemoryTaxonContainerImple container;
	private final File dataFolder;

	public EvaluationReader(TaxonContainer container, File dataFolder) {
		this.container = (InMemoryTaxonContainerImple) container;
		this.dataFolder = dataFolder;
	}

	public Collection<Taxon> readEvaluationData(int year, Qname checklistVersion) throws Exception {
		Map<Qname, Taxon> taxa = readTaxaFromTriplets(year, checklistVersion);

		addEvaluations(year, checklistVersion, taxa);

		for (Taxon t : taxa.values()) {
			validate(t);
		}

		return taxa.values();
	}

	private void validate(Taxon t) {
		Taxon parent = t;
		while (parent.hasParent()) {
			parent = parent.getParent();
		}
		if (!BIOTA_QNAME.equals(parent.getId())) {
			throw new IllegalStateException("Taxon " + t.getId() + " parent chain ends with " + parent.getId() + " instead of biota");
		}
		if (!t.getRedListEvaluations().isEmpty()) {
			if (t.getRedListEvaluationGroups().isEmpty()) {
				throw new IllegalStateException("No red list evaluation groups for " + t.getId());
			}
		}
	}

	private Map<Qname, Taxon> readTaxaFromTriplets(int year, Qname checklistVersion) throws Exception {
		InMemoryTaxonContainerImple container = this.container.copy();
		container.setLatestLockedRedListEvaluationYear(year);

		String fileName = dataFolder+"/"+checklistVersion.toString().replace(".", "_")+"_taxon_triplets.txt";
		for (String line : FileUtils.readLines(new File(fileName))) {
			String[] parts = line.split(Pattern.quote("\t"));
			Qname taxonId = new Qname(parts[0]);
			Qname predicate = new Qname(parts[1]);
			Qname object = new Qname(parts[2]);
			String resourceliteral = s(parts, 3);
			String locale = s(parts, 4);
			container.handle(taxonId, predicate, object, resourceliteral, locale, null);
		}

		for (Taxon t : container.getAll()) {
			t.setFinnish(true);
			t.setChecklist(FINBIF_MASTER_CHECKLIST);
			addImagesFromMasterTo(t);
		}

		List<InfiniteTaxonLoopException> ex = container.generateTaxonomicOrders();
		if (!ex.isEmpty()) throw ex.get(0);
		return container.getAll().stream().collect(Collectors.toMap(Taxon::getId, t->t));
	}

	private static final List<Qname> EVALUATION_OCCURRENCE_AREAS = Utils.list(
			new Qname("ML.690"), new Qname("ML.691"), new Qname("ML.692"), new Qname("ML.693"),
			new Qname("ML.694"), new Qname("ML.695"), new Qname("ML.696"), new Qname("ML.697"),
			new Qname("ML.698"), new Qname("ML.699"), new Qname("ML.700"));

	private void addEvaluations(int year, Qname checklistVersion, Map<Qname, Taxon> taxa) throws Exception {
		File file = new File(dataFolder, checklistVersion.toString().replace(".", "_")+"_evaluations.txt");
		for (String line : FileUtils.readLines(file)) {
			if (!line.startsWith("MX.")) continue;
			String[] parts = line.split(Pattern.quote("\t"));

			Qname taxonId = new Qname(parts[0]);

			Taxon taxon = taxa.get(taxonId);
			if (taxon == null) {
				throw new IllegalStateException("Missing taxon information for evaluation of " + taxonId);
			}

			int i = 7 + EVALUATION_OCCURRENCE_AREAS.size();
			String primaryHabitat = s(parts, i++);
			String secondaryHabitats = s(parts, i++);
			String endangermentReasons = s(parts, i++);
			String threats = s(parts, i++);
			String status = s(parts, i++);
			String criteria = s(parts, i++);
			String externalImpact = s(parts, i++);
			String reasonForStatusChange = s(parts, i++);
			String possiblyRe = s(parts, i++);
			String lastSightingNotes = s(parts, i++);

			Model model = new Model(new Qname(taxonId.toString()+"_eval"));
			Evaluation evaluation = Evaluation.createNonPredicateCheckingEvaluation(model);
			model.addStatementIfObjectGiven(Evaluation.EVALUATED_TAXON, taxonId);
			model.addStatementIfObjectGiven(Evaluation.EVALUATION_YEAR, String.valueOf(year));
			model.addStatementIfObjectGiven(Evaluation.STATE, Evaluation.STATE_READY);
			model.addStatementIfObjectGiven(Evaluation.RED_LIST_STATUS, new Qname("MX.iucn"+status));
			model.addStatementIfObjectGiven(Evaluation.CRITERIA_FOR_STATUS, criteria);
			if ("-1".equals(externalImpact)) model.addStatementIfObjectGiven(Evaluation.EXTERNAL_IMPACT, new Qname("MKV.externalPopulationImpactOnRedListStatusEnumMinus1"));
			if (given(reasonForStatusChange)) {
				for (String reason : reasonForStatusChange.split(Pattern.quote(","))) {
					reason = reason.trim();
					model.addStatementIfObjectGiven(Evaluation.REASON_FOR_STATUS_CHANGE, REASON_FOR_STATUS_CHANGE.get(reason));
				}
			}
			if (given(possiblyRe)) {
				model.addStatementIfObjectGiven(Evaluation.POSSIBLY_RE, new Qname("MX.iucn"+possiblyRe));
			}
			if (given(lastSightingNotes)) {
				model.addStatementIfObjectGiven(Evaluation.LAST_SIGHTING_NOTES, lastSightingNotes);
			}
			if (given(primaryHabitat)) {
				evaluation.setPrimaryHabitat(habitatObject(primaryHabitat, 0));
			}
			if (given(secondaryHabitats)) {
				int order = 0;
				for (String habitat : secondaryHabitats.split(Pattern.quote(","))) {
					habitat = habitat.trim();
					evaluation.addSecondaryHabitat(habitatObject(habitat, order++));
				}
			}

			endangermentObjects(endangermentReasons).forEach(e -> evaluation.addEndangermentReason(e));
			endangermentObjects(threats).forEach(e -> evaluation.addThreat(e));

			addOccurrences(parts, evaluation);

			taxon.addRedListEvaluation(evaluation);
		}
	}

	private void addOccurrences(String[] parts, Evaluation evaluation) {
		int i = 7;
		for (Qname area : EVALUATION_OCCURRENCE_AREAS) {
			String areaData = s(parts, i++);
			addOccurrence(areaData, area, evaluation);
		}
	}

	private void addOccurrence(String areaData, Qname area, Evaluation evaluation) {
		if (!given(areaData)) return;
		Qname status = occurrenceStatus(areaData);
		Boolean threatened = threatened(areaData);
		if (status != null) {
			Occurrence o = new Occurrence(null, area, status);
			o.setThreatened(threatened);
			evaluation.addOccurrence(o);
		}
	}

	private static final Map<String, Qname> OCCURRENCE_STATUS;
	static {
		OCCURRENCE_STATUS = new HashMap<>();
		OCCURRENCE_STATUS.put("x", new Qname("MX.typeOfOccurrenceOccurs"));
		OCCURRENCE_STATUS.put("x (RT)", new Qname("MX.typeOfOccurrenceOccurs"));
		OCCURRENCE_STATUS.put("RE", new Qname("MX.typeOfOccurrenceExtirpated"));
		OCCURRENCE_STATUS.put("NA", new Qname("MX.typeOfOccurrenceAnthropogenic"));
		OCCURRENCE_STATUS.put("p", new Qname("MX.typeOfOccurrenceUncertain"));
		OCCURRENCE_STATUS.put("--", new Qname("MX.doesNotOccur"));
	}

	private Qname occurrenceStatus(String s) {
		return OCCURRENCE_STATUS.get(s);
	}

	private Boolean threatened(String areaData) {
		if (areaData.endsWith("(RT)")) return true;
		return null;
	}

	private void addImagesFromMasterTo(Taxon t) throws Exception {
		if (container.hasTaxon(t.getId())) {
			Taxon masterChecklistTaxon = container.getTaxon(t.getId());
			for (Image i : masterChecklistTaxon.getMultimedia()) {
				t.addMultimedia(i);
			}
		}
	}

	private List<EndangermentObject> endangermentObjects(String string) {
		if (!given(string)) return Collections.emptyList();
		string = clean(string);
		List<EndangermentObject> objects = new ArrayList<>();
		int i = 0;
		for (String s : string.split(Pattern.quote(","))) {
			objects.add(endangermentObject(s.trim(), i++));
		}
		return objects;
	}

	private EndangermentObject endangermentObject(String s, int order) {
		if (s.equals("?")) s = "T";
		Qname endangerment = new Qname("MKV.endangermentReason"+s);
		return new EndangermentObject(new Qname(""), endangerment, order);
	}

	private HabitatObject habitatObject(String habitatString, int order) {
		// Mlk ca v va
		habitatString = clean(habitatString);
		Iterator<String> i = Utils.list(habitatString.split(Pattern.quote(" "))).iterator();

		String habitatValue = i.next().trim();
		if ("?".equals(habitatValue)) habitatValue = "U";
		Qname habitat = new Qname("MKV.habitat"+habitatValue);
		HabitatObject habitatObject = new HabitatObject(new Qname(""), habitat, order);
		while (i.hasNext()) {
			String s = i.next();
			if (s.equals("pa")) s = "pak";
			if (s.equals("va")) s = "vak";
			habitatObject.addHabitatSpecificType(new Qname("MKV.habitatSpecificType"+s.toUpperCase()));
		}
		return habitatObject;
	}

	private String clean(String string) {
		while (string.contains("  ")) {
			string = string.replace("  ", " ");
		}
		return string.trim();
	}

	private static final Map<String, Qname> REASON_FOR_STATUS_CHANGE;
	static {
		REASON_FOR_STATUS_CHANGE = new HashMap<>();
		REASON_FOR_STATUS_CHANGE.put("1", new Qname("MKV.reasonForStatusChangeGenuine"));
		REASON_FOR_STATUS_CHANGE.put("2", new Qname("MKV.reasonForStatusChangeGenuineBeforePreviousEvaluation"));
		REASON_FOR_STATUS_CHANGE.put("3", new Qname("MKV.reasonForStatusChangeChangesInCriteria"));
		REASON_FOR_STATUS_CHANGE.put("4", new Qname("MKV.reasonForStatusChangeMoreInformation"));
		REASON_FOR_STATUS_CHANGE.put("5", new Qname("MKV.reasonForStatusChangeChangesInTaxonomy"));
		REASON_FOR_STATUS_CHANGE.put("6", new Qname("MKV.reasonForStatusChangeError"));
		REASON_FOR_STATUS_CHANGE.put("7", new Qname("MKV.reasonForStatusChangeErroneousInformation"));
		REASON_FOR_STATUS_CHANGE.put("8", new Qname("MKV.reasonForStatusChangeOther"));
	}

	private String s(String[] parts, int index) {
		if (index >= parts.length) return null;
		String s = parts[index];
		if (s == null) return null;
		return s.trim();
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

}

