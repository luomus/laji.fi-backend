package fi.luomus.lajitietokeskus.taxonomy.dao;

import java.util.LinkedHashMap;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;

public interface GBIFTaxonDAO {

	public static class GBIFTaxon {
		private final Integer id;
		private Qname taxonRank;
		private String scientificName;
		private String author;
		private Map<Qname, String> higherTaxa = new LinkedHashMap<>();

		public GBIFTaxon(Integer id) {
			if (id == null || id < 0) throw new IllegalArgumentException("Invalid id " + id);
			this.id = id;
		}

		public Integer getId() {
			return id;
		}

		public Qname getTaxonRank() {
			return taxonRank;
		}

		public String getScientificName() {
			return scientificName;
		}

		public String getAuthor() {
			return author;
		}

		public Map<Qname, String> getHigherTaxa() {
			return higherTaxa;
		}

		public void setTaxonRank(Qname taxonRank) {
			this.taxonRank = taxonRank;
		}

		public void setScientificName(String scientificName) {
			this.scientificName = scientificName;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public void setHigherTaxa(Map<Qname, String> higherTaxa) {
			this.higherTaxa = higherTaxa;
		}

		@Override
		public String toString() {
			return "GBIFTaxon [id=" + id + ", taxonRank=" + taxonRank + ", scientificName=" + scientificName + ", author=" + author + ", higherTaxa=" + higherTaxa + "]";
		}
	}

	GBIFTaxon get(Integer id);

	GBIFTaxon search(String scientificName, String kingdom);
}
