package fi.luomus.traitdb.models;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ClassDef {

	String triplestorePrefix();

}
