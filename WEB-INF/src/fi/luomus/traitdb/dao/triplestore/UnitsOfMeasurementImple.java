package fi.luomus.traitdb.dao.triplestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.Param;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.UnitOfMeasurement;

public class UnitsOfMeasurementImple extends TriplestoreBaseDAO<UnitOfMeasurement> {

	public UnitsOfMeasurementImple(Config config) {
		super(config);
	}

	@Override
	public ValidationResponse validate(UnitOfMeasurement entity, boolean insert) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(Qname id) throws EntityNotFoundException {
		throw new UnsupportedOperationException();
	}

	@Override
	protected Class<UnitOfMeasurement> concreteClass() {
		return UnitOfMeasurement.class;
	}

	@Override
	protected Model createModel(Qname id, UnitOfMeasurement entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected List<UnitOfMeasurement> getAllActual() {
		Document doc = query("TDF.unitOfMeasurementEnum");
		Map<Qname, Integer> order = new HashMap<>();
		for (Node n : doc.getRootNode().getChildNodes().iterator().next().getChildNodes()) {
			if (n.getName().startsWith("rdf:_")) {
				Integer o = Integer.valueOf(n.getName().replace("rdf:_", ""));
				Qname unit = Qname.fromURI(n.getAttribute("rdf:resource"));
				order.put(unit, o);
			}
		}
		doc = query("search", new Param("type", "TDF.unitOfMeasurement"));
		List<UnitOfMeasurement> units = new ArrayList<>();
		for (Node n : doc.getRootNode().getChildNodes()) {
			UnitOfMeasurement unit = new UnitOfMeasurement();
			unit.setId(getId(n));
			unit.setUnit(getLiteral("rdfs:label", n));
			unit.setBase(getResource("TDF.baseUnit", n));
			unit.setIsBaseUnit(unit.getBase().equals(unit.getId()));
			if (unit.getIsBaseUnit()) {
				unit.setConversionFactor(1.0);
			} else {
				unit.setConversionFactor(Double.valueOf(getLiteral("TDF.baseConversion", n)));
			}
			if (!order.containsKey(unit.getId())) throw new IllegalStateException("Unit of Measurement " + unit.getId() + " has not been added to TDF.unitOfMeasurementEnum");
			unit.setOrder(order.get(unit.getId()));
			units.add(unit);
		}
		return units;
	}

	@Override
	public ValidationResponse validateDelete(Qname id) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

}
