package fi.luomus.lajitietokeskus.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.services.ResponseData;
import fi.luomus.lajitietokeskus.dao.JSONSerializer;
import fi.luomus.lajitietokeskus.models.News;
import fi.luomus.lajitietokeskus.models.NewsItem;

@WebServlet(urlPatterns = {"/api/news/*"})
public class ApiNewsServlet extends BaseServlet {

	private static final long serialVersionUID = 7879430447761704085L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		News news = getDAO().getNews();

		if (req.getRequestURI().contains("/tags")) {
			return tags(news);
		}

		String id = getLastPart(req);
		if (given(id)) {
			return getById(res, news, id);
		}

		return list(req, news);
	}

	private List<String> getTags(HttpServletRequest req) {
		if (req.getParameter("tag") == null) return Collections.emptyList();
		List<String> tags = new ArrayList<>();
		for (String param : req.getParameterValues("tag")) {
			param = param.trim();
			if (!given(param)) continue;
			for (String paramPart : param.split(Pattern.quote(","))) {
				paramPart = paramPart.trim();
				if (given(paramPart)) {
					tags.add(paramPart);
				}
			}
		}
		return tags;
	}

	private ResponseData list(HttpServletRequest req, News news) {
		List<String> tags = getTags(req);
		List<NewsItem> newsItems = news.get(getLocale(req), tags);

		int page = getPage(req);
		int pageSize = getPageSize(req);
		int totalResults = newsItems.size();
		int lastPage = (int) Math.ceil( (double) totalResults / pageSize);
		if (lastPage == 0) lastPage = 1;
		if (page > lastPage) {
			page = lastPage;
		}
		int offset = (page - 1) * pageSize;
		int lastIndex = pageSize + offset;
		if (lastIndex > totalResults) {
			lastIndex = totalResults;
		}
		newsItems = newsItems.subList(offset, lastIndex);

		JSONObject response = new JSONObject();
		response.setInteger("currentPage", page);
		if (page > 1) {
			response.setInteger("prevPage", page - 1);
		}
		if (page != lastPage) {
			response.setInteger("nextPage", page + 1);
		}
		response.setInteger("lastPage", lastPage);
		response.setInteger("pageSize", pageSize);
		response.setInteger("total", totalResults);

		for (NewsItem newsItem : newsItems) {
			response.getArray("results").appendObject(JSONSerializer.serialize(newsItem));
		}

		return jsonResponse(response);
	}

	private ResponseData getById(HttpServletResponse res, News news, String id) {
		NewsItem newsItem = news.getById(id);
		if (newsItem != null) {
			return jsonResponse(JSONSerializer.serialize(newsItem));
		}
		return status404(res);
	}

	private ResponseData tags(News news) {
		Map<String, String> textsFi = getLocalizedTexts().getAllTexts("fi");
		Map<String, String> textsSv = getLocalizedTexts().getAllTexts("sv");
		Map<String, String> textsEn = getLocalizedTexts().getAllTexts("en");

		JSONObject response = new JSONObject();

		for (String tag : news.getTags()) {
			String fi = getText(textsFi, tag);
			String sv = getText(textsSv, tag);
			String en = getText(textsEn, tag);
			response.getArray("results").appendObject(
					new JSONObject().setString("tag", tag)
					.setObject("name", new JSONObject()
							.setString("fi", fi)
							.setString("sv", sv)
							.setString("en", en)
							));
		}
		return jsonResponse(response);
	}

	private String getText(Map<String, String> texts, String tag) {
		String text = texts.get(tag);
		if (given(text)) return text;
		return tag;
	}

	private int getPageSize(HttpServletRequest req) {
		String param = req.getParameter("pageSize");
		if (!given(param)) return 10;
		try {
			int i = Integer.valueOf(param);
			if (i < 1) return 10;
			return i;
		} catch (Exception e) {
			return 10;
		}
	}

	private int getPage(HttpServletRequest req) {
		String param = req.getParameter("page");
		if (!given(param)) return 1;
		try {
			int i = Integer.valueOf(param);
			if (i < 1) return 1;
			return i;
		} catch (Exception e) {
			return 1;
		}
	}

}
