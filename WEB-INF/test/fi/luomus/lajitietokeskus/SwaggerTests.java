package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonomyDAO_Imple;
import fi.luomus.lajitietokeskus.util.SwaggerV3APIDescriptionGenerator;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAOImple;
import fi.luomus.traitdb.models.TraitSchemaMappings;

public class SwaggerTests {

	@Test
	public void swagger() throws Exception {
		ErrorReporter errorReporter = new ErrorReportingToSystemErr();
		try (TaxonomyDAO_Imple taxonomyDAO = new TaxonomyDAO_Imple(TestConfig.getConfig(), errorReporter);
				TraitDAO traitDAO = new TraitDAOImple(TestConfig.getConfig(), null, null, errorReporter)) {
			Map<Class<?>, Map<String, Object>> taxaEndpointMappings = taxonomyDAO.getTaxonPropertyMapping();
			Map<Class<?>, Map<String, Object>> traitEndpointMappings = new TraitSchemaMappings(traitDAO).getMapping();
			assertEquals(getContents("expected-taxa-mappings.txt"), prettyPrintMap(taxaEndpointMappings));
			assertEquals(getContents("expected-trait-mappings.txt"), prettyPrintMap(traitEndpointMappings));
			assertEquals(getContents("expected-swagger.json"), SwaggerV3APIDescriptionGenerator.generateSwaggerDescription(taxaEndpointMappings, traitEndpointMappings).beautify());
		}
	}

	private String getContents(String filename) throws Exception {
		return TestUtil.getContents(SwaggerTests.class, filename);
	}

	public static String prettyPrintMap(Map<?, ?> map) {
		StringBuilder result = new StringBuilder();
		prettyPrintMap(map, result, 0);
		return result.toString().trim();
	}

	private static void prettyPrintMap(Map<?, ?> map, StringBuilder result, int indent) {
		StringBuilder indentation = new StringBuilder();
		for (int i = 0; i < indent * 4; i++) {
			indentation.append(" ");
		}
		result.append(indentation).append("{\n");
		for (Map.Entry<?, ?> entry : map.entrySet()) {
			result.append(indentation).append("    ").append(entry.getKey()).append(": ");
			Object value = entry.getValue();
			if (value instanceof Map) {
				result.append("\n");
				prettyPrintMap((Map<?, ?>) value, result, indent + 1);
			} else {
				result.append(value).append(",\n");
			}
		}
		result.append(indentation).append("}");
		result.append("\n");
	}

}
