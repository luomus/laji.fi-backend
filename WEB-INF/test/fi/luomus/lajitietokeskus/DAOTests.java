package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.lajitietokeskus.dao.DAOImple;
import fi.luomus.lajitietokeskus.models.CMSContent;
import fi.luomus.lajitietokeskus.models.News;
import fi.luomus.lajitietokeskus.taxonomy.dao.TaxonomyDAO_Imple;

public class DAOTests {

	private static DAOImple dao;
	private static TaxonomyDAO_Imple taxonomyDao;

	@BeforeClass
	public static void init() {
		dao = new DAOImple(TestConfig.getConfig(), new ErrorReportingToSystemErr());
		taxonomyDao = new TaxonomyDAO_Imple(TestConfig.getConfig(), new ErrorReportingToSystemErr());
	}

	@AfterClass
	public static void close() {
		if (taxonomyDao != null) taxonomyDao.close();
	}

	@Test
	public void loadTaxa() {
		Taxon t = taxonomyDao.getTaxon(new Qname("MX.37600"));
		assertEquals("Biota", t.getScientificName());
	}

	@Test
	public void loadTaxa_force_triplet_refresh() {
		taxonomyDao.reloadTriplets();
		taxonomyDao.clearTaxonContainer();
		Taxon t = taxonomyDao.getTaxon(new Qname("MX.37600"));
		assertEquals("Biota", t.getScientificName());
	}

	@Test
	@Ignore // note: this test only works inside Uni network (VPN is not enough)
	public void cms() throws Exception {
		CMSContent content = dao.getCMSContent();
		assertEquals("Suomen Lajitietokeskus", content.getById("619").getTitle());
		assertEquals("Yhteystiedot", content.getById("i-102").getTitle());
		assertEquals("Luokitus", content.getById("r-63").getTitle());
		assertTrue(content.getById("r-63").getContent().contains("IUCN:n uhanalaisuusluokat"));
	}

	@Test
	@Ignore // note: this test only works inside Uni network (VPN is not enough)
	public void news() throws Exception {
		News news = dao.getNews();
		assertEquals("Lajitietokeskuksen kehitystiedote", news.getById("2819").getTitle());
		assertEquals("Rahoitusta haitallisten vieraslajien torjuntaan", news.getById("i-1200").getTitle());
		assertTrue(news.getById("i-1200").getContent().contains("Kainuun ELY-keskuksesta"));
	}

}
