package fi.luomus.traitdb.dao.triplestore;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.http.Param;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.ReflectionUtil;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOException;
import fi.luomus.traitdb.dao.TraitDAO.ValidationResponse;
import fi.luomus.traitdb.models.ClassDef;

public abstract class TriplestoreBaseDAO<T extends Comparable<T>> implements TraitDAO.TraitDAOEntity<T> {

	protected final Config config;

	public TriplestoreBaseDAO(Config config) {
		this.config = config;
	}

	protected abstract Class<T> concreteClass();

	protected abstract Model createModel(Qname id, T entity);

	protected abstract List<T> getAllActual();

	protected HttpClientService getTriplestoreClient() {
		try {
			return new HttpClientService(triplestoreURL(), config.get("TraitTriplestoreUsername"), config.get("TraitTriplestorePassword"));
		} catch (Exception e) {
			throw new RuntimeException("Triplestore not properly configured", e);
		}
	}

	protected String triplestoreURL() {
		return config.get("TraitTriplestoreURL");
	}

	@Override
	public T get(Qname id) throws EntityNotFoundException {
		T entity = getAllMapCached().get(id);
		if (entity != null) return entity;
		throw new EntityNotFoundException(id);
	}

	@Override
	public void delete(Qname id) throws EntityNotFoundException {
		boolean notFound = false;
		CACHE = null;
		try (HttpClientService client = getTriplestoreClient()) {
			HttpDelete delete = new HttpDelete(triplestoreURL()+"/"+id.toString());
			CloseableHttpResponse response = null;
			try {
				response = client.execute(delete);
				int status = response.getStatusLine().getStatusCode();
				if (status == 404) notFound = true;
				else if (status != 200) throw new IOException("Triplestore returned status " + status + " with message: " + EntityUtils.toString(response.getEntity()));
			} finally {
				if (response != null) close(response);
			}
		} catch (Exception e) {
			throw exception("delete", id, e);
		}
		if (notFound) throw new EntityNotFoundException(id);
	}

	@Override
	public synchronized T insert(T entity) {
		CACHE = null;
		try (HttpClientService client = getTriplestoreClient()) {
			Qname id = newId(client);
			Model model = createModel(id, entity);
			executeUpdate(client, id, model);
			setId(entity, id);
			postInsertHook(entity);
			return get(id);
		} catch (Exception e) {
			throw exception("insert", entity, e);
		}
	}

	protected void postInsertHook(@SuppressWarnings("unused") T entity) {}

	@Override
	public synchronized T update(T entity) throws EntityNotFoundException {
		CACHE = null;
		try (HttpClientService client = getTriplestoreClient()) {
			Qname id = getId(entity);
			Model model = createModel(id, entity);
			executeUpdate(client, id, model);
			return get(id);
		} catch (Exception e) {
			throw exception("update", entity, e);
		}
	}

	@Override
	public List<ValidationResponse> validate(Collection<T> entities) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void insert(Collection<T> entities) {
		if (entities.isEmpty()) return;
		CACHE = null;
		T currentEntity = null;
		try (HttpClientService client = getTriplestoreClient()) {
			for (T entity : entities) {
				currentEntity = entity;
				Qname id = newId(client);
				Model model = createModel(id, entity);
				executeUpdate(client, id, model);
				setId(entity, id);
			}
		} catch (Exception e) {
			throw exception("insert multi", currentEntity, e);
		}
	}

	@Override
	public Collection<T> getAll() {
		return getAllMapCached().values();
	}

	@Override
	public List<T> search(Map<String, List<String>> searchParams, int limit) {
		List<T> results = new ArrayList<>();
		Map<String, Method> getters = ReflectionUtil.getGettersMap(concreteClass());
		for (T entity : getAll()) {
			boolean matches = true;
			for (Map.Entry<String, List<String>> e : searchParams.entrySet()) {
				String searchTerm = e.getKey();
				List<String> searchValues = toLowerCase(e.getValue());
				if (!given(searchTerm) || searchValues == null || searchValues.isEmpty()) continue;
				if (!getters.containsKey(searchTerm)) continue;
				try {
					Object value = getters.get(searchTerm).invoke(entity);
					if (!searchValues.contains(value)) {
						matches = false;
						break;
					}
				} catch (Exception ex) {
					throw exception("search", searchTerm, ex);
				}
			}
			if (matches) results.add(entity);
		}
		if (results.size() > limit) throw exception("search", searchParams, new IllegalArgumentException("Number of items (" + results.size() + ") has exeeded limit of " + limit));
		return results;
	}

	private List<String> toLowerCase(List<String> values) {
		List<String> cased = new ArrayList<>(values.size());
		for (String s : values) {
			if (s == null) cased.add(null);
			else cased.add(s.toLowerCase());
		}
		return cased;
	}

	private volatile LinkedHashMap<Qname, T> CACHE = null;

	private LinkedHashMap<Qname, T> getAllMapCached() {
		LinkedHashMap<Qname, T> local = CACHE;
		if (local == null) {
			synchronized (this) {
				local = CACHE;
				if (local == null) {
					local = getAllMap();
					CACHE = local;
				}
			}
		}
		if (local == null) {
			throw new IllegalStateException("CACHE sync logic failure");
		}
		return local;
	}

	private LinkedHashMap<Qname, T> getAllMap() {
		try {
			List<T> all = getAllActual();
			Collections.sort(all);
			LinkedHashMap<Qname, T> map = new LinkedHashMap<>(all.size());
			for (T t : all) {
				map.put(getId(t), t);
			}
			return map;
		} catch (Exception e) {
			throw exception("getAll", e);
		}
	}

	protected boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	protected TraitDAOException exception(String method, Exception e) {
		return new TraitDAOException(concreteClass(), method, null, e);
	}

	protected TraitDAOException exception(String method, Object param, Exception e) {
		return new TraitDAOException(concreteClass(), method, param == null ? "null" : param.toString(), e);
	}

	private void executeUpdate(HttpClientService client, Qname id, Model model) throws ClientProtocolException, IOException {
		HttpPost post = new HttpPost(triplestoreURL()+"/"+id.toString());
		post.setEntity(new StringEntity(model.getRDF(), Charset.forName("UTF-8")));
		CloseableHttpResponse response = null;
		try {
			response = client.execute(post);
			int status = response.getStatusLine().getStatusCode();
			if (status != 200) throw new IOException("Triplestore returned status " + status + " with message: " + EntityUtils.toString(response.getEntity()));
		} finally {
			if (response != null) close(response);
		}
	}

	private void close(CloseableHttpResponse response) {
		try { response.close(); } catch (Exception e) {}
	}

	protected Qname newId(HttpClientService client) {
		String prefix = concreteClass().getAnnotation(ClassDef.class).triplestorePrefix();
		try {
			URIBuilder uri = new URIBuilder(triplestoreURL() + "/uri/" + prefix);
			JSONObject response = client.contentAsJson(new HttpGet(uri.toString()));
			String qname = response.getObject("response").getString("qname");
			if (!given(qname)) throw new IllegalStateException("got null qname");
			return new Qname(qname);
		} catch (Exception e) {
			throw exception("newId", prefix, e);
		}
	}

	protected Document query(Qname id, Param ...params) {
		return query(id.toString(), params);
	}

	protected Document query(String path, Param ...params) {
		try (HttpClientService client = getTriplestoreClient()) {
			URIBuilder uri = new URIBuilder(triplestoreURL() + "/" + path);
			for (Param param : params) {
				uri.addParameter(param.getKey(), param.getValue());
			}
			uri.addParameter("format", "rdfxml");
			return client.contentAsDocument(new HttpGet(uri.getURI()));
		} catch (Exception e) {
			throw exception("query", Utils.debugS(path, params), e);
		}
	}

	protected Qname getId(Node n) {
		return Qname.fromURI(n.getAttribute("rdf:about"));
	}

	protected String getLiteral(String predicate, Node n) {
		if (!n.hasChildNodes(predicate)) return null;
		return n.getNode(predicate).getContents();
	}

	protected String getLiteral(String predicate, String lang, Node n) {
		for (Node child : n.getChildNodes(predicate)) {
			if (child.hasAttribute("xml:lang") && lang.equals(child.getAttribute("xml:lang"))) return child.getContents();
		}
		return null;
	}

	protected Qname getResource(String predicate, Node n) {
		if (!n.hasChildNodes(predicate)) return null;
		return getResource(n.getNode(predicate));
	}

	protected Qname getResource(Node n) {
		return Qname.fromURI(n.getAttribute("rdf:resource"));
	}

	private Qname getId(T entity) {
		try {
			return (Qname) ReflectionUtil.getGetter("id", concreteClass()).invoke(entity);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	private void setId(T entity, Qname id) {
		try {
			ReflectionUtil.getSetter("id", concreteClass()).invoke(entity, id);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

}
