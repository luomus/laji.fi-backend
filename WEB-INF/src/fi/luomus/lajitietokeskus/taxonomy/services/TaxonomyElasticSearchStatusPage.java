package fi.luomus.lajitietokeskus.taxonomy.services;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.lajitietokeskus.services.BaseServlet;
import fi.luomus.lajitietokeskus.taxonomy.dao.ElasticSearchPushService;

@WebServlet(urlPatterns = {"/api/es-load-status/*"})
public class TaxonomyElasticSearchStatusPage extends BaseServlet {

	private static final long serialVersionUID = -1085795676649346350L;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		try {
			ElasticSearchPushService service = getTaxonomyDAO().getElasticSearchPushService();
			if (!service.hasErrors()) {
				res.setStatus(200);
				return response("ok", "text/plain");
			}
			res.setStatus(500);
			return response(service.getErrorSummary(), "text/plain");
		} catch (Exception e) {
			res.setStatus(500);
			return response(LogUtils.buildStackTrace(e), "text/plain");
		}
	}

}
