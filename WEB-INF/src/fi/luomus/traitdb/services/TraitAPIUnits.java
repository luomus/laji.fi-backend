package fi.luomus.traitdb.services;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOEntity;
import fi.luomus.traitdb.models.UnitOfMeasurement;
import fi.luomus.traitdb.services.TraitAPI.Method;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIAuthorizedRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIEndpoint;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIResource;

public class TraitAPIUnits implements TraitAPIResource {

	private final TraitAPIRequest req;
	private final TraitDAOEntity<UnitOfMeasurement> dao;

	public TraitAPIUnits(TraitAPIRequest req) {
		this.req = req;
		this.dao = req.getDAO().units();
	}

	@Override
	public TraitAPIEndpoint endpoint() {
		String endpoint = req.getEndpoint();
		if (Method.GET == req.getMethod()) {
			if ("".equals(endpoint)) return getAll();
			if ("{id}".equals(endpoint)) return getSingle(req.getId());
		}
		throw req.noEndpointException();
	}

	private TraitAPIAuthorizedRequest allowAll(TraitAPIEndpoint endpoint) {
		return new TraitAPIAuthorizedRequest(endpoint);
	}

	private TraitAPIEndpoint getSingle(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.get(id);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

	private TraitAPIEndpoint getAll() {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.getAll();
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() {
				return allowAll(this);
			}
		};
	}

}
