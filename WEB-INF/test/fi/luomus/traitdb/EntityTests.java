package fi.luomus.traitdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.Enumeration;
import fi.luomus.traitdb.models.InputRow;
import fi.luomus.traitdb.models.Subject;
import fi.luomus.traitdb.models.Trait;
import fi.luomus.traitdb.models.TraitEnumerationValue;
import fi.luomus.traitdb.models.TraitGroup;
import fi.luomus.traitdb.models.TraitValue;
import fi.luomus.traitdb.models.UnitOfMeasurement;
import fi.luomus.traitdb.models.search.Interpreter;
import fi.luomus.traitdb.models.search.TraitSearchRow;
import fi.luomus.traitdb.models.search.TraitTaxon;

public class EntityTests {

	private final TraitDAO dao = new TraitDAOStub() {
		@Override
		public Enumeration getEnumeration(Qname id) {
			Enumeration enumeration = new Enumeration(id);
			enumeration.add(new Qname("foo"), "bar");
			enumeration.add(new Qname("TDF.typeTaxon"), "Taxon");
			enumeration.add(new Qname("TDF.umMM"), "mm");
			enumeration.add(new Qname("TDF.umCM"), "cm");
			enumeration.add(new Qname("xsd:double"), "double");
			enumeration.add(new Qname("TDF.statisticalMethodAvg"), "Avg");
			enumeration.add(new Qname("MX.iucnLC"), "LC");
			enumeration.add(new Qname("MKV.habitatMk"), "Mk – heath forests");
			enumeration.add(new Qname("MKV.habitatSpecificTypeVAK"), "va - shady");
			enumeration.add(new Qname("MY.lifeStageEgg"), "Egg");
			enumeration.add(Interpreter.LICENSE, "Creative Commons Attribution 4.0");
			return enumeration;
		}
	};

	@Test
	public void test_subject() throws Exception {
		Subject subject = new Subject();
		subject.setAuthor("author");
		subject.setDatasetId(new Qname("HR.123"));
		subject.setCoordinateAccuracy(1000);
		subject.setCreated(DateUtils.convertToDate("5.6.2023"));
		subject.setDateBegin(DateUtils.convertToDateValue("1.1.2000"));
		subject.setGbifTaxonId(235232);
		subject.setLat(63.20114411552);

		JSONObject json = subject.toJSON();
		assertEquals("" +
				"{\"datasetId\":\"HR.123\",\"author\":\"author\",\"gbifTaxonId\":235232,\"dateBegin\":\"2000-01-01\",\"lat\":63.201144,\"coordinateAccuracy\":1000,\"created\":\"2023-06-05\"}",
				json.toString());

		Subject copy = new Subject(json);
		assertEquals(copy, subject);
		assertEquals(copy.toString(), subject.toString());
	}

	@Test
	public void test_subject_invalid_value() throws Exception {
		Subject subject = new Subject();
		subject.setDatasetId(new Qname("HR.123"));

		JSONObject json = subject.toJSON();
		assertEquals("" +
				"{\"datasetId\":\"HR.123\"}",
				json.toString());
		json.setString("foo", "bar");
		Subject copy = new Subject(json);
		assertEquals(copy, subject);
		assertEquals(copy.toString(), subject.toString());
	}

	@Test
	public void test_traitvalue() throws Exception {
		TraitValue tv = new TraitValue();
		tv.setId(new Qname("TD.123"));
		tv.setTraitId(new Qname("TDF.7"));
		tv.setValue("MX.1");
		tv.setObjectTaxonLifeStage(new Qname("MY.lifeStagePupa"));
		tv.setReference("reference");
		tv.setMeasurementRemarks("nots");

		JSONObject json = tv.toJSON();
		assertEquals("" +
				"{\"id\":\"TD.123\",\"traitId\":\"TDF.7\",\"value\":\"MX.1\",\"objectTaxonLifeStage\":\"MY.lifeStagePupa\",\"warnings\":false,\"measurementRemarks\":\"nots\",\"reference\":\"reference\"}",
				json.toString());

		TraitValue copy = new TraitValue(json);
		assertEquals(copy, tv);
		assertEquals(copy.toString(), tv.toString());
	}

	@Test
	public void test_traitvalue_2() throws Exception {
		TraitValue tv = new TraitValue();
		tv.setId(new Qname("TD.123"));
		tv.setTraitId(new Qname("TDF.7"));
		tv.setValue("63.2");
		tv.setStatisticalMethod(new Qname("TDF.statisticalMethodMin"));
		tv.setUnit(new Qname("TDF.umMM"));
		tv.setMeasurementAccuracy(0.5);
		tv.setWarnings(true);

		JSONObject json = tv.toJSON();
		assertEquals("" +
				"{\"id\":\"TD.123\",\"traitId\":\"TDF.7\",\"value\":\"63.2\",\"unit\":\"TDF.umMM\",\"statisticalMethod\":\"TDF.statisticalMethodMin\",\"measurementAccuracy\":0.5,\"warnings\":true}",
				json.toString());

		json.setString("foo", "bar");
		json.setInteger("valueInteger", 123);

		TraitValue copy = new TraitValue(json);
		assertEquals(copy, tv);
		assertEquals(copy.toString(), tv.toString());
	}

	@Test
	public void test_traitvalue_3() throws Exception {
		TraitValue tv = new TraitValue();
		tv.setId(new Qname("TD.123"));
		tv.setTraitId(new Qname("TDF.7"));
		tv.setValue("63.2");
		tv.setStatisticalMethod(new Qname("TDF.statisticalTypeMin"));
		tv.setUnit(new Qname("TDF.umMM"));
		tv.setMeasurementAccuracy(0.5);
		tv.setWarnings(false);

		JSONObject json = tv.toJSON();
		assertEquals("" +
				"{\"id\":\"TD.123\",\"traitId\":\"TDF.7\",\"value\":\"63.2\",\"unit\":\"TDF.umMM\",\"statisticalMethod\":\"TDF.statisticalTypeMin\",\"measurementAccuracy\":0.5,\"warnings\":false}",
				json.toString());

		json.setString("foo", "bar");
		json.setInteger("valueInteger", 123);

		TraitValue copy = new TraitValue(json);
		assertEquals(copy, tv);
		assertEquals(copy.toString(), tv.toString());
	}

	@Test
	public void test_traitvalue_4() {
		String json = "" +
				"{\"id\":\"TD.123\",\"traitId\":\"TDF.7\",\"value\":\"63,2\",\"unit\":\"TDF.umMM\",\"statisticalMethod\":\"TDF.statisticalTypeMin\",\"measurementAccuracy\":\"0,5\",\"warnings\":false}";
		try {
			new TraitValue(new JSONObject(json));
			fail("Should fail to 0,5 not being a double");
		} catch (Exception e) {
			assertEquals("" +
					"JSON deserialization failed: Get double for key measurementAccuracy\n" +
					"{\"id\":\"TD.123\",\"traitId\":\"TDF.7\",\"value\":\"63,2\",\"unit\":\"TDF.umMM\",\"statisticalMethod\":\"TDF.statisticalTypeMin\",\"measurementAccuracy\":\"0,5\",\"warnings\":false}",
					e.getMessage());
		}
	}

	@Test
	public void input_row() throws Exception {
		Subject subject = new Subject();
		subject.setDatasetId(new Qname("HR.123"));
		subject.setTaxonId(new Qname("MX."));
		subject.setType(new Qname("TDF.typeTaxon"));

		TraitValue tv1 = new TraitValue();
		tv1.setTraitId(new Qname("TDF.1"));
		tv1.setValue("a");
		TraitValue tv2 = new TraitValue();
		tv2.setTraitId(new Qname("TDF.2"));
		tv2.setValue("b");

		InputRow row = new InputRow(subject);
		row.addTrait(tv1);
		row.addTrait(tv2);

		JSONObject json = row.toJSON();
		assertEquals("" +
				"{\"subject\":{\"datasetId\":\"HR.123\",\"type\":\"TDF.typeTaxon\",\"taxonId\":\"MX.\"},\"traits\":[{\"traitId\":\"TDF.1\",\"value\":\"a\",\"warnings\":false},{\"traitId\":\"TDF.2\",\"value\":\"b\",\"warnings\":false}]}",
				json.toString());
		InputRow copy = new InputRow(json);
		assertEquals(copy, row);
		assertEquals(copy.toString(), row.toString());
	}

	@Test
	public void trait() throws Exception {
		Trait trait = createTrait();

		JSONObject json = trait.toJSON();
		assertEquals("" +
				"{\"id\":\"TDF.1\",\"group\":\"TDF.2\",\"dataEntryName\":\"wing_length\",\"name\":\"Wing length\",\"description\":\"Wing length measured using min method\",\"exampleValues\":\"50, 50.5\",\"baseUnit\":\"TDF.umMM\",\"range\":\"xsd:double\",\"enumerations\":[{\"id\":\"TDF.3\",\"dataEntryName\":\"a\",\"name\":\"A\",\"description\":\"A is A\"}],\"reference\":\"foobar\",\"identifiers\":[\"some:winglength\",\"purl:sursur\"]}",
				json.toString());
		Trait copy = new Trait(json);
		assertEquals(copy, trait);
		assertEquals(copy.toString(), trait.toString());
	}

	private static Trait createTrait() {
		Trait trait = new Trait();
		trait.setId(new Qname("TDF.1"));
		trait.setGroup(new Qname("TDF.2"));
		trait.setDataEntryName("wing_length");
		trait.setName("Wing length");
		trait.setDescription("Wing length measured using min method");
		trait.setExampleValues("50, 50.5");
		trait.setBaseUnit(new Qname("TDF.umMM"));
		trait.setRange(new Qname("xsd:double"));

		TraitEnumerationValue e1 = new TraitEnumerationValue();
		e1.setId(new Qname("TDF.3"));
		e1.setDataEntryName("a");
		e1.setName("A");
		e1.setDescription("A is A");
		trait.getEnumerations().add(e1);
		trait.setReference("foobar");
		trait.getIdentifiers().add("some:winglength");
		trait.getIdentifiers().add("purl:sursur");
		return trait;
	}

	@Test
	public void trait_taxon() {
		TraitTaxon taxon = createTraitTaxon();

		JSONObject json = taxon.toJSON();
		assertEquals("" +
				"{\"id\":\"http://tun.fi/MX.1\",\"taxonomicOrder\":1,\"taxonRank\":\"species\",\"scientificName\":\"Parus major\",\"cursiveName\":true,\"author\":\"L.\",\"higherTaxa\":{\"kingdom\":\"Animalia\",\"class\":\"Aves\",\"family\":\"Paridae\"},\"iucnStatus\":\"MX.iucnLC\",\"primaryHabitat\":\"MKV.habitatMk\",\"habitatSpecifiers\":[\"MKV.habitatSpecificTypeVAK\"],\"sensitive\":false}",
				json.toString());
		TraitTaxon copy = new TraitTaxon(json);
		assertEquals(copy, taxon);
		assertEquals(copy.toString(), taxon.toString());
	}

	private static TraitTaxon createTraitTaxon() {
		TraitTaxon taxon = new TraitTaxon();
		taxon.setScientificName("Parus major");
		taxon.setAuthor("L.");
		taxon.setId(new Qname("MX.1").toURI());
		taxon.setTaxonRank("species");
		taxon.getHigherTaxa().put("kingdom", "Animalia");
		taxon.getHigherTaxa().put("class", "Aves");
		taxon.getHigherTaxa().put("family", "Paridae");
		taxon.setIucnStatus(new Qname("MX.iucnLC"));
		taxon.setPrimaryHabitat(new Qname("MKV.habitatMk"));
		taxon.getHabitatSpecifiers().add(new Qname("MKV.habitatSpecificTypeVAK"));
		taxon.setCursiveName(true);
		taxon.setTaxonomicOrder(1);
		return taxon;
	}

	@Test
	public void dataset() throws Exception {
		Dataset dataset = createDataset();

		JSONObject json = dataset.toJSON();
		assertEquals("" +
				"{\"id\":\"HR.1\",\"name\":\"My dataset\",\"additionalIdentifiers\":[\"https://opentraits.org/datasets/xxxx-2005\",\"GUID\"],\"published\":true,\"shareToFinBIF\":true,\"shareToGBIF\":false}",
				json.toString());
		Dataset copy = new Dataset(json);
		assertEquals(copy, dataset);
		assertEquals(copy.toString(), dataset.toString());
	}

	private static Dataset createDataset() {
		Dataset dataset = new Dataset();
		dataset.setId(new Qname("HR.1"));
		dataset.setName("My dataset");
		dataset.getAdditionalIdentifiers().add("https://opentraits.org/datasets/xxxx-2005");
		dataset.getAdditionalIdentifiers().add("GUID");
		dataset.setPublished(true);
		dataset.setShareToFinBIF(true);
		dataset.setShareToGBIF(false);
		return dataset;
	}

	@Test
	public void search_row() {
		TraitSearchRow row = createTestRow();

		JSONObject json = row.toJSON();
		assertEquals("" +
				"{\n" +
				"  \"id\": \"TD.2\",\n" +
				"  \"subject\": {\n" +
				"    \"id\": \"TD.1\",\n" +
				"    \"type\": \"TDF.typeTaxon\",\n" +
				"    \"ageYears\": 1.5,\n" +
				"    \"individualCount\": 1,\n" +
				"    \"dateBegin\": \"2000-05-15\",\n" +
				"    \"yearBegin\": 2000,\n" +
				"    \"yearEnd\": 2000,\n" +
				"    \"measurementDeterminedBy\": \"me\",\n" +
				"    \"measurementDeterminedDate\": \"2005-01-20\"\n" +
				"  },\n" +
				"  \"year\": 2000,\n" +
				"  \"eventDate\": \"2000\",\n" +
				"  \"geodeticDatum\": \"WGS84\",\n" +
				"  \"trait\": {\n" +
				"    \"id\": \"TDF.1\",\n" +
				"    \"dataEntryName\": \"wing_length\",\n" +
				"    \"name\": \"Wing length\",\n" +
				"    \"description\": \"Wing length measured using min method\",\n" +
				"    \"baseUnit\": \"TDF.umMM\",\n" +
				"    \"range\": \"xsd:double\",\n" +
				"    \"reference\": \"foobar\",\n" +
				"    \"identifiers\": [\n" +
				"      \"some:winglength\",\n" +
				"      \"purl:sursur\"\n" +
				"    ]\n" +
				"  },\n" +
				"  \"traitGroup\": {\n" +
				"    \"name\": \"Group name\"\n" +
				"  },\n" +
				"  \"statisticalMethod\": \"TDF.statisticalMethodAvg\",\n" +
				"  \"value\": \"16\",\n" +
				"  \"valueNumeric\": 16,\n" +
				"  \"unit\": \"TDF.umMM\",\n" +
				"  \"measurementAccuracy\": 5,\n" +
				"  \"originalValue\": \"1.6\",\n" +
				"  \"originalValueNumeric\": 1.6,\n" +
				"  \"originalUnit\": \"TDF.umCM\",\n" +
				"  \"originalMeasurementAccuracy\": 0.5,\n" +
				"  \"subjectFinBIFTaxon\": {\n" +
				"    \"id\": \"http://tun.fi/MX.1\",\n" +
				"    \"taxonomicOrder\": 1,\n" +
				"    \"taxonRank\": \"species\",\n" +
				"    \"scientificName\": \"Parus major\",\n" +
				"    \"cursiveName\": true,\n" +
				"    \"author\": \"L.\",\n" +
				"    \"higherTaxa\": {\n" +
				"      \"kingdom\": \"Animalia\",\n" +
				"      \"class\": \"Aves\",\n" +
				"      \"family\": \"Paridae\"\n" +
				"    },\n" +
				"    \"iucnStatus\": \"MX.iucnLC\",\n" +
				"    \"primaryHabitat\": \"MKV.habitatMk\",\n" +
				"    \"habitatSpecifiers\": [\n" +
				"      \"MKV.habitatSpecificTypeVAK\"\n" +
				"    ],\n" +
				"    \"sensitive\": false\n" +
				"  },\n" +
				"  \"objectTaxonLifeStage\": \"MY.lifeStageEgg\",\n" +
				"  \"objectTaxonVerbatim\": \"Parmaj\",\n" +
				"  \"objectFinBIFTaxon\": {\n" +
				"    \"id\": \"http://tun.fi/MX.1\",\n" +
				"    \"taxonomicOrder\": 1,\n" +
				"    \"taxonRank\": \"species\",\n" +
				"    \"scientificName\": \"Parus major\",\n" +
				"    \"cursiveName\": true,\n" +
				"    \"author\": \"L.\",\n" +
				"    \"higherTaxa\": {\n" +
				"      \"kingdom\": \"Animalia\",\n" +
				"      \"class\": \"Aves\",\n" +
				"      \"family\": \"Paridae\"\n" +
				"    },\n" +
				"    \"iucnStatus\": \"MX.iucnLC\",\n" +
				"    \"primaryHabitat\": \"MKV.habitatMk\",\n" +
				"    \"habitatSpecifiers\": [\n" +
				"      \"MKV.habitatSpecificTypeVAK\"\n" +
				"    ],\n" +
				"    \"sensitive\": false\n" +
				"  },\n" +
				"  \"warnings\": true,\n" +
				"  \"measurementRemarks\": \"notes\",\n" +
				"  \"reference\": \"ref\",\n" +
				"  \"dataset\": {\n" +
				"    \"id\": \"HR.1\",\n" +
				"    \"name\": \"My dataset\",\n" +
				"    \"additionalIdentifiers\": [\n" +
				"      \"https://opentraits.org/datasets/xxxx-2005\",\n" +
				"      \"GUID\"\n" +
				"    ]\n" +
				"  },\n" +
				"  \"license\": \"MZ.intellectualRightsCC-BY-4.0\"\n" +
				"}",
				json.beautify());
		TraitSearchRow copy = new TraitSearchRow(json);
		assertNotEquals(copy, row); // Fields annotated onlySearch are missing from the copy
		assertEquals(copy.toJSON().toString(), json.toString());
	}

	public static TraitSearchRow createTestRow() {
		TraitSearchRow row = new TraitSearchRow();

		row.setId(new Qname("TD.2"));

		Subject subject = new Subject();
		subject.setId(new Qname("TD.1"));
		subject.setDatasetId(new Qname("HR.123"));
		subject.setTaxonId(new Qname("MX.1"));
		subject.setType(new Qname("TDF.typeTaxon"));
		subject.setYearBegin(2000);
		subject.setYearEnd(2000);
		subject.setDateBegin(DateUtils.convertToDateValue("15.5.2000"));
		subject.setIndividualCount(1);
		subject.setAgeYears(1.5);
		subject.setMeasurementDeterminedBy("me");
		try {subject.setMeasurementDeterminedDate(DateUtils.convertToDate("20.1.2005")); } catch (Exception e) {throw new RuntimeException(e); }
		row.setSubject(subject);

		row.setYear(2000);
		row.setEventDate("2000");
		row.setGeodeticDatum("WGS84");

		row.setSubjectFinBIFTaxon(createTraitTaxon());

		row.setTrait(createTrait());

		TraitGroup group = new TraitGroup();
		group.setId(row.getTrait().getGroup());
		group.setName("Group name");
		row.setTraitGroup(group);

		row.setStatisticalMethod(new Qname("TDF.statisticalMethodAvg"));
		row.setOriginalValue("1.6");
		row.setOriginalValueNumeric(1.6);
		row.setOriginalUnit(new Qname("TDF.umCM"));
		row.setOriginalMeasurementAccuracy(0.5);

		row.setValue("16");
		row.setValueNumeric(16.0);
		row.setUnit(new Qname("TDF.umMM"));
		row.setMeasurementAccuracy(5.0);

		row.setObjectTaxonLifeStage(new Qname("MY.lifeStageEgg"));
		row.setObjectFinBIFTaxon(createTraitTaxon());
		row.setObjectTaxonVerbatim("Parmaj");

		row.setWarnings(true);
		row.setMeasurementRemarks("notes");
		row.setReference("ref");

		row.setDataset(createDataset());
		row.setLicense(Interpreter.LICENSE);
		return row;
	}

	@Test
	public void validate() {
		TraitGroup group = new TraitGroup();
		group.setName("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

		boolean insert = true;
		assertEquals("{description=Required field}", group.validate(insert, dao).toString());

		group.setDescription("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		assertEquals("{}", group.validate(insert, dao).toString());

		group.setName(group.getName() + group.getName());
		assertEquals("{name=Too long value, maximum is 50}", group.validate(insert, dao).toString());

		boolean update = false;
		assertEquals("{id=Required field, name=Too long value, maximum is 50}", group.validate(update, dao).toString());
	}

	@Test
	public void validate_2() {
		boolean insert = true;
		boolean update = false;

		TraitEnumerationValue e1 = new TraitEnumerationValue();
		e1.setDataEntryName("a");
		e1.setDescription("desc");
		assertEquals("{name=Required field}", e1.validate(insert, dao).toString());

		Trait trait = new Trait();
		trait.setDataEntryName("tdename");
		trait.setDescription("desc");
		trait.setGroup(new Qname("TDF.123"));
		trait.setRange(new Qname("foo"));
		assertEquals("{name=Required field}", trait.validate(insert, dao).toString());

		trait.setName("trname");
		assertEquals("{}", trait.validate(insert, dao).toString());

		trait.setId(new Qname("TDF.456"));
		assertEquals("{id=Id set before insert}", trait.validate(insert, dao).toString());
		assertEquals("{}", trait.validate(update, dao).toString());

		trait.getEnumerations().add(e1);
		assertEquals("{enumerations[0]name=Required field}", trait.validate(update, dao).toString());

		e1.setName("ename");
		e1.setId(new Qname("TDF.789"));

		assertEquals("{}", trait.validate(update, dao).toString());

		while (trait.getIdentifiers().toString().length() < 4000) {
			trait.getIdentifiers().add("idxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		}
		assertEquals("{}", trait.validate(update, dao).toString());

		String longString = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
		while (longString.length() < 4000) {
			longString += longString;
		}
		trait.getIdentifiers().add(longString);
		assertEquals("{identifiers[52]=Too long value, maximum is 200}", trait.validate(update, dao).toString());

		trait.getIdentifiers().clear();

		trait.setRange(new Qname("notfoundenumname"));
		assertEquals("{range=Unknown value notfoundenumname}", trait.validate(update, dao).toString());
	}

	@Test
	public void unitOfMeasurement() {
		UnitOfMeasurement unit = new UnitOfMeasurement();
		unit.setId(new Qname("TDF.umMM"));
		unit.setBase(new Qname("TDF.umM"));
		unit.setIsBaseUnit(false);
		unit.setConversionFactor(1000.0);
		unit.setUnit("mm");

		JSONObject json = unit.toJSON();
		assertEquals("{\"id\":\"TDF.umMM\",\"unit\":\"mm\",\"base\":\"TDF.umM\",\"conversionFactor\":1000,\"isBaseUnit\":false}",
				json.toString());

		UnitOfMeasurement copy = new UnitOfMeasurement(json);
		assertEquals(unit.toString(), copy.toString());
	}

	@Test
	public void search_row_TSV() {
		TraitSearchRow row = createTestRow();

		assertEquals(""+
				"measurementID	subjectID	type	basisOfRecord	finbifOccurrenceId	gbifOccurrenceId	otherOccurrenceId	kingdom	verbatimScientificName	author	functionalGroupName	otherTaxonId	sex	lifeStage	age	individualCount	dateBegin	dateEnd	yearBegin	yearEnd	seasonBegin	seasonEnd	decimalLatitude	decimalLongitude	latMin	latMax	lonMin	lonMax	coordinateAccuracy	elevation	higherGeography	country	municipality	verbatimLocality	locationIdentifiers	habitat	occurrenceRemarks	measurementDeterminedBy	measurementDeterminedDate	created	modified	year	month	day	eventDate	geodeticDatum	traitID	verbatimTraitName	traitName	traitDescription	expectedUnit	valueType	source	identifier	traitGroupName	statisticalMethod	traitValue	valueNumeric	traitUnit	measurementAccuracy	verbatimTraitValue	originalValueNumeric	verbatimTraitUnit	originalMeasurementAccuracy	subject_finbif_taxonID	subject_finbif_taxonRank	subject_finbif_scientificName	subject_finbif_author	subject_finbif_iucnStatus	subject_finbif_primaryHabitat	subject_finbif_habitatSpecifiers	subject_finbif_sensitive	subject_gbif_taxonID	subject_gbif_taxonRank	subject_gbif_scientificName	subject_gbif_author	subject_gbif_iucnStatus	subject_gbif_primaryHabitat	subject_gbif_habitatSpecifiers	subject_gbif_sensitive	objectTaxonLifeStage	objectTaxonVerbatim	object_finbif_taxonID	object_finbif_taxonRank	object_finbif_scientificName	object_finbif_author	object_finbif_iucnStatus	object_finbif_primaryHabitat	object_finbif_habitatSpecifiers	object_finbif_sensitive	object_gbif_taxonID	object_gbif_taxonRank	object_gbif_scientificName	object_gbif_author	object_gbif_iucnStatus	object_gbif_primaryHabitat	object_gbif_habitatSpecifiers	object_gbif_sensitive	warnings	measurementRemarks	references	datasetID	datasetName	datasetDescription	bibliographicCitation	rightsHolder	author	contactEmail	measurementMethod	samplingProtocol	finbifDOI	gbifDOI	additionalIdentifiers	license	\n" +
				"",
				TraitSearchRow.getTSVHeader());

		assertEquals("" +
				"TD.2	TD.1	Taxon												1.5	1	2000-05-15		2000	2000																		me	2005-01-20			2000			2000	WGS84	TDF.1	wing_length	Wing length	Wing length measured using min method	mm	double	foobar	[some:winglength, purl:sursur]	Group name	Avg	16	16.0	mm	5.0	1.6	1.6	cm	0.5	http://tun.fi/MX.1	species	Parus major	L.	LC	Mk – heath forests	[va - shady]	false									Egg	Parmaj	http://tun.fi/MX.1	species	Parus major	L.	LC	Mk – heath forests	[va - shady]	false									true	notes	ref	HR.1	My dataset										[https://opentraits.org/datasets/xxxx-2005, GUID]	Creative Commons Attribution 4.0	\n" +
				"",
				row.toTSV(dao));
	}

}
