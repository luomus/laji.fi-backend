package fi.luomus.traitdb.dao.db;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.models.InputRow;

public interface DbDAO extends AutoCloseable {

	List<Dataset> getDatasets(List<Dataset> datasets) throws EntityNotFoundException;
	void insert(Dataset dataset);
	void update(Dataset dataset) throws EntityNotFoundException;
	void delete(Dataset dataset) throws EntityNotFoundException;

	DatasetPermissions getPermissions(Qname datasetId) throws EntityNotFoundException;
	void upsert(DatasetPermissions datasetPermissions);
	List<DatasetPermissions> getPermissions();

	void insert(Collection<InputRow> rows);
	void update(InputRow row) throws EntityNotFoundException;
	void delete(InputRow row) throws EntityNotFoundException;
	List<InputRow> search(Map<String, List<String>> searchParams, int limit);

}