package fi.luomus.lajitietokeskus;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fi.luomus.lajitietokeskus.dao.SykeNewsHTMLScraperService;
import fi.luomus.lajitietokeskus.models.NewsItem;

public class SykeNewsHTMLScraperServiceTests {

	@Test
	public void parsingNews() throws Exception {
		String html = getContents("syke_news.htm");
		List<NewsItem> news = SykeNewsHTMLScraperService.scrape(html);
		assertEquals(20, news.size());

		NewsItem n1 = news.get(0);
		assertEquals("Uudet tiedot paljastavat arktisen merijään vähenemisen eläinplanktonille tuomat haasteet", n1.getTitle());
		assertEquals("syke.fi", n1.getTag());
		assertEquals("syke.fi-65909/25245", n1.getId());
		assertEquals("2023-08-31 00:00:00.0", n1.getPosted().toString());
		assertEquals("https://www.syke.fi/fi-FI/content/65909/25245", n1.getExternalUrl().toString());
	}

	private String getContents(String filename) throws Exception {
		return TestUtil.getContents(SykeNewsHTMLScraperServiceTests.class, filename);
	}

}
