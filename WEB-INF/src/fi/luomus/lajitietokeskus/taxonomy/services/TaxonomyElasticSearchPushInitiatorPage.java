package fi.luomus.lajitietokeskus.taxonomy.services;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.ResponseData;
import fi.luomus.lajitietokeskus.services.BaseServlet;
import fi.luomus.lajitietokeskus.taxonomy.dao.ElasticSearchPushService;

@WebServlet(urlPatterns = {"/api/es-load-start/*"})
public class TaxonomyElasticSearchPushInitiatorPage extends BaseServlet {

	private static final long serialVersionUID = 1049957907797830163L;
	private static final Object LOCK = new Object();
	private boolean running = false;

	@Override
	protected ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception {
		res.setStatus(404);
		response("use POST", "text/plain");
		res.setHeader("Connection", "close");
		return new ResponseData().setOutputAlreadyPrinted();
	}


	@Override
	protected ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception {
		if (!getConfig().get("ES_push_secret").equals(req.getParameter("secret"))) {
			return status403(res);
		}

		if (running) throw new ElasticSearchPushService.AlreadyRunningException();

		synchronized (LOCK) {
			if (running) throw new ElasticSearchPushService.AlreadyRunningException();
			running = true;

			Thread t = new Thread(() -> {
				try {
					getTaxonomyDAO().nightyTasks().run();
				} finally {
					running = false;
				}
			});

			t.start();
		}

		return response("started", "text/plain");
	}

}
