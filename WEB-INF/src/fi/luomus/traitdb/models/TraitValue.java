package fi.luomus.traitdb.models;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.Utils;
import fi.luomus.traitdb.dao.TraitDAO;
import fi.luomus.traitdb.dao.TraitDAO.EntityNotFoundException;

public class TraitValue extends AbstractBaseEntity {

	private static final Qname NON_NEGATIVE_INTEGER_RANGE = new Qname("xsd:nonNegativeInteger");
	private static final Qname POSITIVE_INTEGER_RANGE = new Qname("xsd:positiveInteger");
	private static final Qname INTEGER_RANGE = new Qname("xsd:integer");
	private static final Qname DECIMAL_RANGE = new Qname("xsd:decimal");
	private static final Qname TAXON_RANGE = new Qname("MX.taxon");
	private static final Set<Qname> INTEGER_RANGES = Utils.set(INTEGER_RANGE, POSITIVE_INTEGER_RANGE, NON_NEGATIVE_INTEGER_RANGE);
	private static final Set<Qname> NUMERIC_RANGES = Utils.set(INTEGER_RANGES, DECIMAL_RANGE);

	@FieldDef(order=1.1, required=true)
	private Qname id;

	@FieldDef(order=1.3, required=true)
	private Qname traitId;

	@FieldDef(order=2, length=1000, required=true)
	private String value;

	@FieldDef(order=3.1, enumeration="TDF.unitOfMeasurementEnum")
	private Qname unit;

	@FieldDef(order=3.2, enumeration="TDF.statisticalMethodEnum")
	private Qname statisticalMethod;

	@FieldDef(order=3.3, length=10)
	private Double measurementAccuracy;

	@FieldDef(order=4, enumeration="MY.lifeStages")
	private Qname objectTaxonLifeStage;

	@FieldDef(order=5)
	private boolean warnings;

	@FieldDef(order=6, length=2000)
	private String measurementRemarks;

	@FieldDef(order=7, length=200)
	private String reference;

	public TraitValue() {
		super();
	}

	public TraitValue(JSONObject json) throws ParseException {
		super(json);
	}

	public Qname getId() {
		return id;
	}

	public Qname getTraitId() {
		return traitId;
	}

	public String getValue() {
		return value;
	}

	public Qname getUnit() {
		return unit;
	}

	public Qname getStatisticalMethod() {
		return statisticalMethod;
	}

	public Double getMeasurementAccuracy() {
		return measurementAccuracy;
	}

	public Qname getObjectTaxonLifeStage() {
		return objectTaxonLifeStage;
	}

	public boolean isWarnings() {
		return warnings;
	}

	public String getMeasurementRemarks() {
		return measurementRemarks;
	}

	public String getReference() {
		return reference;
	}

	public void setId(Qname id) {
		this.id = id;
	}

	public void setTraitId(Qname traitId) {
		this.traitId = traitId;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setUnit(Qname unit) {
		this.unit = unit;
	}

	public void setStatisticalMethod(Qname statisticalMethod) {
		this.statisticalMethod = statisticalMethod;
	}

	public void setMeasurementAccuracy(Double measurementAccuracy) {
		this.measurementAccuracy = measurementAccuracy;
	}

	public void setObjectTaxonLifeStage(Qname lifeStage) {
		this.objectTaxonLifeStage = lifeStage;
	}

	public void setWarnings(boolean warnings) {
		this.warnings = warnings;
	}

	public void setMeasurementRemarks(String measurementRemarks) {
		this.measurementRemarks = measurementRemarks;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((objectTaxonLifeStage == null) ? 0 : objectTaxonLifeStage.hashCode());
		result = prime * result + ((measurementAccuracy == null) ? 0 : measurementAccuracy.hashCode());
		result = prime * result + ((measurementRemarks == null) ? 0 : measurementRemarks.hashCode());
		result = prime * result + ((reference == null) ? 0 : reference.hashCode());
		result = prime * result + ((statisticalMethod == null) ? 0 : statisticalMethod.hashCode());
		result = prime * result + ((traitId == null) ? 0 : traitId.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + (warnings ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TraitValue other = (TraitValue) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (objectTaxonLifeStage == null) {
			if (other.objectTaxonLifeStage != null)
				return false;
		} else if (!objectTaxonLifeStage.equals(other.objectTaxonLifeStage))
			return false;
		if (measurementAccuracy == null) {
			if (other.measurementAccuracy != null)
				return false;
		} else if (!measurementAccuracy.equals(other.measurementAccuracy))
			return false;
		if (measurementRemarks == null) {
			if (other.measurementRemarks != null)
				return false;
		} else if (!measurementRemarks.equals(other.measurementRemarks))
			return false;
		if (reference == null) {
			if (other.reference != null)
				return false;
		} else if (!reference.equals(other.reference))
			return false;
		if (statisticalMethod == null) {
			if (other.statisticalMethod != null)
				return false;
		} else if (!statisticalMethod.equals(other.statisticalMethod))
			return false;
		if (traitId == null) {
			if (other.traitId != null)
				return false;
		} else if (!traitId.equals(other.traitId))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (warnings != other.warnings)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TraitValue [id=" + id + ", traitId=" + traitId + ", value=" + value + ", unit=" + unit + ", statisticalMethod=" + statisticalMethod + ", measurementAccuracy="
				+ measurementAccuracy + ", objectTaxonLifeStage=" + objectTaxonLifeStage + ", warnings=" + warnings + ", measurementRemarks=" + measurementRemarks + ", reference=" + reference + "]";
	}

	@Override
	public Map<String, String> validate(boolean insert, TraitDAO dao) {
		Map<String, String> errors = super.validate(insert, dao);

		Trait trait = null;
		try {
			trait = dao.traits().get(this.getTraitId());
		} catch (Exception e) {
			// missing/invalid trait will be validated in InputRow validations
		}
		if (trait == null) return errors;

		boolean isNumeric = isNumeric(trait);
		boolean isInteger = isInteger(trait);
		boolean isDecimal = isDecimal(trait);
		boolean isTaxon = isTaxon(trait);

		if (!isNumeric) {
			if (this.getMeasurementAccuracy() != null) errors.put("measurementAccuracy", "Only available for numeric traits");
			if (this.getStatisticalMethod() != null) errors.put("statisticalMethod", "Only available for numeric traits");
			if (this.getUnit() != null) errors.put("unit", "Only available for numeric traits");
		} else {
			if (this.getMeasurementAccuracy() != null) {
				if (this.getMeasurementAccuracy() <= 0.0) {
					errors.put("measurementAccuracy", "Must be creater than zero");
				}
			}
			if (trait.getBaseUnit() != null) {
				if (this.getUnit() == null) {
					errors.put("unit", "Unit of measurement is required for the selected trait");
				} else {
					if (!unitsMatch(trait, dao)) errors.put("unit", "The unit of measurement you entered is not compatible with the specified trait");
				}
			}
			if (this.getValue() != null) {
				if (isInteger) {
					Integer i = null;
					try {
						i = Integer.valueOf(this.getValue());
					} catch (NumberFormatException e) {}
					if (i == null) {
						errors.put("value", "Value for this trait should be an integer");
					} else {
						if (trait.getRange().equals(NON_NEGATIVE_INTEGER_RANGE)) {
							if (i < 0) errors.put("value", "Value for this trait should be non negative");
						} else if (trait.getRange().equals(POSITIVE_INTEGER_RANGE)) {
							if (i < 1) errors.put("value", "Value for this trait should be a positive integer");
						}
					}
				} else if (isDecimal) {
					try {
						Double.valueOf(this.getValue().replace(",", "."));
					} catch (NumberFormatException e) {
						errors.put("value", "Value for this trait should be a decimal number");
					}
				}
			}
		}

		if (!isTaxon && this.getObjectTaxonLifeStage() != null) {
			errors.put("objectTaxonLifeStage", "Only available for taxon traits");
		}

		if (this.getValue() != null && !trait.getEnumerations().isEmpty()) {
			boolean found = false;
			for (TraitEnumerationValue e : trait.getEnumerations()) {
				if (e.getDataEntryName().equalsIgnoreCase(this.getValue())) {
					found = true;
					break;
				}
			}
			if (!found) errors.put("value", "Must be one of " + trait.getEnumerations().stream().map(e->e.getDataEntryName()).collect(Collectors.joining(", ")));
		}
		return errors;
	}

	private boolean unitsMatch(Trait trait, TraitDAO dao) {
		if (this.getUnit().equals(trait.getBaseUnit())) return true;

		UnitOfMeasurement traitUoM = null;
		try {
			traitUoM = dao.units().get(trait.getBaseUnit());
		} catch (EntityNotFoundException e) {
			return false; // something is wrong with the trait itself... this should not happen
		}
		try {
			UnitOfMeasurement thisUoM = dao.units().get(this.getUnit());
			return thisUoM.getBase().equals(traitUoM.getBase());
		} catch (EntityNotFoundException e) {
			// missing/invalid unit of measurement is already validated by base validations, no need for double validation error message
			return true;
		}
	}

	private boolean isTaxon(Trait trait) {
		return TAXON_RANGE.equals(trait.getRange());
	}

	private boolean isNumeric(Trait trait) {
		return NUMERIC_RANGES.contains(trait.getRange());
	}

	private boolean isInteger(Trait trait) {
		return INTEGER_RANGES.contains(trait.getRange());
	}

	private boolean isDecimal(Trait trait) {
		return DECIMAL_RANGE.equals(trait.getRange());
	}

}