package fi.luomus.traitdb.services;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.traitdb.dao.TraitDAO.TraitDAOEntity;
import fi.luomus.traitdb.models.Dataset;
import fi.luomus.traitdb.models.DatasetPermissions;
import fi.luomus.traitdb.services.TraitAPI.Method;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIAuthorizedRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIEndpoint;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIRequest;
import fi.luomus.traitdb.services.TraitAPI.TraitAPIResource;

public class TraitAPIDatasets implements TraitAPIResource {

	private final TraitAPIRequest req;
	private final TraitDAOEntity<Dataset> dao;

	public TraitAPIDatasets(TraitAPIRequest req) {
		this.req = req;
		this.dao = req.getDAO().datasets();
	}

	@Override
	public TraitAPIEndpoint endpoint() {
		String endpoint = req.getEndpoint();
		if (Method.GET == req.getMethod()) {
			if ("".equals(endpoint)) return getAll();
			if ("{id}".equals(endpoint)) return getSingle(req.getId());
		}
		if (Method.POST == req.getMethod()) {
			if ("validate".equals(endpoint)) return validate(req.getJson());
			if ("validate-update/{id}".equals(endpoint)) return validateUpdate(req.getId(), req.getJson());
			if ("validate-delete/{id}".equals(endpoint)) return validateDelete(req.getId());
			if ("".equals(endpoint)) return insert(req.getJson());
		}
		if (Method.PUT == req.getMethod()) {
			if ("{id}".equals(endpoint)) return update(req.getId(), req.getJson());
		}
		if (Method.DELETE == req.getMethod()) {
			if ("{id}".equals(endpoint)) return delete(req.getId());
		}
		throw req.noEndpointException();
	}

	private TraitAPIAuthorizedRequest requirePermissions(Qname id, TraitAPIEndpoint endpoint) throws IllegalAccessException {
		if (req.getDAO().permissions().get(id).getUserIds().contains(req.getUserId())) {
			return new TraitAPIAuthorizedRequest(endpoint);
		}
		throw new IllegalAccessException("No permissions to dataset");
	}

	private void validateIdMatches(Qname id, Dataset dataset) {
		if (dataset.getId() == null || !dataset.getId().equals(id)) throw new IllegalArgumentException("Id in path and model do not match");
	}

	private TraitAPIEndpoint delete(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				dao.validateDelete(id).requirePass();
				dao.delete(id);
				return "ok";
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(id, this);
			}
		};
	}

	private TraitAPIEndpoint validateDelete(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.validateDelete(id);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(id, this);
			}
		};
	}

	private TraitAPIEndpoint update(Qname id, JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				Dataset dataset = new Dataset(json);
				validateIdMatches(id, dataset);
				dao.validate(dataset, false).requirePass();
				Dataset existing = dao.get(id);
				dataset = dao.update(dataset);
				if (existing.isPublished() != dataset.isPublished()) {
					req.getDAO().search().reprocessDatasetBackground(dataset);
				}
				return dataset;
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(id, this);
			}
		};
	}

	private TraitAPIEndpoint validateUpdate(Qname id, JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				Dataset dataset = new Dataset(json);
				validateIdMatches(id, dataset);
				return dao.validate(dataset, false);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return requirePermissions(id, this);
			}
		};
	}

	private TraitAPIEndpoint insert(JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				Dataset dataset = new Dataset(json);
				dao.validate(dataset, true).requirePass();
				dataset = dao.insert(dataset);
				DatasetPermissions permissions = new DatasetPermissions();
				permissions.setDatasetId(dataset.getId());
				permissions.getUserIds().add(req.getUserId());
				req.getDAO().permissions().insert(permissions);
				return dataset;
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return new TraitAPIAuthorizedRequest(this);
			}
		};
	}

	private TraitAPIEndpoint validate(JSONObject json) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				Dataset dataset = new Dataset(json);
				return dao.validate(dataset, true);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return new TraitAPIAuthorizedRequest(this);
			}
		};
	}

	private TraitAPIEndpoint getSingle(Qname id) {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.get(id);
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return new TraitAPIAuthorizedRequest(this);
			}
		};
	}

	private TraitAPIEndpoint getAll() {
		return new TraitAPIEndpoint() {

			@Override
			public Object execute() throws Exception {
				return dao.getAll();
			}

			@Override
			public TraitAPIAuthorizedRequest authorize() throws IllegalAccessException {
				return new TraitAPIAuthorizedRequest(this);
			}
		};
	}

}
