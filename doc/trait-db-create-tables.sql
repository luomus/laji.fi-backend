CREATE USER trait_prod identified by ...;
CREATE USER trait_staging identified by ...;
CREATE USER trait_tests identified by ...;

DROP TABLE TRAITVALUE;
DROP TABLE SUBJECT;
DROP TABLE DATASETPERMISSION;
DROP TABLE DATASET;


CREATE TABLE DATASET (
	id				VARCHAR2(20 CHAR) PRIMARY KEY NOT NULL,
	published		NUMBER(1) DEFAULT 0 NOT NULL,
	share_finbif	NUMBER(1) DEFAULT 0 NOT NULL
);

CREATE TABLE DATASETPERMISSION (
    dataset_id		CONSTRAINT fk_permission_dataset REFERENCES dataset(id) NOT NULL,
    user_id			VARCHAR2(100 CHAR) NOT NULL,
    CONSTRAINT pk_datasetpermission PRIMARY KEY (dataset_id, user_id)
);
CREATE INDEX ix_datasetpermission_user_id ON datasetpermission(user_id);

CREATE SEQUENCE td_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE SUBJECT (
	id						NUMBER DEFAULT td_seq.nextval PRIMARY KEY,
	dataset_id				CONSTRAINT fk_subject_dataset REFERENCES dataset(id) NOT NULL,
	type					VARCHAR2(100 CHAR) NOT NULL,
	basis_of_record			VARCHAR2(100 CHAR),
	finbif_occurrence_id	VARCHAR2(400 CHAR),
	gbif_occurrence_id		VARCHAR2(400 CHAR),
	other_occurrence_id		VARCHAR2(400 CHAR),
	kingdom					VARCHAR2(400 CHAR),
	scientific_name			VARCHAR2(400 CHAR),
	author					VARCHAR2(400 CHAR),
	functional_group_name	VARCHAR2(400 CHAR),
	taxon_id				VARCHAR2(100 CHAR),
	gbif_taxon_id			NUMBER(12),
	other_taxon_id			VARCHAR2(400 CHAR),
	sex						VARCHAR2(100 CHAR),
	life_stage				VARCHAR2(100 CHAR),
	age_years				NUMBER(13, 6),
	individual_count		NUMBER(4),
	date_begin				DATE,
	date_end				DATE,
	year_begin				NUMBER(4),
	year_end				NUMBER(4),
	season_begin			NUMBER(4),
	season_end				NUMBER(4),
	lat						NUMBER(9,7),
	lon						NUMBER(9,7),
	lat_min					NUMBER(9,7),
	lat_max					NUMBER(9,7),
	lon_min					NUMBER(9,7),
	lon_max					NUMBER(9,7),
	coordinate_accuracy		NUMBER(12),
	elevation				NUMBER(12),
	higher_geography		VARCHAR2(400 CHAR),
	country					VARCHAR2(400 CHAR),
	municipality			VARCHAR2(400 CHAR),
	locality				VARCHAR2(400 CHAR),
	location_identifiers	VARCHAR2(400 CHAR),
	habitat					VARCHAR2(400 CHAR),
	occurrence_remarks		VARCHAR2(4000 CHAR),
	measurement_determined_by	VARCHAR2(400 CHAR),
	measurement_determined_date	DATE,
	created					DATE DEFAULT SYSDATE NOT NULL,
	modified				DATE,
	created_by				VARCHAR2(400 CHAR) NOT NULL,
	modified_by				VARCHAR2(400 CHAR)
);
CREATE INDEX ix_subject_dataset_id ON SUBJECT(dataset_id);

CREATE TABLE TRAITVALUE (
	id						NUMBER DEFAULT td_seq.nextval PRIMARY KEY,
	subject_id				CONSTRAINT fk_traitvalue_subject REFERENCES subject(id) ON DELETE CASCADE NOT NULL,
	trait_id				VARCHAR2(100 CHAR) NOT NULL,
	value					VARCHAR2(1000 CHAR) NOT NULL,
	unit					VARCHAR2(100 CHAR),
	measurement_accuracy	NUMBER(10, 8),
	statistical_method		VARCHAR2(100 CHAR),
	object_taxon_life_stage	VARCHAR2(100 CHAR),
	warnings				NUMBER(1) DEFAULT 0 NOT NULL,
	measurement_remarks		VARCHAR2(4000 CHAR),
	reference				VARCHAR2(4000 CHAR)
);
CREATE INDEX ix_traitvalue_subject_id ON TRAITVALUE(subject_id);
CREATE INDEX ix_traitvalue_trait_id ON TRAITVALUE(trait_id);
CREATE INDEX ix_traitvalue_value ON TRAITVALUE(value);


--- TRIPLESTORE CLASSES

CREATE SEQUENCE rdf_tdf_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE rdf_td_seq START WITH 1 INCREMENT BY 1;
GRANT SELECT ON rdf_tdf_seq TO XXXXX;
GRANT SELECT ON rdf_td_seq TO XXXXX;

exec addresource('TDF.trait');
exec addresource('TDF.partOfGroup');
exec addresource('TDF.dataEntryName');
exec addresource('TDF.name');
exec addresource('TDF.description');
exec addresource('TDF.exampleValues');
exec addresource('TDF.baseUnit');
exec addresource('TDF.reference');
exec addresource('TDF.externalIdentifier');
exec addresource('TDF.traitGroup');
exec addresource('TDF.traitEnumValue');
exec addresource('TDF.unitOfMeasurement');
exec addresource('TDF.isBaseUnit');
exec addresource('TDF.baseUnit');
exec addresource('TDF.baseConversion');
exec addresource('TDF.hasEnumeration');
exec addresource('MY.additionalIdentifier');
exec addresource('MY.doi');

exec addstatement('TDF.trait', 'rdf:type', 'rdfs:Class');
exec addstatement('TDF.traitGroup', 'rdf:type', 'rdfs:Class');
exec addstatement('TDF.traitEnumValue', 'rdf:type', 'rdfs:Class');
exec addstatement('TDF.unitOfMeasurement', 'rdf:type', 'rdfs:Class');

exec addstatementL('TDF.trait', 'rdfs:label', 'Trait', 'en');
exec addstatementL('TDF.traitGroup', 'rdfs:label', 'Trait value', 'en');
exec addstatementL('TDF.traitEnumValue', 'rdfs:label', 'Trait enumeration', 'en');
exec addstatementL('TDF.unitOfMeasurement', 'rdfs:label', 'Unit of measurement', 'en');

exec addstatement('TDF.partOfGroup', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.partOfGroup', 'rdfs:label', 'Part of trait group', 'en');
exec addstatement('TDF.partOfGroup', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.partOfGroup', 'rdfs:range', 'TDF.traitGroup');
exec addstatementL('TDF.partOfGroup', 'xsd:minOccurs', '1');
exec addstatementL('TDF.partOfGroup', 'xsd:maxOccurs', '1');

exec addstatement('TDF.dataEntryName', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.dataEntryName', 'rdfs:label', 'Data entry short name/code', 'en');
exec addstatement('TDF.dataEntryName', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.dataEntryName', 'rdfs:domain', 'TDF.traitEnumValue');
exec addstatement('TDF.dataEntryName', 'rdfs:range', 'xsd:string');
exec addstatementL('TDF.dataEntryName', 'xsd:minOccurs', '1');
exec addstatementL('TDF.dataEntryName', 'xsd:maxOccurs', '1');

exec addstatement('TDF.name', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.name', 'rdfs:label', 'Name', 'en');
exec addstatement('TDF.name', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.name', 'rdfs:domain', 'TDF.traitGroup');
exec addstatement('TDF.name', 'rdfs:domain', 'TDF.traitEnumValue');
exec addstatement('TDF.name', 'rdfs:range', 'xsd:string');
exec addstatementL('TDF.name', 'xsd:minOccurs', '1');
exec addstatementL('TDF.name', 'xsd:maxOccurs', '1');

exec addstatement('TDF.description', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.description', 'rdfs:label', 'Description', 'en');
exec addstatement('TDF.description', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.description', 'rdfs:domain', 'TDF.traitGroup');
exec addstatement('TDF.description', 'rdfs:domain', 'TDF.traitEnumValue');
exec addstatement('TDF.description', 'rdfs:range', 'xsd:string');
exec addstatementL('TDF.description', 'xsd:minOccurs', '1');
exec addstatementL('TDF.description', 'xsd:maxOccurs', '1');

exec addstatement('TDF.exampleValues', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.exampleValues', 'rdfs:label', 'Example values', 'en');
exec addstatement('TDF.exampleValues', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.exampleValues', 'rdfs:range', 'xsd:string');
exec addstatementL('TDF.exampleValues', 'xsd:minOccurs', '0');
exec addstatementL('TDF.exampleValues', 'xsd:maxOccurs', '1');

exec addstatement('TDF.baseUnit', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.baseUnit', 'rdfs:label', 'Base unit of measurement', 'en');
exec addstatement('TDF.baseUnit', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.baseUnit', 'rdfs:domain', 'TDF.unitOfMeasurement');
exec addstatement('TDF.baseUnit', 'rdfs:range', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.baseUnit', 'xsd:minOccurs', '0');
exec addstatementL('TDF.baseUnit', 'xsd:maxOccurs', '1');

exec addstatement('rdfs:range', 'rdfs:domain', 'TDF.trait');

exec addstatement('TDF.reference', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.reference', 'rdfs:label', 'Reference/publication for this trait', 'en');
exec addstatement('TDF.reference', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.reference', 'rdfs:range', 'xsd:string');
exec addstatementL('TDF.reference', 'xsd:minOccurs', '0');
exec addstatementL('TDF.reference', 'xsd:maxOccurs', 'unbounded');

exec addstatement('TDF.externalIdentifier', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.externalIdentifier', 'rdfs:label', 'Identifier of this trait in some other vocabulary', 'en');
exec addstatement('TDF.externalIdentifier', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.externalIdentifier', 'rdfs:range', 'xsd:string');
exec addstatementL('TDF.externalIdentifier', 'xsd:minOccurs', '0');
exec addstatementL('TDF.externalIdentifier', 'xsd:maxOccurs', 'unbounded');

exec addstatement('TDF.hasEnumeration', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.hasEnumeration', 'rdfs:label', 'Has enumeration', 'en');
exec addstatement('TDF.hasEnumeration', 'rdfs:domain', 'TDF.trait');
exec addstatement('TDF.hasEnumeration', 'rdfs:range', 'TDF.traitEnumValue');
exec addstatementL('TDF.hasEnumeration', 'xsd:minOccurs', '0');
exec addstatementL('TDF.hasEnumeration', 'xsd:maxOccurs', 'unbounded');

exec addstatement('sortOrder', 'rdfs:domain', 'TDF.traitEnumValue');

exec addstatement('rdfs:label', 'rdfs:domain', 'TDF.unitOfMeasurement');

exec addstatement('TDF.isBaseUnit', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.isBaseUnit', 'rdfs:label', 'Is base unit?', 'en');
exec addstatement('TDF.isBaseUnit', 'rdfs:domain', 'TDF.unitOfMeasurement');
exec addstatement('TDF.isBaseUnit', 'rdfs:range', 'xsd:boolean');
exec addstatementL('TDF.isBaseUnit', 'xsd:minOccurs', '1');
exec addstatementL('TDF.isBaseUnit', 'xsd:maxOccurs', '1');

exec addstatement('TDF.baseConversion', 'rdf:type', 'rdf:Property');
exec addstatementL('TDF.baseConversion', 'rdfs:label', 'Conversion factor', 'en');
exec addstatement('TDF.baseConversion', 'rdfs:domain', 'TDF.unitOfMeasurement');
exec addstatement('TDF.baseConversion', 'rdfs:range', 'xsd:decimal');
exec addstatementL('TDF.baseConversion', 'xsd:minOccurs', '1');
exec addstatementL('TDF.baseConversion', 'xsd:maxOccurs', '1');

exec addstatement('MY.additionalIdentifier', 'rdf:type', 'rdf:Property');
exec addstatementL('MY.additionalIdentifier', 'rdfs:label', 'Identifier of this dataset/collection in other databases', 'en');
exec addstatement('MY.additionalIdentifier', 'rdfs:domain', 'MY.collection');
exec addstatement('MY.additionalIdentifier', 'rdfs:range', 'xsd:string');
exec addstatementL('MY.additionalIdentifier', 'xsd:minOccurs', '0');
exec addstatementL('MY.additionalIdentifier', 'xsd:maxOccurs', 'unbounded');

exec addstatement('MY.doi', 'rdf:type', 'rdf:Property');
exec addstatementL('MY.doi', 'rdfs:label', 'DOI provided by FinBIF', 'en');
exec addstatement('MY.doi', 'rdfs:domain', 'MY.collection');
exec addstatement('MY.doi', 'rdfs:range', 'xsd:string');
exec addstatementL('MY.doi', 'xsd:minOccurs', '0');
exec addstatementL('MY.doi', 'xsd:maxOccurs', '1');

exec addresource('TDF.typeEnum');
exec addresource('TDF.typeIndividual');
exec addresource('TDF.typePopulationGroup');
exec addresource('TDF.typeTaxon');
exec addstatement('TDF.typeEnum', 'rdf:type', 'rdf:Alt');
exec addstatement('TDF.typeEnum', 'rdf:_1', 'TDF.typeIndividual');
exec addstatement('TDF.typeEnum', 'rdf:_2', 'TDF.typePopulationGroup');
exec addstatement('TDF.typeEnum', 'rdf:_3', 'TDF.typeTaxon');

exec addstatementL('TDF.typeIndividual', 'rdfs:label', 'Individual');
exec addstatementL('TDF.typePopulationGroup', 'rdfs:label', 'Population group');
exec addstatementL('TDF.typeTaxon', 'rdfs:label', 'Taxon');
exec addstatementL('TDF.typeIndividual', 'rdfs:label', 'Individual', 'en');
exec addstatementL('TDF.typePopulationGroup', 'rdfs:label', 'Population group', 'en');
exec addstatementL('TDF.typeTaxon', 'rdfs:label', 'Taxon', 'en');

exec addstatementL('TDF.typeIndividual', 'rdfs:label', 'Yksilö', 'fi');
exec addstatementL('TDF.typePopulationGroup', 'rdfs:label', 'Populaatioryhmä', 'fi');
exec addstatementL('TDF.typeTaxon', 'rdfs:label', 'Taksoni', 'fi');

exec addstatementL('TDF.typeIndividual', 'rdfs:label', 'Individ', 'sv');
exec addstatementL('TDF.typePopulationGroup', 'rdfs:label', 'Population grupp', 'sv');
exec addstatementL('TDF.typeTaxon', 'rdfs:label', 'Taxon', 'sv');

exec addresource('TDF.statisticalMethodEnum');
exec addresource('TDF.statisticalMethodMedian');
exec addresource('TDF.statisticalMethodAvg');
exec addresource('TDF.statisticalMethodMin');
exec addresource('TDF.statisticalMethodMax');
exec addresource('TDF.statisticalMethodMode');
exec addresource('TDF.statisticalMethodSD');
exec addstatement('TDF.statisticalMethodEnum', 'rdf:type', 'rdf:Alt');
exec addstatement('TDF.statisticalMethodEnum', 'rdf:_1', 'TDF.statisticalMethodMin');
exec addstatement('TDF.statisticalMethodEnum', 'rdf:_2', 'TDF.statisticalMethodMax');
exec addstatement('TDF.statisticalMethodEnum', 'rdf:_3', 'TDF.statisticalMethodAvg');
exec addstatement('TDF.statisticalMethodEnum', 'rdf:_4', 'TDF.statisticalMethodMedian');
exec addstatement('TDF.statisticalMethodEnum', 'rdf:_5', 'TDF.statisticalMethodSD');
exec addstatement('TDF.statisticalMethodEnum', 'rdf:_6', 'TDF.statisticalMethodMode');

exec addstatementL('TDF.statisticalMethodMedian', 'rdfs:label', 'Median');
exec addstatementL('TDF.statisticalMethodAvg', 'rdfs:label', 'Average');
exec addstatementL('TDF.statisticalMethodMin', 'rdfs:label', 'Minimum');
exec addstatementL('TDF.statisticalMethodMax', 'rdfs:label', 'Maximum');
exec addstatementL('TDF.statisticalMethodMode', 'rdfs:label', 'Mode');
exec addstatementL('TDF.statisticalMethodSD', 'rdfs:label', 'Standard Deviation');

exec addstatementL('TDF.statisticalMethodMedian', 'rdfs:label', 'Median', 'en');
exec addstatementL('TDF.statisticalMethodAvg', 'rdfs:label', 'Average', 'en');
exec addstatementL('TDF.statisticalMethodMin', 'rdfs:label', 'Minimum', 'en');
exec addstatementL('TDF.statisticalMethodMax', 'rdfs:label', 'Maximum', 'en');
exec addstatementL('TDF.statisticalMethodMode', 'rdfs:label', 'Mode', 'en');
exec addstatementL('TDF.statisticalMethodSD', 'rdfs:label', 'Standard Deviation', 'en');

exec addstatementL('TDF.statisticalMethodMedian', 'rdfs:label', 'Mediaani', 'fi');
exec addstatementL('TDF.statisticalMethodAvg', 'rdfs:label', 'Keskiarvo', 'fi');
exec addstatementL('TDF.statisticalMethodMin', 'rdfs:label', 'Vähimmäisarvo', 'fi');
exec addstatementL('TDF.statisticalMethodMax', 'rdfs:label', 'Maksimiarvo', 'fi');
exec addstatementL('TDF.statisticalMethodMode', 'rdfs:label', 'Moodi', 'fi');
exec addstatementL('TDF.statisticalMethodSD', 'rdfs:label', 'Keskihajonta', 'fi');

exec addstatementL('TDF.statisticalMethodMedian', 'rdfs:label', 'Median', 'sv');
exec addstatementL('TDF.statisticalMethodAvg', 'rdfs:label', 'Genomsnitt', 'sv');
exec addstatementL('TDF.statisticalMethodMin', 'rdfs:label', 'Minimum', 'sv');
exec addstatementL('TDF.statisticalMethodMax', 'rdfs:label', 'Maximum', 'sv');
exec addstatementL('TDF.statisticalMethodMode', 'rdfs:label', 'Modus', 'sv');
exec addstatementL('TDF.statisticalMethodSD', 'rdfs:label', 'Standardavvikelse', 'sv');

exec addresource('TDF.umM');
exec addstatement('TDF.umM', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umM', 'rdfs:label', 'm');
exec addstatementL('TDF.umM', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umM', 'TDF.baseUnit', 'TDF.umM');
exec addstatementL('TDF.umM', 'TDF.baseConversion', '1');

exec addresource('TDF.umUM');
exec addstatement('TDF.umUM', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umUM', 'rdfs:label', 'μm');
exec addstatementL('TDF.umUM', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umUM', 'TDF.baseUnit', 'TDF.umM');
exec addstatementL('TDF.umUM', 'TDF.baseConversion', '0.000001');

exec addresource('TDF.umNM');
exec addstatement('TDF.umNM', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umNM', 'rdfs:label', 'nm');
exec addstatementL('TDF.umNM', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umNM', 'TDF.baseUnit', 'TDF.umM');
exec addstatementL('TDF.umNM', 'TDF.baseConversion', '0.000000001');

exec addresource('TDF.umMM');
exec addstatement('TDF.umMM', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umMM', 'rdfs:label', 'mm');
exec addstatementL('TDF.umMM', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umMM', 'TDF.baseUnit', 'TDF.umM');
exec addstatementL('TDF.umMM', 'TDF.baseConversion', '0.001');

exec addresource('TDF.umCM');
exec addstatement('TDF.umCM', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umCM', 'rdfs:label', 'cm');
exec addstatementL('TDF.umCM', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umCM', 'TDF.baseUnit', 'TDF.umM');
exec addstatementL('TDF.umCM', 'TDF.baseConversion', '0.01');

exec addresource('TDF.umKM');
exec addstatement('TDF.umKM', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umKM', 'rdfs:label', 'km');
exec addstatementL('TDF.umKM', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umKM', 'TDF.baseUnit', 'TDF.umM');
exec addstatementL('TDF.umKM', 'TDF.baseConversion', '1000');

exec addresource('TDF.umG');
exec addstatement('TDF.umG', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umG', 'rdfs:label', 'g');
exec addstatementL('TDF.umG', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umG', 'TDF.baseUnit', 'TDF.umG');
exec addstatementL('TDF.umG', 'TDF.baseConversion', '1');

exec addresource('TDF.umKG');
exec addstatement('TDF.umKG', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umKG', 'rdfs:label', 'kg');
exec addstatementL('TDF.umKG', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umKG', 'TDF.baseUnit', 'TDF.umG');
exec addstatementL('TDF.umKG', 'TDF.baseConversion', '1000');

exec addresource('TDF.umA');
exec addstatement('TDF.umA', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umA', 'rdfs:label', 'years');
exec addstatementL('TDF.umA', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umA', 'TDF.baseUnit', 'TDF.umA');
exec addstatementL('TDF.umA', 'TDF.baseConversion', '1');

exec addresource('TDF.umS');
exec addstatement('TDF.umS', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umS', 'rdfs:label', 's');
exec addstatementL('TDF.umS', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umS', 'TDF.baseUnit', 'TDF.umS');
exec addstatementL('TDF.umS', 'TDF.baseConversion', '1');

exec addresource('TDF.umL');
exec addstatement('TDF.umL', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umL', 'rdfs:label', 'L');
exec addstatementL('TDF.umL', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umL', 'TDF.baseUnit', 'TDF.umL');
exec addstatementL('TDF.umL', 'TDF.baseConversion', '1');

exec addresource('TDF.umML');
exec addstatement('TDF.umML', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umML', 'rdfs:label', 'mL');
exec addstatementL('TDF.umML', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umML', 'TDF.baseUnit', 'TDF.umL');
exec addstatementL('TDF.umML', 'TDF.baseConversion', '0.001');

exec addresource('TDF.umMOL');
exec addstatement('TDF.umMOL', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umMOL', 'rdfs:label', 'mol');
exec addstatementL('TDF.umMOL', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umMOL', 'TDF.baseUnit', 'TDF.umMOL');
exec addstatementL('TDF.umMOL', 'TDF.baseConversion', '1');

exec addresource('TDF.umMMOL');
exec addstatement('TDF.umMMOL', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umMMOL', 'rdfs:label', 'mmol');
exec addstatementL('TDF.umMMOL', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umMMOL', 'TDF.baseUnit', 'TDF.umMOL');
exec addstatementL('TDF.umMMOL', 'TDF.baseConversion', '0.001');

exec addresource('TDF.umUMOL');
exec addstatement('TDF.umUMOL', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umUMOL', 'rdfs:label', 'μmol');
exec addstatementL('TDF.umUMOL', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umUMOL', 'TDF.baseUnit', 'TDF.umMOL');
exec addstatementL('TDF.umUMOL', 'TDF.baseConversion', '0.000001');

exec addresource('TDF.umHZ');
exec addstatement('TDF.umHZ', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umHZ', 'rdfs:label', 'Hz');
exec addstatementL('TDF.umHZ', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umHZ', 'TDF.baseUnit', 'TDF.umHZ');
exec addstatementL('TDF.umHZ', 'TDF.baseConversion', '1');

exec addresource('TDF.umP');
exec addstatement('TDF.umP', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umP', 'rdfs:label', '%');
exec addstatementL('TDF.umP', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umP', 'TDF.baseUnit', 'TDF.umP');
exec addstatementL('TDF.umP', 'TDF.baseConversion', '1');

exec addresource('TDF.umPPT');
exec addstatement('TDF.umPPT', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umPPT', 'rdfs:label', '‰');
exec addstatementL('TDF.umPPT', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umPPT', 'TDF.baseUnit', 'TDF.umP');
exec addstatementL('TDF.umPPT', 'TDF.baseConversion', '0.1');

exec addresource('TDF.umPPM');
exec addstatement('TDF.umPPM', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umPPM', 'rdfs:label', 'ppm');
exec addstatementL('TDF.umPPM', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umPPM', 'TDF.baseUnit', 'TDF.umPPM');
exec addstatementL('TDF.umPPM', 'TDF.baseConversion', '1');

exec addresource('TDF.umC');
exec addstatement('TDF.umC', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umC', 'rdfs:label', '°C');
exec addstatementL('TDF.umC', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umC', 'TDF.baseUnit', 'TDF.umC');
exec addstatementL('TDF.umC', 'TDF.baseConversion', '1');

exec addresource('TDF.umM2');
exec addstatement('TDF.umM2', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umM2', 'rdfs:label', 'm²');
exec addstatementL('TDF.umM2', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umM2', 'TDF.baseUnit', 'TDF.umM2');
exec addstatementL('TDF.umM2', 'TDF.baseConversion', '1');

exec addresource('TDF.umMM2');
exec addstatement('TDF.umMM2', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umMM2', 'rdfs:label', 'mm²');
exec addstatementL('TDF.umMM2', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umMM2', 'TDF.baseUnit', 'TDF.umM2');
exec addstatementL('TDF.umMM2', 'TDF.baseConversion', '0.000001');

exec addresource('TDF.umCM2');
exec addstatement('TDF.umCM2', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umCM2', 'rdfs:label', 'cm²');
exec addstatementL('TDF.umCM2', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umCM2', 'TDF.baseUnit', 'TDF.umM2');
exec addstatementL('TDF.umCM2', 'TDF.baseConversion', '0.001');

exec addresource('TDF.umARE');
exec addstatement('TDF.umARE', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umARE', 'rdfs:label', 'are');
exec addstatementL('TDF.umARE', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umARE', 'TDF.baseUnit', 'TDF.umM2');
exec addstatementL('TDF.umARE', 'TDF.baseConversion', '100');

exec addresource('TDF.umHA');
exec addstatement('TDF.umHA', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umHA', 'rdfs:label', 'ha');
exec addstatementL('TDF.umHA', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umHA', 'TDF.baseUnit', 'TDF.umM2');
exec addstatementL('TDF.umHA', 'TDF.baseConversion', '10000');

exec addresource('TDF.umKM2');
exec addstatement('TDF.umKM2', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umKM2', 'rdfs:label', 'km²');
exec addstatementL('TDF.umKM2', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umKM2', 'TDF.baseUnit', 'TDF.umM2');
exec addstatementL('TDF.umKM2', 'TDF.baseConversion', '1000000');

exec addresource('TDF.umM3');
exec addstatement('TDF.umM3', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umM3', 'rdfs:label', 'm³');
exec addstatementL('TDF.umM3', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umM3', 'TDF.baseUnit', 'TDF.umM3');
exec addstatementL('TDF.umM3', 'TDF.baseConversion', '1');

exec addresource('TDF.umMM3');
exec addstatement('TDF.umMM3', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umMM3', 'rdfs:label', 'mm³');
exec addstatementL('TDF.umMM3', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umMM3', 'TDF.baseUnit', 'TDF.umM3');
exec addstatementL('TDF.umMM3', 'TDF.baseConversion', '0.000000001');

exec addresource('TDF.umCM3');
exec addstatement('TDF.umCM3', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umCM3', 'rdfs:label', 'cm³');
exec addstatementL('TDF.umCM3', 'TDF.isBaseUnit', 'false');
exec addstatement('TDF.umCM3', 'TDF.baseUnit', 'TDF.umM3');
exec addstatementL('TDF.umCM3', 'TDF.baseConversion', '0.000001');

exec addresource('TDF.umGMOL');
exec addstatement('TDF.umGMOL', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umGMOL', 'rdfs:label', 'g/mol');
exec addstatementL('TDF.umGMOL', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umGMOL', 'TDF.baseUnit', 'TDF.umGMOL');
exec addstatementL('TDF.umGMOL', 'TDF.baseConversion', '1');

exec addresource('TDF.umMOLL');
exec addstatement('TDF.umMOLL', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umMOLL', 'rdfs:label', 'mol/L');
exec addstatementL('TDF.umMOLL', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umMOLL', 'TDF.baseUnit', 'TDF.umMOLL');
exec addstatementL('TDF.umMOLL', 'TDF.baseConversion', '1');

exec addresource('TDF.umCM2H');
exec addstatement('TDF.umCM2H', 'rdf:type', 'TDF.unitOfMeasurement');
exec addstatementL('TDF.umCM2H', 'rdfs:label', 'cm²/h');
exec addstatementL('TDF.umCM2H', 'TDF.isBaseUnit', 'true');
exec addstatement('TDF.umCM2H', 'TDF.baseUnit', 'TDF.umCM2H');
exec addstatementL('TDF.umCM2H', 'TDF.baseConversion', '1');



exec addresource('TDF.unitOfMeasurementEnum');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:type', 'rdf:Alt');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_1', 'TDF.umNM');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_2', 'TDF.umUM');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_3', 'TDF.umMM');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_4', 'TDF.umCM');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_5', 'TDF.umM');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_6', 'TDF.umKM');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_10', 'TDF.umG');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_11', 'TDF.umKG');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_20', 'TDF.umA');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_21', 'TDF.umS');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_30', 'TDF.umML');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_31', 'TDF.umL');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_40', 'TDF.umMOL');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_41', 'TDF.umMMOL');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_42', 'TDF.umUMOL');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_50', 'TDF.umHZ');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_60', 'TDF.umP');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_61', 'TDF.umPPT');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_62', 'TDF.umPPM');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_70', 'TDF.umC');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_80', 'TDF.umMM2');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_81', 'TDF.umCM2');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_82', 'TDF.umM2');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_83', 'TDF.umARE');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_84', 'TDF.umHA');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_85', 'TDF.umKM2');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_90', 'TDF.umMM3');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_91', 'TDF.umCM3');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_92', 'TDF.umM3');

exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_100', 'TDF.umGMOL');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_101', 'TDF.umMOLL');
exec addstatement('TDF.unitOfMeasurementEnum', 'rdf:_102', 'TDF.umCM2H');


exec addresource('TDF.rangeEnum');
exec addstatement('TDF.rangeEnum', 'rdf:type', 'rdf:Alt');
exec addstatement('TDF.rangeEnum', 'rdf:_1', 'xsd:string');
exec addstatement('TDF.rangeEnum', 'rdf:_2', 'xsd:decimal');
exec addstatement('TDF.rangeEnum', 'rdf:_3', 'xsd:integer');
exec addstatement('TDF.rangeEnum', 'rdf:_4', 'xsd:positiveInteger');
exec addstatement('TDF.rangeEnum', 'rdf:_5', 'xsd:nonNegativeInteger');	
exec addstatement('TDF.rangeEnum', 'rdf:_6', 'xsd:boolean');
exec addstatement('TDF.rangeEnum', 'rdf:_7', 'MX.taxon');

exec addstatementL('xsd:decimal', 'rdfs:label', 'decimal', 'en');
exec addstatementL('xsd:integer', 'rdfs:label', 'integer', 'en');
exec addstatementL('xsd:nonNegativeInteger', 'rdfs:label', 'non-negative integer', 'en');
exec addstatementL('xsd:positiveInteger', 'rdfs:label', 'positive integer', 'en');
exec addstatementL('xsd:string', 'rdfs:label', 'string', 'en');
exec addstatementL('xsd:boolean', 'rdfs:label', 'boolean', 'en');
exec addstatementL('MX.taxon', 'rdfs:label', 'taxon', 'en');

exec addresource('MY.collectionTypeTrait');
exec addstatementL('MY.collectionTypeTrait', 'rdfs:label', 'Trait data', 'en');
exec addstatementL('MY.collectionTypeTrait', 'rdfs:label', 'Trait-tietoja', 'fi');
exec addstatementL('MY.collectionTypeTrait', 'rdfs:label', 'Traits/Egenskapsdata', 'sv');
exec addstatement('MY.collectionTypes', 'rdf:_20', 'MY.collectionTypeTrait');
