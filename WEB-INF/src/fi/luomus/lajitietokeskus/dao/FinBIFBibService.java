package fi.luomus.lajitietokeskus.dao;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Mapping.Pair;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLWriter;
import fi.luomus.lajitietokeskus.models.CMSContent;
import fi.luomus.lajitietokeskus.models.CMSPage;

public class FinBIFBibService {

	private static final String NAME = "name";
	private static final String IS_PART_OF = "isPartOf";
	private static final String _BLANK = "_blank";
	private static final String TARGET = "target";
	private static final String HREF = "href";
	private static final String ITEMPROP = "itemprop";
	private static final String ITEMTYPE = "itemtype";
	private static final String ITEMSCOPE = "itemscope";
	private static final String CLASS = "class";
	private static final String ARTICLE = "article";

	public interface FinBIFBibServiceDataProvider {
		JSONArray getData();
	}

	private static class HttpClientImple implements FinBIFBibServiceDataProvider {

		private final Config config;

		public HttpClientImple(Config config) {
			this.config = config;
		}

		@Override
		public JSONArray getData() {
			try (HttpClientService client = new HttpClientService()) {
				String uri = config.get("FinBIFBibURL");
				try {
					return client.contentAsJsonArray(new HttpGet(uri));
				} catch (Exception e) {
					throw new RuntimeException("Reading " + uri, e);
				}
			}
		}

	}

	private final FinBIFBibServiceDataProvider provider;

	public FinBIFBibService(Config config) {
		this.provider = new HttpClientImple(config);
	}

	public FinBIFBibService(FinBIFBibServiceDataProvider provider) {
		this.provider = provider;
	}

	public void generateContent(CMSContent cmsContent) {
		JSONArray data = provider.getData();
		cmsContent.add(generateContent("finbif-bib-top", data, 3));
		cmsContent.add(generateContent("finbif-bib-all", data));
	}

	private CMSPage generateContent(String pageName, JSONArray data) {
		return generateContent(pageName, data, Integer.MAX_VALUE);
	}

	private CMSPage generateContent(String pageName, JSONArray data, int top) {
		String content = generateContent(data, top);
		return new CMSPage(pageName, now(), now(), null, pageName, content, 1, null);
	}

	private String generateContent(JSONArray data, int top) {
		StringBuilder b = new StringBuilder();
		for (JSONObject json : top(data, top)) {
			b.append(generateContent(json)).append("\n");
		}
		return b.toString();
	}

	private String generateContent(JSONObject json) {
		String url = json.getString("url");

		Document d = new Document(ARTICLE);
		d.getRootNode().addAttribute(ITEMSCOPE).addAttribute(ITEMTYPE, "https://schema.org/ScholarlyArticle").addAttribute(CLASS, "publication");

		Node header = d.getRootNode().addChildNode("header");
		header.addChildNode("h1").addAttribute(ITEMPROP, NAME).addAttribute(CLASS, "publication-title")
		.addChildNode("a").addAttribute(HREF, url).addAttribute(TARGET, _BLANK).setContents(json.getString("title"));
		header.addChildNode("link").addAttribute(ITEMPROP, "url").addAttribute(HREF, url);

		Node authors = header.addChildNode("p").addAttribute(CLASS, "authors");
		List<String> authorNames = json.getArray("author").iterateAsObject().stream().map(o->authorName(o)).filter(s->given(s)).collect(Collectors.toList());
		int i = 0;
		for (String authorName : authorNames) {
			authors.addChildNode("span").addAttribute(ITEMPROP, "author publisher").addAttribute(CLASS, "author-name").setContents(authorName);
			i++;
			if (i <= authorNames.size() - 2) {
				delimeter(", ", authors);
			}
			if (i == authorNames.size() - 1) {
				delimeter(" & ", authors);
			}
		}

		Node publication = header.addChildNode("p");

		publication.addChildNode("span").addAttribute(ITEMPROP, IS_PART_OF).addAttribute(ITEMSCOPE).addAttribute(ITEMTYPE, "https://schema.org/Periodical")
		.addChildNode("span").addAttribute(ITEMPROP, NAME).addAttribute(CLASS, "publication-journal").setContents(json.getString("containerTitle"));

		String volume = json.getString("volume");
		if (given(volume)) {
			delimeter(" ", publication);
			publication.addChildNode("span").addAttribute(ITEMPROP, IS_PART_OF).addAttribute(ITEMSCOPE).addAttribute(ITEMTYPE, "https://schema.org/PublicationVolume")
			.addChildNode("span").addAttribute(ITEMPROP, "volumeNumber").addAttribute(CLASS, "publication-volume").setContents(volume);
		}

		String issue = json.getString("issue");
		if (given(issue)) {
			delimeter(":", publication);
			publication.addChildNode("span").addAttribute(ITEMPROP, IS_PART_OF).addAttribute(ITEMSCOPE).addAttribute(ITEMTYPE, "https://schema.org/PublicationIssue")
			.addChildNode("span").addAttribute(ITEMPROP, "issueNumber").addAttribute(CLASS, "publication-issue").setContents(issue);
		}

		Pair<Integer> pages = pages(json);
		if (pages != null) {
			delimeter(" ", publication);
			publication.addChildNode("span").addAttribute(ITEMPROP, "pageStart").addAttribute(CLASS, "publication-page-start").setContents(pages.getValue1());
			if (pages.getValue2() != null) {
				delimeter("–", publication);
				publication.addChildNode("span").addAttribute(ITEMPROP, "pageEnd").addAttribute(CLASS, "publication-page-end").setContents(pages.getValue2());
			}
		}

		String displayTime = displayTime(json);
		if (given(displayTime)) {
			delimeter(" ", publication);
			publication.addChildNode("time").addAttribute("datetime", dateTime(json)).addAttribute(ITEMPROP, "datePublished").setContents(displayTime);
		}

		header.addChildNode("p").addAttribute(CLASS, "publication-doi")
		.addChildNode("a").addAttribute(ITEMPROP, "sameAs").addAttribute(HREF, url).addAttribute(TARGET, _BLANK).setContents(json.getString("doi"));

		return new XMLWriter(d).generateXML(false).replace("\r", "").replace("\n", "");
	}

	private void delimeter(String delimeter, Node target) {
		Node n = target.addChildNode("span");
		if (delimeter.contains(" ")) {
			n.contentIsXML(true);
		}
		if (delimeter.trim().isEmpty()) {
			delimeter = "&nbsp;";
		}
		n.setContents(delimeter);
	}

	private Pair<Integer> pages(JSONObject json) {
		String page = json.getString("page");
		if (!given(page)) return null;
		Integer start = null;
		Integer end = null;
		try {
			if (page.contains("-")) {
				start = Integer.valueOf(page.split(Pattern.quote("-"))[0]);
				end = Integer.valueOf(page.split(Pattern.quote("-"))[1]);
			} else {
				start = Integer.valueOf(page);
			}
		} catch (Exception e) {
			return null;
		}
		return new Pair<>(start, end);
	}

	private String displayTime(JSONObject json) {
		String month = json.getString("month");
		Integer year = json.hasKey("year") ? json.getInteger("year") : null;
		if (year == null) return null;
		if (!given(month)) return "(" + String.valueOf(year) + ")";
		return "(" + month + " " + year + ")";
	}

	private String dateTime(JSONObject json) {
		String month = toMonth(json.getString("month"));
		Integer year = json.hasKey("year") ? json.getInteger("year") : null;
		if (year == null) return null;
		if (!given(month)) return String.valueOf(year);
		return year + "-" + month;
	}

	private static final Map<String, String> MONTHS;
	static {
		MONTHS = new HashMap<>();
		MONTHS.put("January", "01");
		MONTHS.put("February", "02");
		MONTHS.put("March", "03");
		MONTHS.put("April", "04");
		MONTHS.put("May", "05");
		MONTHS.put("June", "06");
		MONTHS.put("July", "07");
		MONTHS.put("August", "08");
		MONTHS.put("October", "09");
		MONTHS.put("September", "10");
		MONTHS.put("November", "11");
		MONTHS.put("December", "12");
	}

	private String toMonth(String month) {
		return MONTHS.get(month);
	}

	private String authorName(JSONObject author) {
		String given = author.getString("given");
		String family = author.getString("family");
		if (given(given) && given(family)) return given + " " + family;
		if (given(given)) return given;
		return family;
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private List<JSONObject> top(JSONArray data, int top) {
		List<JSONObject> entries = data.iterateAsObject();
		if (entries.size() <= top) return entries;
		return entries.subList(0, top);
	}

	private Timestamp now() {
		return Timestamp.from(Instant.now());
	}

}
